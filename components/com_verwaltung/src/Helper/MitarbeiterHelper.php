<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Helper;

use Joomla\CMS\Factory;
use Joomla\Database\DatabaseInterface;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use stdClass;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Helper */
abstract class MitarbeiterHelper
{

    /**
     * @param mixed $ma_id 
     * @return mixed 
     * @throws KeyNotFoundException 
     */
    public static function getComputer($ma_id) {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select
            (
                [
                    $db->qn('d.hostname'),
                    $db->qn('r.id'),
                    $db->qn('d.ipnummer'),
                    $db->qn('d.wname'),
                    $db->qn('i.ma_id'),
                    $db->qn('i.inventarnummer'),
                    $db->qn('z.zimmer')
                ]
            )
            ->from($db->qn('#__iwf_rechner', 'r'))
            ->leftJoin($db->qn('#__iwf_dns', 'd'), $db->qn('d.id') . '=' . $db->qn('r.dns_id'))
            ->leftJoin($db->qn('#__iwf_inventar', 'i'), $db->qn('r.inventar_id') . '=' . $db->qn('i.id'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('i.ma_id') . '=' . $db->qn('m.id'))
            ->leftJoin($db->qn('#__iwf_zimmer', 'z'), $db->qn('i.standort') . '=' . $db->qn('z.id'))
            ->where($db->qn('m.id') . '=:id')
            ->bind(':id', $ma_id, ParameterType::INTEGER)
            ->order($db->qn('d.wname'));
        return $db->setQuery($query)->loadObjectList();
    }

    /**
     * @param mixed $ma_id 
     * @return mixed 
     * @throws KeyNotFoundException 
     */
    public static function getKeys($ma_id) {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select
            (
                [
                    $db->qn('a.id'),
                    $db->qn('s.bezeichnung'),
                    $db->qn('m.id', 'ma_id'),
                ]
            )
            ->from($db->qn('#__iwf_schluessel', 'a'))
            ->leftJoin($db->qn('#__iwf_schliessanlage', 's'), $db->qn('a.schluessel') . '=' . $db->qn('s.id'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('a.ma_id') . '=' . $db->qn('m.id'))
            ->where($db->qn('m.id') . '=:id')
            ->bind(':id', $ma_id, ParameterType::INTEGER)
            ->order($db->qn('s.bezeichnung'));
        return $db->setQuery($query)->loadObjectList();
    }

    /**
     * @param mixed $ma_id 
     * @return mixed 
     * @throws KeyNotFoundException 
     */
    public static function getStock($ma_id) {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select
            (
                [
                    $db->qn('a.id'),
                    $db->qn('a.inventarnummer'),
                    $db->qn('a.beschreibung'),
                    $db->qn('a.ma_id'),
                ]
            )
            ->from($db->qn('#__iwf_inventar', 'a'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('a.ma_id') . '=' . $db->qn('m.id'))
            ->where($db->qn('m.id') . '=:id')
            ->whereIn($db->qn('a.inventartyp'), [50,51,53,56])
            ->where($db->qn('a.deinventarisiert') . '=0')
            ->bind(':id', $ma_id, ParameterType::INTEGER)
            ->order($db->qn('a.inventarnummer'));
        return $db->setQuery($query)->loadObjectList();
    }

    /**
     * @param mixed $r_id 
     * @return mixed 
     * @throws KeyNotFoundException 
     */
    public static function getLicenses($r_id) {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select
            (
                [
                    $db->qn('a.id'),
                    $db->qn('r.id', 'r_id'),
                    $db->qn('d.hostname'),
                    $db->qn('s.produkt'),
                ]
            )
            ->from($db->qn('#__iwf_lizenzen', 'a'))
            ->leftJoin($db->qn('#__iwf_rechner', 'r'), $db->qn('r.id') . '=' . $db->qn('a.rechner_id'))
            ->leftJoin($db->qn('#__iwf_software', 's'), $db->qn('a.software_id') . '=' . $db->qn('s.id'))
            ->leftJoin($db->qn('#__iwf_dns', 'd'), $db->qn('d.id') . '=' . $db->qn('r.dns_id'))
            ->where($db->qn('r.id') . '=:id')
            ->bind(':id', $r_id, ParameterType::INTEGER)
            ->order($db->qn('d.hostname'));
        return $db->setQuery($query)->loadObjectList();
    }
}