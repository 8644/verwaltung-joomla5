<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Helper;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Helper */
abstract class FormHelper
{

    /**
     * Gibt die Form zurück, die dem Dienstverhaeltnis entspricht.
     * Dienstverhältnis-Ids sind in defines.php hinterlegt (s. Admin-Bereich - Extension)
     * @param mixed $dienstverhaeltnis 
     * @return string 
     */
    public static function getMaForm($dienstverhaeltnis)
    {
        switch ($dienstverhaeltnis) {
            case DIENSTVERHAELTNIS_OEAW:
                return 'ma_oeawangestellter';
            case DIENSTVERHAELTNIS_GAST:
                return 'ma_gast';
            case DIENSTVERHAELTNIS_DIENSTE:
                return 'ma_dienste';
            case DIENSTVERHAELTNIS_FREIERDIENSTVERTRAG:
                return 'ma_freierdienstvertrag';
            case DIENSTVERHAELTNIS_ANDERERVERTRAG:
                return 'ma_anderervertrag';
            case DIENSTVERHAELTNIS_AUSBILDUNGOHNEVERTRAG:
                return 'ma_ausbildungohnevertrag';
            default:
                return 'ma';
        }
    }

    /**
     * Gibt die Form zurück, die der Publikation entspricht.
     * Publikations-Ids sind in defines.php hinterlegt (s. Admin-Bereich - Extension)
     * @param mixed $ptyp 
     * @return string 
     */
    public static function getPublicationForm($ptyp) {
        switch ($ptyp) {
            case P_TYP_ZEITSCHRIFT_REFERIERT:
                return 'ptyp_zeitschrift_referiert';
            case P_TYP_ZEITSCHRIFT_NICHT_REFERIERT:
                return 'ptyp_zeitschrift_nicht_referiert';
            case P_TYP_IWF_BERICHT:
                return 'ptyp_iwf_bericht';
            case P_TYP_BUCHBEITRAG:
                return 'ptyp_buchbeitrag';
            case P_TYP_BUCH:
                return 'ptyp_buch';
            case P_TYP_BUCH_HERAUSGABE:
                return 'ptyp_buch_herausgabe';
            case P_TYP_DIPLOM_BAKK_ARBEIT:
                return 'ptyp_diplom_bakk_arbeit';
            case P_TYP_DISSERTATION:
                return 'ptyp_dissertation';
            case P_TYP_VORTRAG_BEI_TAGUNGEN:
                return 'ptyp_vortrag_bei_tagungen';
            case P_TYP_POSTER:
                return 'ptyp_poster';
            case P_TYP_PROPOSAL:
                return 'ptyp_proposal';
            case P_TYP_BERICHT_ALLGEMEIN:
                return 'ptyp_bericht_allgemein';
            case P_TYP_VORTRAG_BEI_INSTITUTION:
                return 'ptyp_vortrag_bei_institution';
            case P_TYP_HABILITATION:
                return 'ptyp_habilitation';
            case P_TYP_ANDERER_VORTRAG:
                return 'ptyp_anderer_vortrag';
            case P_TYP_VORTRAG_EINES_GASTES:
                return 'ptyp_vortrag_eines_gastes';
            case P_TYP_VORTRAG_BEI_PROJEKTMEETING:
                return 'ptyp_vortrag_bei_projektmeeting';
            default:
                return 'ptyp';
        }
    }

}
