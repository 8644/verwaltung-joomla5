<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Helper;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Multilanguage;
use Joomla\CMS\Router\Route;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\Helper */
abstract class RouterHelper
{

	/**
	 * @return string 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 */
	private static function addLanguageTag()
	{
		$langTag = "";
		if (Multilanguage::isEnabled()) {
			$lang = Factory::getApplication()->getLanguage();
			$langTag = '&lang=' . substr($lang->getTag(), 0, 2);
		}
		return $langTag;
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return string 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 */
	public static function getMaEditRoute($id, $text, $title = "", $icon = ""): string
	{
		if (Extensions::isAllowed('edit.ma')) {
			$route = 'index.php?option=com_verwaltung&task=ma.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
			$route = 'index.php?option=com_verwaltung&view=ma&layout=edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return string 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 */
	public static function getInventarEditRoute($id, $text, $title = "", $icon = "", $fromView = 'stocks'): string
	{
		if (Extensions::isAllowed('manage.inventar')) {
			$route = 'index.php?option=com_verwaltung&from=' . $fromView . '&task=stock.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
			return $text;
		}
	}

	/**
	 * @param mixed $state 
	 * @param mixed $search 
	 * @param mixed $dienstverhaeltnis 
	 * @param mixed $abteilung 
	 * @param mixed $eintritt 
	 * @param mixed $zimmer 
	 * @param mixed $abgelaufen 
	 * @return string 
	 */
	public static function getPersonalListeRoute($state, $search, $dienstverhaeltnis, $abteilung, $eintritt, $zimmer, $abgelaufen)
	{
		$link = 'index.php?option=com_verwaltung&view=personalliste&tmpl=component';
		if ($state) {
			$link .= '&state=' . $state;
		}
		if ($search) {
			$link .= '&suche=' . $search;
		}
		if ($dienstverhaeltnis) {
			$link .= '&dienstverhaeltnis=' . $dienstverhaeltnis;
		}
		if ($abteilung) {
			$link .= '&abteilung=' . $abteilung;
		}
		if ($eintritt) {
			$link .= '&eintritt=' . $eintritt;
		}
		if ($zimmer) {
			$link .= '&zimmer=' . $zimmer;
		}
		if ($abgelaufen) {
			$link .= '&abgelaufen=' . $abgelaufen;
		}
		return $link;
	}

	/**
	 * @param mixed $publikationstyp 
	 * @param mixed $search 
	 * @param mixed $erscheinungsjahr 
	 * @param mixed $erstautoren 
	 * @param mixed $autoren 
	 * @param mixed $hauptautor_forschungseinheit 
	 * @return string 
	 */
	public static function getAkademisZitatformRoute($publikationstyp, $search, $erscheinungsjahr, $erstautoren, $autoren, $hauptautor_forschungseinheit)
	{
		$link = 'index.php?option=com_verwaltung&view=zitatliste&tmpl=component$print=1';
		if ($publikationstyp) {
			$link .= '&publikationstyp=' . $publikationstyp;
		}
		if ($search) {
			$link .= '&search=' . $search;
		}
		if ($erscheinungsjahr) {
			$link .= '&erscheinungsjahr=' . $erscheinungsjahr;
		}
		if ($erstautoren) {
			$link .= '&erstautoren=' . $erstautoren;
		}
		if ($autoren) {
			$link .= '&autoren=' . $autoren;
		}
		if ($hauptautor_forschungseinheit) {
			$link .= '&ahuptautor_forschungseinheit=' . $hauptautor_forschungseinheit;
		}
		return $link;
	}

	/** @return string  */
	public static function getAkademisRoute() {
		$link = 'index.php?option=com_verwaltung&view=akademis&tmpl=component';
		return $link;
	}

	/**
	 * @param mixed $id 
	 * @return string 
	 */
	public static function getPrintGastformularRoute($id) {
		return 'index.php?option=com_verwaltung&view=printgastform&id=' . $id . '&tmpl=component&print=1';
	}

	/**
	 * 
	 * @param mixed $id 
	 * @return string 
	 */
	public static function getPrintBestellformularRoute($id) {
		return 'index.php?option=com_verwaltung&view=printorder&id=' . $id . '&tmpl=component&print=1';
	}

	/**
	 * @param mixed $id 
	 * @return string 
	 */
	public static function getPrintChecklistformularRoute($id) {
		return 'index.php?option=com_verwaltung&view=printchecklist&id=' . $id . '&tmpl=component&print=1';
	}

	/**
	 * @param mixed $id 
	 * @return string 
	 */
	public static function getPrintKeyRoute($id) {
		return 'index.php?option=com_verwaltung&view=printkey&id=' . $id . '&tmpl=component&print=1';
	}

	/** @return string  */
	public static function getWlanUserRoute() {
		$link = 'index.php?option=com_verwaltung&view=wlanuser&tmpl=component';
		return $link;
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return mixed 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 */
	public static function getComputerEditRoute($id, $text, $title = "", $icon = "", $from = 'computer')
	{
		if (!empty($text) && Extensions::isAllowed('manage.hw')) {
			$route = 'index.php?option=com_verwaltung&from=' . $from . '&task=computer.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
			return $text;
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return mixed 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getLicenseEditRoute($id, $text, $title = "", $icon = "", $from = 'license')
	{
		if (!empty($text) && Extensions::isAllowed('manage.sw')) {
			$route = 'index.php?option=com_verwaltung&from=' . $from . '&task=license.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
			return $text;
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return mixed 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getIssuedkeyEditRoute($id, $text, $title = "", $icon = "", $from = 'issuedkey')
	{
		if (!empty($text) && Extensions::isAllowed('manage.schluessel')) {
			$route = 'index.php?option=com_verwaltung&from=' . $from . '&task=issuedkey.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
			return $text;
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return mixed 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getQueryEditRoute($id, $text, $title = "", $icon = "")
	{
		if (!empty($text) && Extensions::isAllowed(['manage.hw', 'manage.sw'])) {
			$route = 'index.php?option=com_verwaltung&task=query.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
			return $text;
		}
	}

	/**
	 * @param mixed $person 
	 * @param mixed $bestellung 
	 * @param string $title 
	 * @param string $icon 
	 * @return mixed 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getOrderEditRoute($person, $bestellung, $title = "", $icon = "")
	{
		if (Extensions::isAllowed(['manage.abteilung', 'create.bestellungen']) || $person->joomla_user->id == $bestellung->owner) {
			$route = 'index.php?option=com_verwaltung&task=order.edit&id=' . $bestellung->id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $bestellung->bestellnummer . '</a>';
		}
		else {
			return $bestellung->bestellnummer;
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return mixed 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getGastformEditRoute($id, $text, $title = "", $icon = "")
	{
		if (!empty($text) && Extensions::isAllowed(['edit.gastform'])) {
			$route = 'index.php?option=com_verwaltung&task=gastform.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
			return $text;
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param mixed $readonly 
	 * @param string $title 
	 * @param string $icon 
	 * @return mixed|void 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getRoomEditRoute($id, $text, $readonly, $title = "", $icon = "")
	{
		if (!empty($text) && (Extensions::isAllowed(['delete.reservierungen']) || !$readonly)) {
			$route = 'index.php?option=com_verwaltung&task=room.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
			return $text;
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return mixed 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getFlatEditRoute($id, $text, $title = "", $icon = "")
	{
		if (!empty($text) && (Extensions::isAllowed(['manage.abteilung']))) {
			$route = 'index.php?option=com_verwaltung&task=flat.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
			return $text;
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return mixed 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getDeskEditRoute($id, $text, $title = "", $icon = "")
	{
		if (!empty($text) && (Extensions::isAllowed(['manage.abteilung']))) {
			$route = 'index.php?option=com_verwaltung&task=desk.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
			return $text;
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return mixed 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getPublicationEditRoute($id, $text, $title = "", $icon = "")
	{
		if (!empty($text) && (Extensions::isAllowed(['manage.literatur']))) {
			$route = 'index.php?option=com_verwaltung&task=publication.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
			return $text;
		}
	}
}
