<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use Joomla\CMS\Form\Field\TextField;
use Joomla\DI\Exception\KeyNotFoundException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class BestellnummerField extends TextField
{

    /**
     * @return string 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    protected function getInput()
    {
        $this->readonly = true;

        return parent::getInput();
    }
}
