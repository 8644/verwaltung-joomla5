<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class MitarbeiterField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    protected function getOptions()
    {

        $uid = Factory::getApplication()->getIdentity()->id;
        $ma = Person::getPersonByJoomlaId($uid);
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('id'),
                    'CONCAT(' . $db->qn('nachname') . '," ",' . $db->qn('vorname') . ') AS ' . $db->qn('name')
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter'))
            ->where($db->qn('dienstverhaeltnis') . '=' . DIENSTVERHAELTNIS_OEAW)
            ->where($db->qn('state') . '=1');
        if (Extensions::isAllowed('manage.hw')) {
            $query->extendWhere(
                    'OR',
                    [
                        $db->qn('id') . ' IN (' . EDVX . ')'
                    ],
                    'OR'
                );
        }
        if ($ma->ma_id) {
            $query->extendWhere(
                    'OR',
                    [
                        $db->qn('id') . '=:val'
                    ],
                    'OR'
                    )
                ->bind(':val', $ma->ma_id, ParameterType::INTEGER);
        }
        $query->order($db->qn('nachname'));
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $options[] = HtmlHelper::_('select.option', $item->id, $item->name);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
