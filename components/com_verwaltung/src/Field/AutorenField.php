<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use RuntimeException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class AutorenField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        // Mögliche Formate der weiteren Autoren: wenn Initialen, dann 2 Werte, sonst nur einer (Nachname, V.)
        $query = $db->createQuery()
            ->select
            (
                [
                    "CASE WHEN a.initialen<>'' THEN concat(a.initialen,' ',a.nachname,'|',substring(a.vorname,1,1),'. ',a.nachname) ELSE concat(substring(a.vorname,1,1),'. ',a.nachname) END AS akademisname",
                    "CASE WHEN a.initialen<>'' THEN concat(a.nachname,', ',substring(a.vorname,1,1),'.') ELSE concat(a.nachname,', ',substring(a.vorname,1,1),'.') END AS name"
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter', 'a'))
            ->whereIn($db->qn('a.abteilung'), explode(',', ABTEILUNGEN_AKADEMIS_AUTOREN), ParameterType::INTEGER)
            ->where($db->qn('dienstverhaeltnis') . '<>' . DIENSTVERHAELTNIS_DIENSTE)
            ->whereNotIn($db->qn('a.nachname'), ['EDV1', 'EDV2'], ParameterType::STRING)
            ->order($db->qn('a.nachname'));
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $options[] = HTMLHelper::_('select.option', $item->akademisname, $item->name);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
