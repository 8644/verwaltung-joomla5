<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Field\TextField;
use Joomla\Database\DatabaseInterface;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\ParameterType;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class ClwohnungField extends TextField
{

    /**
     * @return string 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    protected function getInput() {
        // Mitarbeiter-Id holen
        $app = Factory::getApplication();
        $ma_id = $app->getUserState('com_verwaltung.checklist.ma_id');
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select($db->qn('l.inhalt', 'wohnung'))
            ->from($db->qn('#__iwf_mitarbeiter', 'm'))
            ->leftJoin($db->qn('#__iwf_reservierungen', 'r'), $db->qn('m.id') . '=' . $db->qn('r.gast_id_wohnung'))
            ->leftJoin($db->qn('#__iwf_listen', 'l'), $db->qn('l.id') . '=' . $db->qn('r.text'))
            ->where($db->qn('m.id')  . '=:id')
            ->bind(':id', $ma_id, ParameterType::INTEGER);
        $hasWohnung = false;
        $wohnung = "keine Reservierung";
        if ($result = $db->setQuery($query)->loadObject()) {
            $hasWohnung = !empty($result->wohnung);
            if ($hasWohnung) {
                $wohnung = $result->wohnung;
            }
        }
        return '<label class="w-100 control-label">am FZG: ' . $wohnung . '</label>';
    }

}

