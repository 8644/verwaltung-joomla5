<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Language\Text;
use RuntimeException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class EinsatzbereichField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('id'),
                    $db->qn('inhalt'),
                    $db->qn('jtext'),
                ]
            )
            ->from($db->qn('#__iwf_listen'))
            ->where($db->qn('kategorie') . ' LIKE "einsatzbereich"')
            ->where($db->qn('aktiv') . '=1')
            ->order([$db->qn('reihenfolge'), $db->qn('inhalt')]);
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $text = "";
            if (!empty($item->jtext)) {
                $text = Text::_($item->jtext);
            }
            if (empty($text) || $text == $item->jtext) {
                $text = $item->inhalt;
            }
            $options[] = HtmlHelper::_('select.option', $item->id, $text);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
