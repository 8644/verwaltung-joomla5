<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Iwf\Component\Verwaltung\Administrator\Helper\MailHelper;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class MailabteilungField extends ListField
{

    /**
     * @return object[] 
     * @throws KeyNotFoundException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $options = [];
        $items = MailHelper::getMailabteilungen();
        foreach ($items as $item) {
            $options[] = HTMLHelper::_('select.option', $item->id, $item->mailverteiler);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
