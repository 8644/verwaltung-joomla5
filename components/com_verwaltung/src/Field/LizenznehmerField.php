<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Iwf\Verwaltung\Extensions;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use UnexpectedValueException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class LizenznehmerField extends ListField
{

    /**
     * @return object[] 
     * @throws Exception 
     * @throws DatabaseNotFoundException 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select
            (
                [
                    'DISTINCT ' . $db->qn('m.id'), 
                    $query->concatenate([$db->qn('m.nachname'), $db->qn('m.vorname')], ' ') . ' AS name',
                ]
            )
            ->from($db->qn('#__iwf_lizenzen', 'l'))
            ->leftJoin($db->qn('#__iwf_rechner', 'r'), $db->qn('l.rechner_id') . '=' . $db->qn('r.id'))
            ->leftJoin($db->qn('#__iwf_inventar', 'i'), $db->qn('r.inventar_id') . '=' . $db->qn('i.id'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('i.ma_id') . '=' . $db->qn('m.id'));
        if (!Extensions::isAllowed('manage.edv')) {
            $person = Person::getInstance();
            $query->where($db->qn('m.abteilung') . '=:abt')
                ->bind(':abt', $person->abteilung_id, ParameterType::INTEGER);
        }
        $query->order($db->qn('m.nachname'));
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $options[] = HTMLHelper::_('select.option', $item->id, $item->name);
        }
        return array_merge(parent::getOptions(), $options);
    }
}

