<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Joomla\CMS\Form\Field\TextField;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class ZitatField extends TextField
{
    /** @return string  */
    protected function getInput() {
        (string) $this->readonly = true;
        return '<span class="form-control" style="font-size:smaller;font-style:italic" name="jform[zitat]" id="jform_zitat">' . $this->value . '</span>';
    }

}
