<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Field\TextField;
use Joomla\CMS\Language\Text;
use Joomla\Database\DatabaseInterface;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\ParameterType;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class ClgastField extends TextField
{

    /**
     * @return string 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    protected function getInput() {
        // Mitarbeiter-Id holen
        $app = Factory::getApplication();
        $ma_id = $app->getUserState('com_verwaltung.checklist.ma_id');
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery();
        $query->select
            (
                [
                    $query->concatenate([$db->qn('m.nachname'), $db->qn('m.vorname')], ' ') . ' AS name',
                    $query->concatenate([$db->qn('b.nachname'), $db->qn('b.vorname')], ' ') . ' AS betreuer',
                    $db->qn('m.gastverantwortlicher', 'einladender_id')
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter', 'm'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'b'), $db->qn('m.gastverantwortlicher') . '=' . $db->qn('b.id'))
            ->where($db->qn('m.id')  . '=:id')
            ->bind(':id', $ma_id, ParameterType::INTEGER);
        $name = "";
        $betreuer = "N/A";
        $betreuer_id = 0;
        if ($result = $db->setQuery($query)->loadObject()) {
            $name = $result->name;
            if ($result->betreuer) {
                $betreuer = $result->betreuer;
                $betreuer_id = $result->einladender_id;
            }
        }
        $label2 = $this->getLabel2();
        return '<label class="w-100 control-label"><b>'
                . $name . '</b> (' . $label2 . $betreuer . ')</label>' .
                '<input type="hidden" value="' . $ma_id . '" name="jform[ma_id]" id="jform_ma_id">' .
                '<input type="hidden" value="' . $betreuer_id . '" name="jform[einladender_id]" id="jform_einladender_id">';
    }

    /*
     * Beschreibung für Betreuer: label2=""
     * Gibt nur den unformatierten Text zurück
     * @return mixed  
    */
    private function getLabel2() {
        // Get the label2 text from the XML element, defaulting to the element name.
        $text = $this->element['label2'] ? (string) $this->element['label2'] : (string) $this->element['name'];
        return $this->translateLabel ? Text::_($text) : $text;
    }

}

