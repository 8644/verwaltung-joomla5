<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Iwf\Verwaltung\Extensions;
use Joomla\Database\Exception\DatabaseNotFoundException;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class ZimmermitmaField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('z.id'),
                    'CONCAT(' . $db->qn('z.zimmer') . ', " (", ' . $db->qn('z.raumbezeichnung') . ', ") ", GROUP_CONCAT(' . $db->qn('m.nachname') . ' ORDER BY ' . $db->qn('m.nachname') . ' SEPARATOR "/")) AS ' . $db->qn('zmitma'),
                    $db->qn('z.zimmer'),
                    $db->qn('z.raumbezeichnung'),
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter', 'm'))
            ->leftJoin($db->qn('#__iwf_zimmer', 'z'), $db->qn('z.id') . ' = ' . $db->qn('m.zimmer'))
            ->where($db->qn('m.state') . ' = 1')
            ->where($db->qn('m.deleted') . ' = 0')
            ->where($db->qn('z.istbuero') . ' = 1')
            ->group($db->qn('z.id'));
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $options[] = HTMLHelper::_('select.option', $item->id, $item->zmitma);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
