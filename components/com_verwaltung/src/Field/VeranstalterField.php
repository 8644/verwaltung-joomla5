<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class VeranstalterField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $typ = RESERVIERUNGSTYP_RAUM;
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select(
                [
                    'DISTINCT ' . $db->qn('a.ma_id', 'id'),
                    $query->concatenate([$db->qn('m.nachname'), $db->qn('m.vorname')], ' ') . ' AS ' . $db->qn('name'),
                ]
            )
            ->from($db->qn('#__iwf_reservierungen', 'a'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('a.ma_id') . '=' . $db->qn('m.id'))
            ->where($db->qn('a.reservierungstyp') . '=:typ')
            ->bind(':typ', $typ, ParameterType::STRING)
            ->order('m.nachname');
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            if (!empty($item->name)) {
                $options[] = HTMLHelper::_('select.option', $item->id, $item->name);
            }
        }
        return array_merge(parent::getOptions(), $options);
    }
}