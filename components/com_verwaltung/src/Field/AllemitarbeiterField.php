<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class AllemitarbeiterField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $dv = DIENSTVERHAELTNIS_DIENSTE;
        $query->select(
                [
                    $db->qn('a.id', 'id'),
                    $query->concatenate([$db->qn('a.nachname'), $db->qn('a.vorname')], ' ') . ' AS name',
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter', 'a'))
            ->where($db->qn('a.dienstverhaeltnis') . '<>:dv')
            ->bind(':dv', $dv, ParameterType::INTEGER)
            ->where($db->qn('a.state') . '=1');
            if ($this->value) {
                $query->extendWhere('OR', $db->qn('a.id') . '=:id')
                ->bind(':id', $this->value, ParameterType::INTEGER);
            }
        $query->order($db->qn('a.nachname'));
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $options[] = HTMLHelper::_('select.option', $item->id, $item->name);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
