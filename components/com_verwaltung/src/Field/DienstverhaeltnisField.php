<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\Database\Exception\DatabaseNotFoundException;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class DienstverhaeltnisField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws InvalidArgumentException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('id'),
                    $db->qn('inhalt'),
                    $db->qn('jtext')
                ]
            )
            ->from($db->qn('#__iwf_listen'))
            ->where($db->qn('kategorie') . '="dv"')
            ->where($db->qn('aktiv') . '=1')
            ->order(
                [
                    $db->qn('reihenfolge'),
                    $db->qn('inhalt')
                ]
            );
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $text = "";
            if (!empty($item->jtext)) {
                $text = Text::_($item->jext);
            }
            if (empty($text) || $text == $item->jtext) {
                $text = $item->inhalt;
            }
            $options[] = HTMLHelper::_('select.option', $item->id, $text);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
