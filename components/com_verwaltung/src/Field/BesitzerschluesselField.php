<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\Database\Exception\DatabaseNotFoundException;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class BesitzerschluesselField extends ListField
{
    
    /**
     * @return object[] 
     * @throws Exception 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $person = Person::getInstance();
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select(
                [
                    'DISTINCT ' . $query->concatenate([$db->qn('m.nachname'), $db->qn('m.vorname')], ' ') . ' AS name',
                    $db->qn('m.id', 'id')
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter', 'm'))
            ->rightJoin($db->qn('#__iwf_schluessel', 'a'), $db->qn('a.ma_id') . '=' . $db->qn('m.id'))
            ->where('NOT ISNULL(' . $db->qn('m.nachname') . ')');
        if ($person->profil->abteilung_filter) {
            $query->where(sprintf($person->abteilung_filter, $db->qn('m.abteilung')));
        }
        $query->order($db->qn('m.nachname'), 'ASC');
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $options[] = HtmlHelper::_('select.option', $item->id, $item->name);
        }
        $this->hidden = count($options) == 0;
        return array_merge(parent::getOptions(), $options);
    }
}
