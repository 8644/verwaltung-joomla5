<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class MonatjahrField extends ListField
{

    /**
     * @return object[] 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions() {
        $this_date_found = false;
        $date = (int) date('Y');
        $months = explode(',', 'Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec');
        $options = [];
        for ($i = $date + 1; $i > $date - 15; $i--) {
            for ($j = 11; $j > -1; $j--) {
                $month_year = sprintf('%s %d', $months[$j], $i);
                $options[] = HTMLHelper::_('select.option', $month_year, $month_year);
                if (!$this_date_found) {
                    $this_date_found = $this->value == $month_year;
                }
            }
        }
        if (!$this_date_found) {
            $options[] = HTMLHelper::_('select.option', $this->value, $this->value);
        }
        return array_merge(parent::getOptions(), $options);
    }
    
}