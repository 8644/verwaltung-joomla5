<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\TextField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class PersonField extends TextField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws KeyNotFoundException 
     * @throws Exception 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('id'),
                    $db->qn('zimmer'),
                    $db->qn('raumbezeichnung')
                ]
            )
            ->from($db->qn('#__iwf_zimmer'));
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $options[] = HtmlHelper::_('select.option', $item->id, $item->zimmer . ((strlen($item->raumbezeichnung) > 1) ? " (" . $item->raumbezeichnung . ")" : ""));
        }
        return array_merge(parent::getOptions(), $options);
    }
}
