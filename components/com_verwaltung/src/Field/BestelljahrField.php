<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class BestelljahrField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select('DISTINCT YEAR(' . $db->qn('a.bestelldatum') . ') AS ' . $db->qn('bestelljahr'))
            ->from($db->qn('#__iwf_bestellungen', 'a'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('a.ma_id') . '=' . $db->qn('m.id'));

        $filter = Factory::getApplication()->input->get('filter', null, 'array');
        if ($filter) {
            $besteller = $filter['besteller'];
            if ($besteller) {
                $query->where($db->qn('m.id') . '=:besteller')
                    ->bind(':besteller', $besteller, ParameterType::INTEGER);
            }
        }
        $query->where('NOT ISNULL(' . $db->qn('a.bestelldatum') . ')');
        $query->order($db->qn('bestelldatum') . ' DESC');
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $options[] = HTMLHelper::_('select.option', $item->bestelljahr, $item->bestelljahr);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
