<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use InvalidArgumentException;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\Exception\DatabaseNotFoundException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class InventarnummerField extends ListField
{

    /**
     * @return object[] 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws DatabaseNotFoundException 
     */
    protected function getOptions()
    {
        if (!Extensions::isAllowed('manage.hw')) {
            return [HtmlHelper::_('select.option', $this->value, $this->value)];
        }
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('i.id'),
                    $db->qn('i.inventarnummer'),
                    $db->qn('i.beschreibung'),
                    sprintf('CONCAT(%s, " ",%s) AS %s', $db->qn('m.nachname'), $db->qn('m.vorname'), $db->qn('name'))
                ]
            )
            ->from($db->qn('#__iwf_inventar', 'i'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('i.ma_id') . '=' . $db->qn('m.id'))
            ->where($db->qn('m.nachname') . '<>""')
            ->where($db->qn('i.inventartyp') . 'IN(' . INVENTARTYP_RECHNER . ')')
            ->order($db->qn('i.inventarnummer'));
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $options[] = HtmlHelper::_('select.option', $item->id, $item->inventarnummer . ' (' . $item->name . '), ' . substr($item->beschreibung, 0, 50));
        }
        return array_merge(parent::getOptions(), $options);
    }
}
