<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Field\TextField;
use Joomla\Database\DatabaseInterface;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\ParameterType;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class ClkostenField extends TextField
{

    /**
     * @return string 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    protected function getInput() {
        // Mitarbeiter-Id holen
        $app = Factory::getApplication();
        $ma_id = $app->getUserState('com_verwaltung.checklist.ma_id');
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select
            (
                [
                    $db->qn('g.kosten_hotel', 'hotel'),
                    $db->qn('g.kosten_diaeten', 'diaeten'),
                    $db->qn('g.kosten_reise', 'reise')
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter', 'm'))
            ->leftJoin($db->qn('#__iwf_gastchecklisten', 'g'), $db->qn('m.id') . '=' . $db->qn('g.ma_id'))
            ->where($db->qn('m.id')  . '=:id')
            ->bind(':id', $ma_id, ParameterType::INTEGER);
        $summe = 0;
        if ($result = $db->setQuery($query)->loadObject()) {
            $summe = $result->hotel + $result->diaeten + $result->reise;
        }
        return '<label class="w-100 control-label" style="text-decoration:underline;font-family:Courier New">' . number_format($summe, 2) . '</label>';
    }

}

