<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class BestellerField extends ListField
{

    /**
     * @return object[] 
     * @throws Exception 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        //$person = Person::getInstance();
        $db = $this->getDatabase();
        $query3 = $db->createQuery()
            ->select('1')
            ->from($db->qn('#__iwf_bestellungen', 'b'))
            ->where($db->qn('b.ma_id') . '=' . $db->qn('m.id'));
        $query2 = $db->createQuery();
        $query2->select(
                [
                    'DISTINCT ' . $db->qn('m.id'),
                    $query2->concatenate([$db->qn('m.nachname'), $db->qn('m.vorname')], ' ') . ' AS name'
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter', 'm'))
            ->where($db->qn('m.state') . '=0')
            ->where('EXISTS(' . $query3 . ')');
        $query = $db->createQuery();
        $query->select
            (
                [
                    'DISTINCT ' . $db->qn('m.id'),
                    $query->concatenate([$db->qn('m.nachname'), $db->qn('m.vorname')], ' ') . ' AS name'
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter', 'm'))
            ->leftJoin($db->qn('#__iwf_bestellungen', 'b'), $db->qn('b.ma_id') . '=' . $db->qn('m.id'))
            ->where($db->qn('m.state') . '=1')
            ->whereIn($db->qn('m.abteilung'), explode(',', ABTEILUNGEN_IWF))
            ->union($query2)
            ->order($db->qn('name'));
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $options[] = HTMLHelper::_('select.option', $item->id, $item->name);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
