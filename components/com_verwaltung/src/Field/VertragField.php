<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class VertragField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('id'),
                    $db->qn('inhalt'),
                    $db->qn('jtext'),
                ]
            )
            ->from($db->qn('#__iwf_listen'));
        $typ = 'gastvertrag';
        $query->where($db->qn('kategorie') . '=:typ')
            ->where($db->qn('aktiv') . '=1')
            ->bind(':typ', $typ, ParameterType::STRING)
            ->order([$db->qn('reihenfolge'), $db->qn('inhalt')]);
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $text = "";
            if (!empty($item->jtext)) {
                $text = Text::_($item->jtext);
            }
            if (empty($text) || $text == $item->jtext) {
                $text = $item->inhalt;
            }
            $options[] = HTMLHelper::_('select.option', $item->id, $text);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
