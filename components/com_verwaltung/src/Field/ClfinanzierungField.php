<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Field\TextField;
use Joomla\Database\DatabaseInterface;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\ParameterType;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class ClfinanzierungField extends TextField
{

    /**
     * @return string 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    protected function getInput() {
        // Mitarbeiter-Id holen
        $app = Factory::getApplication();
        $ma_id = $app->getUserState('com_verwaltung.checklist.ma_id');
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select
            (
                [
                    $db->qn('l.id'),
                    $db->qn('l.inhalt', 'finanzierung'),
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter', 'm'))
            ->leftJoin($db->qn('#__iwf_listen', 'l'), $db->qn('m.gastfinanzierung') . '=' . $db->qn('l.id'))
            ->where($db->qn('m.id')  . '=:id')
            ->bind(':id', $ma_id, ParameterType::INTEGER);
        $finanzierung = 'N/A';
        if ($result = $db->setQuery($query)->loadObject()) {
            $finanzierung = $result->finanzierung;
        }
        $query->clear()
            ->select
            (
                [
                    $db->qn('p.projekt'),
                    $db->qn('p.projektbez', 'projektnummer')
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter', 'm'))
            ->leftJoin($db->qn('#__iwf_projekte', 'p'), $db->qn('m.gastprojekt') . '=' . $db->qn('p.id'))
            ->where($db->qn('m.id')  . '=:id')
            ->bind(':id', $ma_id, ParameterType::INTEGER);
        $projekt = '';
        if ($result = $db->setQuery($query)->loadObject()) {
            if ($projekt) {
                $projekt =sprintf(' (%s / %s)', $result->projekt, $result->projektnummer);
            }
        }
        return '<label class="w-100 control-label">' . $finanzierung . $projekt . '</label>';
    }

}

