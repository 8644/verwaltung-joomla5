<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Field\TextField;
use Joomla\CMS\Language\Text;
use Joomla\Database\DatabaseInterface;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class ClaufenthaltField extends TextField
{

    /**
     * @return string 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws InvalidArgumentException 
     */
    protected function getInput() {
        // Mitarbeiter-Id holen
        $app = Factory::getApplication();
        $ma_id = $app->getUserState('com_verwaltung.checklist.ma_id');

        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select
            (
                [
                    $db->qn('eintritt', 'beginn'),
                    $db->qn('vertragsende', 'ende')
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter'))
            ->where($db->qn('id') . '=:id')
            ->bind(':id', $ma_id, ParameterType::INTEGER);
        $beginn = "N/A";
        $ende = "N/A";
        if ($result = $db->setQuery($query)->loadObject()) {
            if ($result->beginn) {
                $beginn = HTMLHelper::_('date', htmlspecialchars($result->beginn, ENT_COMPAT, 'UTF-8'), 'd.m.Y');
            }
            if ($result->ende) {
                $ende = HTMLHelper::_('date', htmlspecialchars($result->ende, ENT_COMPAT, 'UTF-8'), 'd.m.Y');
            }
        }
        $label2 = $this->getLabel2();
        return '<label class="w-100 control-label">' . $beginn . $label2 . $ende . '</label>';
    }

    /*
     * Beschreibung für Aufenthaltsende: label2=""
     * Gibt nur den unformatierten Text zurück
     * @return mixed  
    */
    private function getLabel2() {
        // Get the label2 text from the XML element, defaulting to the element name.
        $text = $this->element['label2'] ? (string) $this->element['label2'] : (string) $this->element['name'];
        return $this->translateLabel ? Text::_($text) : $text;
    }
}
