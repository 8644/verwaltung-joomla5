<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use InvalidArgumentException;
use Joomla\CMS\Form\Field\GroupedlistField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;
use RuntimeException;
use UnexpectedValueException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class ProjektField extends GroupedlistField
{

    // beim Original ist group.items auf null gesetzt, daher fehlen dann die Optionen!
    /**
     * @return string 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws UnexpectedValueException 
     * @throws InvalidArgumentException 
     */
    protected function getInput()
    {
        $html = array();
        $attr = '';
        $attr .= $this->element['class'] ? ' class="' . (string) $this->element['class'] . '"' : 'class="form-select"';
        $attr .= ((string) $this->element['disabled'] == 'true') ? ' disabled="disabled"' : '';
        $attr .= $this->element['size'] ? ' size="' . (int) $this->element['size'] . '"' : '';
        $attr .= $this->multiple ? ' multiple="multiple"' : '';
        $attr .= $this->element['onchange'] ? ' onchange="' . (string) $this->element['onchange'] . '"' : '';
        $groups = (array) $this->getGroups();
        if ((string) $this->element['readonly'] == 'true') {
            $html[] = HTMLHelper::_(
                'select.groupedlist',
                $groups,
                null,
                array(
                    'list.attr' => $attr,
                    'id' => $this->id,
                    'list.select' => $this->value,
                    'group.items' => 'items',
                    'option.key.toHtml' => false,
                    'option.text.toHtml' => false
                )
            );
            $html[] = '<input type="hidden" name="' . $this->name . '" value="' . $this->value . '"/>';
        }
        else {
            $html[] = HTMLHelper::_(
                'select.groupedlist',
                $groups,
                $this->name,
                array(
                    'list.attr' => $attr,
                    'id' => $this->id,
                    'list.select' => $this->value,
                    'group.items' => 'items',
                    'option.key.toHtml' => false,
                    'option.text.toHtml' => false
                )
            );
        }
        return implode($html);
    }

    /**
     * @return array[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws UnexpectedValueException 
     * @throws InvalidArgumentException 
     */
    protected function getGroups()
    {
        $aktuell = 1;
        $db = $this->getDatabase();
        $query = $db->createQuery(true)
            ->select(
                [
                    $db->qn('p.id'),
                    'CONCAT(' . $db->qn('a.institut') . ',"-",' . $db->qn('a.abteilung') . ') AS ' . $db->qn('abteilung'),
                    $db->qn('a.konto'),
                    'CONCAT(' . $db->qn('p.projekt') . '," (",' . $db->qn('p.projektbez') . ',")") AS ' . $db->qn('projekt')
                ]
            )
            ->from($db->qn('#__iwf_projekte', 'p'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('p.ma_id') . '=' . $db->qn('m.id'))
            ->leftJoin($db->qn('#__iwf_abteilungen', 'a'), $db->qn('a.id') . '=' . $db->qn('m.abteilung'))
            ->where($db->qn('p.aktuell') . '= :aktuell')
            ->bind(':aktuell', $aktuell, ParameterType::INTEGER)
            ->order(
                [
                    $db->qn('m.abteilung'),
                    $db->qn('p.projekt')
                ]
            );
        $items = $db->setQuery($query)->loadObjectList();
        $groups = [];
        $g = parent::getGroups();
        if (count($g)) {
            if (count($g[0])) {
                $groups[0] = array();
                $groups[0]['items'] = array();
                $groups[0]['items'][] = HTMLHelper::_('select.option', $g[0][0]->value, $g[0][0]->text);
            }
        }
        foreach ($items as $item) {
            if (!isset($groups[$item->abteilung])) {
                $groups[$item->abteilung] = array();
                $groups[$item->abteilung]['text'] = $item->abteilung . ((empty($item->konto)) ? '' : ' ( **' . $item->konto . '** )');
                $groups[$item->abteilung]['items'] = array();
            }
            $groups[$item->abteilung]['items'][] = HTMLHelper::_('select.option', $item->id, $item->projekt);
        }
        return $groups;
    }
}
