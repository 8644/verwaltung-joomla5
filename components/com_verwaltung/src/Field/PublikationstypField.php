<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Form\FormHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;

defined('_JEXEC') or die;

FormHelper::loadFieldClass('list');

/** @package Iwf\Component\Verwaltung\Site\Field */
class PublikationstypField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws InvalidArgumentException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $typ = 'publikationstyp';
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('a.value', 'value'),
                    $db->qn('a.inhalt', 'inhalt'),
                    $db->qn('a.jtext', 'jtext'),
                ]
            )
            ->from($db->qn('#__iwf_listen', 'a'))
            ->where($db->qn('a.kategorie') . '=:typ')
            ->bind(':typ', $typ, ParameterType::STRING)
            ->where($db->qn('a.aktiv') . '=1')
            ->order('a.reihenfolge')
            ->order('a.inhalt');
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $text = '';
            if (!empty($item->jtext)) {
                $text = Text::_($item->jtext);
            }
            if (empty($text) || $text == $item->jtext) {
                $text = $item->inhalt;
            }
            $options[] = HtmlHelper::_('select.option', $item->value, $text);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
