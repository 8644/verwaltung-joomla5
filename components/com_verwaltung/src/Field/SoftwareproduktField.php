<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class SoftwareproduktField extends ListField
{
    
    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('a.id', 'id'),
                    $db->qn('a.produkt', 'produkt'),
                    $db->qn('r.dns_id'),
                    $db->qn('i.ma_id'),
                ]
            )
            ->from($db->qn('#__iwf_software', 'a'))
            ->leftJoin($db->qn('#__iwf_lizenzen', 'l'), $db->qn('a.id') . '=' . $db->qn('l.software_id'))
            ->leftJoin($db->qn('#__iwf_rechner', 'r'), $db->qn('l.rechner_id') . '=' . $db->qn('r.id'))
            ->leftJoin($db->qn('#__iwf_inventar', 'i'), $db->qn('i.id') . '=' . $db->qn('r.inventar_id'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('m.id') . '=' . $db->qn('i.ma_id'))
            ->where(['NOT isnull(' . $db->qn('r.dns_id') . ')', 'NOT isnull(' . $db->qn('i.ma_id') . ')']);

        $filter = Factory::getApplication()->input->get('filter', null, 'array');
        if ($filter) {
             $ma = $filter['ma'];
             if ($ma) {
                $query->where($db->qn('m.id') . '=:ma')
                    ->bind(':ma', $ma, ParameterType::INTEGER);
             }
             $rechner = $filter['rechner'];
             if ($rechner) {
                $query->where($db->qn('r.dns_id') . '=:rechner')
                    ->bind(':rechner', $rechner, ParameterType::INTEGER);
             }
        }
        $query->group($db->qn('a.id'))
            ->order($db->qn('nachname'));
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $options[] = HtmlHelper::_('select.option', $item->id, $item->produkt);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
