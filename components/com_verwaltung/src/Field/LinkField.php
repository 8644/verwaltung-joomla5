<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Field\TextField;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class LinkField extends TextField {

    /**
     * @return string 
     * @throws Exception 
     */
    protected function getInput() {
        $app = Factory::getApplication();
        $v = $app->getUserState('com_verwaltung.order.link');
        $html = '<div class="sap-button">' . $v . '</div>';
        return $html;
    }

}
