<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class VerantwortlicherinventarField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    'DISTINCT ' . $db->qn('a.id'),
                    'CONCAT(' . $db->qn('a.nachname') . '," ",' . $db->qn('a.vorname') . ') AS verantwortlicher'
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter', 'a'))
            ->innerJoin($db->qn('#__iwf_inventar', 'i'), $db->qn('i.ma_id') . '=' . $db->qn('a.id'))
            ->order($db->qn('a.nachname'));
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            if (!empty($item->verantwortlicher)) {
                $options[] = HTMLHelper::_('select.option', $item->id, $item->verantwortlicher);
            }
        }
        return array_merge(parent::getOptions(), $options);
    }
}
