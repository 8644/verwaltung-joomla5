<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class BesitzerField extends ListField
{

    /**
     * @return object[] 
     * @throws Exception 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $person = Person::getInstance();
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select('DISTINCT ' . $db->qn('m.id') . ', CONCAT(' . $db->qn('m.nachname') . '," ",' . $db->qn('m.vorname') . ') AS name')
            ->from($db->qn('#__iwf_rechner', 'r'))
            ->leftJoin($db->qn('#__iwf_inventar', 'i'), $db->qn('r.inventar_id') . '=' . $db->qn('i.id'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('m.id') . '=' . $db->qn('i.ma_id'));
        //Abteilungsfilter
        if ($person->abteilung_filter) {
            $query->where(sprintf($person->abteilung_filter, 'm.abteilung'));
        }

        $query->where('NOT ISNULL(' . $db->qn('m.nachname') . ')')
            ->order($db->qn('m.nachname'));
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $options[] = HTMLHelper::_('select.option', $item->id, $item->name);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
