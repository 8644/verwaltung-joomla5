<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class RechnerlizenzenField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    'DISTINCT ' . $db->qn('d.id'),
                    $db->qn('d.hostname'),
                ]
            )
            ->from($db->qn('#__iwf_lizenzen', 'l'))
            ->leftJoin($db->qn('#__iwf_rechner', 'r'), $db->qn('l.rechner_id') . '=' . $db->qn('r.id'))
            ->leftJoin($db->qn('#__iwf_dns', 'd'), $db->qn('r.dns_id') . '=' . $db->qn('d.id'))
            ->leftJoin($db->qn('#__iwf_inventar', 'i'), $db->qn('r.inventar_id') . '=' . $db->qn('i.id'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('i.ma_id') . '=' . $db->qn('m.id'));
            $filter = Factory::getApplication()->input->get('filter', null, 'array');
            if ($filter) {
                $ma = $filter['ma'];
                if ($ma) {
                $query->where($db->qn('i.ma_id') . '=:ma')
                    ->bind(':ma', $ma, ParameterType::INTEGER);
                }
            }
        $query->order($db->qn('d.hostname'));
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $options[] = HtmlHelper::_('select.option', $item->id, $item->hostname);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
