<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 * 
 * protected function getOptions()
 */

//In old File FormFieldCombo protected function getInput und protected getOptions
namespace Iwf\Component\Verwaltung\Site\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class RechnerField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select
            (
                [
                    $db->qn('a.id', 'id'),
                    $db->qn('d.wname', 'wname'),
                    $db->qn('d.hostname', 'hostname'),
                    $db->qn('m.nachname'),
                ]
            )
            ->from($db->qn('#__iwf_rechner', 'a'))
            ->leftJoin($db->qn('#__iwf_dns', 'd'), $db->qn('a.dns_id') . '=' . $db->qn('d.id'))
            ->leftJoin($db->qn('#__iwf_inventar', 'i'), $db->qn('a.inventar_id') . '=' . $db->qn('i.id'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('m.id') . '=' . $db->qn('i.ma_id'))
            ->order(
                [
                    $db->qn('m.nachname'),
                    $db->qn('d.hostname')
                ]
            );
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            if (!empty($item->nachname)) {
                $wname = empty($item->wname) ? '' : ' [' . strtoupper($item->wname) . ']';
                $options[] = HTMLHelper::_('select.option', $item->id, str_pad($item->hostname, 16, '.') . str_pad($wname, 16, '.') . '(' . $item->nachname . ')');
            }
        }
        return array_merge(parent::getOptions(), $options);
    }
}
