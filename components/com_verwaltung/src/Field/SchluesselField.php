<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class SchluesselField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select(
                [
                    $db->qn('a.id', 'id'),
                    'CASE WHEN ' . $db->qn('a.tuer') . '<> "" THEN ' . $query->concatenate([$db->qn('a.bezeichnung'), $db->qn('a.tuer')], '---> Tuer ') . ' WHEN ' . $db->qn('a.transponderkennung') . '<>"" ' .
                    'THEN ' . $query->concatenate([$db->qn('a.bezeichnung'),$db->qn('a.transponderkennung')], '--->Transponder ') . ' WHEN ' . $db->qn('a.raum_gruppe') . '<>"" ' .
                    'THEN ' . $query->concatenate([$db->qn('a.bezeichnung'),$db->qn('a.raum_gruppe')], '--->Raum-Gruppe ') . ' ELSE ' . $db->qn('a.bezeichnung') . ' END as name'
                ]
            )
            ->from($db->qn('#__iwf_schliessanlage', 'a'))
            ->order($db->qn('a.bezeichnung'));
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $options[] = HTMLHelper::_('select.option', $item->id, $item->name);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
