<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Field;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Field */
class SchluesseltypField extends ListField
{
    
    /**
     * @return object[] 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws InvalidArgumentException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $options = [];
            $options[] = HTMLHelper::_('select.option', "0", Text::_("JOPTION_SELECT_SCHLUESSEL_TYP_UNKNOWN"));
            $options[] = HTMLHelper::_('select.option', "1", Text::_("JOPTION_SELECT_SCHLUESSEL_TYP_STANDARD"));
            $options[] = HTMLHelper::_('select.option', "2", Text::_("JOPTION_SELECT_SCHLUESSEL_TYP_CHIP"));
            $options[] = HTMLHelper::_('select.option', "3", Text::_("JOPTION_SELECT_SCHLUESSEL_TYP_CLIP"));
        return array_merge(parent::getOptions(), $options);
    }
}
