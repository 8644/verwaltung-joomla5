<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

//In old File FormFieldCombo protected function getInput und protected getOptions
namespace Iwf\Component\Verwaltung\Site\Field;

use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;

defined('_JEXEC') or die;

class SoftwareField extends ListField
{

    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('a.id', 'id'),
                    $db->qn('a.produkt'),
                ]
            )
        ->from('#__iwf_software as a');
//$query->leftJoin('fzg_abteilungen as ab on a.abteilung=ab.id');

        /* $abteilungsfilter = "";
          if ($actions->get('core.manage.sw') || $actions->get('core.admin'))
          {
          $abteilungsfilter = sprintf($actions->abteilungsfilter,'a.abteilung');
          }
          else
          {
          $abteilungsfilter = "a.id='$this->value'";
          }
          if ($abteilungsfilter)
          $query->where($abteilungsfilter);
         */
        $query->order('a.produkt');

        $db->setQuery((string) $query);
        $options = [];
        if ($db->execute()) {
            $items = $db->loadObjectList();
            if ($items) {
                foreach ($items as $item) {
                    $options[] = HTMLHelper::_('select.option', $item->id, $item->produkt);
                }
            }
        }
        return array_merge(parent::getOptions(), $options);
    }
}
