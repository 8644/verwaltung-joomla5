<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Rule;

use InvalidArgumentException;
use Joomla\CMS\Date\Date;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Form\FormRule;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\DatabaseInterface;
use SimpleXMLElement;
use Joomla\Registry\Registry;
use UnexpectedValueException;
use Joomla\DI\Exception\KeyNotFoundException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Rule */
class RaumreservierungRule extends FormRule
{

    protected $regex = '^([0-9]{4})[-]{1}([0-9]{2})[-]{1}([0-9]{2})$';

    /**
     * Prüft, ob keine Überlappungen mit anderen Veranstaltungen bestehen
     * @param SimpleXMLElement $element 
     * @param mixed $value 
     * @param string $group 
     * @param null|Registry $input 
     * @param null|Form $form 
     * @return bool 
     * @throws UnexpectedValueException 
     * @throws KeyNotFoundException 
     * @throws InvalidArgumentException 
     */
    public function test($element, $value, $group = null, $input = null, $form = null)
    {
        // Zugriff auf Feldattribute: (string)$element['attributname']
        // Zugriff auf Daten: $input->get('feldname')
        // Test the value against the regular expression.
        if (!parent::test($element, $value, $group, $input, $form)) {
            return false;
        }
        if ($input->get('isclone')) { // als Kopie speichern
            return true;
        }
        $id = $input->get('id');
        $dat = new Date($value);
        $datum = $dat->toSql();
        $beginn = date('H:i', strtotime($input->get('beginn')) + 60);  // +1min, damit keine Überlappung bei direkt angrenzender Veranstaltung auftritt
        $ende = date('H:i', strtotime($input->get('ende')) - 60); // -1min, damit keine Überlappung bei direkt angrenzender Veranstaltung auftritt
        $raum = $input->get('text');

        // hier auf eine Überlappung der Veranstaltung prüfen
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery();
        $query->select
            (
                [
                    $db->qn('a.veranstaltung'),
                    $db->qn('a.datum'),
                    $db->qn('a.beginn'),
                    $db->qn('a.ende'),
                    $db->qn('a.text', 'raum'),
                ]
            )
            ->from('#__iwf_reservierungen as a');
        $q = "((a.reservierungstyp=" . RESERVIERUNGSTYP_RAUM . ") " .
            "AND ((datum='$datum') " .
            "AND ((('$beginn' BETWEEN beginn AND ende) OR ('$ende' BETWEEN beginn AND ende) OR (beginn BETWEEN '$beginn' AND '$ende') OR (ende BETWEEN '$beginn' AND '$ende'))) " .
            "AND (text='$raum')) " .
            "AND (wiederholungsintervall=" . RESERVIERUNG_EINMALIG . " OR wiederholungsintervall=" . RESERVIERUNG_MEHRTAEGIG . ")";
        if ($id) {
            $q .= " AND (a.id <> '$id')";
        }
        $q .= ')';
        $query->where($q, 'OR');

        // periodische Veranstaltungen beruecksichtigen
        if ($input->get('checkperiodic')) {
            $q = "((((wiederholungsintervall IN (" . RESERVIERUNG_WOECHENTLICH . "," . RESERVIERUNG_ZWEIWOECHENTLICH . ")) AND (DAYOFWEEK(datum)=DAYOFWEEK('$datum')) " .
                "AND (('$beginn' BETWEEN beginn AND ende) OR ('$ende' BETWEEN beginn AND ende) " .
                "OR (beginn BETWEEN '$beginn' AND '$ende') OR (ende BETWEEN '$beginn' AND '$ende'))) AND (a.text='$raum'))";
            if ($id) {
                $q .= " AND (a.id <> '$id')";
            }
            $q .= ')';
            $query->where($q);
        }
        $items = $db->setQuery((string) $query)->loadObjectList();
        if (count($items)) {
            $element['message'] = "Ueberlappung mit der Veranstaltung '" . $items[0]->veranstaltung . "' von " . HTMLHelper::_('date', $items[0]->beginn, 'G.i', null) . " bis " . HTMLHelper::_('date', $items[0]->ende, 'G.i', null);
            return false;
        }
        return true;
    }
}
