<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Rule;

use Joomla\CMS\Form\Form;
use Joomla\CMS\Form\FormRule;
use Joomla\CMS\Language\Text;
use SimpleXMLElement;
use Joomla\Registry\Registry;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Rule */
class EigenerklaerungRule extends FormRule
{
    protected $regex = '';

    /**
     * @param SimpleXMLElement $element 
     * @param mixed $value 
     * @param string $group 
     * @param null|Registry $input 
     * @param null|Form $form 
     * @return bool 
     */
    public function test($element, $value, $group = null, $input = null, $form = null) {
        $preis = 0;
        $preis1 = (float) $input->get('preis1');
        $preis2 = (float) $input->get('preis2');
        $preis3 = (float) $input->get('preis3');
        if ($preis < $preis1) {
            $preis = $preis1;
        }
        if ($preis < $preis2) {
            $preis = $preis2;
        }
        if ($preis < $preis3) {
            $preis = $preis3;
        }
        if ($preis >= 5000) {
            if ($value && $value != "N/A" && strlen($value) > 3) {
                return true;
            } else {
                $element['message'] = Text::_('COM_VERWALTUNG_RULE_EIGENERKLAERUNG');
                return false;
            }
        }
        return true;
    }
}
