<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Rule;

use DateTime;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Form\FormRule;
use SimpleXMLElement;
use Joomla\Registry\Registry;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Rule */
class BestelldatumRule extends FormRule
{
    protected $regex = '^([0-9]{4})[-]{1}([0-9]{2})[-]{1}([0-9]{2})$';

    /**
     * @param SimpleXMLElement $element 
     * @param mixed $value 
     * @param string $group 
     * @param null|Registry $input 
     * @param null|Form $form 
     * @return bool 
     */
    public function test($element, $value, $group = null, $input = null, $form = null) {
        $date = (new DateTime($value))->format('Y');
        $now = (new DateTime())->format('Y');
        if ((int) $date > (int) $now) {
            $element['message'] = 'Jahr darf nicht größer als ' . $now . ' sein!';
            return false;
        }
        return true;
    }

}
