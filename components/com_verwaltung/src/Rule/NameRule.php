<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Rule;

use Exception;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Form\FormRule;
use Joomla\Registry\Registry;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\Database\DatabaseInterface;
use Joomla\Database\ParameterType;
use SimpleXMLElement;
use UnexpectedValueException;
use Joomla\DI\Exception\KeyNotFoundException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Rule */
class NameRule extends FormRule
{

    /**
     * [\p{L}\p{M}\p{N}\p{P}\p{S}]*: Beliebig viele Zeichen, die Buchstaben (\p{L}), diakritische Zeichen (\p{M}), 
     * Ziffern (\p{N}), Interpunktionszeichen (\p{P}) oder Symbole (\p{S}) sind.
     */
    protected $regex = '^[^\d\s_][À-ÖØ-öø-ÿ\s\p{L}\p{M}\p{N}\p{P}\p{S}]*$';
    protected $modifiers = 'u';

    /**
     * @param SimpleXMLElement $element 
     * @param mixed $value 
     * @param string $group 
     * @param null|Registry $input 
     * @param null|Form $form 
     * @return bool 
     * @throws UnexpectedValueException 
     * @throws KeyNotFoundException 
     * @throws Exception 
     */
    public function test(\SimpleXMLElement $element, $value, $group = null, Registry $input = null, Form $form = null)
    {
        // Zugriff auf Feldattribute: (string)$element['attributname']
        // Zugriff auf Daten: $input->get('feldname')
        // Test the value against the regular expression.
        if (is_null($value)) {
            return true;
        }
        if (!parent::test($element, $value, $group, $input, $form)) {
            return false;
        }

        $nachname = $input->get('nachname');
        $vorname = $input->get('vorname');
        $id = $input->get('id');

        // Prüfung nur bei neuem Datensatz
        if ($id == 0) {
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $query = $db->createQuery();
            $query->select(
                    [
                        $db->qn('nachname'),
                        $db->qn('vorname'),
                    ]
                )
                ->from($db->qn('#__iwf_mitarbeiter'));
            if (!empty($nachname)) {
                if (!empty($vorname)) {
                    $query->where($db->qn('nachname') . ' LIKE :nachname')
                        ->where($db->qn('vorname') . ' LIKE :vorname')
                        ->bind(':nachname', $nachname, ParameterType::STRING)
                        ->bind(':vorname', $vorname, ParameterType::STRING);
                } else {
                    $query->where($db->qn('nachname') . ' LIKE :nachname')
                        ->bind(':nachname', $nachname, ParameterType::STRING);
                }
            } else {
                return false;
            }
            if ($db->setQuery($query)->execute()) {
                if ($db->getAffectedRows() > 0) {
                    $element['message'] = Text::_('COM_VERWALTUNG_MITARBEITER_EXISTIERT');
                    return false;
                }
            }
        }
        return true;
    }
}
