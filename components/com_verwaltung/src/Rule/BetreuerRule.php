<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Rule;

use Joomla\CMS\Form\Form;
use Joomla\CMS\Form\FormRule;
use SimpleXMLElement;
use Joomla\Registry\Registry;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Rule */
class BetreuerRule extends FormRule
{

    protected $regex = '';

    /**
     * @param SimpleXMLElement $element 
     * @param mixed $value 
     * @param string $group 
     * @param null|Registry $input 
     * @param null|Form $form 
     * @return bool 
     */
    public function test($element, $value, $group = null, $input = null, $form = null) {
        return true;
    }

}
