<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Rule;

use Exception;
use Joomla\CMS\Application\CMSApplicationInterface;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Form\FormRule;
use Joomla\Database\DatabaseInterface;
use Joomla\Database\ParameterType;
use SimpleXMLElement;
use Joomla\Registry\Registry;
use UnexpectedValueException;
use Joomla\DI\Exception\KeyNotFoundException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Rule */
class LiteraturtitelRule extends FormRule
{

    protected $regex = '[.]*';

    /**
     * @param SimpleXMLElement $element 
     * @param mixed $value 
     * @param string $group 
     * @param null|Registry $input 
     * @param null|Form $form 
     * @return bool 
     * @throws UnexpectedValueException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function test($element, $value, $group = null, $input = null, $form = null) {
        if (!parent::test($element, $value, $group, $input, $form)) {
            return false;
        }
        $recordId = Factory::getApplication()->input->getInt('id');
        $typ = $input->get('publikationstyp');
        if ($recordId == 0)
        {
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $query = $db->createQuery()
                ->select($db->qn('a.titel'))
                ->from($db->qn('#__iwf_literatur', 'a'))
                ->where($db->qn('a.titel') . ' LIKE :value')
                ->where($db->qn('a.publikationstyp') . '=:typ')
                ->where($db->qn('a.id') . '=:id')
                ->bind(':value', $value, ParameterType::STRING)
                ->bind(':typ', $typ, ParameterType::INTEGER)
                ->bind(':id', $recordId, ParameterType::INTEGER);
            if ($db->setQuery($query)->execute()) {
                if ($db->getAffectedRows() > 0) {
                    Factory::getApplication()->enqueueMessage('Eine Publikation mit diesem Titel existiert bereits!', CMSApplicationInterface::MSG_ERROR);
                    return false;
                }
            }
        }
        return true;
    }

}
