<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Rule;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Application\CMSApplicationInterface;
use Joomla\CMS\Date\Date;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Form\FormRule;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\DatabaseInterface;
use Joomla\Database\ParameterType;
use SimpleXMLElement;
use Joomla\Registry\Registry;
use UnexpectedValueException;
use Joomla\DI\Exception\KeyNotFoundException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Rule */
class WohnungsreservierungRule extends FormRule
{
    protected $regex = '^([0-9]{4})[-]{1}([0-9]{2})[-]{1}([0-9]{2})$';

    /**
     * Prüft, ob keine Überlappungen mit anderen Wohnungsreservierungen bestehen
     * @param SimpleXMLElement $element 
     * @param mixed $value 
     * @param string $group 
     * @param null|Registry $input 
     * @param null|Form $form 
     * @return bool 
     * @throws UnexpectedValueException 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws InvalidArgumentException 
     */
    public function test($element, $value, $group = null, $input = null, $form = null) {
        // Zugriff auf Feldattribute: (string)$element['attributname']
        // Zugriff auf Daten: $input->get('feldname')
        // Test the value against the regular expression.
        if (!parent::test($element, $value, $group, $input, $form)) {
            return false;
        }
        $id = $input->get('id');
        $datumvon = (new Date($value))->toSql();
        $datumbis = (new Date($input->get('datumbis')))->toSql();
        $wohnung = $input->get('text');
        $typ = RESERVIERUNGSTYP_WOHNUNG;
        // hier auf eine Ueberlappung der Veranstaltung pruefen
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select
            (
                [
                    $db->qn('a.datum', 'datumvon'),
                    $db->qn('a.datumbis', 'datumbis'),
                    $db->qn('a.text', 'wohnung'),
                    $db->qn('l.inhalt', 'wohnungsbezeichnung'),
                ]
            )
            ->from($db->qn('#__iwf_reservierungen', 'a'))
            ->leftJoin($db->qn('#__iwf_listen', 'l'), $db->qn('l.id') . '=' . $db->qn('a.text'))
            ->where($db->qn('a.reservierungstyp') . '=:typ')
            ->bind(':typ', $typ, ParameterType::INTEGER)
            ->andWhere
            (
                [
                    $db->qn('datum') . ' BETWEEN :datumvon AND :datumbis' ,
                    $db->qn('datumbis') . ' BETWEEN :datumvon AND :datumbis',
                    ':datumvon BETWEEN ' . $db->qn('datum') . ' AND ' . $db->qn('datumbis'),
                    ':datumbis BETWEEN ' . $db->qn('datum') . ' AND ' . $db->qn('datumbis'),
                ],
                'OR'
            )
            ->where($db->qn('text') . '=:wohnung')
            ->bind(':datumvon', $datumvon, ParameterType::STRING)
            ->bind(':datumbis', $datumbis, ParameterType::STRING)
            ->bind(':wohnung', $wohnung, ParameterType::INTEGER);
        if ($id) {
            $query->where($db->qn('a.id') . ' <> :id')
            ->bind(':id', $id, ParameterType::INTEGER);
        }
        if ($result = $db->setQuery($query)->loadObject()) {
            if ($result->wohnung == WOHNUNG3) {
                $doppelbelegung_erlaubt = $input->get('doppelbelegung') != 0;
                if (!$doppelbelegung_erlaubt) {
                    $element['message'] = "Doppelbelegung wurde nicht erlaubt! Falls trotzdem erwünscht, Schalter für Doppelbelegung auf Ja setzen und Erstbeleger um Erlaubnis fragen!";
                    return false;
                }
                $app = Factory::getApplication();
                $app->enqueueMessage($result->wohnungsbezeichnung . ": Sie haben sich für eine Doppelbelegung im Zeitraum " . 
                    HTMLHelper::_('date', $result->datumvon, 'd.m.Y') . " bis " . HtmlHelper::_('date', $result->datumbis, 'd.m.Y') . 
                    " entschieden. Bitte stellen Sie sicher, dass der Erstbeleger damit einverstanden ist!", CMSApplicationInterface::MSG_WARNING);
                return true;  // Bei Doppelzimmerwohnung dürfen 2 Reservierungen existieren!
            }
            $element['message'] = $result->wohnungsbezeichnung . " bereits vergeben: " . HtmlHelper::_('date', $result->datum, 'd.m.Y') . " bis " . HtmlHelper::_('date', $result->datumbis, 'd.m.Y');
            return false;
        }
        return true;
    }

}
