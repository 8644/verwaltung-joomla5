<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Akademis;

use Exception;
use Joomla\CMS\Factory;
use Joomla\Database\DatabaseInterface;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use XMLWriter;

defined('_JEXEC') or die('Restricted access');

/** @package Iwf\Component\Verwaltung\Site\Akademis */
class akademis {

    protected $options;
    protected $akademis_result = array();
    protected $tables_to_export = array();
    protected $export_path;
    protected $db_link = null;
    protected $akatexte = null;
    protected $dbrec = null;
    protected $converted_types = array();
    public $HasRecords = false;

    /**
     * @param mixed $options 
     * @return void 
     * @throws KeyNotFoundException 
     */
    function __construct($options = null) {
        if ($options) {
            $this->options = $options;
            $this->akademis_result[] = '';
            require_once(dirname(__FILE__) . '/akademis_constants.php');
            require_once(dirname(__FILE__) . '/akademis_classes.php');
            $this->export_path = dirname(__FILE__) . '/akademis_export_' . $this->options->jahr;
            $this->akatexte = new lit_texte();
            $this->dbrec = new lit_record();
            if ($this->options->jahr) {
                $jahr = '%' . $this->options->jahr . '%';
                $db = Factory::getContainer()->get(DatabaseInterface::class);
                $query = $db->createQuery()
                    ->select($db->qn('a.id', 'id'))
                    ->from($db->qn('#__iwf_literatur', 'a'))
                    ->where($db->qn('a.akademis1') . '=0')
                    ->andWhere
                    (
                        [
                            $db->qn('a.tagungsdatum') . ' LIKE :jahr',
                            $db->qn('a.erscheinungsjahr') . '=:ejahr'
                        ],
                        'OR'
                    )
                    ->bind(':jahr', $jahr, ParameterType::STRING)
                    ->bind(':ejahr', $this->options->jahr, ParameterType::INTEGER);

                if ($options->filter) {
                    $query->whereIn($db->qn('a.publikationstyp'), explode(',', $options->filter));
                }
                if ($db->setQuery($query)->execute()) {
                    if ($db->getNumRows() == 0) {
                        $this->AddResultText('<p>Keine zu berabeitenden Datensätze gefunden (wurde vielleicht das Erledigt-Flag bereits gesetzt?)');
                    } else {
                        $this->HasRecords = true;
                        $this->prepareTables();
                    }
                }
            }
        }
    }

    /**
     * ändert die Eingabeliste der AkademIS-Autoren so, daß der Vorname nur mehr aus einem Buchstaben + "." besteht
     * damit läßt sich ein Autor sicher in der Mitarbeiterliste finden, die ebenfalls nur den ersten Vornamenbuchstaben verwendet.
     * @param mixed $ak_autoren 
     * @return array<mixed, string> 
     */
    private static function getAkademisAutorenListe($ak_autoren) {
        $ret = array();
        $treffer = null;
        foreach($ak_autoren as $key => $val) {    
            if (preg_match("/^[^\.]+\./", $val, $treffer)) {
                $ret[$key] = $treffer[0];
            }
        }
        return $ret;
    }

    /**
     * liefert die Akademis-Id eines Institutsautors
     * @param mixed $author 
     * @param mixed $authors 
     * @return mixed 
     */
    private static function getAkademisIdOfAuthor($author, $authors) {
        foreach ($authors as $key => $val) {    
            if (strpos($val, $author) !== false) {
                return $key;
            }
        }
        return 0;
    }

    /**
     * @param mixed $text 
     * @return void 
     */
    public function AddResultText($text) {
        $this->akademis_result[] = $text;
    }

    /** @return array  */
    public function GetResult() {
        return $this->akademis_result;
    }

    /**
     * Debugausgabe für string
     * @param mixed $msg 
     * @return void 
     */
    private function _e($msg) {
        if (defined("DEBUG")) {
            if (is_array($msg)) {
                $this->akademis_result[] = print_r($msg, true);
            } else {
                $this->akademis_result[] = $msg;
            }
            $this->akademis_result[] = "<br><br>";
        }
    }

    /**
     * gibt das Instituts-Format von Doppelvornamen im Akademis-Format zurück
     * $input kann sein: A. (1 Vorname), A.B. (2 Vornamen usw.)
     * @param mixed $input 
     * @return string 
     */
    private static function getFirstNames($input) {
        $ret = "";
        $inp = trim($input);
        if (substr($inp, strlen($inp) - 1, 1) == ".") {  // das sollte immer der Fall sein
            $inp = substr($inp, 0, strlen($inp) - 1);
        }
        $firstnames = explode(".", $inp);
        foreach ($firstnames as $val) {
            if (!(strpos($val, "-") === false)) { // Doppelvorname! kein Leerzeichen
                $ret = trim($ret) . $val . ". ";
            } else {
                $ret .= $val . ". ";
            }
        }
        return trim($ret);
    }

    /**
     * Instituts-Autorenformat ist immer: 1.Name: Nachname V., dann V. Nachname
     * bei 1. Namen ist Nachname und Vorname zusätzlich durch Komma getrennt!
     * Bei mehreren Vornamen ist kein Leerzeichen zwischen den Vornamen
     * @param mixed $input 
     * @return string 
     */
    private static function convertAutoren($input) {
        $etal_pos = strpos($input, 'et al.');
        if (!($etal_pos === false)) {
            $input = trim(substr($input, 0, $etal_pos));
        }
        if (substr($input, strlen($input) - 1, 1) == ",") {
            $input = trim(substr($input, 0, strlen($input) - 1));
        }
        $namen = explode(",", $input);
        if (count($namen) == 0) {
            return "";
        }
        if (count($namen) == 1) {
            return $namen[0];
        }
        //0 u. 1 sind Nachname, Vorname(n)
        // alle anderen Vorname(n) + Nachname
        $ret = trim($namen[0]) . ", " . self::getFirstNames($namen[1]);
        for ($i = 2; $i < count($namen); $i++) {
            $ret .= BR;
            $namen1 = explode(" ", trim($namen[$i]));
            if (count($namen1) != 2) { // möglicherweise Leerzeichen zwischen Vornamensabkürzung.
                if (count($namen1) == 1) { // nicht erlaubtes Namensformat
                    continue;
                }
                $firstname = "";
                $lastname = "";
                foreach ($namen1 as $val) {
                    if (!(strpos($val, ".") === false)) {
                        $firstname .= $val;
                    } else {
                        $lastname = $val;
                    }
                }
                if ($firstname != "" && $lastname != "") {
                    $ret .= $lastname . ", " . self::getFirstNames($firstname);
                }
            } else {  // dies ist der Standardfall
                // $namen1[0] auf Mehrfachvornamen untersuchen
                $ret .= $namen1[1] . ", " . self::getFirstNames($namen1[0]);
            }
        }
        if (!($etal_pos === false)) {
            $ret .= BR . "et al.";
        }
        return $ret;
    }

    /**
     * gibt alle Datensätze mit id und Typ nach Typ sortiert zurück
     * akademis1 dient als Flag, um kennzuzeichnen, welche Datensätze nicht mehr behandelt werden sollen
     * @param mixed $convertable_type 
     * @return mixed 
     * @throws KeyNotFoundException 
     */
    private function getSortedPublicationList($convertable_type) {
        $types_to_convert = explode(',', $convertable_type);
        if ($this->options->filter) {
            // gefilterte Publikationen, die nicht in $convertable_type enthalten sind,
            // aus dem Filter entfernen
            $filtered_types = explode(',', $this->options->filter);
            $valid_types = "";
            foreach ($filtered_types as $value) {
                if (in_array($value, $types_to_convert, true)) {
                    $valid_types .= $value . ",";
                }
            }
            $types_to_convert = $valid_types ? explode(',', trim($valid_types, ',')) : null;
        }
        $items = [];
        if ($types_to_convert) {
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $query = $db->createQuery()
                ->select
                (
                    [
                        $db->qn('id'),
                        $db->qn('publikationstyp'),
                        $db->qn('akademis1'),
                        $db->qn('tagungsdatum'),
                        $db->qn('erscheinungsjahr'),
                    ]
                )
                ->from($db->qn('#__iwf_literatur'));
            if (in_array(VORTRAG_BEI_TAGUNGEN, $types_to_convert)) {
                $j = "%" . $this->options->jahr . "%";
                $query->where($db->qn('tagungsdatum') . ' LIKE :jahr')
                    ->bind(':jahr', $j, ParameterType::STRING);
            } else {
                $query->where($db->qn('erscheinungsjahr') . '=:jahr')
                    ->bind(':jahr', $this->options->jahr, ParameterType::INTEGER);
            }
            $query->where($db->qn('akademis1') . '=0')
                ->whereIn($db->qn('publikationstyp'), $types_to_convert);
            $query->order($db->qn('publikationstyp'));
            $items = $db->setQuery($query)->loadObjectList();
            $this->_e($items);
        }
        return $items;
    }

    /**
     * @return array<mixed, string> 
     * @throws KeyNotFoundException 
     */
    private function getInstitutAutorenListe() {
        $autoren = [];
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select
            (
                [
                    $db->qn('a.id'),
                    $db->qn('a.nachname'),
                    $db->qn('a.vorname'),
                    $db->qn('a.initialen'),
                    $db->qn('a.akademis_node'),
                    $db->qn('b.abteilung'),
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter', 'a'))
            ->leftJoin($db->qn('#__iwf_abteilungen','b'), $db->qn('a.abteilung') . '=' . $db->qn('b.id'))
            ->where($db->qn('a.kein_akademis_export') . '=0')
            ->whereIn($db->qn('a.abteilung'), explode(',', ABTEILUNGEN_IWF))
            ->order($db->qn('a.nachname'));
        $rows = $db->setQuery($query)->loadObjectList();
        foreach($rows as $row) {
            $ini = mb_substr($row->vorname, 0, 1, 'UTF-8');
            $ini_variations = null;
            $ini_variations[] = mb_substr($row->vorname, 0, 1, 'UTF-8') . ".";
            if ($row->initialen && $row->initialen != "$ini.") {
                $ini_variations[] = $row->initialen;
            }
            $names = explode(" ", $row->nachname);
            $nx = "";
            $name_variations = null;
            foreach ($names as $nn) {
                $nx .= " $nn";
                $name_variations[] = trim($nx);
            }
            $name = "";
            foreach ($name_variations as $nn) {
                foreach ($ini_variations as $ii) {
                    $name .= "$nn, $ii|";
                }
            }
            $autoren[$row->akademis_node] = trim($name, "|");
        }
        return $autoren;
    }

    /**
     * Ermittelt, ob der erste Autor der Autorenliste ein Mitarbeiter/Gast des Instituts ist
     * in diesem Fall ist Rückgabe 1, sonst -1
     * @param mixed $autoren 
     * @return int 
     * @throws KeyNotFoundException 
     */
    private function istFederfuehrend($autoren) {
        $autoren_arr = explode(BR, $autoren);
        if (count($autoren_arr) > 0) {
            $autor = explode(",", $autoren_arr[0]);
            $name = $autor[0] . " " . substr($autor[1], 1, 1);
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $query = $db->createQuery();
            $query->select('a.nachname AS nachname,a.vorname as vorname');
            $query->from('#__iwf_mitarbeiter AS a');
            $query->where('a.abteilung IN (' . ABTEILUNGEN_IWF . ')');
            $query->where("CONCAT(a.nachname,' ',SUBSTR(a.vorname,1,1)) LIKE '" . $name . "'");
            $db->setQuery((string) $query);
            if ($db->execute()) {
                if ($db->getNumRows()) {
                    return 1;
                }
            }
        }
        return -1;
    }

    /**
     * @return array 
     * @throws KeyNotFoundException 
     */
    private function getFachzeitschriftenListe() {
        $zeitschriften = [];
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select
            (
                [
                    $db->qn('import_node'),
                    $db->qn('zeitschrift')
                ]
            )
            ->from($db->qn('#__iwf_zeitschriften'));
        $rows = $db->setQuery($query)->loadObjectList();
        foreach($rows as $row) {
            if ($row->import_node != 0) {
                $zeitschriften[$row->import_node] = $row->zeitschrift;
            }
        }
        return $zeitschriften;
    }

    /** 
     * gibt den Node der Organisationseinheit (Abteilung) aufgrund der Mitarbeiter-Akademis-Node zurück
     * @return int  */
    private function getAbteilungNode() {
        return 11400;  // alle werden zu IWF gelinkt
    }

    /**
     * gibt eine Liste aller Publikationen/Vorträge mit dem zugeordneten Forschungsprojekt-Node zurück
     * @return array 
     * @throws KeyNotFoundException 
     */
    private function getForschungsprojektNodes() {
        $ret = [];
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select
            (
                [
                    $db->qn('a.id'),
                    $db->qn('p.akademis_projekt_node', 'node'),
                ]
                
            )
            ->from($db->qn('#__iwf_literatur', 'a'))
            ->leftJoin($db->qn('#__iwf_lit_projekte', 'p'), $db->qn('a.titelprojekte') . '=' . $db->qn('p.id'));
        $rows = $db->setQuery($query)->loadObjectList();
        foreach ($rows as $row) {
            $ret[$row->id] = $row->node;
        }
        return $ret;
    }

    /**
     * gibt ein array mit allen Vortrags-Ids zu den betreffenden Teilveranstaltungen zurück.
     * @param mixed $teilveranstaltung_ids 
     * @return null 
     * @throws KeyNotFoundException 
     */
    function getVortragListe($teilveranstaltung_ids) {
        $ret = null;
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery();
        $query->select('a.id AS id')->from('#__iwf_literatur AS a');
        $query->where('a.veranstaltung_id IN (' . $teilveranstaltung_ids . ') AND a.publikationstyp IN (' . VORTRAEGE . ')');
        $db->setQuery((string) $query);
        if ($db->execute()) {
            $rows = $db->loadObjectList();
            if (count($rows)) {
                foreach ($rows as $row) {
                    $ret[] = $row->id;
                }
            }
        }
        return $ret;
    }

    /**
     * Tabelle aka_j_012 erhält Feld Federfuehrend
     * Tabelle aka_j_016 erhält Feld Federfuehrend
     * @return void 
     * @throws KeyNotFoundException 
     */
    private function prepareTables() {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery();
        $query->setQuery("ALTER TABLE `iwf23_iwf_aka_j_012` ADD `Federfuehrend` smallint(6) NOT NULL DEFAULT '1'");
        try {
            $db->setQuery($query)->execute();
        } 
        catch (Exception $e) { }
        $query->setQuery("ALTER TABLE `iwf23_iwf_aka_j_016` ADD `Federfuehrend` smallint(6) NOT NULL DEFAULT '1'");
        try {
            $db->setQuery($query)->execute();
        } 
        catch (Exception $e) { }
    }

    /**
     * @return void 
     * @throws KeyNotFoundException 
     */
    private function finalizeTables() {
        // Zusatzfelder löschen
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery();
        $query->setQuery('ALTER TABLE ' . $db->qn('#__iwf_aka_j_012') . ' DROP ' . $db->qn('Federfuehrend'));
        try {
            $db->setQuery($query)->execute();
        } 
        catch (Exception $e) { 
            $i = 0;
        }
        $query->clear();
        $query->setQuery('ALTER TABLE ' . $db->qn('#__iwf_aka_j_016') . ' DROP ' . $db->qn('Federfuehrend'));
        try {
            $db->setQuery($query)->execute();
        } 
        catch (Exception $e) { }
    }

    /**
     * Organisationseinheit - Publikation
     * @return void 
     * @throws KeyNotFoundException 
     */
    function CreateRelationsR_017() {
        $rel_cnt = 0;
        $autoren = $this->getInstitutAutorenListe();
        if (count($autoren)) {
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $query = $db->createQuery()
                ->delete($db->qn('#__iwf_aka_r_017'));
            $db->setQuery($query)->execute();
            $query->clear();
            $query->select('*')
                ->from($db->qn('#__iwf_aka_j_012'));
            $rows = $db->setQuery($query)->loadObjectList();
            foreach ($rows as $row) {
                $this->_e($row);
                if ($row->{A122} <> "") {
                    $ak_autoren = self::getAkademisAutorenListe(explode(BR, $row->{A122}));  //Autoren
                } else {
                    $ak_autoren = self::getAkademisAutorenListe(explode(BR, $row->{A306}));  //Herausgeber (bei Buchherausgabe keine A122-Autoren)
                }
                $federfuehrend = $row->{federfuehrend};
                foreach ($ak_autoren as $val) {
                    if (self::getAkademisIdOfAuthor($val, $autoren)) {
                        $id = $row->{J012A};
                        $this->_e($id);
                        // hier zugehörige Abteilung herausfinden!!
                        $abt_node = $this->getAbteilungNode();
                        if ($abt_node) {
                            $query->clear();
                            $query->insert($db->qn('#__iwf_aka_r_017'))
                                ->columns(
                                    [
                                        $db->qn(J012A),
                                        $db->qn(J002),
                                        $db->qn(E106),
                                    ]
                                )
                            ->values(':id, :node, :feder')
                            ->bind(':id', $id, ParameterType::STRING)
                            ->bind(':node', $abt_node, ParameterType::INTEGER)
                            ->bind(':feder', $federfuehrend, ParameterType::STRING);
                            $db->setQuery($query)->execute();
                            $rel_cnt++;
                            break;
                        }
                    }
                }
            }
            if ($rel_cnt == 0) {
                $this->AddResultText(sprintf("%s%skeine Relationen R-017 Organisationseinheit -< Publikation%s", NBSP, NBSP, BR));
            } else {
                $this->AddResultText(sprintf("%s%s%s%s", NBSP, NBSP, htmlentities("$rel_cnt Relationen R-017 Organisationseinheit -< Publikation erzeugt"), BR));
                $this->tables_to_export[] = '#__iwf_aka_r_017';
            }
        }
    }

    /**
     * Person >< Publikation Relationen
     * @return void 
     * @throws KeyNotFoundException 
     */
    function CreateRelationsR_025() {
        $rel_cnt = 0;
        $no_institut_autoren = "";
        $autoren = $this->getInstitutAutorenListe();
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->delete($db->qn('#__iwf_aka_r_025'));
        $db->setQuery($query)->execute();
        $query->clear();
        $query->select('*')
            ->from($db->qn('#__iwf_aka_j_012'));
        $rows = $db->setQuery($query)->loadObjectList();
        foreach ($rows as $row) {
            $query->clear();
            $this->_e($row);
            if ($row->{A121} <> "") {  // es handelt sich um eine Publikation (Titel ist nicht leer)
                switch ($row->{A127}) {
                    case A_127FACHZEITSCHRIFT:
                    case A_127SAMMELWERK:
                    case A_127FORSCHUNGSBERICHT:
                    case A_127KONFPROC:
                    case A_127KONFPOST:
                    case A_127HABILITATION:
                    case A_127DIPLOMARBEIT:
                    case A_127DISSERTATION:
                    case A_127BUCH:
                        $ak_autoren = self::getAkademisAutorenListe(explode(BR, $row->{A122}));
                        $n = 0;
                        $institut_autoren_cnt = 0;
                        $cnt = count($ak_autoren);
                        foreach ($ak_autoren as $val) {
                            $n++;
                            $ak_id = self::getAkademisIdOfAuthor($val, $autoren);
                            if ($ak_id) {
                                if ($cnt == 1) {
                                    $attr = E_035ALLEINAUTOR;
                                } else {
                                    if ($n == 1) {
                                        $attr = E_035HAUPTAUTOR;
                                    } else {
                                        $attr = E_035KOAUTOR;
                                    }
                                }
                                $id = $row->{J012A};
                                $this->_e($id);
                                $query->clear();
                                $query->insert($db->qn('#__iwf_aka_r_025'))
                                    ->columns(
                                        [
                                            $db->qn(J012A),
                                            $db->qn(J008),
                                            $db->qn(E035),
                                        ]
                                    )
                                    ->values(':id, :ak_id, :attr')
                                    ->bind(':id', $id, ParameterType::STRING)
                                    ->bind(':ak_id', $ak_id, ParameterType::INTEGER)
                                    ->bind(':attr', $attr, ParameterType::STRING);
                                $db->setQuery($query)->execute();
                                $rel_cnt++;
                                $institut_autoren_cnt++;
                            }
                        }
                        if ($institut_autoren_cnt == 0) {
                            $no_institut_autoren .= $row->{J012} . ", ";
                        }
                        break;
                    case A_127HERAUSGEBERSCHAFT:  // Herausgabe
                        $ak_autoren = self::getAkademisAutorenListe(explode(BR, $row->{A306}));
                        $n = 0;
                        $institut_autoren_cnt = 0;
                        $cnt = count($ak_autoren);
                        foreach ($ak_autoren as $val) {
                            $n++;
                            $ak_id = self::getAkademisIdOfAuthor($val, $autoren);
                            if ($ak_id) {
                                if ($n == 1) {
                                    $attr = E_035HERAUSGEBER;
                                } else {
                                    $attr = E_035KOHERAUSGEBER;
                                }
                                $id = $row->{J012A};
                                $query->clear();
                                $query->insert($db->qn('#__iwf_aka_r_025'))
                                    ->columns(
                                        [
                                            $db->qn(J012A),
                                            $db->qn(J008),
                                            $db->qn(E035)
                                        ]
                                    )
                                    ->values(':id, :ak_id, :attr')
                                    ->bind(':id', $id, ParameterType::STRING)
                                    ->bind(':ak_id', $ak_id, ParameterType::INTEGER)
                                    ->bind(':attr', $attr, ParameterType::STRING);
                                $db->setQuery($query)->execute();
                                $rel_cnt++;
                                $institut_autoren_cnt++;
                            }
                        }
                        if ($institut_autoren_cnt == 0) {
                            $no_institut_autoren .= $row->{J012} . ", ";
                        }
                        break;
                }
            }
        }
        if ($no_institut_autoren <> "") {
            $no_institut_autoren = "<b>" . BR . "&nbsp;&nbsp;&nbsp;&nbsp;Publikationen mit folgenden Ids enthielten keinen Instituts-Autor: " . trim($no_institut_autoren, ", ") . "</b>";
        }
        if ($rel_cnt == 0) {
            $this->AddResultText(sprintf("%s%skeine Relationen R-025 Person -< Publikation%s", NBSP, NBSP, BR));
        } else {
            $this->AddResultText(sprintf("%s%s%s%s%s", NBSP, NBSP, htmlentities("$rel_cnt Relationen R-025 Person -< Publikation erzeugt"), $no_institut_autoren, BR));
            $this->tables_to_export[] = '#__iwf_aka_r_025';
        }
    }

    /**
     * Forschungsprojekt >< Publikation Relationen
     * @return void 
     * @throws KeyNotFoundException 
     */
    function CreateRelationsR_029() {
        $rel_cnt = 0;
        $fp_nodes = $this->getForschungsprojektNodes();
        if (count($fp_nodes)) {
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $query = $db->createQuery()
                ->delete($db->qn('#__iwf_aka_r_029'));
            $db->setQuery($query)->execute();
            $query->clear();
            $query->select('*')
                ->from($db->qn('#__iwf_aka_j_012'));
            $rows = $db->setQuery($query)->loadObjectList();
            foreach ($rows as $row) {
                $this->_e($row);
                $query->clear();
                $node = $fp_nodes[$row->{J012}];
                if ($node) {
                    $id = $row->{J012A};
                    $query->insert($db->qn('#__iwf_aka_r_029'))
                        ->columns(
                            [
                                $db->qn(J010),
                                $db->qn(J012A)
                            ]
                        )
                        ->values(':node, :id')
                        ->bind(':node', $node, ParameterType::INTEGER)
                        ->bind(':id', $id, ParameterType::STRING);
                    $db->setQuery($query)->execute();
                    $rel_cnt++;
                }
            }
        }
        if ($rel_cnt == 0) {
            $this->AddResultText(sprintf("%s%skeine Relationen R-029 Forschungsprojekt >< Publikation%s", NBSP, NBSP, BR));
        } else {
            $this->AddResultText(sprintf("%s%s%s%s", NBSP, NBSP, htmlentities("$rel_cnt Relationen R-029 Forschungsprojekt >< Publikation erzeugt"), BR));
            $this->tables_to_export[] = '#__iwf_aka_r_029';
        }
    }

    /**
     * Person >< Vortrag Relationen
     * @return void 
     * @throws KeyNotFoundException 
     */
    function CreateRelationsR_030() {
        $rel_cnt = 0;
        $no_institut_autoren = "";
        $autoren = $this->getInstitutAutorenListe();
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->delete($db->qn('#__iwf_aka_r_030'));
        $db->setQuery($query)->execute();
        $query->clear();
        $query->select('*')
            ->from($db->qn('#__iwf_aka_j_016'));
        $rows = $db->setQuery($query)->loadObjectList();
        foreach ($rows as $row) {
            $query->clear();
            $this->_e($row);
            if ($row->{A163} <> "") {  // Vortrag/Poster
                $ak_autoren = self::getAkademisAutorenListe(explode(BR, $row->{A164}));
                $institut_autoren_cnt = 0;
                $n = 0;
                foreach ($ak_autoren as $val) {
                    $n++;
                    $ak_id = self::getAkademisIdOfAuthor($val, $autoren);
                    if ($ak_id) {
                        $id = $row->{J016A};
                        if ($n == 1 && $row->{federfuehrend} == 1) {
                            $beteiligtals = E_208KOAUTOR_UND_PRAESENTATOR;
                        } else {
                            $beteiligtals = E_208KOAUTOR;
                        }
                        $this->_e($id);
                        $query->clear()
                            ->insert($db->qn('#__iwf_aka_r_030'))
                            ->columns(
                                [
                                    $db->qn(J016A),
                                    $db->qn(E208),
                                    $db->qn(J008),
                                ]
                            )
                            ->values(':id, :beteiligt, :ak_id')
                            ->bind(':id', $id, ParameterType::STRING)
                            ->bind(':beteiligt', $beteiligtals, ParameterType::STRING)
                            ->bind(':ak_id', $ak_id, ParameterType::INTEGER);
                        $db->setQuery($query)->execute();
                        $rel_cnt++;
                        $institut_autoren_cnt++;
                    }
                }
                if ($institut_autoren_cnt == 0) {
                    $no_institut_autoren .= $row->{J016} . ", ";
                }
            }
        }
        if ($no_institut_autoren <> "") {
            $no_institut_autoren = "<b><br>&nbsp;&nbsp;&nbsp;&nbsp;Vorträge mit folgenden Ids enthielten keinen Instituts-Autor: " . trim($no_institut_autoren, ", ") . "</b>";
        }
        if ($rel_cnt === (int) 0) {
            $this->AddResultText(sprintf("%s%skeine Relationen R-030 Relationen Person -< Vortrag%s", NBSP, NBSP, BR));
        } else {
            $this->AddResultText(sprintf("%s%s%s%s%s", NBSP, NBSP, htmlentities("$rel_cnt Relationen R-030 Person -< Vortrag erzeugt"), $no_institut_autoren, BR));
            $this->tables_to_export[] = '#__iwf_aka_r_030';
        }
    }

    /**
     * Organisationseinheit - Vortrag/Poster
     * @return void 
     * @throws KeyNotFoundException 
     */
    function CreateRelationsR_031() {
        $rel_cnt = 0;
        $autoren = $this->getInstitutAutorenListe();
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->delete($db->qn('#__iwf_aka_r_031'));
        $db->setQuery($query)->execute();
        $query->clear();
        $query->select('*')
            ->from($db->qn('#__iwf_aka_j_016'));
        $rows = $db->setQuery($query)->loadObjectList();
        foreach ($rows as $row) {
            $this->_e($row);
            $ak_autoren = $this->getAkademisAutorenListe(explode(BR, $row->{A164}));
            $federfuehrend = $row->{federfuehrend};
            foreach ($ak_autoren as $val) {
                $query->clear();
                $ak_id = self::getAkademisIdOfAuthor($val, $autoren);
                if ($ak_id) {
                    $id = $row->{J016A};
                    $this->_e($id);
                    // hier zugehörige Abteilung herausfinden!!
                    $abt_node = $this->getAbteilungNode();
                    if ($abt_node) {
                        $query->insert($db->qn('#__iwf_aka_r_031'))
                            ->columns(
                                [
                                    $db->qn(J016A),
                                    $db->qn(J002),
                                    $db->qn(E107),
                                ]
                            )
                            ->values(':id, :node, :feder')
                            ->bind(':id', $id, ParameterType::STRING)
                            ->bind(':node', $abt_node, ParameterType::INTEGER)
                            ->bind(':feder', $federfuehrend, ParameterType::STRING);
                        $db->setQuery($query)->execute();
                        $rel_cnt++;
                        break;
                    }
                }
            }
        }
        if ($rel_cnt == 0) {
            $this->AddResultText(sprintf("%s%skeine Relationen R-031 Organisationseinheit -< Vortrag/Poster%s", NBSP, NBSP, BR));
        } else {
            $this->AddResultText(sprintf("%s%s%s%s%s", NBSP, NBSP, htmlentities("$rel_cnt Relationen R-031 Organisationseinheit -< Vortrag/Poster erzeugt"), "", BR));
            $this->tables_to_export[] = '#__iwf_aka_r_031';
        }
    }

    /**
     * Forschungsprojekt >< Vortrag/Poster Relationen
     * @return void 
     * @throws KeyNotFoundException 
     */
    function CreateRelationsR_032() {
        $rel_cnt = 0;
        $fp_nodes = $this->getForschungsprojektNodes();
        if (count($fp_nodes)) {
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $query = $db->createQuery()
                ->delete($db->qn    ('#__iwf_aka_r_032'));
            $db->setQuery($query)->execute();
            $query->clear();            
            $query->select('*')
                ->from($db->qn('#__iwf_aka_j_016'));
            $rows = $db->setQuery($query)->loadObjectList();
            foreach ($rows as $row) {
                $query->clear();
                $this->_e($row);
                $node = $fp_nodes[$row->{J016}];
                if ($node) {
                    $id = $row->{J016A};
                    $query->insert($db->qn('#__iwf_aka_r_032'))
                        ->columns(
                            [
                                $db->qn(J010),
                                $db->qn(J016A),
                            ]
                        )
                        ->values(':node, :id')
                        ->bind(':node', $node, ParameterType::INTEGER)
                        ->bind(':id', $id, ParameterType::STRING);
                    $db->setQuery($query)->execute();
                    $rel_cnt++;
                }
            }
        }
        if ($rel_cnt == 0) {
            $this->AddResultText(sprintf("%s%skeine Relationen R-032 Forschungsprojekt >< Vortrag/Poster%s", NBSP, NBSP, BR));
        } else {
            $this->AddResultText(sprintf("%s%s%s%s%s", NBSP, NBSP, htmlentities("$rel_cnt Relationen R-032 Forschungsprojekt >< Vortrag/Poster erzeugt"), "", BR));
            $this->tables_to_export[] = '#__iwf_aka_r_032';
        }
    }

    /**
     * Publikation - Fachzeitschrift (nur für ref. u. nicht ref. Zeitschriften)
     * @return void 
     * @throws KeyNotFoundException 
     */
    function CreateRelationsR_083() {
        $rel_cnt = 0;
        $zeitschriften = $this->getFachzeitschriftenListe();
        if (count($zeitschriften)) {
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $query = $db->createQuery();
            $query->delete($db->qn('#__iwf_aka_r_083'));
            $db->setQuery($query)->execute();
            $query->clear();
            $query->select('*')
                ->from($db->qn('#__iwf_aka_j_012'));
            $rows = $db->setQuery($query)->loadObjectList();
            foreach ($rows as $row) {
                $query->clear();
                $this->_e($row);
                if ($row->{A121} <> "") {  // es handelt sich um eine Publikation (Titel ist nicht leer)
                    switch ($row->{A127}) {
                        case A_127FACHZEITSCHRIFT:
                            $id = $row->{J012A};
                            if (in_array($row->{A311}, $zeitschriften)) {
                                $z_id = array_search($row->{A311}, $zeitschriften);
                                $query->insert($db->qn('#__iwf_aka_r_083'))
                                    ->columns(
                                        [
                                            $db->qn(J012A),
                                            $db->qn(J044),
                                        ]
                                    )
                                    ->values(':id, :z_id')
                                    ->bind(':id', $id, ParameterType::STRING)
                                    ->bind(':z_id', $z_id, ParameterType::INTEGER);
                                $db->setQuery($query)->execute();
                                $rel_cnt++;
                            }
                            break;
                    }
                }
            }
        }
        if ($rel_cnt == 0) {
            $this->AddResultText(sprintf("%s%skeine Relationen R-083 Publikation - Fachzeitschrift%s", NBSP, NBSP, BR));
        } else {
            $this->AddResultText(sprintf("%s%s%s%s", NBSP, NBSP, htmlentities("$rel_cnt Relationen R-083 Publikation - Fachzeitschrift"), BR));
            $this->tables_to_export[] = '#__iwf_aka_r_083';
        }
    }

    /**
     * @return void 
     * @throws KeyNotFoundException 
     */
    function ConvertPublikationen() {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery();
        $query->delete($db->qn('#__iwf_aka_j_012'));
        $db->setQuery($query)->execute();
        $count = 0;
        $fields = null;
        $values = null;
        $items = $this->getSortedPublicationList(PUBLIKATIONEN);
        foreach($items as $val) {
            $query->clear();
            $this->dbrec->set($val->id);  // füllt den Literatur-Datensatz
            $autoren = self::convertAutoren($this->dbrec->autor);
            $herausgeber = self::convertAutoren($this->dbrec->herausgeber);
            $fields = $query->qn(federfuehrend) . ',';
            if ($this->dbrec->publikationstyp == 6) { // bei Herausgabe eines Buch keine Autorenliste!
                $values = $query->q($this->istFederfuehrend($herausgeber)) . ",";
            } else {
                $values = $query->q($this->istFederfuehrend($autoren)) . ",";
            }
            $fields .= $query->qn(J012) . ",";
            $values .= $query->q($this->dbrec->id) . ",";
            $fields .= $query->qn(J012A) . ",";
            $values .= $query->q('IWF' . $this->options->jahr . "-" . $this->dbrec->id) . ",";
            $fields .= $query->qn(A122) . ",";
            $values .= $query->q(strtr($autoren, "\"", "'")) . ',';
            $fields .= $query->qn(A123) . ",";
            $values .= $query->q('') . ',';
            $titel = trim(strtr($this->dbrec->titel, "\r\n", " "));
            $t = trim(strtr($titel, "\n", " "));
            $fields .= $query->qn(A121) . ",";
            $values .= $query->q(strtr($t, "\"", "'")) . ',';
            $fields .= $query->qn(A125) . ",";
            $datum = Date("d-m-Y", mktime(0, 0, 0, 6, 15, $this->dbrec->erscheinungsjahr));
            $values .= $query->q($datum) . ',';
            $fields .= $query->qn(A126) . ",";
            //$datum = Date("d-m-Y", mktime(0,0,0,6,15,$this->dbrec->erscheinungsjahr));
            $values .= $query->q($datum) . ',';
            $fields .= $query->qn(A494) . ",";
            $values .= $query->q($this->dbrec->mehr_als_zehntausend_zeichen == 1 ? 1 : -1) . ',';
            $fields .= $query->qn(A492) . ",";
            $values .= $query->q($this->dbrec->ist_populaerwissenschaftlich == 1 ? 1 : -1) . ',';
            $pushme = false;
            switch ($this->dbrec->publikationstyp) {
                case ZEITSCHRIFT_REFERIERT:  // ref. Zeitschrift
                    $fields .= $query->qn(A307) . ",";
                    $values .= $query->q($this->dbrec->band) . ',';
                    $fields .= $query->qn(A311) . ",";
                    $values .= $query->q($this->dbrec->zeitschrift) . ',';
                    $fields .= $query->qn(A308) . ",";
                    $values .= $query->q($this->dbrec->seitenzahlen) . ',';
                    $fields .= $query->qn(A745) . ",";
                    $values .= $query->q($this->dbrec->doi) . ',';
                    $fields .= $query->qn(A493) . ",";
                    $values .= $query->q(1) . ',';
                    $fields .= $query->qn(A127);
                    $values .= $query->q(A_127FACHZEITSCHRIFT);
                    $pushme = true;
                    break;
                case ZEITSCHRIFT_NICHT_REFERIERT:  // nicht ref. Zeitschrift
                    $fields .= $query->qn(A307) . ",";
                    $values .= $query->q($this->dbrec->band) . ',';
                    $fields .= $query->qn(A311) . ",";
                    $values .= $query->q($this->dbrec->zeitschrift) . ',';
                    $fields .= $query->qn(A308) . ",";
                    $values .= $query->q($this->dbrec->seitenzahlen) . ',';
                    $fields .= $query->qn(A745) . ",";
                    $values .= $query->q($this->dbrec->doi) . ',';
                    $fields .= $query->qn(A127);
                    $values .= $query->q(A_127FACHZEITSCHRIFT);
                    $pushme = true;
                    break;
                case IWF_BERICHT:   // Instituts-Bericht
                case BERICHT_ALLGEMEIN:  // Bericht allgemein
                    //TODO! mit preg_match die erste Zahl herausholen
                    $fields .= $query->qn(A322) . ",";   // bei beiden Typen Seitenzahl in dieses Feld??? (statt 308) Muß ein INTEGER Sein!!!!!!!!!
                    $values .= $query->q($this->dbrec->seitenzahlen) . ',';
                    $fields .= $query->qn(A317) . ",";
                    $values .= $query->q(INSTITUT) . ",";
                    $fields .= $query->qn(A304) . ",";
                    $values .= $query->q('Graz') . ',';
                    $fields .= $query->qn(A127);
                    $values .= $query->q(A_127FORSCHUNGSBERICHT);
                    $pushme = true;
                    break;
                case BUCHBEITRAG:  // Buchbeitrag/Proceeding
                    $fields .= $query->qn(A493) . ",";
                    $values .= $query->q($this->dbrec->begutachtet ? 1 : 1) . ',';
                    $fields .= $query->qn(A308) . ",";
                    $values .= $query->q($this->dbrec->seitenzahlen) . ',';
                    $fields .= $query->qn(A745) . ",";
                    $values .= $query->q($this->dbrec->doi) . ',';
                    $fields .= $query->qn(A306) . ",";
                    $values .= $query->q($herausgeber) . ',';
                    $fields .= $query->qn(A305) . ",";
                    $values .= $query->q($this->dbrec->verlag) . ',';
                    if (!stristr($this->dbrec->verlag, "Eigenverlag") === False) {
                        $fields .= $query->qn(A317) . ",";
                        $values .= $query->q(INSTITUT) . ",";
                    }
                    if (!stristr($this->dbrec->gesamttitel, "proc.") === false || !stristr($this->dbrec->gesamttitel, "proceed") === false) {  // ->Proceeding, d.h. Typ = KONFPROC
                        $fields .= $query->qn(A313) . ",";
                        $values .= $query->q(strtr($this->dbrec->gesamttitel, "\"", "'")) . ',';
                        $fields .= $query->qn(A127) . ',';
                        $values .= $query->q(A_127KONFPROC) . ',';
                    } else {  // Sammelwerk
                        $fields .= $query->qn(A313) . ",";
                        $values .= $query->q(strtr($this->dbrec->gesamttitel, "\"", "'")) . ',';
                        $fields .= $query->qn(A127) . ',';
                        $values .= $query->q(A_127SAMMELWERK) . ',';
                    }
                    $fields .= $query->qn(A304);
                    $values .= $query->q($this->dbrec->verlagsort);
                    $pushme = true;
                    break;
                case BUCH:  // Buch
                    $fields .= $query->qn(A322) . ",";
                    $values .= $query->q($this->dbrec->seitenzahlen) . ',';
                    $fields .= $query->qn(A306) . ",";
                    $values .= $query->q($herausgeber) . ',';
                    $fields .= $query->qn(A305) . ",";
                    $values .= $query->q($this->dbrec->verlag) . ',';
                    $fields .= $query->qn(A304) . ",";
                    $values .= $query->q($this->dbrec->verlagsort) . ',';
                    $fields .= $query->qn(A127);
                    $values .= $query->q(A_127BUCH);
                    $pushme = true;
                    break;
                case BUCH_HERAUSGABE:  // Herausgeberschaft
                    $fields .= $query->qn(A322) . ",";
                    $values .= $query->q($this->dbrec->seitenzahlen) . ',';
                    $fields .= $query->qn(A306) . ",";
                    $values .= $query->q($herausgeber) . ',';
                    $fields .= $query->qn(A305) . ",";
                    $values .= $query->q($this->dbrec->verlag) . ',';
                    $fields .= $query->qn(A304) . ",";
                    $values .= $query->q($this->dbrec->verlagsort) . ',';
                    if (!stristr($this->dbrec->verlag, "Eigenverlag") === False) {
                        $fields .= $query->qn(A317) . ",";
                        $values .= $query->q(INSTITUT) . ",";
                    }
                    $fields .= $query->qn(A127);
                    $values .= $query->q(A_127HERAUSGEBERSCHAFT);
                    $pushme = true;
                    break;
                case DIPLOMARBEIT:  // Diplomarbeit
                    $fields .= $query->qn(A308) . ",";
                    $values .= $query->q($this->dbrec->seitenzahlen) . ',';
                    $fields .= $query->qn(A323) . ",";
                    $values .= $query->q($this->akatexte->getTextByValue($this->dbrec->arbeitstyp)) . ',';
                    $fields .= $query->qn(A321) . ",";
                    $values .= $query->q($this->dbrec->institut) . ',';
                    $fields .= $query->qn(A127);
                    $values .= $query->q(A_127DIPLOMARBEIT);
                    $pushme = true;
                    break;
                case DISSERTATION:  // Dissertation
                    $fields .= $query->qn(A308) . ",";
                    $values .= $query->q($this->dbrec->seitenzahlen) . ',';
                    $fields .= $query->qn(A321) . ",";
                    $values .= $query->q($this->dbrec->institut) . ',';
                    $fields .= $query->qn(A127);
                    $values .= $query->q(A_127DISSERTATION);
                    $pushme = true;
                    break;
                case HABILITATION:  // Habilitation
                    $fields .= $query->qn(A308) . ",";
                    $values .= $query->q($this->dbrec->seitenzahlen) . ',';
                    $fields .= $query->qn(A321) . ",";
                    $values .= $query->q($this->dbrec->institut) . ',';
                    $fields .= $query->qn(A127);
                    $values .= $query->q(A_127HABILITATION);
                    $pushme = true;
                    break;
            }
            if ($pushme && !in_array($this->dbrec->publikationstyp, $this->converted_types, true)) {
                array_push($this->converted_types, $this->dbrec->publikationstyp);
            }
            if ($fields && $values) {
                $query->insert($db->qn('#__iwf_aka_j_012'))
                    ->columns($fields)->values($values);
                $this->_e((string) $query);
                try {
                    if ($db->setQuery($query)->execute()) {
                        $count++;
                    }
                } catch (Exception $e) {
                    die(var_dump($e));
                }
            }
        }
        if ($count == 0) {
            $this->AddResultText(sprintf("%s%skeine Publikationen%s", NBSP, NBSP, BR));
        } else {
            $this->AddResultText(sprintf("%s%s%s Publikationen konvertiert.%s", NBSP, NBSP, $count, BR));
            $this->tables_to_export[] = '#__iwf_aka_j_012';
        }
    }

    /**
     * @return void 
     * @throws KeyNotFoundException 
     */
    function ConvertVortraege() {
        $items = $this->getSortedPublicationList(VORTRAEGE);
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->delete($db->qn('#__iwf_aka_j_016'));
        $db->setQuery($query)->execute();
        $month_names = explode(',', MONTHS);
        $count = 0;
        if (count($items)) {
            foreach($items as $val) {    
                $query->clear();
                $this->dbrec->set($val->id);
                $autoren = self::convertAutoren($this->dbrec->autor);
                $fields = $query->qn(federfuehrend) . ",";
                $values = $query->q($this->istFederfuehrend($autoren)) . ",";
                $fields .= $query->qn(J016) . ",";
                $values .= $query->q($this->dbrec->id) . ',';
                $fields .= $query->qn(J016A) . ",";
                $values .= $query->q('IWF' . $this->options->jahr . "-" . $this->dbrec->id) . ',';

                $fields .= $query->qn(A164) . ",";
                $values .= $query->q($autoren) . ',';
                $titel = trim(strtr($this->dbrec->titel, "\r\n", " "));
                $t = trim(strtr($titel, "\n", " "));
                $fields .= $query->qn(A163) . ",";
                $values .= $query->q(strtr($t, "\"", "'")) . ',';
                $tdat = explode(" ", $this->dbrec->tagungsdatum);
                if (count($tdat) == 2) {
                    $mon = trim($tdat[0]);
                    $jahr = trim($tdat[1]);
                    $m = array_search($mon, $month_names);
                    $datum = Date("d-m-Y", mktime(0, 0, 0, $m + 1, 15, $jahr));
                    $fields .= $query->qn(A165) . ",";
                    $values .= $query->q($datum) . ',';
                }
                $fields .= $query->qn(A171) . ",";
                $values .= $query->q($this->dbrec->tagungsland) . ',';
                $fields .= $query->qn(A170) . ",";
                $values .= $query->q($this->dbrec->tagungsort) . ',';
                $fields .= $query->qn(A733) . ",";
                $values .= $query->q($this->dbrec->ist_international) . ',';
                $fields .= $query->qn(A734) . ",";
                $values .= $query->q($this->dbrec->veranstaltungtyp) . ',';
                $pushme = false;
                switch ($this->dbrec->publikationstyp) {
                    case VORTRAG_BEI_TAGUNGEN:
                        $fields .= $query->qn(A166) . ",";
                        $titel = trim(strtr($this->dbrec->tagungstitel, "\r\n", " "));
                        $t = trim(strtr($titel, "\n", " "));
                        $values .= $query->q(strtr($t, "\"", "'")) . ',';
                        $fields .= $query->qn(A168) . ",";
                        if ($this->dbrec->qualitaetstyp) {
                            $values .= $query->q(A_168VORTRAGP) . ',';
                        } else {
                            $values .= $query->q(A_168VORTRAGW) . ',';
                        }
                        $fields .= $query->qn(A169) . ",";
                        $values .= $query->q(A_169VORTRAG) . ',';
                        if ($this->dbrec->akademis2) {  // Hilfsfeld: derzeit für Keynote-lecture
                            $fields .= $query->qn(A173) . ",";
                            $values .= $query->q(A_173VORTRAGKEYNOTE) . ',';
                        } else {
                            if ($this->dbrec->eingeladen == 1) {
                                $fields .= $query->qn(A173);
                                $values .= $query->q(A_173VORTRAGINV);
                            } else {
                                $fields .= $query->qn(A173);
                                $values .= $query->q(A_173VORTRAGSONSTIG);
                            }
                        }
                        $pushme = true;
                        break;
                    case POSTER:
                        $fields .= $query->qn(A166) . ",";
                        $titel = trim(strtr($this->dbrec->tagungstitel, "\r\n", " "));
                        $t = trim(strtr($titel, "\n", " "));
                        $values .= $query->q(strtr($t, "\"", "'")) . ',';
                        $fields .= $query->qn(A168) . ",";
                        $values .= $query->q(A_168VORTRAGW) . ',';
                        $fields .= $query->qn(A169) . ",";
                        $values .= $query->q(A_169POSTER) . ',';
                        if ($this->dbrec->eingeladen == 1) {
                            $fields .= $query->qn(A173);
                            $values .= $query->q(A_173VORTRAGINV);
                        } else {
                            $fields .= $query->qn(A173);
                            $values .= $query->q(A_173VORTRAGSONSTIG);
                        }
                        $pushme = true;
                        break;
                    case VORTRAG_BEI_INSTITUTIONEN:
                        $fields .= $query->qn(A166) . ",";
                        $titel = trim(strtr($this->dbrec->tagungstitel, "\r\n", " "));
                        $t = trim(strtr($titel, "\n", " "));
                        $values .= $query->q(strtr($t, "\"", "'")) . ',';
                        $fields .= $query->qn(A172) . ",";
                        $values .= $query->q($this->dbrec->institut) . ',';
                        $fields .= $query->qn(A168) . ",";
                        if ($this->dbrec->qualitaetstyp) {
                            $values .= $query->q(A_168VORTRAGP) . ',';
                        } else {
                            $values .= $query->q(A_168VORTRAGW) . ',';
                        }
                        $fields .= $query->qn(A169) . ",";
                        $values .= $query->q(A_169VORTRAG) . ',';
                        $fields .= $query->qn(A173);
                        $values .= $query->q(A_173VORTRAGINV);
                        $pushme = true;
                        break;
                    case VORTRAG_BEI_PROJEKTMEETING:
                        $fields .= $query->qn(A166) . ",";
                        $titel = trim(strtr($this->dbrec->tagungstitel, "\r\n", " "));
                        $t = trim(strtr($titel, "\n", " "));
                        $values .= $query->q(strtr($t, "\"", "'")) . ',';
                        $fields .= $query->qn(A168) . ",";
                        $values .= $query->q(A_168VORTRAGW) . ',';
                        $fields .= $query->qn(A169) . ",";
                        $values .= $query->q(A_169VORTRAG) . ',';
                        $fields .= $query->qn(A173);
                        $values .= $query->q(A_173VORTRAGSONSTIG);
                        $pushme = true;
                        break;
                }
                if ($pushme && !in_array($this->dbrec->publikationstyp, $this->converted_types, true)) {
                    array_push($this->converted_types, $this->dbrec->publikationstyp);
                }
                if ($fields && $values) {
                    $query->insert('#__iwf_aka_j_016')->columns($fields)->values($values);
                    $this->_e((string) $query);
                    if ($db->setQuery($query)->execute()) {
                        $count++;
                    }
                }
            }
        }
        if ($count == 0) {
            $this->AddResultText(sprintf("%s%skeine Vorträge/Poster%s", NBSP, NBSP, BR));
        } else {
            $this->AddResultText(sprintf("%s%s%s Vorträge konvertiert.%s", NBSP, NBSP, $count, BR));
            $this->tables_to_export[] = '#__iwf_aka_j_016';
        }
    }

    /**
     * @return void 
     * @throws KeyNotFoundException 
     */
    function Finalize() {
        if (isset($this->options->erledigt) && count($this->converted_types)) {
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $query = $db->createQuery();
            $query->update($db->qn('#__iwf_literatur'))->set($db->qn('akademis1') . '=1')
                ->whereIn($db->qn('publikationstyp'), $this->converted_types)
                ->andWhere
                (
                    [
                        $db->qn('erscheinungsjahr') . '=:jahr',
                        $db->qn('tagungsdatum') . ' LIKE "%' . $this->options->jahr . '%"'
                    ],
                    'OR'
                )
                ->bind(':jahr', $this->options->jahr, ParameterType::INTEGER);
            if ($db->setQuery($query)->execute()) {
                $this->AddResultText('<p>Die gew&auml;hlten Datens&auml;tze wurden als erledigt gekennzeichnet und werden bei k&uuml;nftigen Konvertierungen nicht mehr ber&uuml;cksichtigt.</p>');
            } else {
                $this->AddResultText('<p>Erledigt-Flag konnte nicht gesetzt werden.</p>');
            }
        }
        if ($this->HasRecords) {
            $this->finalizeTables();
            $this->AddResultText('<p>' . NBSP . NBSP . BR . 'Folgende Tabellen wurden in das Verzeichnis <b>' . $this->export_path . '</b> exportiert:</p>');
            foreach ($this->tables_to_export as $table) {
                $this->dumpTable($table);
            }
        }
    }

    /**
     * @param mixed $table 
     * @return void 
     * @throws KeyNotFoundException 
     */
    function dumpTable($table) {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery();
        $query->select('*')->from($db->qn($table));
        $db->setQuery($query);
        $rows = $db->setQuery($query)->loadObjectList();
        if (count($rows)) {
            $path = $this->export_path;
            if (!file_exists($path)) {
                if (!mkdir($path)) {
                    $this->AddResultText("Error creating directory $path");
                    return;
                }
            }
            $path .= DIRECTORY_SEPARATOR . str_replace('#__iwf_', '', $table) . '.xml';

            $xmlWriter = new XMLWriter();
            $xmlWriter->openUri($path);
            $xmlWriter->setIndent(true);
            $xmlWriter->setIndentString('    ');
            if ($xmlWriter) {
                $xmlWriter->startDocument('1.0', 'UTF-8');
                $xmlWriter->startElement('ROOT');
                $xmlWriter->writeAttribute('xmlns:xsi', "http://www.w3.org/2001/XMLSchema-instance");
                foreach($rows as $row) {
                    $xmlWriter->startElement('row');
                    foreach ($row as $tag => $val) {
                        $xmlWriter->startElement('field');
                        $xmlWriter->writeAttribute('name', $tag);
                        $xmlWriter->text($val);
                        $xmlWriter->endElement();
                    }
                    $xmlWriter->endElement();
                }
                $xmlWriter->endElement();
                $xmlWriter->endDocument();
                $this->AddResultText(sprintf("%s%s%s%s", NBSP, NBSP, str_replace('#__iwf_', '', $table), BR));
            }
        }
    }

}
