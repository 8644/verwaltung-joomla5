<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

define("INSTITUT", "IWF");
define("LITERATUR_DATENBANK", "literatur");
define("INSTITUTS_ID", "11400");
define("CONVERTABLE_TYPES", "1,2,3,4,5,6,7,8,9,10,12,13,14,17");
define("PUBLIKATIONEN", "1,2,3,4,5,6,7,8,12,14");
define("VORTRAEGE", "9,10,13,17");
define("MONTHS", "Jan,Feb,Mar,Apr,May,Jun,Jul,Aug,Sep,Oct,Nov,Dec");

define("ZEITSCHRIFT_REFERIERT", 1);
define("ZEITSCHRIFT_NICHT_REFERIERT", 2);
define("IWF_BERICHT", 3);
define("BUCHBEITRAG", 4);
define("BUCH", 5);
define("BUCH_HERAUSGABE", 6);
define("DIPLOMARBEIT", 7);
define("DISSERTATION", 8);
define("VORTRAG_BEI_TAGUNGEN", 9);
define("POSTER", 10);
define("PROPOSAL", 11);
define("BERICHT_ALLGEMEIN", 12);
define("VORTRAG_BEI_INSTITUTIONEN", 13);
define("HABILITATION", 14);
define("ANDERER_VORTRAG", 15);
define("GAST_VORTRAG", 16);
define("VORTRAG_BEI_PROJEKTMEETING", 17);

define("ABTEILUNGSNODES", "11400:iwf,11459:phys,11460:exp,11461:satgeo");

define("REISE_TYP_AUFENTHALT", 134);  // aus Tabelle fzg_listen!
define("REISE_TYP_VERANSTALTUNG", 135);
define("REISE_TYP_NICHT_BESUCHTE_VERANSTALTUNG", 220);

define("BR", "<br />");
define("NBSP", "&nbsp;");

define("J002", "J_002_Organisationseinheit_ID");

define("J008", "J_008_Personen_ID");
define("J010", "J_010_Forschungsprojekt_ID");
define("J012", "J_012_Publikationen_ID");
define("J012A", "J_012_Publikationen_Fremd_ID");
define("J016", "J_016_Vortraege_Poster_ID");
define("J016A", "J_016_Vortraege_Poster_Fremd_ID");
define("A121", "A_121_Original_Titel");
define("A122", "A_122_AutorInnen");
define("A123", "A_123_AutorInnen_2");
define("A125", "A_125_Veroeffentlichungsdatum");
define("A126", "A_126_Annahmedatum");
define("A127", "A_127_DE_Typ_der_Publikation");
define("federfuehrend", "Federfuehrend");

define("A492", "A_492_Ist_Populaerwissenschaftlich");
define("A494", "A_494_Hat_mehr_als_zehntausend_Zeichen");
define("A493", "A_493_Ist_peer_reviewed");

define("A304", "A_304_Ort");
define("A305", "A_305_Verlag");
define("A306", "A_306_Herausgeber");
define("A307", "A_307_Band");
define("A308", "A_308_Seiten");
define("A745", "A_745_DOI");
define("A311", "A_311_Zeitschrift");
define("A313", "A_313_Buchtitel");
define("A317", "A_317_Herausgebende_Forschungseinheit");
define("A318", "A_318_Konferenz");
define("A321", "A_321_Universitaet");
define("A322", "A_322_Seitenanzahl");
define("A323", "A_323_DE_Typ_der_Arbeit");
define("A324", "A_324_Magazin");

define("A163", "A_163_Original_Titel");
define("A164", "A_164_Vortragender");
define("A165", "A_165_Vortragsdatum");
define("A166", "A_166_Veranstaltungsname");
define("A167", "A_167_DE_Veranstaltungstyp");
define("A168", "A_168_DE_Qualitaetstyp");
define("A169", "A_169_DE_Art");
define("A170", "A_170_DE_Veranstaltungsort");
define("A171", "A_171_EN_Veranstaltungsland");
define("A172", "A_172_DE_Veranstalter");
define("A173", "A_173_DE_Typ");

// J-022 Aufenthalt(out)
define("J022", "J_022_Aufenthalt_ID");  // Id des Aufenthaltes aus der Reise-Tabelle
define("J022A", "J_022_Aufenthalt_Fremd_ID");  // Fremd-Id des Aufenthaltes für Relation
define("J003", "J_003_Externe_Einheit_ID");  // Id des externen Einheit aus der Reise-Tabelle
define("A195", "A_195_Aufenthalt_Zweck");
define("A197", "A_197_Aufenthalt_Beginndatum");
define("A198", "A_198_Aufenthalt_Enddatum");
define("A199DE", "A_199_DE_Aufenthalt_Beschreibung");
define("A199EN", "A_199_EN_Aufenthalt_Beschreibung");
// folgendes Feld ist noch einzufügen (Verknüpfung aus unserer Finanzierung zu Akademis-Finanzierung herstellen!
define("A241", "A_241_Aufenthalt_Finanzierung");
define("A507", "A_507_Besuchte_Institution");
define("A508", "A_508_Aufenthalt_Land");
define("EXTERNEEINHEIT", "ExterneEinheit");

// Wahlmöglichkeiten für die Finanzierung
define("A_241EIGENMITTEL", "Eigenmittel der Forschungseinrichtung");
define("A_241SONSTIGE_GELDGEBER", "Sonstige externe Geldgeber oder Privatfinanzierung");
define("A_241STIPENDIUM", "Stipendium");
define("A_241OEAW_STIPENDIUM", "ÖAW-Stipendium");
define("A_241WISSENSCHAFTLERAUSTAUSCH", "ÖAW im Rahmen Wissenschaftleraustauschprogramm");
define("A_241MISCHFINANZIERUNG", "Mischfinanzierung");

// J-007 Veranstaltung
define("J007", "J_007_Veranstaltung_ID"); // Id der Veranstaltung aus der Reise-Tabelle
define("J007A", "J_007_Veranstaltung_Fremd_ID"); // Fremd-Id der Veranstaltung für Relationen
define("A060DE", "A_060_DE_Veranstaltung_Titel");
define("A060EN", "A_060_EN_Veranstaltung_Titel");
define("A061", "A_061_Veranstaltung_Ort");
define("A062", "A_062_Veranstaltung_Beginndatum");
define("A063", "A_063_Veranstaltung_Enddatum");
define("A064DE", "A_064_DE_Veranstaltung_Inhalt_Beschreibung");
define("A064EN", "A_064_EN_Veranstaltung_Inhalt_Beschreibung");
define("A066", "A_066_Veranstaltung_Lehrveranstaltung_Semester");  // erhält den Wert N/A
define("A419", "A_419_Veranstaltung_Lehrveranstaltungs_ID");
define("A067", "A_067_Veranstaltung_Typ");
define("A257", "A_257_Veranstaltung_hat_mindestens_fuenf_Vortragende");
define("A068", "A_068_Veranstaltung_ist_populaerwissenschaftlich");
define("A258", "A_258_Veranstaltung_dauert_mindestens_vier_Stunden");
define("A499", "A_499_Veranstaltung_Reichweite");
define("A502", "A_502_Veranstaltung_Land");
define("TEILNEHMER", "Teilnehmer");  // auch für Aufenthalte
define("FUNKTION", "Funktion");      // auch für Aufenthalte
define("TEILVERANSTALTUNGEN", "Teilveranstaltungen");    // nur bei Veranstaltungen; dieses Feld nimmt die Ids der Teilveranstaltungen auf, aus denen die Veranstaltung zusammengesetzt wurde

define("A_066N_A", "N/A");
define("A_067KONGRESS", "Kongress/Symposium/Konferenz/Tagung");
define("A_067WORKSHOP", "Workshop/Seminar");
define("A_067SPEZIALVORLESUNG", "Spezialvorlesung (keine Lehrveranstaltung, z. B. Antrittsvorlesung)");
define("A_067VORTRAGSABEND", "Vortragsabend/Buchpräsentation");
define("A_067SCIENCEWEEK", "Science Week etc.");
define("A_067PUBLIKUMSABEND", "Publikumsabend");
define("A_067PUBLICLECTURE", "Public Lecture");
define("A_067PODIUMSDISKUSSION", "Podiumsdiskussion");
define("A_067AUSSTELLUNGSORGANISATION", "Ausstellungsorganisation");
define("A_067LEHRGANG", "Lehrgang universitären Charakters");
define("A_067LEHRVERANSTALTUNGUNI", "Lehrveranstaltung an Universität");
define("A_067LEHRVERANSTALTUNGFH", "Lehrveranstaltung an Fachhochschule");

define("E035", "E_035_DE_Autorenschaft");
define("E097", "E_097");
define("E048", "E_048");
define("E208", "E_208_Vortrag2Person");
define("E122", "E_122_Funktion");
define("A_127BUCH", "Buch/Monographie");
define("A_127HERAUSGEBERSCHAFT", "Herausgeberschaft");
define("A_127FACHZEITSCHRIFT", "Beitrag in Fachzeitschrift");
define("A_127SAMMELWERK", "Beitrag in Sammelwerk");
define("A_127FORSCHUNGSBERICHT", "Forschungsbericht");
define("A_127KONFPROC", "Konferenzbeitrag: Publikation in Proceedingsband");
define("A_127KONFPOST", "Konferenzbeitrag: Poster (in Proceedingsband)");
define("A_127HABILITATION", "Habilitationsschrift");
define("A_127DISSERTATION", "Dissertation");
define("A_127DIPLOMARBEIT", "Diplomarbeit / Bakkalaureatsarbeit");

define("A_323DIPLOM", "Diplomarbeit");
define("A_323BAKKALAUREAT", "Bakkalaureatsarbeit");
define("A_323HABIL", "Habilitationsschrift");
define("A_323THESIS", "Master Thesis");

define("A_169VORTRAG", "Vortrag");
define("A_169POSTER", "Posterpräsentation");

define("A_168VORTRAGW", "wissenschaftlich");
define("A_168VORTRAGP", "populärwissenschaftlich");

define("A_167BIGEVENT", "Großveranstaltung (Konferenz, Kongress, Tagung, Symposium etc.)");
define("A_167VORTRAGEVENT", "Vortragsveranstaltung, Teil einer Vortragsserie");

define("A_173VORTRAGKEYNOTE", "Keynote");
define("A_173VORTRAGNAMEDLECTU>RE", "Named Lecture");
define("A_173VORTRAGINV", "Sonstiger eingeladener Veranstaltungsbeitrag");
define("A_173VORTRAGSONSTIG", "Sonstiger Veranstaltungsbeitrag");

define("E106", "E_106_Ist_Federfuehrend");
define("E107", "E_107_Ist_Federfuehrend");

define("A733", "A_733_REICHWEITE");
define("A734", "A_734_EVENTTYP");

define("E_035ALLEINAUTOR", "AlleinautorIn");
define("E_035HAUPTAUTOR", "HauptautorIn");
define("E_035KOAUTOR", "KoautorIn");
define("E_035HERAUSGEBER", "HerausgeberIn");
define("E_035KOHERAUSGEBER", "KoherausgeberIn");

define("E_122ORGANISATOR", "OrganisatorIn");
define("E_122TEILNEHMER", "TeilnehmerIn");
define("E_122REFERENT", "ReferentIn");
define("E_122PODIUMSDISKUTANT", "PodiumsdiskutantIn");
define("E_122POSTERPRAESENTATOR", "PosterpräsentatorIn");

define("E_208KOAUTOR_UND_PRAESENTATOR", "(Ko-)Autor UND Präsentator/Vortragender");
define("E_208KOAUTOR", "(Ko-)AutorIn");

define("J044", "J_044_Fachzeitschrift_Node");
