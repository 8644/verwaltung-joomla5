<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Akademis;

use Iwf\Component\Verwaltung\Administrator\Extension\VText;
use Joomla\CMS\Factory;
use Joomla\Database\DatabaseInterface;
use Joomla\Database\ParameterType;

class lit_record {

    var $id = 0;
    var $einheitoeaw = "";
    var $autor = "";
    var $titel = "";
    var $untertitel = "";
    var $gesamttitel = "";
    var $zeitschrift = "";
    var $seitenzahlen = "";
    var $doi = "";
    var $band = "";
    var $heft = "";
    var $erscheinungsjahr = 0;
    var $isbn = "";
    var $herausgeber = "";
    var $verlag = "";
    var $verlagsort = "";
    var $publikationstyp = 0;
    var $arbeitstyp = "";
    var $status = 0;
    var $beschlagwortung = "";
    var $abstract = "";
    var $sprache = "";
    var $link = "";
    var $titelprojekte = "";
    var $berichtnummer = "";
    var $weltraumagentur = "";
    var $mission = "";
    var $dokumentreferenz = "";
    var $tagungstitel = "";
    var $eintragsdatum = null;
    var $eintragender = "";
    var $host = "";
    var $tagungsmonat = "";
    var $tagungsjahr = "";
    var $tagungsdatum = "";
    var $tagungsort = "";
    var $tagungsland = "";
    var $institut = "";
    var $betreuer = "";
    var $berichttyp = "";
    var $adressat = "";
    var $passwort = "";
    var $gesperrt = 0;
    var $begutachtet = 0;
    var $qualitaetstyp = 0;
    var $eingeladen = 0;
    var $hauptautor_forschungseinheit = 0;
    var $ist_populaerwissenschaftlich = -1;
    var $mehr_als_zehntausend_zeichen = 0;
    var $ist_international = 0;
    var $veranstaltungtyp = "";
    var $akademis1 = 0;
    var $akademis2 = 0;

    function set($id) {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery();
        $query->select('*')
            ->from($db->qn('#__iwf_literatur'))
            ->where($db->qn('id') . '=:id')
            ->bind(':id', $id, ParameterType::INTEGER);
        if ($row = $db->setQuery($query)->loadObject()) {
            $this->id = $row->id;
            $this->autor = $row->autoren;
            $this->einheitoeaw = $row->einheitoeaw;
            $this->titel = $row->titel;
            $this->untertitel = $row->untertitel;
            $this->gesamttitel = $row->gesamttitel;
            $this->zeitschrift = $row->zeitschrift;
            $this->seitenzahlen = $row->seitenzahlen;
            $this->doi = $row->doi;
            $this->band = $row->band;
            $this->heft = $row->heft;
            $this->erscheinungsjahr = $row->erscheinungsjahr;
            $this->isbn = $row->isbn;
            $this->herausgeber = $row->herausgeber;
            $this->verlag = $row->verlag;
            $this->verlagsort = $row->verlagsort;
            $this->publikationstyp = $row->publikationstyp;
            $this->arbeitstyp = $row->arbeitstyp;
            $this->status = $row->status;
            $this->beschlagwortung = $row->beschlagwortung;
            $this->abstract = $row->abstract;
            $this->sprache = $row->sprache;
            $this->link = $row->link;
            //$this->gesperrt = $row->gesperrt;
            $this->titelprojekte = $row->titelprojekte;
            $this->berichtnummer = $row->berichtnummer;
            $this->weltraumagentur = $row->weltraumagentur;
            $this->mission = $row->mission;
            $this->dokumentreferenz = $row->dokumentreferenz;
            $this->tagungstitel = $row->tagungstitel;
            $this->eintragsdatum = $row->modified_time;
            $this->eintragender = $row->modified_by_fzguser_id;
            $tmp = explode(" ", $row->tagungsdatum);
            $mon = "";
            $jahr = "";
            if (!is_null($tmp)) {
                if (count($tmp) == 2) {
                    $mon = $tmp[0];
                    $jahr = $tmp[1];
                    $this->tagungsmonat = $mon;
                    $this->tagungsjahr = $jahr;
                }
            }
            $this->tagungsdatum = trim($mon . " " . $jahr);
            $this->tagungsort = $row->tagungsort;
            $this->tagungsland = $row->tagungsland;
            if (strstr($this->tagungsort, ",") && strlen($this->tagungsland) == 0) {
                $ort_land = list($ort, $land) = explode(",", $row->tagungsort, 2);
                if ($ort_land) { // bei alten Daten steht Land oft neben Ort
                    $this->tagungsort = trim($ort);
                    $this->tagungsland = strtoupper(trim($land));
                }
            }
            $this->institut = $row->institut;
            $this->betreuer = $row->betreuer;
            $this->berichttyp = $row->berichttyp;
            $this->adressat = $row->adressat;
            $this->akademis1 = $row->akademis1;
            $this->akademis2 = $row->akademis2;
            $this->hauptautor_forschungseinheit = $row->hauptautor_forschungseinheit == 1 ? 1 : 0;
            $this->qualitaetstyp = $row->qualitaetstyp == 1 ? 1 : 0;
            $this->eingeladen = $row->eingeladen == 1 ? 1 : 0;
            $this->ist_populaerwissenschaftlich = $row->ist_populaerwissenschaftlich == 1 ? 1 : 0;
            $this->begutachtet = $row->begutachtet == 1 ? 1 : 0;
            $this->mehr_als_zehntausend_zeichen = $row->mehr_als_zehntausend_zeichen == 1 ? 1 : 0;
            $this->ist_international = $row->ist_international;
            $this->veranstaltungtyp = $row->veranstaltungtyp;
        }
    }
}

class lit_texte {

    protected $items = null;

    function __construct() {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select('a.*')
            ->from($db->qn('#__iwf_aka_listen', 'a'))
            ->where($db->qn('a.aktiv') . '<>0');
            $this->items = $db->setQuery($query)->loadObjectList();
            foreach ($this->items as $item) {
                $item->translated = VText::_($item->inhalt, $item->jtext);
            }
    }

    function getTextById($id) {
        if (!is_null($this->items)) {
            foreach ($this->items as $item) {
                if ($item->id == $id) {
                    return $item->translated;
                }
            }
        }
        return null;
    }

    function getTextByValue($value) {
        if (!is_null($this->items)) {
            foreach ($this->items as $item) {
                if ($item->value == $value) {
                    return $item->translated;
                }
            }
        }
        return null;
    }

}
