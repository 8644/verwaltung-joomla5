<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Model\AdminModel;
use Joomla\CMS\Table\Table;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use stdClass;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\Model */
class PrintgastformModel extends AdminModel
{
    
    /**
     * @param string $name 
     * @param string $prefix 
     * @param array $options 
     * @return Table 
     * @throws DatabaseNotFoundException 
     * @throws UnexpectedValueException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function getTable($name = '', $prefix = '', $options = array())
	{
		$name = 'iwf_gastformular';
		$prefix = 'Table';
		if ($table = $this->_createTable($name, $prefix, $options)) {
			return $table;
		}
		throw new \Exception(Text::sprintf('JLIB_APPLICATION_ERROR_TABLE_NAME_NOT_SUPPORTED', $name), 0);
	}

    /**
     * @param array $data 
     * @param bool $loadData 
     * @return Form 
     */
    public function getForm($data = array(), $loadData = true)
    {
        return null;
    }

    /**
     * @return array 
     * @throws Exception 
     * @throws InvalidArgumentException 
     * @throws RuntimeException 
     * @throws UnexpectedValueException 
     */
    protected function loadFormData()
    {
        $data = Factory::getApplication()->getUserState('com_verwaltung.edit.gastform.data', array());
        if (empty($data)) {
            $data = $this->getItem();
			if ($data === false) {
				$data = [];
			}
        }
        return $data;
    }

    /**
     * @param int $pk 
     * @return stdClass|false 
     * @throws Exception 
     * @throws InvalidArgumentException 
     * @throws RuntimeException 
     * @throws UnexpectedValueException 
     */
    public function getItem($pk = null)
    {
        $item = parent::getItem();
        if ($item) {
            $item->name = sprintf('%s %s', $item->nachname, $item->vorname);
            $db = $this->getDatabase();
            $query = $db->createQuery()
                ->select($db->qn('a.inhalt', 'zweck'))
                ->from($db->qn('#__iwf_listen', 'a'))
                ->where($db->qn('a.id') . '=:id')
                ->bind(':id', $item->aufenthaltszweck, ParameterType::INTEGER);
            $item->aufenthaltszweck = $db->setQuery($query)->loadObject()->zweck;
            $query = $db->createQuery()
                ->select($db->qn('a.zimmer', 'zimmer'))
                ->from($db->qn('#__iwf_zimmer', 'a'))
                ->where($db->qn('a.id') . '=:platz')
                ->bind(':platz', $item->arbeitsplatz, ParameterType::INTEGER);
            $item->arbeitsplatz = $db->setQuery($query)->loadObject()->zimmer;
            $query = $db->createQuery()
                ->select($db->qn('a.land', 'nation'))
                ->from($db->qn('#__iwf_nationen', 'a'))
                ->where($db->qn('a.id') . '=:nation')
                ->bind(':nation', $item->nation, ParameterType::INTEGER);
            $item->nation = $db->setQuery($query)->loadObject()->nation;
            $query = $db->createQuery()
                ->select($db->qn('a.land', 'land'))
                ->from($db->qn('#__iwf_nationen', 'a'))
                ->where($db->qn('a.id') . '=:land')
                ->bind(':land', $item->land, ParameterType::INTEGER);
            $item->land = $db->setQuery($query)->loadObject()->land;
            $query = $db->createQuery()
                ->select($db->qn('a.inhalt', 'wohnung'))
                ->from($db->qn('#__iwf_listen', 'a'))
                ->where($db->qn('a.id') . '=:wohnung')
                ->bind(':wohnung', $item->wohnung, ParameterType::INTEGER);
            if ($result = $db->setQuery($query)->loadObject()) {
                $result = $result->wohnung;
            }
            $item->wohnung = Text::_("COM_VERWALTUNG_GASTFORM_KEINE_WOHNUNG");
            if ($result) {
                $item->wohnung = $result;
            }
            $item->computer = Text::_(sprintf('COM_VERWALTUNG_GASTFORM_COMPUTER_OPTION_%d', $item->computer));
            $query = $db->createQuery()
                ->select($db->qn('a.inhalt', 'finanzierung'))
                ->from($db->qn('#__iwf_listen', 'a'))
                ->where($db->qn('a.id') . '=:finanzierung')
                ->bind(':finanzierung', $item->finanzierung, ParameterType::INTEGER);
            $item->finanzierung = $db->setQuery($query)->loadObject()->finanzierung;
            $query = $db->createQuery();
            $query->select('CONCAT(' . $db->qn('a.projekt') . '," (",' . $db->qn('a.projektbez') . ',")") AS ' . $db->qn('projekt'))
                ->from($db->qn('#__iwf_projekte', 'a'))
                ->where($db->qn('a.id') . '=:projekt')
                ->bind(':projekt', $item->finanziert_aus_projekt, ParameterType::INTEGER);
            if ($result = $db->setQuery($query)->loadObject()) {
                $result = $result->projekt;
            }
            $item->finanziert_aus_projekt = 'N/A';
            if ($result) {
                $item->finanziert_aus_projekt = $result;
            }
            $item->braucht_schluessel = ($item->braucht_schluessel == 0) ? "NEIN" : "JA";
            $query = $db->createQuery();
            $query->select($query->concatenate([$db->qn('a.nachname'), $db->qn('a.vorname')], ' ') . ' AS ' . $db->qn('betreuer'))
                ->from($db->qn('#__iwf_mitarbeiter', 'a'))
                ->where($db->qn('a.id') . '=:betreuer')
                ->bind(':betreuer', $item->betreuer_id);
            $result = $db->setQuery($query)->loadObject()->betreuer;
            $item->betreuer = "N/A";
            if ($result) {
                $item->betreuer = $result;
            }
        }
        return $item;
    }
}