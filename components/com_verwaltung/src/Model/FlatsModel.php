<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 * 
 * Cannot declare class Iwf\Component\Verwaltung\Site\Field\VeranstalterinventarField, because the name is already in use"
*file =
*"D:\Verwaltung\Website_neu\components\com_verwaltung\src\Field\VeranstalterField.php"
*line =
*18

 */

namespace Iwf\Component\Verwaltung\Site\Model;

use Joomla\CMS\Factory;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Model */
class FlatsModel extends ListModel
{

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     */
    protected function getListQuery()
    {
        $typ = RESERVIERUNGSTYP_WOHNUNG;
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select
            (
                [
                    $db->qn('a.id', 'id'),
                    $db->qn('a.gast', 'gast2'),
                    $db->qn('l.inhalt', 'wohnung'),
                    $query->concatenate([$db->qn('m.nachname'), $db->qn('m.vorname')], ' ') . ' AS ' . $db->qn('name'),
                    $query->concatenate([$db->qn('g.nachname'), $db->qn('g.vorname')], ' ') . ' AS ' . $db->qn('gastname'),
                    $query->concatenate([$db->qn('ab.institut'), $db->qn('ab.abteilung')], '-') . ' AS ' . $db->qn('abteilung'),
                    $db->qn('a.datum', 'datumvon'),
                    $db->qn('a.datumbis', 'datumbis'),
                    $db->qn('a.vorgemerkt', 'vorgemerkt'),
                    $db->qn('a.created_by', 'owner'),
                    'CASE WHEN ' . $db->qn('a.created_by') . '<>' . Factory::getApplication()->getIdentity()->id . ' THEN 1 ELSE 0 END AS ' . $db->qn('readonly'),
                ]   
            )
            ->from($db->qn('#__iwf_reservierungen', 'a'))   //<-----fzg_reservierungen-----Reservierungen not implementet in verwaltung5 
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('a.ma_id') . ' = ' . $db->qn('m.id'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'g'), $db->qn('a.gast_id_wohnung') . ' = ' . $db->qn('g.id'))
            ->leftJoin($db->qn('#__iwf_abteilungen', 'ab'), $db->qn('ab.id') . ' = ' . $db->qn('m.abteilung'))
            ->leftJoin($db->qn('#__iwf_listen', 'l'), $db->qn('a.text') . ' = ' . $db->qn('l.id'))
            ->where($db->qn('a.reservierungstyp') . ' =:typ')
            ->bind(':typ', $typ, ParameterType::INTEGER);
        $query->order($db->qn('a.datum'));
        $query->order($db->qn('l.inhalt'));
        return $query;
    }
}
