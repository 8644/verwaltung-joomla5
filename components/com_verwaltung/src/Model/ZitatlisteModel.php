<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

defined('_JEXEC') or die;

use Exception;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Form;
use Joomla\CMS\MVC\Model\AdminModel;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;
use RuntimeException;

/** @package Iwf\Component\Verwaltung\Site\Model */
class ZitatlisteModel extends AdminModel
{

    /**
     * @param array $data 
     * @param bool $loadData 
     * @return Form 
     */
    public function getForm($data = array(), $loadData = true)
    {
        return null;
    }

    /**
     * @return object[] 
     * @throws Exception 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     */
    public function getItems()
    {
        $input = Factory::getApplication()->input;
        $publikationstyp = $input->get('publikationstyp');
        $erscheinungsjahr = $input->get('erscheinungsjahr');
        $erstautoren = $input->get('erstautoren', '', 'raw');
        $autoren = $input->get('autoren', '', 'raw');
        $hauptautor_forschungseinheit = $input->get('hauptautor_forschungseinheit', '', 'raw');
        $search = $input->get('search', '', 'raw');
        $typ = 'publikationstyp';
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('a.id'),
                    $db->qn('a.zitat'),
                    $db->qn('l.inhalt', 'typ'),
                    $db->qn('l.jtext'),
                ]
            )
            ->from($db->qn('#__iwf_literatur', 'a'))
            ->leftJoin($db->qn('#__iwf_listen', 'l'), $db->qn('l.value') . '=' . $db->qn('a.publikationstyp'))
            ->where($db->qn('l.kategorie') . '=:tp')
            ->bind(':tp', $typ, ParameterType::STRING);

        if (!empty($publikationstyp)) {
            $query->where($db->qn('a.publikationstyp') . '=:ptyp')
                ->bind(':ptyp', $publikationstyp, ParameterType::INTEGER);
        }
        if (is_numeric($erscheinungsjahr)) {
            $query->where($db->qn('a.erscheinungsjahr') . '=:jahr')
                ->bind(':jahr', $erscheinungsjahr, ParameterType::INTEGER);
        }
        if (!empty($erstautoren)) {
            $autorliste = explode('|', $erstautoren);
            $q = [];
            foreach ($autorliste as $autor) {
                $autor = '%' . trim($autor) . '%';
                $q[] = $db->qn('a.autoren') . ' LIKE "' . $autor . '"';
            }
            $query->andWhere($q, 'OR');
        }
        if (!empty($autoren)) {
            $autorliste = explode('|', $autoren);
            $q = [];
            foreach ($autorliste as $autor) {
                $autor = '%' . trim($autor) . '%';
                $q[] = $db->qn('a.autoren') . ' LIKE "' . $autor . '"';
            }
            $query->andWhere($q, 'OR');
        }
        if (!empty($hauptautor_forschungseinheit)) {
            $query->where($db->qn('a.hauptautor_forschungseinheit') . '=:hautor')
                ->bind(':hautor', $hauptautor_forschungseinheit, ParameterType::INTEGER);
        }
        if (!empty($search)) {
            $search = '%' . trim($search) . '%';
            $where = [
                $db->qn('a.autoren') . " LIKE \"$search\"",
                $db->qn('a.herausgeber') . " LIKE \"$search\"",
                $db->qn('a.titel') . " LIKE \"$search\"",
                $db->qn('a.gesamttitel') . " LIKE \"$search\"",
                $db->qn('a.tagungstitel') . " LIKE \"$search\"",
                $db->qn('a.zeitschrift') . " LIKE \"$search\"",
                $db->qn('a.titelprojekte') . " LIKE \"$search\"",
            ];
            if (is_null($query->where)) {
                $query->where($where, 'OR');
            } else {
                $query->andWhere($where, 'OR');
            }
        }
        $query->order([$db->qn('l.inhalt'), $db->qn('a.autoren'),$db->qn('a.titel')]);
        return $this->_getList($query);
    }
}
