<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

use Exception;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\CMS\Language\Text;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Table\Table;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use UnexpectedValueException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Model */
class ComputersModel extends ListModel
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'abteilung',
                'a.abteilung',
                'zimmer',
                'z.zimmer',
                'name',
                'm.nachname',
                'inventartyp',
                'os',
                'a.os',
                'd.hostname',
                'd.wname',
                'd.ipnummer',
                'd.mac',
                'z.standort',
                'i.inventarnummer',
                'l.inhalt',
                'lizenzen'
            );
        }
        $this->person = Person::getInstance();
        parent::__construct($config);
    }

    /**
     * @param string $ordering 
     * @param string $direction 
     * @return void 
     * @throws Exception 
     */
    protected function populateState($ordering = 'd.hostname', $direction = 'ASC')
    {
        parent::populateState($ordering, $direction);
    }

    /**
     * @param string $name 
     * @param string $prefix 
     * @param array $options 
     * @return Table 
     * @throws DatabaseNotFoundException 
     * @throws UnexpectedValueException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function getTable($name = '', $prefix = '', $options = array())
    {
        $name = 'iwf_rechner';
        $prefix = 'Table';
        if ($table = $this->_createTable($name, $prefix, $options)) {
            return $table;
        }
        throw new \Exception(Text::sprintf('JLIB_APPLICATION_ERROR_TABLE_NAME_NOT_SUPPORTED', $name), 0);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    protected function getListQuery()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('a.id'),
                    $db->qn('a.os'),
                    $db->qn('d.hostname'),
                    $db->qn('d.wname'),
                    $db->qn('d.ipnummer'),
                    $db->qn('d.mac'),
                    $db->qn('d.titel'),
                    $db->qn('i.inventarnummer'),
                    $db->qn('i.id', 'inventar_id'),
                    $db->qn('z.zimmer'),
                    $db->qn('l.inhalt', 'inventartyp'),
                    $db->qn('m.nachname'),
                    'CONCAT(' . $db->qn('m.nachname') . '," ",' . $db->qn('m.vorname') . ') AS name',
                    $db->qn('a.baramundi_sync', 'baramundi'),
                    '(SELECT count(' . $db->qn('rechner_id') . ') FROM ' . $db->qn('#__iwf_lizenzen') . 'WHERE ' . $db->qn('rechner_id') . '=' . $db->qn('a.id') . ') AS lizenzen'
                ]
            )
            ->from($db->qn('#__iwf_rechner', 'a'))
            ->leftJoin($db->qn('#__iwf_dns', 'd'), $db->qn('a.dns_id') . '=' . $db->qn('d.id'))
            ->leftJoin($db->qn('#__iwf_inventar', 'i'), $db->qn('a.inventar_id') . '=' . $db->qn('i.id'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('i.ma_id') . '=' . $db->qn('m.id'))
            ->leftJoin($db->qn('#__iwf_zimmer', 'z'), $db->qn('i.standort') . '=' . $db->qn('z.id'))
            ->leftJoin($db->qn('#__iwf_listen', 'l'), $db->qn('i.inventartyp') . '=' . $db->qn('l.id'))
            ->where('NOT ISNULL(' . $db->qn('d.hostname') . ')');
        //abteilungsfilter
        $abteilung = $this->getState('filter.abteilung');
        if ($abteilung) {
            $query->where($db->qn('m.abteilung') . '=:abteilung')
                ->bind(':abteilung', $abteilung, ParameterType::INTEGER);
        } else {
            if ($this->person->abteilung_filter) {
                $query->where(sprintf($this->person->abteilung_filter, $db->qn('m.abteilung')));
            }
        }

        //Zimmer
        $zimmer = $this->getState('filter.zimmer');
        if (!empty($zimmer)) {
            $query->where($db->qn('i.standort') . '=:zimmer')
                ->bind(':zimmer', $zimmer, ParameterType::INTEGER);
        }
        //Inventartyp
        $inventartyp = $this->getState('filter.inventartyp');
        if (!empty($inventartyp)) {
            $query->where($db->qn('l.id') . '=:inventartyp')
                ->bind(':inventartyp', $inventartyp, ParameterType::INTEGER);
        }
        //Betriebssystem
        $os = $this->getState('filter.os');
        if (!empty($os)) {
            $query->where($db->qn('a.os') . '=:os')
                ->bind(':os', $os, ParameterType::STRING);
        }
        //Besitzer
        $name = $this->getState('filter.name');
        if (empty($name) && !Extensions::isAllowed('manage.hw')) {
            $name = $this->person->ma_id;
        }
        if ($name) {
            $query->where($db->qn('m.id') . '=:id')
                ->bind(':id', $name, ParameterType::INTEGER);
        }
        //Suche
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = '%' . trim($search) . '%';
            $where = [
                $db->qn('d.ipnummer') . " LIKE \"$search\"",
                $db->qn('d.wname') . " LIKE \"$search\"",
                $db->qn('a.einsatzbereich') . " LIKE \"$search\"",
                $db->qn('m.nachname') . " LIKE \"$search\"",
                $db->qn('z.zimmer') . " LIKE \"$search\"",
                $db->qn('l.inhalt') . " LIKE \"$search\"",
                $db->qn('a.takt') . " LIKE \"$search\"",
                $db->qn('i.inventarnummer') . " LIKE \"$search\""
            ];
            if (is_null($query->where)) {
                $query->where($where, 'OR');
            } else {
                $query->andWhere($where, 'OR');
            }
        }
        $ordering = $this->getState('list.ordering', 'd.hostname');
        $direction = $this->getState('list.direction', 'ASC');
        $query->order($ordering . ' ' . $direction);
        return $query;
    }
}
