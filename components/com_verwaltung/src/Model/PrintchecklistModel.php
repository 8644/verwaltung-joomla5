<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Model\AdminModel;
use Joomla\CMS\Table\Table;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use stdClass;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\Model */
class PrintchecklistModel extends AdminModel
{
    
    /**
     * @param string $name 
     * @param string $prefix 
     * @param array $options 
     * @return Table 
     * @throws DatabaseNotFoundException 
     * @throws UnexpectedValueException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function getTable($name = '', $prefix = '', $options = array())
	{
		$name = 'iwf_gastchecklisten';
		$prefix = 'Table';
		if ($table = $this->_createTable($name, $prefix, $options)) {
			return $table;
		}
		throw new \Exception(Text::sprintf('JLIB_APPLICATION_ERROR_TABLE_NAME_NOT_SUPPORTED', $name), 0);
	}

    /**
     * @param array $data 
     * @param bool $loadData 
     * @return Form 
     */
    public function getForm($data = array(), $loadData = true)
    {
		$form = $this->loadForm('com_verwaltung.checklist', 'checklist', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		return $form;
    }

    /**
     * @return array 
     * @throws Exception 
     * @throws InvalidArgumentException 
     * @throws RuntimeException 
     * @throws UnexpectedValueException 
     */
    protected function loadFormData()
    {
        $data = Factory::getApplication()->getUserState('com_verwaltung.edit.checklist.data', array());
        if (empty($data)) {
            $data = $this->getItem();
			if ($data === false) {
				$data = [];
			}
        }
        return $data;
    }

    /**
     * @param int $pk 
     * @return stdClass|false 
     * @throws Exception 
     * @throws InvalidArgumentException 
     * @throws RuntimeException 
     * @throws UnexpectedValueException 
     */
    public function getItem($pk = null)
    {
        $item = parent::getItem();
        if ($item) {
            $db = $this->getDatabase();
            $query = $db->createQuery();
            $query->select
                    (
                        [
                            $query->concatenate([$db->qn('m.nachname'), $db->qn('m.vorname')], ' ') . ' AS name_gast',
                            $query->concatenate([$db->qn('v.nachname'), $db->qn('v.vorname')], ' ') . ' AS name_verantwortlicher',
                            'a.*'
                        ]
                    
                    )
                ->from($db->qn('#__iwf_gastchecklisten', 'a'))
                ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('m.id') . '=' . $db->qn('a.ma_id'))
                ->leftJoin($db->qn('#__iwf_mitarbeiter', 'v'), $db->qn('v.id') . '=' . $db->qn('m.gastverantwortlicher'))
                ->where($db->qn('a.id') . '=:id')
                ->bind(':id', $item->id, ParameterType::INTEGER);
            $items = $db->setQuery($query)->loadObjectList();
            return $items;
        }
        return null;
    }
}