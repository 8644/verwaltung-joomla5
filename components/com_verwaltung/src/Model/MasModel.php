<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

use Exception;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\CMS\Language\Text;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Table\Table;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use UnexpectedValueException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Model */
class MasModel extends ListModel
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'abteilung',
                'a.abteilung',
                'dienstverhaeltnis',
                'zimmer',
                'z.zimmer',
                'state',
                'abgelaufen',
                'm.nachname',
                'm.tel',
            );
        }
        parent::__construct($config);
        $this->person = Person::getInstance();
    }

    /**
     * @param string $ordering 
     * @param string $direction 
     * @return void 
     * @throws Exception 
     */
    protected function populateState($ordering = 'm.nachname', $direction = 'ASC')
    {
        $app = Factory::getApplication();
        $published = $this->getUserStateFromRequest($this->context . '.filter.state', 'filter_state', 1, 'none', false);
        $app->setUserState($this->context . '.filter.state', $published);
        $app->setUserState($this->context . '.filter.abteilung', isset($this->person->profil->abteilung_filter) ? $this->person->abteilung_id : null);
        parent::populateState($ordering, $direction);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    protected function getListQuery()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select(
                [
                    $db->qn('m.id'),
                    $db->qn('m.dienstverhaeltnis', 'dv'),
                    $db->qn('m.vorname'),
                    $db->qn('m.nachname'),
                    'CONCAT(' . $db->qn('m.nachname') . ',' . '" "' . ',' . $db->qn('m.vorname') . ') AS ' . $db->qn('name'),
                    $db->qn('m.kfz'),
                    $db->qn('m.tel'),
                    $db->qn('m.state'),
                    $db->qn('m.deleted'),
                    $db->qn('m.kopieradressbuch'),
                    $db->qn('m.swb_unterzeichnet', 'swb'),
                    $db->qn('m.bsv_unterzeichnet', 'bsv'),
                    $db->qn('m.vertragsende'),
                    $db->qn('z.zimmer'),
                    $db->qn('a.abteilung'),
                    $db->qn('l.inhalt', 'dienstverhaeltnis'),
                    $db->qn('g.id', 'checkliste'),
                    'CASE WHEN ((NOT ' . $db->qn('m.vertragsende') . ' IS NULL OR ' . $db->qn('m.vertragsende') . ' <> 0) AND '
                        . $db->qn('m.vertragsende') . ' < NOW()) THEN true ELSE false END AS '
                        . $db->qn('vertrag_abgelaufen'),
                    '(SELECT COUNT(' . $db->qn('s.schluessel') . ') FROM '
                        . $db->qn('#__iwf_schluessel', 's') . ' WHERE '
                        . $db->qn('s.ma_id') . ' = ' . $db->qn('m.id') . ') AS ' . $db->qn('haskey')
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter', 'm'))
            ->leftJoin($db->qn('#__iwf_zimmer', 'z'), $db->qn('z.id') . ' = ' . $db->qn('m.zimmer'))
            ->leftJoin($db->qn('#__iwf_abteilungen', 'a'), $db->qn('a.id') . ' = ' . $db->qn('m.abteilung'))
            ->leftJoin($db->qn('#__iwf_listen', 'l'), $db->qn('l.id') . ' = ' . $db->qn('m.dienstverhaeltnis'))
            ->leftJoin($db->qn('#__iwf_gastchecklisten', 'g'), $db->qn('g.ma_id') . ' = ' . $db->qn('m.id'));
        if (!Extensions::isAllowed('edit.ma') && !Extensions::isAllowed('manage.schluessel')) {
            $query->where($db->qn('m.state') . ' = 1');
        } else {
            $published = $this->getState('filter.state');
            if (is_numeric($published)) {
                $query->where($db->qn('m.state') . '=:pub')
                    ->bind(':pub', $published, ParameterType::INTEGER);
            }
        }
        // EDV ausblenden
        if (!Extensions::isAllowed('manage.sw') && !Extensions::isAllowed('manage.hw')) {
            $query->where($db->qn('m.nachname') . ' NOT LIKE "EDV%"');
        }
        // "gelöschte" Benutzer ausblenden
        if (!Extensions::isAllowed('manage.abteilung')) {
            $query->where($db->qn('m.deleted') . '=0');
        }
        $zimmer = $this->getState('filter.zimmer');
        if (!empty($zimmer)) {
            $query->where($db->qn('m.zimmer') . '=:zimmer')
                ->bind(':zimmer', $zimmer, ParameterType::INTEGER);
        }
        if (empty($zimmer)) {
            $abteilung = $this->getState('filter.abteilung');
            if (!empty($abteilung)) {
                $query->where($db->qn('m.abteilung') . '=:abteilung')
                    ->bind(':abteilung', $abteilung, ParameterType::INTEGER);
            }
        } else {
            $this->setState('filter.abteilung', '');
        }
        $dienstverhaeltnis = $this->getState('filter.dienstverhaeltnis');
        if (!empty($dienstverhaeltnis)) {
            $query->where($db->qn('m.dienstverhaeltnis') . '=:dv')
                ->bind(':dv', $dienstverhaeltnis, ParameterType::INTEGER);
        }
        $eintritt = $this->getState('filter.eintritt');
        if (!empty($eintritt)) {
            $query->where('YEAR(' . $db->qn('m.eintritt') . ') >= :eintritt')
                ->bind(':eintritt', $eintritt, ParameterType::INTEGER);
        }
        $vertrag_abgelaufen = $this->getState('filter.abgelaufen');
        if (is_numeric($vertrag_abgelaufen)) {
            switch ($vertrag_abgelaufen) {
                case 0:
                    $query->where('NOT ' . $db->qn('m.vertragsende') . '< NOW()', 'AND');
                    break;
                case 1:
                    if ($query->where == null) {
                        $query->where(
                                [
                                    $db->qn('m.vertragsende') . '< NOW()',
                                    $db->qn('m.vertragsende') . ' IS NULL'
                                ],
                                'OR'
                            );
                    } else {
                        $query->extendWhere(
                                'AND',
                                [
                                    $db->qn('m.vertragsende') . '< NOW()',
                                    $db->qn('m.vertragsende') . ' IS NULL'
                                ],
                                'OR'
                            );
                    }
                    $query->extendWhere('AND', $db->qn('m.dienstverhaeltnis') . ' IN (' . ECHTE_DIENSTVERHAELTNISSE . ')');
                    break;
            }
        }
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = trim($search) . '%';
            $where = [
                $db->qn('m.vorname') . " LIKE \"$search\"",
                $db->qn('m.nachname') . " LIKE \"$search\""
            ];
            if (is_null($query->where)) {
                $query->where($where, 'OR');
            } else {
                $query->andWhere($where, 'OR');
            }
        }
        $orderCol  = $this->getState('list.ordering', 'm.nachname');
        $orderDirn = $this->getState('list.direction', 'ASC');
        $ordering = $db->escape($orderCol) . ' ' . $db->escape($orderDirn);
        $query->order($ordering);
        return $query;
    }

    /**
     * @param string $name 
     * @param string $prefix 
     * @param array $options 
     * @return Table 
     * @throws DatabaseNotFoundException 
     * @throws UnexpectedValueException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function getTable($name = '', $prefix = '', $options = array())
    {
        $name = 'iwf_mitarbeiter';
        $prefix = 'Table';
        if ($table = $this->_createTable($name, $prefix, $options)) {
            return $table;
        }
        throw new \Exception(Text::sprintf('JLIB_APPLICATION_ERROR_TABLE_NAME_NOT_SUPPORTED', $name), 0);
    }

    /**
     * @param array $data 
     * @param bool $loadData 
     * @return Form|null 
     */
    public function getFilterForm($data = [], $loadData = true)
    {
        if (empty($this->filterFormName)) {
            $classNameParts = explode('Model', \get_called_class());

            if (\count($classNameParts) >= 2) {
                $this->filterFormName = 'filter_' . str_replace('\\', '', strtolower($classNameParts[1]));
            }
        }
        if (empty($this->filterFormName)) {
            return null;
        }
        try {
            $filterForm = $this->loadForm($this->context . '.filter', $this->filterFormName, ['control' => '', 'load_data' => $loadData]);
            if (!Extensions::isAllowed('edit.ma')) {
                $fields = $filterForm->getGroup('filter');
                foreach ($fields as $field) {
                    if ($filterForm->getFieldAttribute($field->fieldname, 'hide', false, 'filter') == 'true') {
                        $filterForm->removeField($field->fieldname, 'filter');
                    }
                }
            }
            return $filterForm;
        } catch (\RuntimeException $e) {}
        return null;
    }

}
