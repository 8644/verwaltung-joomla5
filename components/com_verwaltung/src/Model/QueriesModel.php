<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

use Exception;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Model */
class QueriesModel extends ListModel
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'a.beschreibung',
                'beschreibung',
                'a.query',
                'query',
            );
        }
        parent::__construct($config);
    }

    /**
     * @param string $ordering 
     * @param string $direction 
     * @return void 
     * @throws Exception 
     */
    protected function populateState($ordering = 'a.beschreibung', $direction = 'ASC')
    {
        parent::populateState($ordering, $direction);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     */
    protected function getListQuery()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        if (Extensions::isAllowed(['manage.hw',',manage.sw'], 'AND')) {
            $query->select
                (
                    [
                        $db->qn('a.id', 'id'),
                        $db->qn('a.beschreibung', 'beschreibung'),
                        $db->qn('a.query', 'query'),
                    ]
                )
                ->from($db->qn('#__iwf_queries', 'a'))
                ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('a.ma_id') . ' = ' . $db->qn('m.id'));
            $search = $this->getState('filter.search');
            if (!empty($search)) {
                $search = '%' . trim($search) . '%';
                $where = [
                    $db->qn('a.query') . " LIKE \"$search\"",
                    $db->qn('a.beschreibung') . " LIKE \"$search\"",
                ];
                if (is_null($query->where)) {
                    $query->where($where, 'OR');
                } else {
                    $query->andWhere($where, 'OR');
                }
            }
            $ordering = $this->getState('list.ordering', 'a.beschreibung');
            $direction = $this->getState('list.direction', 'ASC');
            $query->order($ordering . ' ' . $direction);
        }
        return $query;
    }
}
