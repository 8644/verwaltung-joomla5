<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Site\Helper\FormHelper;
use Joomla\CMS\MVC\Model\AdminModel;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Table\Table;
use Joomla\Database\Exception\DatabaseNotFoundException;
use UnexpectedValueException;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;

/** @package Iwf\Component\Verwaltung\Site\Model */
class PublicationModel extends AdminModel
{

	/**
	 * @param string $name 
	 * @param string $prefix 
	 * @param array $options 
	 * @return Table 
	 * @throws DatabaseNotFoundException 
	 * @throws UnexpectedValueException 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 */
	public function getTable($name = '', $prefix = '', $options = [])
	{
		$name = 'iwf_literatur';
		$prefix = 'Table';
		if ($table = $this->_createTable($name, $prefix, $options)) {
			return $table;
		}
		throw new \Exception(Text::sprintf('JLIB_APPLICATION_ERROR_TABLE_NAME_NOT_SUPPORTED', $name), 0);
	}

	/**
	 * @param array $data 
	 * @param bool $loadData 
	 * @return Form 
	 * @throws Exception 
	 * @throws InvalidArgumentException 
	 * @throws RuntimeException 
	 * @throws UnexpectedValueException 
	 */
	public function getForm($data = [], $loadData = true)
	{
		$this->isNew = false;
		$app = Factory::getApplication();
		$recordId = $app->input->getInt('id');
		if (!$recordId || $recordId < 5) {
			$this->isNew = true;
		}
		if (empty($data)) {
			$data = $this->loadFormData();
		}
		$data = (object)$data;
		if (isset($data->publikationstyp)) {
			$f = FormHelper::getPublicationForm($data->publikationstyp);
		} else {
			$f = FormHelper::getPublicationForm(0);
			$loadData = false;
		}
		$form = $this->loadForm('com_verwaltung.publication', $f, array('control' => 'jform', 'load_data' => $loadData));
		return $form;
	}

	/**
	 * @return array 
	 * @throws Exception 
	 * @throws InvalidArgumentException 
	 * @throws RuntimeException 
	 * @throws UnexpectedValueException 
	 */
	protected function loadFormData()
	{
		$data = Factory::getApplication()->getUserState('com_verwaltung.edit.publication.data', []);
		if (empty($data) && $this->isNew) {
			$data = Factory::getApplication()->getUserState('com_verwaltung.edit.publication.newdata', []);
		}
		if (empty($data)) {
			$data = $this->getItem();
			if ($data === false) {
				$data = [];
			}
		}
		return $data;
	}
}
