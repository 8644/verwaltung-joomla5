<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

defined('_JEXEC') or die;

use Exception;
use Joomla\CMS\MVC\Model\AdminModel;
use Joomla\CMS\Form\Form;

/** @package Iwf\Component\Verwaltung\Site\Model */
class AkademisModel extends AdminModel
{

    /**
     * @param array $data 
     * @param bool $loadData 
     * @return Form 
     * @throws Exception 
     */
    public function getForm($data = array(), $loadData = true)
	{
		$form = $this->loadForm('com_verwaltung.akademis', 'akademis', array('control' => 'jform', 'load_data' => $loadData));
        if (empty($form)) {
            return false;
        }
		return $form;
	}

	/** @return array  */
	protected function loadFormData()
	{
		return [];
	}
}
