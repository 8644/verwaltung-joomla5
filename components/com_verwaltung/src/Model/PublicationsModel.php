<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

use Exception;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Iwf\Component\Verwaltung\Site\Akademis\akademis;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\Database\ParameterType;
use  Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Model */
class PublicationsModel extends ListModel
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'publikationstyp',
                'a.publikationstyp',
                'erscheinungsjahr',
                'a.erscheinungsjahr',
                'erstautoren',
                'autoren',
                'hauptautor_forschungseinheit',
            );
        }
        $this->person = Person::getInstance();
        parent::__construct($config);
    }

    /**
     * @param string $ordering 
     * @param string $direction 
     * @return void 
     * @throws Exception 
     */
    protected function populateState($ordering = 'a.erscheinungsjahr', $direction = 'DESC')
    {
        $app = Factory::getApplication();
        $orderCol = $app->input->get('filter_order', $ordering);
        if (!in_array($orderCol, $this->filter_fields)) {
            $orderCol = 'a.erscheinungsjahr';
        }
        $this->setState('list.ordering', $orderCol);
        $listOrder = $app->input->get('filter_order_Dir', 'DESC');
        if (!in_array(strtoupper($listOrder), array('ASC', 'DESC', ''))) {
            $listOrder = 'DESC';
        }
        $this->setState('list.direction', $listOrder);
        parent::populateState($orderCol, $listOrder);
    }

    protected function getListQuery()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('a.id', 'id'),
                    $db->qn('a.titel', 'titel'),
                    $db->qn('a.zitat', 'zitat'),
                    $db->qn('hauptautor_forschungseinheit'),
                    $db->qn('a.erscheinungsjahr'),
                    'CASE WHEN INSTR( ' . $db->qn('autoren') . ', ".,") THEN SUBSTR(' . $db->qn('autoren') . ',1,INSTR(' . $db->qn('autoren') . ', ".,")) ELSE ' . $db->qn('autoren') . ' END AS ' . $db->qn('erstautor'),
                    $db->qn('l.inhalt', 'publikationtyp'),
                    $db->qn('l.jtext', 'jtext'),
                ]
            )
            ->from($db->qn('#__iwf_literatur', 'a'))
            ->leftJoin($db->qn('#__iwf_listen', 'l'), $db->qn('l.value') . '=' . $db->qn('a.publikationstyp'))
            ->where($db->qn('l.kategorie') . '=' . '"publikationstyp"');
        $erscheinungsjahr = $this->getState('filter.erscheinungsjahr');
        if (is_numeric($erscheinungsjahr)) {
            $query->where($db->qn('a.erscheinungsjahr') . '=:erscheinungsjahr')
                ->bind(':erscheinungsjahr', $erscheinungsjahr, ParameterType::INTEGER);
        }
        $publicationstyp = $this->getState('filter.publikationstyp');
        if (is_numeric($publicationstyp)) {
            $query->where($db->qn('a.publikationstyp') . '=:publikationstyp')
                ->bind(':publikationstyp', $publicationstyp, ParameterType::INTEGER);
        }
        $erstautoren = $this->getState('filter.erstautoren');
        if (!empty($erstautoren)) {
            $autorliste = explode('|', $erstautoren);
            $q = [];
            foreach ($autorliste as $autor) {
                $autor = '%' . trim($autor) . '%';
                $q[] = $db->qn('a.autoren') . ' LIKE "' . $autor . '"';
            }
            $query->andWhere($q, 'OR');
        }
        $autoren = $this->getState('filter.autoren');
        if (!empty($autoren)) {
            $autorliste = explode('|', $autoren);
            $q = [];
            foreach ($autorliste as $autor) {
                $autor = '%' . trim($autor) . '%';
                $q[] = $db->qn('a.autoren') . ' LIKE "' . $autor . '"';
            }
            $query->andWhere($q, 'OR');
        }
        $hauptautor = $this->getState('filter.hauptautor_forschungseinheit');
        if ((int)$hauptautor == 1) {
            $query->where($db->qn('a.hauptautor_forschungseinheit') . '=:hauptautor')
                ->bind(':hauptautor', $hauptautor, ParameterType::STRING);
        }
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = '%' . trim($search) . '%';
            $where = [
                $db->qn('a.autoren') . " LIKE \"$search\"",
                $db->qn('a.herausgeber') . " LIKE \"$search\"",
                $db->qn('a.titel') . " LIKE \"$search\"",
                $db->qn('a.gesamttitel') . " LIKE \"$search\"",
                $db->qn('a.tagungstitel') . " LIKE \"$search\"",
                $db->qn('a.zeitschrift') . " LIKE \"$search\"",
                $db->qn('a.titelprojekte') . " LIKE \"$search\"",
                $db->qn('a.id') . " LIKE \"$search\"",
            ];
            if (is_null($query->where)) {
                $query->where($where, 'OR');
            } else {
                $query->andWhere($where, 'OR');
            }
        }
        $ordering = $this->getState('list.ordering', 'a.erscheinungsjahr');
        $direction = $this->getState('list.direction', 'DESC');
        $query->order($db->qn($ordering) . ' ' . $direction);
        return $query;
    }

    public function getAkademis() {
        // welche Optionen wurden gewählt? jahr, publikationen, vortraege und/oder veranstaltungen?
        $input = Factory::getApplication()->input;
        $options = json_decode(base64_decode($input->get('d')));  // ausgewählte Optionen des Akademis-Formulars
        // Zugriff auf die Felder von $options mit $options->jahr usw.
        // hier Akademis-Konvertierung
        require_once(JPATH_SITE . '/components/com_verwaltung/src/Akademis/akademis.php');
        $akademis = new akademis($options);
        if ($akademis->HasRecords) {
            $akademis->AddResultText('<h2>' . sprintf(Text::_('COM_VERWALTUNG_AKADEMIS_KONVERTIERUNG_TITEL'), $options->jahr) . '</h2><p>&nbsp;</p>');
            // Hier die Konvertierungsroutinen aufrufen:
            if (isset($options->publikationen) || isset($options->vortraege)) {
                if (isset($options->publikationen)) {
                    $akademis->AddResultText('<h3>Publikationen</h3>');
                    $akademis->ConvertPublikationen();
                    $akademis->CreateRelationsR_025();
                    $akademis->CreateRelationsR_029();
                    $akademis->CreateRelationsR_083();
                    $akademis->CreateRelationsR_017();
                }
                if (isset($options->vortraege)) {
                    $akademis->AddResultText('<h3>Vorträge/Poster</h3>');
                    $akademis->ConvertVortraege();
                    $akademis->CreateRelationsR_030();
                    $akademis->CreateRelationsR_031();
                    $akademis->CreateRelationsR_032();
                }
            } else {
                $akademis->AddResultText('Keine Konvertierung gewählt!');
            }
            $akademis->Finalize();
        }
        return $akademis->GetResult();
    }

}
    // public function getPublikationstypen()
    // {
    //     $db = $this->getDatabase();
    //     $query = $db->createQuery()
    //         ->select(
    //             [
    //                 $db->qn('a.value', 'value'),
    //                 $db->qn('a.inhalt', 'inhalt'),
    //                 $db->qn('a.jtext', 'jtext'),
    //             ]
    //         )
    //         ->from('#__iwf_listen as a')
    //         ->where($db->qn('a.katekorie') . '=' . '"anbote"')
    //         ->order(
    //             [
    //                 $db->qn('a.reihenfolge'),
    //                 $db->qn('a.inhalt'),
    //             ]
    //         );
    //     $db->setQuery((string) $query);
    //     $options = [];
    //     if ($db->execute()) {
    //         $items = $db->loadObjectList();
    //         if ($items) {
    //             foreach ($items as $item) {
    //                 if (!empty($item->nachname)) {
    //                     $options[] = HTMLHelper::_('select.option', $item->value, Text::_($item->inhalt, $item->jtext));
    //                     // $options[] = JHtml::_('select.option', $item->value, VText::_($item->inhalt, $item->jtext)); VText
    //                 }
    //             }
    //         }
    //     }
    //     return array_merge(parent::getOptions(), $options);
    // }

    // public function getAkademis() {
    //     // welche Optionen wurden gewählt? jahr, publikationen, vortraege und/oder veranstaltungen?
    //     $input = Factory::getApplication()->input;
    //     $options = json_decode(base64_decode($input->get('d')));  // ausgewählte Optionen des Akademis-Formulars
        

    //     require_once(JPATH_SITE . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'com_verwaltung' . DIRECTORY_SEPARATOR . 'helpers' . DIRECTORY_SEPARATOR . 'akademis.php');
    //    $akademis = new akademis($options);
    //     if ($akademis->HasRecords) {
    //         $akademis->AddResultText('<h2>' . sprintf(Text::_('COM_VERWALTUNG_AKADEMIS_KONVERTIERUNG_TITEL'), $options->jahr) . '</h2><p>&nbsp;</p>');
            
    //         if (isset($options->publikationen) || isset($options->vortraege)) {
    //             if (isset($options->publikationen)) {
    //                 $akademis->AddResultText('<h2>Publikationen</h2>');
    //                 $akademis->ConvertPublikationen();
    //                 $akademis->CreateRelationsR_025();
    //                 $akademis->CreateRelationsR_029();
    //                 $akademis->CreateRelationsR_083();
    //                 $akademis->CreateRelationsR_017();
    //             }
    //             if (isset($options->vortraege)) {
    //                 $akademis->AddResultText('<h2>Vorträge/Poster</h2>');
    //                 $akademis->ConvertVortraege();
    //                 $akademis->CreateRelationsR_030();
    //                 $akademis->CreateRelationsR_031();
    //                 $akademis->CreateRelationsR_032();
    //             }
    //         } else {
    //             $akademis->AddResultText('Keine Konvertierung gewählt!');
    //         }
    //         $akademis->Finalize();
    //     }
    //     return $akademis->GetResult();
    // }

