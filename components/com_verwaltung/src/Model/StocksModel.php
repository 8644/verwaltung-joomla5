<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

use Exception;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\CMS\Table\Table;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Model */
class StocksModel extends ListModel
{
    
    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'verantwortlicher',
                'a.verantwortlicher',
                'beschreibung',
                'a.beschreibung',
                'inventarnummer',
                'a.inventarnummer',
                'inventartyp',
                'a.inventartyp',
                'anlagejahr',
                'a.anlagejahr',
                'firma',
                'a.firma',
                'zimmer',
                'z.zimmer',
                'b.bestellnummer',
                'd.hostname',
                'letztkontrolle',
                'a.letztkontrolle'
            );
        }
        parent::__construct($config);
    }

    /**
     * @param string $ordering 
     * @param string $direction 
     * @return void 
     * @throws Exception 
     */
    protected function populateState($ordering = 'a.created_time', $direction = 'DESC')
    {
        parent::populateState($ordering, $direction);
    }

    /**
     * @param string $name 
     * @param string $prefix 
     * @param array $options 
     * @return Table 
     * @throws DatabaseNotFoundException 
     * @throws UnexpectedValueException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function getTable($name = '', $prefix = '', $options = array())
    {
        $name = 'iwf_inventar';
        $prefix = 'Table';
        if ($table = $this->_createTable($name, $prefix, $options)) {
            return $table;
        }
        throw new \Exception(Text::sprintf('JLIB_APPLICATION_ERROR_TABLE_NAME_NOT_SUPPORTED', $name), 0);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     */
    protected function getListQuery()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('a.id'),
                    $db->qn('a.deinventarisiert'),
                    $db->qn('a.deinventarisierungsdatum'),
                    $db->qn('a.inventarnummer'),
                    $db->qn('a.created_time'),
                    $db->qn('a.beschreibung'),
                    $db->qn('a.letztkontrolle'),
                    $db->qn('a.inventartyp'),
                    $db->qn('a.firma'),
                    $db->qn('z.zimmer'),
                    $db->qn('b.bestellnummer'),
                    $db->qn('b.bestelldatum'),
                    $db->qn('l.inhalt'),
                    $db->qn('r.id', 'rechner_id'),
                    $db->qn('d.hostname'),
                    'CONCAT(' . $db->qn('m.nachname') . '," ",' . $db->qn('m.vorname') . ') AS name'
                ]
            )
            ->from($db->qn('#__iwf_inventar', 'a'))
            ->leftJoin($db->qn('#__iwf_zimmer', 'z'), $db->qn('z.id') . '=' . $db->qn('a.standort'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('m.id') . '=' . $db->qn('a.ma_id'))
            ->leftJoin($db->qn('#__iwf_bestellungen', 'b'), $db->qn('b.id') . '=' . $db->qn('a.bestellung'))
            ->leftJoin($db->qn('#__iwf_listen', 'l'), $db->qn('a.inventartyp') . '=' . $db->qn('l.id'))
            ->leftJoin($db->qn('#__iwf_rechner', 'r'), $db->qn('r.inventar_id') . '=' . $db->qn('a.id'))
            ->leftJoin($db->qn('#__iwf_dns', 'd'), $db->qn('d.id') . '=' . $db->qn('r.dns_id'));

        $anlagejahr = $this->getState('filter.anlagejahr');
        if (is_numeric($anlagejahr)) {
            $query->where('YEAR(' . $db->qn('a.created_time') . ') LIKE :anl')
                ->bind(':anl', $anlagejahr, ParameterType::INTEGER);
        }
        $firma = $this->getState('filter.firma');
        if (!empty($firma)) {
            $query->where($db->qn('a.firma') . ' LIKE :firma')
                ->bind(':firma', $firma, ParameterType::STRING);
        }
        $zimmer = $this->getState('filter.zimmer');
        if (is_numeric($zimmer)) {
            $query->where($db->qn('a.standort') . '=:zimmer')
                ->bind(':zimmer', $zimmer, ParameterType::INTEGER);
        }
        $inventartyp = $this->getState('filter.inventartyp');
        if (is_numeric($inventartyp)) {
            $query->where($db->qn('a.inventartyp') . '=:typ')
                ->bind(':typ', $inventartyp, ParameterType::INTEGER);
        }
        $verantwortlicher = $this->getState('filter.verantwortlicher');
        if (is_numeric($verantwortlicher)) {
            $query->where($db->qn('a.ma_id') . '=:verantwortlicher')
                ->bind(':verantwortlicher', $verantwortlicher, ParameterType::INTEGER);
        }
        $letztkontrolle = $this->getState('filter.letztkontrolle');
        if (!is_null($letztkontrolle) && !empty($letztkontrolle)) {
            $query->where('DATE(' . $db->qn('a.letztkontrolle') . ') < :letztkontrolle', 'OR')
                ->where($db->qn('a.letztkontrolle') . '="0000-00-00', 'OR')
                ->where('ISNULL(' . $db->qn('a.letzkontrolle') . ')')
                ->bind(':letzkontrolle', $letztkontrolle, ParameterType::STRING);
        }
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = '%' . trim($search) . '%';
            $where = [
                $db->qn('a.beschreibung') . " LIKE \"$search\"",
                $db->qn('a.firma') . " LIKE \"$search\"",
                $db->qn('a.inventarnummer') . " LIKE \"$search\"",
                $db->qn('a.inventartyp') . " LIKE \"$search\"",
                $db->qn('a.anmerkung') . " LIKE \"$search\"",
                $db->qn('m.nachname') . " LIKE \"$search\""
            ];
            if (is_null($query->where)) {
                $query->where($where, 'OR');
            } else {
                $query->andWhere($where, 'OR');
            }
        }
        $ordering = $this->getState('list.ordering', 'a.created_time');
        $direction = $this->getState('list.direction', 'DESC');
        $query->order($db->qn($ordering) . ' ' . $direction);
        return $query;
    }
}
