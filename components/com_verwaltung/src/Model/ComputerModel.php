<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Administrator\Helper\VerwaltungHelper;
use Iwf\Verwaltung\LdapClient;
use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\MVC\Model\AdminModel;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Plugin\PluginHelper;
use Joomla\CMS\Table\Table;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Registry\Registry;
use RuntimeException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\Model */
class ComputerModel extends AdminModel
{
    
    /**
     * @param string $name 
     * @param string $prefix 
     * @param array $options 
     * @return Table 
     * @throws DatabaseNotFoundException 
     * @throws UnexpectedValueException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function getTable($name = '', $prefix = '', $options = array())
    {
        $name = 'iwf_rechner';
        $prefix = 'Table';
        if ($table = $this->_createTable($name, $prefix, $options)) {
            return $table;
        }
        throw new \Exception(Text::sprintf('JLIB_APPLICATION_ERROR_TABLE_NAME_NOT_SUPPORTED', $name), 0);
    }

    /**
     * @param array $data 
     * @param bool $loadData 
     * @return Form 
     * @throws Exception 
     */
    public function getForm($data = array(), $loadData = true)
    {
        $form = $this->loadForm('com_verwaltung.computer', 'computer', array('control' => 'jform', 'load_data' => $loadData));
        if (empty($form)) {
            return false;
        }
        return $form;
    }

    /**
     * @return array 
     * @throws Exception 
     * @throws InvalidArgumentException 
     * @throws RuntimeException 
     * @throws UnexpectedValueException 
     * @throws KeyNotFoundException 
     * @throws DatabaseNotFoundException 
     */
    protected function loadFormData()
    {
        $data = Factory::getApplication()->getUserState('com_verwaltung.edit.computer.data', array());
        if (empty($data)) {
            $data = $this->getItem();
			if ($data === false) {
				$data = [];
			}
        }
        $this->preprocessData('com_verwaltung.ma', $data);
        return $data;
    }

    /**
     * @param string $context 
     * @param mixed &$data 
     * @param string $group 
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws DatabaseNotFoundException 
     */
    protected function preprocessData($context, &$data, $group = 'content')
    {
        parent::preprocessData($context, $data, $group);
        if (!isset($data->id)) {
            return;
        }
        $wname = null;
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select('*')
            ->from($db->qn('#__iwf_dns'))
            ->where($db->qn('id') . '=:id')
            ->bind(':id', $data->dns_id, ParameterType::INTEGER);
        $item = $db->setQuery($query)->loadObject();
        if ($item) {
            if (empty($item->wname) && empty($item->hostname)) {
                return;
            }
            if ($item->wname) {
                $wname = strtoupper($item->wname);
            } else {
                $wname = strtoupper($item->hostname);
            }
        }
        $data->os = "Kein Computerobjekt gefunden";
        $data->osversion = "";
        $data->osservicepack = "";
        $data->lastlogon = "";
        if ($wname) {
            $plugin = PluginHelper::getPlugin('authentication', 'ldap');
            $params = new Registry($plugin->params);
            if ($ldap = new LdapClient($params)) {
                if ($ldap->connect()) {
                    if ($ldap->bind()) {
                        $search = ['cn=' . $wname . '*'];
                        $configParams = ComponentHelper::getParams('com_verwaltung');
                        $baseDn = $configParams->get('ldap_basedn');
                        $binddata = $ldap->search($search, $baseDn, array('cn', 'operatingSystem', 'operatingSystemVersion', 'operatingSystemServicePack', 'lastLogon', 'description'));
                        if ($numData = count($binddata)) {
                            $found = false;
                            for ($n = 0; $n < $numData; $n++) {
                                if (!$found) {
                                    if (isset($binddata[$n]) && isset($binddata[$n]['cn'])) {
                                        if (isset($binddata[$n]['operatingSystem'])) {
                                            $data->os = $binddata[$n]['operatingSystem'][0];
                                        } else {
                                            $data->os = "";
                                        }
                                        if (isset($binddata[$n]['operatingSystemVersion'])) {
                                            $data->osversion = $binddata[$n]['operatingSystemVersion'][0];
                                        } else {
                                            $data->osversion = "";
                                        }
                                        if (isset($binddata[$n]['operatingSystemServicePack'])) {
                                            $data->osservicepack = $binddata[$n]['operatingSystemServicePack'][0];
                                        } else {
                                            $data->osservicepack = "";
                                        }
                                        if (isset($binddata[$n]['lastLogon'])) {
                                            $unixSecondsSince1970 = VerwaltungHelper::ldapTimeToUnixTime($binddata[$n]['lastLogon'][0]);
                                            $data->lastlogon = date('d.m.Y H:i:s', $unixSecondsSince1970);
                                        }
                                        if (isset($binddata[$n]['description'])) {
                                            $data->dhcp = $binddata[$n]['description'][0];
                                        }
                                        $found = true;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        $query->clear()
            ->update($db->qn('#__iwf_inventar'))
            ->set($db->qn('dhcp_text') . '=:text')
            ->bind(':text',  $data->dhcp, ParameterType::STRING)
            ->where($db->qn('id') . '=:id')
            ->bind(':id', $data->inventar_id, ParameterType::INTEGER);
        $db->setQuery($query)->execute();
    }
}
