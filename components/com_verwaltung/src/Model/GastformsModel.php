<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

use Exception;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\CMS\Factory;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Model */
class GastformsModel extends ListModel
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'ankunftsjahr',
                'abreisejahr',
                'name',
                'zimmer',
                'a.ankunft',
                'a.abreise',
                'zweck',
                'betreuer',
            );
        }
        parent::__construct($config);
    }
   
    /**
     * @param string $ordering 
     * @param string $direction 
     * @return void 
     * @throws Exception 
     */
    protected function populateState($ordering = 'a.abreise', $direction = 'DESC') {
        $app = Factory::getApplication();
        $orderCol = $app->input->get('filter_order', $ordering);
        if (!in_array($orderCol, $this->filter_fields)) {
            $orderCol = 'a.abreise';
        }
        $this->setState('list.ordering', $orderCol);

        $listOrder = $app->input->get('filter_order_Dir', $direction);
        if (!in_array(strtoupper($listOrder), array('ASC', 'DESC', ''))) {
            $listOrder = 'DESC';
        }
        $this->setState('list.direction', $listOrder);
        parent::populateState($orderCol, $listOrder);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     */
    protected function getListQuery()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select(
                [
                    'a.*',
                    $query->concatenate([$db->qn('a.nachname'), $db->qn('a.vorname')], ' ') . ' AS ' . $db->qn('name'),
                    $db->qn('z.zimmer', 'zimmer'),
                    $query->concatenate([$db->qn('m.nachname'), $db->qn('m.vorname')], ' ') . ' AS ' . $db->qn('betreuer'),
                    $db->qn('l.inhalt', 'zweck'),
                ]
            )
            ->from($db->qn('#__iwf_gastformular', 'a'))
            ->leftJoin($db->qn('#__iwf_zimmer', 'z'), $db->qn('a.arbeitsplatz') . ' = ' . $db->qn('z.id'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('a.betreuer_id') . ' = ' . $db->qn('m.id'))
            ->leftJoin($db->qn('#__iwf_listen', 'l'), $db->qn('l.id') . ' = ' . $db->qn('a.aufenthaltszweck'));
         $ankunftsjahr = $this->getState('filter.ankunftsjahr');
        if (!empty($ankunftsjahr)) {
            $query->where($db->qn('(a.ankunft="' . $ankunftsjahr . '")'))
                ->bind(':a.ankunftsjahr', $ankunftsjahr, ParameterType::INTEGER);
        }
        $abreisejahr = $this->getState('filter.abreisejahr');
        if (!empty($abreisejahr)) {
            $query->where($db->qn('(a.abreise="' . $abreisejahr . '")'))
                ->bind(':a.abreisejahr', $abreisejahr, ParameterType::INTEGER);
        }
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = '%' . trim($search) . '%';
            $where = [
                $db->qn('a.vorname') . " LIKE \"$search\"",
                $db->qn('a.nachname') . " LIKE \"$search\"",
                $db->qn('m.nachname') . " LIKE \"$search\"",
                $db->qn('m.vorname') . " LIKE \"$search\"",
                $db->qn('z.zimmer') . " LIKE \"$search\"",
                $db->qn('l.inhalt') . " LIKE \"$search\""
            ];
            if (is_null($query->where)) {
                $query->where($where, 'OR');
            } else {
                $query->andWhere($where, 'OR');
            }
        }
        $orderCol  = $this->getState('list.ordering', 'a.abreise');
        $orderDirn = $this->getState('list.direction', 'ASC');
        if ($orderCol === 'reihenfolge') {
            $ordering = $db->qn('a.reihenfolge') . ' ' . $db->escape($orderDirn);
        } else {
            $ordering = $db->qn($orderCol) . ' ' . $db->escape($orderDirn);
        }
        $query->order($ordering);
        return $query;
    }

}
