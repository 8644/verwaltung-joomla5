<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

use Exception;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Model */
class OrdersModel extends ListModel
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'besteller',
                'abteilung',
                'projekt',
                'bestelljahr',
                'a.id',
                'bestellnummer',
                'a.bestellnummer',
                'bestelldatum',
                'a.bestelldatum',
                'beschreibung',
                'a.beschreibung',
                'bezahlt',
                'a.bezahlt',
                'm.nachname',
            );
        }
        $this->person = Person::getInstance();
        parent::__construct($config);
    }

    /**
     * @param string $ordering 
     * @param string $direction 
     * @return void 
     * @throws Exception 
     */
    protected function populateState($ordering = 'a.bestelldatum', $direction = 'DESC')
    {
        parent::populateState($ordering, $direction);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     */
    protected function getListQuery()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select(
                [
                    $db->qn('a.id', 'id'),
                    $db->qn('a.bestellnummer', 'bestellnummer'),
                    $db->qn('a.beschreibung', 'beschreibung'),
                    $db->qn('a.bestelldatum', 'bestelldatum'),
                    $db->qn('a.created_by', 'owner'),
                    $db->qn('a.sap'),
                    $db->qn('m.nachname'),
                    $db->qn('a.bezahlt', 'bezahlt'),
                    'CASE WHEN ' . $db->qn('anbot') . '=229 THEN ' . $db->qn('preis1') . ' WHEN ' . $db->qn('anbot') . '=230 THEN ' . $db->qn('preis2') . ' ELSE ' . $db->qn('preis3') . ' END AS ' . $db->qn('preis'),
                    $query->concatenate([ $db->qn('m.nachname'), $db->qn('m.vorname')], ' ') . ' AS ' . $db->qn('name'),
                    $db->qn('n.land', 'land'),
                ]
            );
        $query->select('CASE WHEN ' . $db->qn('a.created_by') . '<>' . $this->person->joomla_user->id . ' THEN 1 ELSE 0 END AS readonly')
            ->from($db->qn('#__iwf_bestellungen', 'a'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('a.ma_id') . '=' . $db->qn('m.id'))
            ->leftJoin($db->qn('#__iwf_projekte', 'p'), $db->qn('p.id') . '=' . $db->qn('a.projekt'))
            ->leftJoin($db->qn('#__iwf_nationen', 'n'), $db->qn('a.land') . '=' . $db->qn('n.id'))
            ->where($db->qn('m.id') . '<>0');
        $bestelljahr = $this->getState('filter.bestelljahr');
        if (is_numeric($bestelljahr)) {
            $query->where('YEAR(' . $db->qn('a.bestelldatum') . ') LIKE :anl')
                ->bind(':anl', $bestelljahr, ParameterType::STRING);
        }
        $abteilungsfilter = isset($this->person->profil->abteilung_filter) ? sprintf($this->person->abteilung_filter, $db->qn('m.abteilung')) : null;
        $abteilung = $this->getState('filter.abteilung');
        if (!empty($abteilung)) {
            $query->where($db->qn('m.abteilung') . ' LIKE :abteilung')
                ->bind(':abteilung', $abteilung, ParameterType::INTEGER);
        }
        if ($abteilungsfilter) {
            $query->where($abteilungsfilter);
        }
        $projekt = $this->getState('filter.projekt');
        if (is_numeric($projekt)) {
            $query->where($db->qn('a.projekt') . '=:projekt')
                ->bind(':projekt', $projekt, ParameterType::STRING);
        }
        $besteller = $this->getState('filter.besteller');
        if (is_numeric($besteller)) {
            $query->where($db->qn('a.ma_id') . '=:besteller')
                ->bind(':besteller', $besteller, ParameterType::INTEGER);
        }
        $bezahlt = $this->getState('filter.bezahlt');
        if (is_numeric($bezahlt)) {
            $query->where($db->qn('a.bezahlt') . '=:bezahlt')
                ->bind(':bezahlt', $bezahlt, ParameterType::INTEGER);
        }
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = '%' . trim($search) . '%';
            $where = [
                $db->qn('a.bestellnummer') . " LIKE \"$search\"",
                $db->qn('a.beschreibung') . " LIKE \"$search\"",
                $db->qn('a.rechnungsnummer') . " LIKE \"$search\"",
                $db->qn('a.sap') . " LIKE \"$search\"",
                $db->qn('a.anbot1') . " LIKE \"$search\"",
                $db->qn('a.anbot2') . " LIKE \"$search\"",
                $db->qn('a.anbot3') . " LIKE \"$search\""
            ];
            if (is_null($query->where)) {
                $query->where($where, 'OR');
            } else {
                $query->andWhere($where, 'OR');
            }
        }
        $orderCol  = $this->getState('list.ordering', 'a.bestelldatum');
        $orderDirn = $this->getState('list.direction', 'DESC');
        $ordering = $db->escape($orderCol) . ' ' . $db->escape($orderDirn);
        $query->order($ordering);
        return $query;
    }
}
