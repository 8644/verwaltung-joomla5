<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

defined('_JEXEC') or die;

use Exception;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Form;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;

/** @package Iwf\Component\Verwaltung\Site\Model */
class PersonallisteModel extends ListModel
{
    
    /**
     * @param array $data 
     * @param bool $loadData 
     * @return null 
     */
    public function getForm($data = array(), $loadData = true)
    {
        return null;
    }

    /**
     * @param array $data 
     * @param bool $loadData 
     * @return Form|null 
     */
    public function getFilterForm($data = [], $loadData = true)
    {
        if (empty($this->filterFormName)) {
            $classNameParts = explode('Model', \get_called_class());

            if (\count($classNameParts) >= 2) {
                $this->filterFormName = 'filter_' . str_replace('\\', '', strtolower($classNameParts[1]));
            }
        }
        if (empty($this->filterFormName)) {
            return null;
        }
        try {
            $filterForm = $this->loadForm($this->context . '.filter', $this->filterFormName, ['control' => '', 'load_data' => $loadData]);
            return $filterForm;
        } catch (\RuntimeException $e) {
        }
        return null;
    }

    /**
     * @param string $ordering 
     * @param string $direction 
     * @return void 
     */
    protected function populateState($ordering = 'ab.abteilung', $direction = 'ASC')
    {
        $this->setState('list.limit', 1000);
        $this->setState('list.start', 0);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws Exception 
     * @throws DatabaseNotFoundException 
     */
    public function getListQuery()
    {
        $app = Factory::getApplication();
        $state = $app->getInput()->get('state');
        $suche = $app->getInput()->get('suche');
        $dienstverhaeltnis = $app->getInput()->get('dienstverhaeltnis');
        $abteilung = $app->getInput()->get('abteilung');
        $eintritt = $app->getInput()->get('eintritt');
        $zimmer = $app->getInput()->get('zimmer');
        $abgelaufen = $app->getInput()->get('abgelaufen');
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('a.id', 'id'),
                    $db->qn('a.state', 'state'),
                    $db->qn('a.nachname', 'nachname'),
                    $db->qn('a.vorname', 'vorname'),
                    $db->qn('ab.abteilung', 'abteilung'),
                    'YEAR(' . $db->qn('a.eintritt') . ') AS eintritt',
                    $db->qn('l.inhalt', 'dienstverhaeltnis'),
                    $db->qn('z.zimmer', 'zimmer')
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter', 'a'))
            ->leftJoin($db->qn('#__iwf_abteilungen', 'ab'), $db->qn('a.abteilung') . '=' . $db->qn('ab.id'))
            ->leftJoin($db->qn('#__iwf_listen', 'l'), $db->qn('l.id') . '=' . $db->qn('a.dienstverhaeltnis'))
            ->leftJoin($db->qn('#__iwf_zimmer', 'z'), $db->qn('z.id') . '=' . $db->qn('a.zimmer'));
        if (!empty($state)) {
            $query->where($db->qn('a.state') . '=:state')
                ->bind(':state', $state, ParameterType::INTEGER);
        }
        if (!empty($suche)) {
            $suche .= '%';
            $query->andWhere(
                    [
                        $db->qn('a.vorname') . " LIKE \"$suche\"",
                        $db->qn('a.nachname') . " LIKE \"$suche\""
                    ],
                    'OR'
                );
        }
        if (!empty($dienstverhaeltnis)) {
            $query->where($db->qn('a.dienstverhaeltnis') . '=:dv')
                ->bind(':dv', $dienstverhaeltnis, ParameterType::INTEGER);
        }
        if (!empty($abteilung)) {
            $query->where($db->qn('a.abteilung') . '=:abt')
                ->bind(':abt', $abteilung, ParameterType::INTEGER);
        }
        if (!empty($eintritt)) {
            $query->where('YEAR(' . $db->qn('a.eintritt') . ')' . ' >= :eintritt')
                ->bind(':eintritt', $eintritt, ParameterType::INTEGER);
        }
        if (!empty($zimmer)) {
            $query->where($db->qn('a.zimmer') . '=:zimmer')
                ->bind(':zimmer', $zimmer, ParameterType::INTEGER);
        }
        if ($abgelaufen) {
            $query->where($db->qn('vertragsende') . '< NOW()');
        }
        $query->order(
                [
                    $db->qn('ab.abteilung'),
                    $db->qn('a.nachname'),
                    $db->qn('a.dienstverhaeltnis')
                ]
            );
        return $query;
    }
}
