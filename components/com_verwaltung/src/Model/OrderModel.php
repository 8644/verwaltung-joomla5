<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\MVC\Model\AdminModel;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Table\Table;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;

/** @package Iwf\Component\Verwaltung\Site\Model */
class OrderModel extends AdminModel
{
	
	/**
	 * @param object $record 
	 * @return bool 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws RuntimeException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	protected function canDelete($record) {
        return parent::canDelete($record) || Extensions::isAllowed('delete.bestellungen');
    }

	/**
	 * @param string $name 
	 * @param string $prefix 
	 * @param array $options 
	 * @return Table 
	 * @throws DatabaseNotFoundException 
	 * @throws UnexpectedValueException 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 */
	public function getTable($name = '', $prefix = '', $options = array())
	{
		$name = 'iwf_bestellungen';
		$prefix = 'Table';
		if ($table = $this->_createTable($name, $prefix, $options)) {
			return $table;
		}
		throw new \Exception(Text::sprintf('JLIB_APPLICATION_ERROR_TABLE_NAME_NOT_SUPPORTED', $name), 0);
	}

	/**
	 * @param array $data 
	 * @param bool $loadData 
	 * @return Form 
	 * @throws Exception 
	 */
	public function getForm($data = array(), $loadData = true)
	{
		$form = $this->loadForm('com_verwaltung.order', 'order', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		return $form;
	}

	/**
	 * @return array 
	 * @throws Exception 
	 * @throws InvalidArgumentException 
	 * @throws RuntimeException 
	 * @throws UnexpectedValueException 
	 */
	protected function loadFormData()
	{
		$data = Factory::getApplication()->getUserState('com_verwaltung.edit.order.data', array());
		if (empty($data)) {
			$data = $this->getItem();
			if ($data === false) {
				$data = [];
			}
		}
		return $data;
	}
}
