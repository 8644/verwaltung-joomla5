<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

defined('_JEXEC') or die;

use Exception;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\CMS\MVC\Model\AdminModel;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Table\Table;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Registry\Format\Json;
use RuntimeException;
use stdClass;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\Model */
class ProfilModel extends AdminModel
{
    
    /**
     * @param string $name 
     * @param string $prefix 
     * @param array $options 
     * @return Table 
     * @throws DatabaseNotFoundException 
     * @throws UnexpectedValueException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function getTable($name = '', $prefix = '', $options = array())
    {
        $name = 'iwf_mitarbeiter';
        $prefix = 'Table';
        if ($table = $this->_createTable($name, $prefix, $options)) {
            return $table;
        }
        throw new \Exception(Text::sprintf('JLIB_APPLICATION_ERROR_TABLE_NAME_NOT_SUPPORTED', $name), 0);
    }

    /**
     * @param array $data 
     * @param bool $loadData 
     * @return Form 
     * @throws Exception 
     */
    public function getForm($data = array(), $loadData = true)
    {
        $form = $this->loadForm('com_verwaltung.profil', 'profil', array('control' => 'jform', 'load_data' => $loadData));
        if (empty($form)) {
            return false;
        }
        return $form;
    }

    /**
     * @return array 
     * @throws Exception 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     */
    protected function loadFormData()
    {
        $data = Factory::getApplication()->getUserState('com_verwaltung.edit.profil.data', array());
        if (empty($data)) {
            $data = $this->getItem();
        }
        return $data;
    }

    /**
     * @param int $pk 
     * @return stdClass|false 
     * @throws Exception 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     */
    public function getItem($pk = null)
    {
        $person = Person::getInstance();
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select($db->qn('profil'))
            ->from($db->qn('#__iwf_mitarbeiter'))
            ->where($db->qn('id') . '=:id')
            ->bind(':id', $person->ma_id, ParameterType::INTEGER);
        $data = (object)[];
        $json = new Json();
        $item = $db->setQuery($query)->loadObject();
        if ($item) {
            $profil = $json->stringToObject($item->profil);
            $data->abteilung_filter = isset($profil->abteilung_filter) ? $profil->abteilung_filter : 0;
            $data->jahr_filter = isset($profil->jahr_filter) ? $profil->jahr_filter : 0;
        }
        $params = $json->stringToObject($person->joomla_user->params);
        $data->sprache = empty($params->language) ? 'de-DE' : $params->language;
        return $data;
    }
}
