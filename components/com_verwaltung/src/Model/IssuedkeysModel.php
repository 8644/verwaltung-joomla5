<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

use Exception;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\CMS\MVC\Model\ListModel;
use Iwf\Verwaltung\Extensions;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use UnexpectedValueException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Model */
class IssuedkeysModel extends ListModel
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'name',
                'schluesseltyp',
                'besitzer',
                'abteilung',
                's.bezeichnung',
                'a.ausgabedatum',
                'a.kommentar',
                's.raum_gruppe',
                's.transponderkennung',
            );
        }
        $this->person = Person::getInstance();
        parent::__construct($config);
    }

    /**
     * @param string $ordering 
     * @param string $direction 
     * @return void 
     * @throws Exception 
     */
    protected function populateState($ordering = 's.bezeichnung', $direction = 'ASC')
    {
        parent::populateState($ordering, $direction);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function getListQuery()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select(
                [
                    $db->qn('a.id'),
                    $db->qn('a.ausgabedatum'),
                    $db->qn('a.kommentar'),
                    $db->qn('s.bezeichnung'),
                    $db->qn('s.transponderkennung', 'kennung'),
                    $db->qn('s.raum_gruppe', 'raum'),
                    $db->qn('m.abteilung'),
                    $query->concatenate([$db->qn('m.nachname'), $db->qn('m.vorname')], ' ') . ' AS name'
                ]
            )
            ->from($db->qn('#__iwf_schluessel', 'a'))
            ->leftJoin($db->qn('#__iwf_schliessanlage', 's'), $db->qn('a.schluessel') . '=' . $db->qn('s.id'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('m.id') . '=' . $db->qn('a.ma_id'));
        if (Extensions::isAllowed('core.manage.schluessel')) {
            $schluesseltyp = $this->getState('filter.schluesseltyp');
            if ($schluesseltyp) {
                $query->where($db->qn('s.schluessel') . '=:schluesseltyp')
                    ->bind(':schluesseltyp', $schluesseltyp, ParameterType::STRING);
            }
            $name = $this->getState('filter.besitzer'); 
            if (is_numeric($name)) {
                $query->where($db->qn('m.id') . '=:name')
                    ->bind(':name', $name, ParameterType::INTEGER);
            }

            $abteilung = $this->getState('filter.abteilung');
            if ($abteilung) {
                $query->where($db->qn('m.abteilung') . '=:abteilung')
                    ->bind(':abteilung', $abteilung, ParameterType::INTEGER);
            }
        } else {
            $query->where($db->qn('m.id') . '=:id')
                ->bind(':id', $this->person->id, ParameterType::INTEGER);
        }
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = '%' . trim($search) . '%';
            $where = [
                $db->qn('s.bezeichnung') . " LIKE \"$search\"",
                $db->qn('s.transponderkennung') . " LIKE \"$search\"",
                $db->qn('s.kommentar') . " LIKE \"$search\"",
                $db->qn('a.kommentar') . " LIKE \"$search\"",
                $db->qn('m.nachname') . " LIKE \"$search\"",
                $db->qn('s.raum_gruppe') . " LIKE \"$search\"",
                $db->qn('m.nachname') . " LIKE \"$search\""
            ];
            if (is_null($query->where)) {
                $query->where($where, 'OR');
            } else {
                $query->andWhere($where, 'OR');
            }
        }
        $ordering = $this->getState('list.ordering', 's.bezeichnung');
        $direction = $this->getState('list.direction', 'ASC');
        $query->order($db->qn($ordering) . ' ' . $direction);

        return $query;
    }
}
