<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 *
 *
 */

namespace Iwf\Component\Verwaltung\Site\Model;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\Model */
class LicensesModel extends ListModel
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'softwareprodukt',
                'ma',
                'rechner',
                's.produkt',
                's.version',
                'd.hostname',
                'ma.nachname',
                'm.label',
                'l.inhalt',
                'bybaramundi',
            );
        }
        $this->person = Person::getInstance();
        parent::__construct($config);
    }

    /**
     * @param string $ordering 
     * @param string $direction 
     * @return void 
     * @throws Exception 
     */
    protected function populateState($ordering = 'a.id', $direction = 'ASC')
    {
        $search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);
        parent::populateState($ordering, $direction);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    public function getListQuery()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select
                (
                    [
                        $db->qn('a.id', 'id'),
                        $db->qn('s.produkt', 'produkt'),
                        $db->qn('s.version', 'version'),
                        $db->qn('r.id', 'rechnerid'),
                        $db->qn('d.hostname', 'host'),
                        $db->qn('m.label', 'mediumlabel'),
                        'CONCAT(' . $db->qn('ma.nachname') . '," ",' . $db->qn('ma.vorname') . ') AS name',
                        $db->qn('l.inhalt', 'sprache'),
                        $db->qn('a.bybaramundi'),
                    ]
                )
                ->from($db->qn('#__iwf_lizenzen', 'a'))
                ->leftJoin($db->qn('#__iwf_rechner', 'r'), $db->qn('a.rechner_id') . '=' . $db->qn('r.id'))
                ->leftJoin($db->qn('#__iwf_inventar', 'i'), $db->qn('r.inventar_id') . '=' . $db->qn('i.id'))
                ->leftJoin($db->qn('#__iwf_dns', 'd'), $db->qn('r.dns_id') . '=' . $db->qn('d.id'))
                ->leftJoin($db->qn('#__iwf_software', 's'), $db->qn('a.software_id') . '=' . $db->qn('s.id'))
                ->leftJoin($db->qn('#__iwf_mitarbeiter', 'ma'), $db->qn('i.ma_id') . '=' . $db->qn('ma.id'))
                ->leftJoin($db->qn('#__iwf_medien', 'm'), $db->qn('m.id') . '=' . $db->qn('s.medienid'))
                ->leftJoin($db->qn('#__iwf_listen', 'l'), $db->qn('l.id') . '=' . $db->qn('s.sprache'));
            $software_id = $this->getState('filter.softwareprodukt');
            if (is_numeric($software_id)) {
                $query->where($db->qn('a.software_id') . '=:swid')
                    ->bind(':swid', $software_id, ParameterType::INTEGER);
            }
        if (Extensions::isAllowed('manage.sw')) {
            $ma = $this->getState('filter.ma');
            if (is_numeric($ma)) {
                $query->where($db->qn('ma.id') . '=:ma')
                    ->bind(':ma', $ma, ParameterType::INTEGER);
            }
            $rechner = $this->getState('filter.rechner');
            if (is_numeric($rechner)) {
                $query->where($db->qn('d.id') . '=:rechner')
                    ->bind(':rechner', $rechner, ParameterType::INTEGER);
            }
            $bybaramundi = $this->getState('filter.bybaramundi');
            if (is_numeric($bybaramundi)) {
                $query->where($db->qn('a.bybaramundi') . '=:bybaramundi')
                    ->bind(':bybaramundi', $bybaramundi, ParameterType::INTEGER);
            }
        } else {
            $query->where($db->qn('ma.id') . '=:ma')
                ->bind(':ma', $this->person->ma_id, ParameterType::INTEGER);
        }
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = '%' . trim($search) . '%';
            $where = [
                $db->qn('s.produkt') . " LIKE \"$search\"",
                $db->qn('ma.nachname') . " LIKE \"$search\"",
                $db->qn('d.hostname') . " LIKE \"$search\"",
            ];
            if (is_null($query->where)) {
                $query->where($where, 'OR');
            } else {
                $query->andWhere($where, 'OR');
            }
        }
        $ordering = $this->state->get('list.ordering', 's.product');
        $direction = $this->state->get('list.direction', 'ASC');
        $query->order($ordering . ' ' . $direction);
        return $query;
    }
}
