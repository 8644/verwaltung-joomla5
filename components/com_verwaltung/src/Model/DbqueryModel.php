<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

defined('_JEXEC') or die;

use Exception;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\CMS\Application\CMSApplicationInterface;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Language\Text;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\Model */
class DbqueryModel extends ListModel
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'search'
            );
        }
        parent::__construct($config);
    }


    /**
     * @param array $data 
     * @param bool $loadData 
     * @return Form 
     */
    public function getForm($data = array(), $loadData = true) {
        return null;
    }

    /**
     * @return DatabaseQuery|string 
     * @throws Exception 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws KeyNotFoundException 
     */
    protected function getListQuery() {
        $id = Factory::getApplication()->input->getInt('id');
        $dbquery = Factory::getApplication()->input->get('query', null);
        if ($dbquery) {
            if ($id) {
                $db = $this->getDatabase();
                $query = $db->createQuery();
                $query->select('a.*')
                    ->from($db->qn('#__iwf_queries', 'a'))
                    ->where($db->qn('a.id') . '=:id')
                    ->bind(':id', $id, ParameterType::INTEGER);
                $items = $db->setQuery($query)->loadObjectList();
                if ($items) {
                    $this->queryTitle = $items[0]->beschreibung;
                }
            }
            $dbquery = \base64_decode($dbquery);
            $this->search = $this->getState('list.search');
            if (stripos($dbquery, 'select') != 0) {
                Factory::getApplication()->enqueueMessage(Text::_('COM_VERWALTUNG_DBQUERY_INVALID_QUERY'), CMSApplicationInterface::MSG_ERROR);
                if ($id) {
                    Factory::getApplication()->redirect('index.php?option=com_verwaltung&view=query&layout=edit&id=' . $id);
                } else {
                    Factory::getApplication()->redirect('index.php?option=com_verwaltung&view=queries');
                }
                return false;
            }
            return $dbquery;
        }
        return [];
    }
}