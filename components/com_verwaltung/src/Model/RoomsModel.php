<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Model;

use Exception;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Model */
class RoomsModel extends ListModel
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'veranstaltung',
                'a.veranstaltung',
                'veranstalter',
                'abteilung',
                'm.abteilung',
                'l.inhalt',
                'a.datum',
                'raum',
            );
        }
        parent::__construct($config);
    }

    /**
     * @param string $ordering 
     * @param string $direction 
     * @return void 
     * @throws Exception 
     */
    protected function populateState($ordering = 'a.datum', $direction = 'ASC')
    {
        parent::populateState($ordering, $direction);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     */
    protected function getListQuery()
    {
        $typ = RESERVIERUNGSTYP_RAUM;
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select
            (
                [
                    $db->qn('a.id', 'id'),
                    $db->qn('a.veranstaltung', 'veranstaltung'),
                    $db->qn('l.inhalt', 'raum'),
                    $query->concatenate([$db->qn('m.nachname'), $db->qn('m.vorname')], ' ') . ' AS ' . $db->qn('name'),
                    $query->concatenate([$db->qn('ab.institut'), $db->qn('ab.abteilung')], ' ') . ' AS ' . $db->qn('abteilung'),
                    $db->qn('a.datum', 'datum'),
                    $db->qn('a.beginn', 'beginn'),
                    $db->qn('a.ende', 'ende'),
                    $db->qn('a.vorgemerkt', 'vorgemerkt'),
                    $db->qn('a.wiederholungsintervall', 'wiederholungsintervall'),
                    $db->qn('a.created_by', 'owner'),
                    'CASE WHEN ' . $db->qn('a.created_by') . '<>' . Factory::getApplication()->getIdentity()->id . ' THEN 1 ELSE 0 END AS ' . $db->qn('readonly'),
                ]   
            )
            ->from($db->qn('#__iwf_reservierungen', 'a'))   //<-----fzg_reservierungen-----Reservierungen not implementet in verwaltung5 
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('a.ma_id') . ' = ' . $db->qn('m.id'))
            ->leftJoin($db->qn('#__iwf_abteilungen', 'ab'), $db->qn('ab.id') . ' = ' . $db->qn('m.abteilung'))
            ->leftJoin($db->qn('#__iwf_listen', 'l'), $db->qn('a.text') . ' = ' . $db->qn('l.id'))
            ->where($db->qn('a.reservierungstyp') . ' =:typ')
            ->bind(':typ', $typ, ParameterType::INTEGER);
        $raum = $this->getState('filter.raum');
        if (!empty($raum)) {
            $query->where($db->qn('l.id') . '=:raum')
                ->bind(':raum', $raum, ParameterType::STRING);
        }
        $abteilung = $this->getState('filter.abteilung');
        if (!empty($abteilung)) {
            $query->where($db->qn('m.abteilung') . '=:abteilung')
                ->bind(':abteilung', $abteilung, ParameterType::INTEGER);
        }
        $veranstalter = $this->getState('filter.veranstalter');
        if (is_numeric($veranstalter)) {
            $query->where($db->qn('a.ma_id') . '=:veranstalter')
                ->bind(':veranstalter', $veranstalter, ParameterType::INTEGER);
        }
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = trim($search) . '%';
            if (is_null($query->where)) {
                $query->where($db->qn('a.veranstalter') . " LIKE \"$search\"", 'OR')
                    ->where($db->qn('a.hinweis') . " LIKE \"$search\"");
            } else {
                $query->andWhere(
                    [
                        $db->qn('a.veranstallter') . " LIKE \"$search\"",
                        $db->qn('a.hinweis') . " LIKE \"$search\"",
                    ],
                    'OR'
                );
            }
        }
        $ordering = $this->getState('list.ordering', 'a.datum');
        $direction = $this->getState('list.direction', 'ASC');
        $query->order($db->qn($ordering) . ' ' . $direction);

        return $query;
    }
}
