<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\View\License;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\Toolbar;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\View\License */
class HtmlView extends BaseHtmlView
{

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function addToolBar()
    { 
        $readOnly = !Extensions::isAllowed('manage.sw');
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        $this->isNew = ($this->item == false);
        IwfToolbarHelper::title($this->isNew ? Text::_('COM_VERWALTUNG_LIZENZEN_CREATING') : Text::_('COM_VERWALTUNG_LIZENZEN_EDITING'), 'lizenz');
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if ($this->isNew && !$readOnly) {
            $this->header = Extensions::getListHeader('iwfactionicon-license-dark-xxl-blue', Text::_('COM_VERWALTUNG_LIZENZEN_CREATING'));
            $this->toolbar->apply('license.apply', 'JAPPLY');
            $this->toolbar->apply('license.save', 'JSAVEANDCLOSE');
        } else if (!$readOnly) {
            $this->header = Extensions::getListHeader('iwfactionicon-license-dark-xxl-blue', $readOnly ? Text::_('COM_VERWALTUNG_LIZENZEN_VIEWING') : Text::_('COM_VERWALTUNG_LIZENZEN_EDITING'));
            $this->toolbar->save('license.apply', 'JAPPLY');
            $saveGroup = $this->toolbar->dropdownButton('save-group');
            $saveGroup->configure(
                function (Toolbar $childBar) {
                    $childBar->save('license.save', 'JSAVEANDCLOSE');
                    $childBar->save2copy('license.save2copy', 'JSAVEASCOPY');
                }
            );
        }
        $this->toolbar->cancel('license.cancel', 'JCANCEL');
        $this->toolbar->inlinehelp();
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    public function display($tpl = null)
    {
        $app = Factory::getApplication();
        $this->readonly = $app->getUserState('com_verwaltung.license.readonly');
        $this->item = $this->get('Item');
        $this->form = $this->get('Form');
        $returnFlag = Factory::getApplication()->getUserState('com_verwaltung.license.return', 0);
        if ($this->item == false) {
            // Lizenzanforderung vom Mitarbeiterformular oder von Rechnerformular aus
            // $rechner_id = $this->getUserStateFromRequest('com_verwaltung.license.rechner_id');
            $app = Factory::getApplication();
            $this->rechner_id = $app->getUserState('com_verwaltung.license.rechner_id', 0);
            if ($this->rechner_id) {
                $this->form->setValue('rechner_id', null, $this->rechner_id);
                $this->form->setValue('installationsdatum', null, Factory::getDate()->format('Y-m-d'));
            }
        }
        switch ($returnFlag) {
            case 0: // Standard
            case 1: // von Ma-Formular - nur einmal setzen (bei speichern verhindern)
                Extensions::setReturnRoute();
                Factory::getApplication()->setUserState('com_verwaltung.license.return', 2);
                break;
            defaut:
            break;
        }
        if ($this->readonly) {
            foreach ($this->form->getFieldset('lizenz_fields') as $field) {
                $this->form->setFieldAttribute($field->fieldname, 'readonly', 'true');
                $this->form->setFieldAttribute($field->fieldname, 'disabled', 'true');
            }
        }
        $this->addToolBar();
        parent::display($tpl);
    }
}
