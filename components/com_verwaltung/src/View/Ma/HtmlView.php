<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\View\Ma;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Error;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\Toolbar;
use Joomla\CMS\Language\Text;
use Iwf\Verwaltung\Extensions;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\View\Ma */
class HtmlView extends BaseHtmlView
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     * @throws Error 
     */
    public function __construct($config = [])
    {
        $this->person = Person::getInstance();
        $this->canCreate = Extensions::isAllowed('create.ma');
        $this->readonly = !Extensions::isAllowed('edit.ma');
        $this->managehw = Extensions::isAllowed('manage.hw');
        $this->manageschluessel = Extensions::isAllowed('manage.schluessel');
        $this->managesw = Extensions::isAllowed('manage.sw');
        parent::__construct($config);
    }

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        $this->isNew = ($this->item == false);
        IwfToolbarHelper::title($this->isNew ? Text::_('COM_VERWALTUNG_MITARBEITER_CREATING') : Text::_('COM_VERWALTUNG_MITARBEITER_EDITING_TITLE'), 'mitarbeiter');
        $hasName = count($this->form->getFieldset()) > 3; // nachdem Dienstverhaeltnis gewählt wurde
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if ($this->isNew) {
            $this->header = Extensions::getListHeader('iwfactionicon-malefemale-dark-xxl-blue', Text::_('COM_VERWALTUNG_MITARBEITER_CREATING'));
            if (Extensions::isAllowed('edit.ma')) {
                if ($hasName) {
                    $this->toolbar->apply('ma.apply', 'JAPPLY');
                    $this->toolbar->apply('ma.save', 'JSAVEANDCLOSE');
                }
            }
        } else {
            $name = Text::sprintf("%s %s", $this->item->vorname, $this->item->nachname);
            if ($this->readonly) {
                $this->header = $this->item->geschlecht == "M" ? Extensions::getListHeader('iwfactionicon-male-dark-xxl-blue', sprintf(Text::_('COM_VERWALTUNG_MITARBEITER_VIEWING_M'), $name)) :
                    Extensions::getListHeader('iwfactionicon-female-dark-xxl-blue', sprintf(Text::_('COM_VERWALTUNG_MITARBEITER_VIEWING_W'), $name));
            } else {
                $this->header = $this->item->geschlecht == "M" ? Extensions::getListHeader('iwfactionicon-male-dark-xxl-blue', sprintf(Text::_('COM_VERWALTUNG_MITARBEITER_EDITING_M'), $name)) :
                    Extensions::getListHeader('iwfactionicon-female-dark-xxl-blue', sprintf(Text::_('COM_VERWALTUNG_MITARBEITER_EDITING_W'), $name));
            }
            if ($this->canCreate && !$this->readonly) {
                if ($this->item->id && Extensions::isAllowed('manage.schluessel')) {
                    $this->toolbar->standardButton('', 'COM_VERWALTUNG_JTOOLBAR_KEY', 'issuedkey.key')
                        ->icon('iwficon-key-blue');
                }
                if ($this->item->id && (int) $this->getModel()->dienstverhaeltnis == DIENSTVERHAELTNIS_GAST) {
                    $this->toolbar->standardButton('', 'COM_VERWALTUNG_JTOOLBAR_CHECKLISTE', 'checklist.check')
                        ->icon('iwficon-hook-blue');
                }
                $this->toolbar->save('ma.apply', 'JAPPLY');
                $saveGroup = $this->toolbar->dropdownButton('save-group');
                $saveGroup->configure(
                    function (Toolbar $childBar) {
                        $childBar->save('ma.save', 'JSAVEANDCLOSE');
                        $childBar->save2copy('ma.save2copy', 'JSAVEASCOPY');
                    }
                );
            }
        }
        $this->toolbar->cancel('ma.cancel', 'JCANCEL');
        $this->toolbar->inlinehelp();
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    function display($tpl = null)
    {
        $this->item  = $this->get('Item');
        $this->form = $this->get('Form');
        if ($this->readonly) {
            foreach ($this->form->getFieldset('ma_fields') as $field) {
                if ($this->form->getField('id')->value != $this->person->ma_id) {
                    if ($this->form->getFieldAttribute($field->fieldname, 'hide', 'false') == 'true') {
                        $this->form->removeField($field->fieldname);
                    }
                }
                $this->form->setFieldAttribute($field->fieldname, 'readonly', 'true');
                $this->form->setFieldAttribute($field->fieldname, 'disabled', 'true');
                $this->form->setFieldAttribute($field->fieldname, 'class', 'readonly');
            }
        }
        $this->addToolBar();
        parent::display($tpl);
    }
}
