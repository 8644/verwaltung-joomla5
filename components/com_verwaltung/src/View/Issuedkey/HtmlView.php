<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

 namespace IWF\Component\Verwaltung\Site\View\Issuedkey;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Site\Helper\RouterHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\Toolbar;
use Joomla\CMS\Language\Text;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package IWF\Component\Verwaltung\Site\View\Issuedkey */
class HtmlView extends BaseHtmlView
{

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        $this->isNew = ($this->item == false);
        IwfToolbarHelper::title($this->isNew ? Text::_('COM_VERWALTUNG_ISSUEDKEYS_CREATING') : Text::_('COM_VERWALTUNG_ISSUEDKEYS_EDITING'), 'schlüssel');
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if ($this->isNew) {
            $this->header = Extensions::getListHeader('iwfactionicon-key-dark-xxl-blue', Text::_('COM_VERWALTUNG_ISSUEDKEYS_CREATING'));
            if (Extensions::isAllowed('manage.schluessel')) {
                $this->toolbar->apply('issuedkey.apply', 'JAPPLY');
                $this->toolbar->apply('issuedkey.save', 'JSAVEANDCLOSE');
            }
        } else {
            $this->header = Extensions::getListHeader('iwfactionicon-key-dark-xxl-blue',Extensions::isAllowed('manage.schluessel') ? Text::_('COM_VERWALTUNG_ISSUEDKEYS_EDITING') : Text::_('COM_VERWALTUNG_ISSUEDKEYS_VIEWING'));
            if (Extensions::isAllowed('manage.schluessel')) {
                $url = RouterHelper::getPrintKeyRoute($this->item->id);
                $this->toolbar->popupButton('print', 'COM_VERWALTUNG_JTOOLBAR_PRINT')
                    ->popupType('iframe')
                    ->textHeader(Text::_('COM_VERWALTUNG_JTOOLBAR_PRINT'))
                    ->url($url)
                    ->modalWidth('900px')
                    ->modalHeight('950px')
                    ->icon('icon-print');
                $this->toolbar->save('issuedkey.apply', 'JAPPLY');
                $saveGroup = $this->toolbar->dropdownButton('save-group');
                $saveGroup->configure(
                    function (Toolbar $childBar) {
                        $childBar->save('issuedkey.save', 'JSAVEANDCLOSE');
                        $childBar->save2copy('issuedkey.save2copy', 'JSAVEASCOPY');
                    }
                );
            }
        }
        $this->toolbar->cancel('issuedkey.cancel', 'JCANCEL');
        $this->toolbar->inlinehelp();
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws InvalidArgumentException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws UnexpectedValueException 
     */
    function display($tpl = null)
    {
        Text::script('COM_VERWALTUNG_VERWALTUNG_ERROR_UNACCEPTABLE');
        $this->item  = $this->get('Item');
        $this->form = $this->get('Form');
        $returnFlag = Factory::getApplication()->getUserState('com_verwaltung.issuedkey.return', 0);
        if ($this->item == false) {
            $ma_id = Factory::getApplication()->getUserState('com_verwaltung.issuedkey.ma_id');
            if ($ma_id) {
                $this->form->setValue('ma_id', null, $ma_id);
                $this->form->setValue('ausgabedatum', null, Factory::getDate()->toSql());
            }
        }
        switch ($returnFlag) {
            case 0: // Standard
            case 1: // von Ma-Formular - nur einmal setzen (bei speichern verhindern)
                Extensions::setReturnRoute();
                Factory::getApplication()->setUserState('com_verwaltung.issuedkey.return', 2);
                break;
            defaut:
            break;
        }
        if (!Extensions::isAllowed('manage.schluessel')) {
            foreach ($this->form->getFieldset('issuedkey_fields') as $field) {
                $this->form->setFieldAttribute($field->fieldname, 'readonly', 'true');
                $this->form->setFieldAttribute($field->fieldname, 'disabled', 'true');
            }
        }
        $this->addToolBar();
        parent::display($tpl);
    }
}
