<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\View\Flat;

defined('_JEXEC') or die;

use Exception;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\Toolbar;
use Joomla\DI\Exception\KeyNotFoundException;

/** @package Iwf\Component\Verwaltung\Site\View\Flat */
class HtmlView extends BaseHtmlView
{
 
    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        $this->isNew = ($this->item == false);
        IwfToolbarHelper::title($this->isNew ? Text::_('COM_VERWALTUNG_RESERVIERUNG_WOHNUNG_CREATING') : Text::_('COM_VERWALTUNG_RESERVIERUNG_WOHNUNG_EDITING'), 'liste');
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if ($this->isNew) {
            $this->header = Extensions::getListHeader('iwfactionicon-flat-dark-xxl-blue', Text::_('COM_VERWALTUNG_RESERVIERUNG_WOHNUNG_CREATING'));
            $this->toolbar->apply('flat.apply', 'JAPPLY');
            $this->toolbar->apply('flat.save', 'JSAVEANDCLOSE');
        } else {
            $this->header = Extensions::getListHeader('iwfactionicon-flat-dark-xxl-blue', $this->isNew ? Text::_('COM_VERWALTUNG_RESERVIERUNG_WOHNUNG_CREATING') : Text::_('COM_VERWALTUNG_RESERVIERUNG_WOHNUNG_EDITING'));
            $this->toolbar->save('flat.apply', 'JAPPLY');
            $saveGroup = $this->toolbar->dropdownButton('save-group');
            $saveGroup->configure(
                function (Toolbar $childBar) {
                    $childBar->save('flat.save', 'JSAVEANDCLOSE');
                    $childBar->save2copy('flat.save2copy', 'JSAVEASCOPY');
                }
            );
        }
        $this->toolbar->cancel('flat.cancel', 'JCANCEL');
        $this->toolbar->inlinehelp();
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function display($tpl = null)
    {
        $this->item = $this->get('Item');
        $this->form = $this->get('Form');
        
        if (!isset($this->item->id)) {
            $this->form->setValue('reservierungstyp', null, RESERVIERUNGSTYP_WOHNUNG);
            $person = Person::getInstance();
            if ($person->ma_id) {
                $this->form->setValue('ma_id', null, $person->ma_id);
            }
            $date = Factory::getDate('now');
            $now = $date->toSql(true);
            $this->form->setValue('vorgemerkt', null, $now);
        }
        $this->addToolBar();
        parent::display($tpl);
    }
}
