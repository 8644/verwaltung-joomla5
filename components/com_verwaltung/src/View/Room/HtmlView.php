<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\View\Room;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Error;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\Toolbar;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\View\Room */
class HtmlView extends BaseHtmlView
{
 
    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     * @throws Error 
     */
    public function __construct($config = [])
    {
        $this->readonly = Factory::getApplication()->getUserState('com_verwaltung.room.readonly');
        parent::__construct($config);
    }

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        $this->isNew = ($this->item == false);
        IwfToolbarHelper::title($this->isNew ? Text::_('COM_VERWALTUNG_RESERVIERUNG_RAUM_CREATING') : Text::_('COM_VERWALTUNG_RESERVIERUNG_RAUM_EDITING'), 'raum');
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if ($this->isNew) {
            $this->header = Extensions::getListHeader('iwfactionicon-room-dark-xxl-blue', Text::_('COM_VERWALTUNG_RESERVIERUNG_RAUM_CREATING'));
            if (!$this->readonly) {
                $this->toolbar->apply('room.apply', 'JAPPLY');
                $this->toolbar->apply('room.save', 'JSAVEANDCLOSE');
            }
        } else {
            $this->header = Extensions::getListHeader('iwfactionicon-room-dark-xxl-blue', $this->isNew ? Text::_('COM_VERWALTUNG_RESERVIERUNG_RAUM_VIEWING') : Text::_('COM_VERWALTUNG_RESERVIERUNG_RAUM_EDITING'));
            if (!$this->readonly) {
                $this->toolbar->save('room.apply', 'JAPPLY');
                $saveGroup = $this->toolbar->dropdownButton('save-group');
                $saveGroup->configure(
                    function (Toolbar $childBar) {
                        $childBar->save('room.save', 'JSAVEANDCLOSE');
                        $childBar->save2copy('room.save2copy', 'JSAVEASCOPY');
                    }
                );
            }
        }
        $this->toolbar->cancel('room.cancel', 'JCANCEL');
        $this->toolbar->inlinehelp();
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function display($tpl = null)
    {
        $this->item = $this->get('Item');
        $this->form = $this->get('Form');
        
        if (!$this->item) {
            $this->form->setValue('reservierungstyp', null, RESERVIERUNGSTYP_RAUM);
            $person = Person::getInstance();
            if ($person->ma_id) {
                $this->form->setValue('ma_id', null, $person->ma_id);
            }
            $date = Factory::getDate('now');
            $now = $date->toSql(true);
            $this->form->setValue('vorgemerkt', null, $now);
        }
        if ($this->readonly) {
            foreach ($this->form->getFieldset('raum_fields') as $field) {
                $this->form->setFieldAttribute($field->fieldname, 'readonly', 'true');
                $this->form->setFieldAttribute($field->fieldname, 'disabled', 'true');
            }
        }
        $this->addToolBar();
        parent::display($tpl);
    }
}
