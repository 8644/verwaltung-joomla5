<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace IWF\Component\Verwaltung\Site\View\Printkey;

defined('_JEXEC') or die;

use Exception;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\Database\DatabaseInterface;
use Joomla\Database\ParameterType;
use stdClass;

/** @package IWF\Component\Verwaltung\Site\View\Printkey */
class HtmlView extends BaseHtmlView
{

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     */
    public function display($tpl = null)
    {
        $this->item = $this->get('Item');
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery();
        $query->select
            (
                [
                    $db->qn('id'),
                    $db->qn('geschlecht'),
                    $query->concatenate([$db->qn('vorname'), $db->qn('nachname'),], ' ') . ' AS ' . $db->qn('name'),
                    $db->qn('eintritt'),
                    $db->qn('vertragsende'),
                    $db->qn('gastverantwortlicher', 'verantwortlicher')
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter'))
            ->where($db->qn('id') . '=:id')
            ->bind(':id', $this->item->ma_id, ParameterType::INTEGER);
        if ($ma = $db->setQuery($query)->loadObject()) {
            $this->mitarbeiter = $ma;
            $this->verantwortlicher = Person::getPersonByMaId($ma->verantwortlicher);
        }
        if ($this->mitarbeiter) {
            $query->clear();
            $query->select
                (
                    [
                        $db->qn('a.schluessel'),
                        $db->qn('a.ausgabedatum'),
                        $db->qn('s.bezeichnung'),
                        'CASE WHEN ' . $db->qn('s.tuer') . '<> "" THEN ' . $query->concatenate([$db->qn('s.bezeichnung'), $db->qn('s.tuer')],  ' --> Tür ') .
                        ' WHEN ' . $db->qn('s.transponderkennung') . '<>"" THEN ' . $query->concatenate([$db->qn('s.bezeichnung'), $db->qn('s.transponderkennung')],  ' --> Transponder ') . 
                        ' WHEN ' . $db->qn('s.raum_gruppe') . '<>"" THEN ' . $query->concatenate([$db->qn('s.bezeichnung'), $db->qn('s.raum_gruppe')],  ' --> Raum-Gruppe ') . 
                        ' ELSE ' . $db->qn('s.bezeichnung') . ' END AS ' . $db->qn('name')
                    ]
                )
                ->from($db->qn('#__iwf_schluessel', 'a'))
                ->leftJoin($db->qn('#__iwf_schliessanlage', 's'), $db->qn('a.schluessel') . '=' . $db->qn('s.id'))
                ->where($db->qn('a.ma_id') . '=:id')
                ->bind(':id', $this->mitarbeiter->id, ParameterType::INTEGER);
            $this->schluesselliste = [];
            $items = $db->setQuery($query)->loadObjectList();
            foreach ($items as $item) {
                $key = new stdClass;
                $key->bezeichnung = $item->name;
                $key->ausgabedatum = $item->ausgabedatum;
                $this->schluesselliste[] = $key;
            }
        }
        parent::display($tpl);
    }
}
