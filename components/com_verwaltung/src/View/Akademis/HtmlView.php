<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace IWF\Component\Verwaltung\Site\View\Akademis;

defined('_JEXEC') or die;

use Exception;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\DI\Exception\KeyNotFoundException;

/** @package IWF\Component\Verwaltung\Site\View\Akademis */
class HtmlView extends BaseHtmlView
{

    /**
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        IwfToolbarHelper::title(Text::_('COM_VERWALTUNG_AKADEMIS_CONFIG_TITLE'), 'akademis');
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        $this->toolbar->addNew('akademis.start', 'COM_VERWALTUNG_AKADEMIS_EXECUTE');
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     */
    public function display($tpl = null)
    {
        $this->form = $this->get('Form');
        $this->addToolBar();
        parent::display($tpl);
    }
}
