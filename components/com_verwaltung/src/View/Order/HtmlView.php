<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\View\Order;

defined('_JEXEC') or die;

use Error;
use Exception;
use InvalidArgumentException;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\Toolbar;
use Joomla\CMS\Language\Text;
use Iwf\Verwaltung\Extensions;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Iwf\Component\Verwaltung\Site\Helper\RouterHelper;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Date\Date;
use Joomla\Database\DatabaseInterface;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\View\Order */
class HtmlView extends BaseHtmlView
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws KeyNotFoundException 
     * @throws Error 
     */
    public function __construct($config = [])
    {
        $this->person = Person::getInstance();
        parent::__construct($config);
    }

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        $this->isNew = ($this->item == false);
        IwfToolbarHelper::title($this->isNew ? Text::_('COM_VERWALTUNG_BESTELLUNG_CREATING') : Text::_('COM_VERWALTUNG_BESTELLUNG_EDITING'), 'bestellung');
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if ($this->isNew) {
            $this->header = Extensions::getListHeader('iwfactionicon-orders-dark-xxl-blue', Text::_('COM_VERWALTUNG_BESTELLUNG_CREATING'));
                $this->toolbar->apply('order.apply', 'JAPPLY');
                $this->toolbar->apply('order.save', 'JSAVEANDCLOSE');
        } else {
            $this->header = Extensions::getListHeader('iwfactionicon-orders-dark-xxl-blue', $this->readonly ? Text::_('COM_VERWALTUNG_BESTELLUNG_VIEWING') : Text::_('COM_VERWALTUNG_BESTELLUNG_EDITING'));
            if (!$this->readonly) {
                if (Date($this->item->created_time) < Date($this->sapStart)) {
                    $url = RouterHelper::getPrintBestellformularRoute($this->item->id);
                    $this->toolbar->popupButton('print', 'COM_VERWALTUNG_JTOOLBAR_PRINT')
                    ->popupType('iframe')
                    ->textHeader(Text::_('COM_VERWALTUNG_JTOOLBAR_PRINT'))
                    ->url($url)
                    ->modalWidth('900px')
                    ->modalHeight('950px')
                    ->icon('icon-print');
                }
                if ($this->newVersion && Extensions::isAllowed('manage.abteilung')) {
                    $this->toolbar->standardButton('pdf', 'Create PDF', 'order.createpdf');
                }
                $this->toolbar->save('order.apply', 'JAPPLY');
                $saveGroup = $this->toolbar->dropdownButton('save-group');
                $saveGroup->configure(
                    function (Toolbar $childBar) {
                        $childBar->save('order.save', 'JSAVEANDCLOSE');
                        $childBar->save2copy('order.save2copy', 'JSAVEASCOPY');
                    }
                );
            }
        }
        $this->toolbar->cancel('order.cancel', 'JCANCEL');
        $this->toolbar->inlinehelp();
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    function display($tpl = null)
    {
        $app = Factory::getApplication();
        $params = $app->getParams('com_verwaltung');
        $this->sapStart = new Date($params->get('sapstart', '2024-04-08'));
        $this->sapRecipients = $params->get('saprecipients');
        $this->readonly = $app->getUserState('com_verwaltung.order.readonly');
        $this->item  = $this->get('Item');
        $this->form = $this->get('Form');
        $ma = Person::getInstance();
        if ($ma && !$this->item) {
            $this->form->setValue('ma_id', null, $ma->ma_id);
        }
        $besteller = null;
        $canorder = false;
        if (isset($this->item->id)) {
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $query = $db->createQuery();
            $query->select($query->concatenate([$db->qn('nachname'), $db->qn('vorname')], ' ') . ' AS ' . $db->qn('name'))
                ->from($db->qn('#__iwf_mitarbeiter'))
                ->where($db->qn('id') . '=:id')
                ->bind(':id', $this->item->ma_id, ParameterType::INTEGER);
            $besteller = $db->setQuery($query)->loadObject();
            $query = $db->createQuery()
                ->select('*')
                ->from($db->qn('#__iwf_projekte'))
                ->where($db->qn('id') . '=:id')
                ->bind(':id', $this->item->projekt, ParameterType::INTEGER);
            $projekt = $db->setQuery($query)->loadObject();
            $query = $db->createQuery()
                ->select($db->qn('canorder'))
                ->from($db->qn('#__iwf_bestellungen'))
                ->where($db->qn('id') . '=:id')
                ->bind(':id', $this->item->id, ParameterType::INTEGER);
            $item = $db->setQuery($query)->loadObject();
            $canorder = (int)$item->canorder == 1;
        }
        $this->newVersion = ($this->item == false && new Date('now') >= $this->sapStart) || new Date($this->item->created_time) >= $this->sapStart;
        $this->bodyText = '';
        if ($this->newVersion && isset($this->item->id)) {
            $this->bodyText = sprintf("Bestellnummer: %s", $this->item->bestellnummer) . "%0D%0A";
            $this->bodyText .= sprintf("Besteller: %s", $besteller->name) . "%0D%0A";
            $this->bodyText .= sprintf("Beschreibung: %s", $this->item->beschreibung) . "%0D%0A";
            if (!empty($this->item->anbot1)) {
                $this->bodyText .= sprintf("Firma: %s", $this->item->anbot1) . "%0D%0A";
            }
            if (!empty($this->item->preis1)) {
                $this->bodyText .= sprintf("Preis: %s EUR", $this->item->preis1) . "%0D%0A";
            }
            $this->bodyText .= sprintf("Projekt: %s (%s)", $projekt->projekt, $projekt->projektbez) . "%0D%0A";
            if ($this->item->eigenerklaerung != "N/A") {
                $this->bodyText .= sprintf("Eigenerklärung: %s", $this->item->eigenerklaerung);
            }
            $this->bodyText .= Text::_('COM_VERWALTUNG_BESTELLUNG_HINWEIS_SAP_EMAIL');
        }
        if (isset($this->item->bestellnummer)) {
            if (!empty($this->sapRecipients)) {
                $app->setUserState('com_verwaltung.order.link', '<a href="mailto:' . $this->sapRecipients . '?subject=Neue Bestellung ' . $this->item->bestellnummer . '&body='. $this->bodyText . '">'. Text::_("COM_VERWALTUNG_BESTELLUNG_SAP_SEND") . '</a>');
            } else {
                $app->setUserState('com_verwaltung.order.link', 'Kein Empfänger gesetzt!');
            }
        }
        if ($this->readonly) {
            foreach ($this->form->getFieldset('order_fields') as $field) {
                if ($this->form->getFieldAttribute($field->fieldname, 'hide', 'false') == 'true') {
                    $this->form->removeField($field->fieldname);
                }
                $this->form->setFieldAttribute($field->fieldname, 'readonly', 'true');
                $this->form->setFieldAttribute($field->fieldname, 'disabled', 'true');
            }
        }
        foreach ($this->form->getFieldset('order_fields') as $field) {
            if ($this->newVersion && $field->fieldname == "link") {
                if ($this->newVersion && isset($this->item->id) && $this->item->sapmailsent) {
                    $this->form->removeField($field->fieldname);
                }
            }
            if ($field->fieldname == 'canorder') {
                if (($this->newVersion && !Extensions::isAllowed('manage.abteilung')) || $canorder) {
                    $this->form->removeField($field->fieldname);
                } else if (!$this->newVersion) {
                    $this->form->removeField($field->fieldname);
                }
            }
            if ($this->newVersion && in_array($field->fieldname, array('anbot2','preis2', 'anbot3', 'preis3', 'anbot', 'anbotbegruendung','rechnungsdatum','rechnungsnummer','bezahlt','bezahldatum'))) {
                $this->form->removeField($field->fieldname);
            }  else if (!$this->newVersion) {
                if (in_array($field->fieldname, array('sap','sapmailsent','link'))) {
                    $this->form->removeField($field->fieldname);
                }
            }
        }
        $this->addToolBar();
        parent::display($tpl);
    }
}
