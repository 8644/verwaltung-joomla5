<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace IWF\Component\Verwaltung\Site\View\Publications;

defined('_JEXEC') or die;

use Iwf\Component\Verwaltung\Site\Helper\RouterHelper;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;

class HtmlView extends BaseHtmlView
{

    protected $isImport = false;

    protected function addToolBar()
    {
        IwfToolbarHelper::title(Text::_('COM_VERWALTUNG_PUBLIKATIONEN_LIST'), 'publikationen');
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if (Extensions::isAllowed(['manage.literatur', 'manage.fzg'])) {
            $url = RouterHelper::getAkademisRoute();
            $this->toolbar->popupButton('print', 'COM_VERWALTUNG_JTOOLBAR_PUBLIKATIONENAKADEMIS')
                ->popupType('iframe')
                ->textHeader(Text::_('COM_VERWALTUNG_JTOOLBAR_PUBLIKATIONENAKADEMIS'))
                ->url($url)
                ->modalWidth('900px')
                ->modalHeight('950px')
                ->icon('icon-list-view')
                ->title(Text::_('COM_VERWALTUNG_JTOOLBAR_PUBLIKATIONENAKADEMIS'));
        }
        if (isset($this->isAkademis)) {
            $this->toolbar->link('back', 'index.php?option=com_verwaltung&view=publications');
        } else {
            if (!isset($this->isAkademis) && !$this->isImport) {
                $model = $this->getModel();
                $publikationstyp = $model->getState('filter.publikationstyp');
                $erscheinungsjahr = $model->getState('filter.erscheinungsjahr');
                $search = $model->getState('filter.search');
                $erstautoren = $model->getState('filter.erstautoren');
                $autoren = $model->getState('filter.autoren');
                $hauptautor_forschungseinheit = $model->getState('filter.hauptautor_forschungseinheit');
                $url = RouterHelper::getAkademisZitatformRoute($publikationstyp, $search, $erscheinungsjahr, $erstautoren, $autoren, $hauptautor_forschungseinheit);
                $this->toolbar->popupButton('print', 'COM_VERWALTUNG_JTOOLBAR_PUBLIKATIONENLISTE')
                    ->popupType('iframe')
                    ->textHeader(Text::_('COM_VERWALTUNG_JTOOLBAR_PUBLIKATIONENLISTE'))
                    ->url($url)
                    ->modalWidth('900px')
                    ->modalHeight('900px')
                    ->icon('icon-eye-open')
                    ->title(Text::_('COM_VERWALTUNG_JTOOLBAR_PUBLIKATIONENLISTE'));
                
            }
            if (Extensions::isAllowed('manage.literatur')) {
                $this->toolbar->addNew('publication.add', 'JNEW');
                $this->toolbar->delete('publications.delete', 'JACTION_DELETE')
                    ->message('JGLOBAL_CONFIRM_DELETE')
                    ->listCheck(true);
            }
        }
    }

    public function display($tpl = null)
    {
        
        if (Factory::getApplication()->getInput()->get('akademis')) { // wir kommen von akademiscontroller->start
            $this->header = Extensions::getListHeader('iwfactionicon-publications-dark-xxl-blue', 'COM_VERWALTUNG_AKADEMIS_CONFIG_TITLE');
            $this->items = $this->get('Akademis'); // aus model publications
            $this->isAkademis = true;
        } else {
            $this->header = Extensions::getListHeader('iwfactionicon-publications-dark-xxl-blue', 'COM_VERWALTUNG_PUBLIKATIONEN_LIST');
            $this->items = $this->get('Items');
            $this->pagination = $this->get('Pagination');
            $this->state = $this->get('State');
            $this->filterForm = $this->get('FilterForm');
            $this->activeFilters = $this->get('ActiveFilters');
        }
        $this->addToolBar();
        parent::display($tpl);
    }
}
