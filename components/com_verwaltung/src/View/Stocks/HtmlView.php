<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace IWF\Component\Verwaltung\Site\View\Stocks;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

/** @package IWF\Component\Verwaltung\Site\View\Stocks */
class HtmlView extends BaseHtmlView
{
    protected $items;
    protected $pagination;
    protected $state;
    public $filterForm;
    public $activeFilters;
    public $toolbar;
    public $menuId;
    public $header;

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    protected function addToolBar()
    {
        if (Extensions::isAllowed('manage.inventar')) {
            IwfToolbarHelper::title(Text::_('COM_VERWALTUNG_INVENTAR_LIST'), 'inventar');
            $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
            $this->toolbar->addNew('stock.add', 'JNEW');
            $this->toolbar->delete('stocks.delete', 'JACTION_DELETE')
                ->message('JGLOBAL_CONFIRM_DELETE')
                ->listCheck(true);
        }
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    public function display($tpl = null)
    {
        Extensions::clearReturnRoute();
        $this->header = Extensions::getListHeader('iwfactionicon-inventar-dark-xxl-blue', 'COM_VERWALTUNG_INVENTAR_LIST');
        $this->menuId = Factory::getApplication()->getMenu()->getActive()->id;
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');
        $this->state = $this->get('State');
        $this->filterForm = $this->get('FilterForm');
        $this->activeFilters = $this->get('ActiveFilters');
        $this->addToolBar();
        parent::display($tpl);
    }
}
