<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace IWF\Component\Verwaltung\Site\View\Zitatliste;

defined('_JEXEC') or die;

use Exception;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;

/** @package IWF\Component\Verwaltung\Site\View\Printgastform */
class HtmlView extends BaseHtmlView
{

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     */
    function display($tpl = null) {
        $this->items = $this->get('Items');
        parent::display($tpl);
    }

}
