<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\View\Gastform;

defined('_JEXEC') or die;

use Exception;
use Iwf\Component\Verwaltung\Site\Helper\RouterHelper;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\DI\Exception\KeyNotFoundException;

/** @package Iwf\Component\Verwaltung\Site\View\Gastform */
class HtmlView extends BaseHtmlView
{

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        $this->isNew = ($this->item == false);
        IwfToolbarHelper::title($this->isNew ? Text::_('COM_VERWALTUNG_GASTFORM_CREATING') : Text::_('COM_VERWALTUNG_GASTFORM_EDITING'), 'liste');
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if ($this->isNew) {
            $this->header = Extensions::getListHeader('iwfactionicon-guestform-dark-xxl-blue', Text::_('COM_VERWALTUNG_GASTFORM_CREATING'));
        } else {
            $this->header = Extensions::getListHeader('iwfactionicon-guestform-dark-xxl-blue', Text::_('COM_VERWALTUNG_GASTFORM_EDITING'));
            $url = RouterHelper::getPrintGastformularRoute($this->item->id);
            $this->toolbar->popupButton('print', 'COM_VERWALTUNG_JTOOLBAR_PRINT')
                ->popupType('iframe')
                ->textHeader(Text::_('COM_VERWALTUNG_JTOOLBAR_PRINT'))
                ->modalWidth('750px')
                ->modalHeight('950px')
                ->url($url)
                ->icon('icon-print');
        }
        $this->toolbar->apply('gastform.apply', 'JAPPLY');
        $this->toolbar->apply('gastform.save', 'JSAVEANDCLOSE');
        $this->toolbar->cancel('gastform.cancel', 'JCANCEL');
        $this->toolbar->inlinehelp();
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function display($tpl = null)
    {
        Text::script('COM_VERWALTUNG_GASTFORM_CONFIRM_SYNC');
        $this->item = $this->get('Item');
        $this->form = $this->get('Form');
        $this->addToolBar();
        parent::display($tpl);
    }
}
