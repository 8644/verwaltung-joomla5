<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace IWF\Component\Verwaltung\Site\View\Dbquery;

defined('_JEXEC') or die;

use Exception;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Application\CMSApplicationInterface;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\DI\Exception\KeyNotFoundException;

/** @package IWF\Component\Verwaltung\Site\View\Dbquery */
class HtmlView extends BaseHtmlView
{

    /**
     * @return void 
     * @throws Exception 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        IwfToolbarHelper::title(Text::_('COM_VERWALTUNG_DBQUERY_RESULT'),'query');
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        $this->toolbar->cancel('dbquery.back', 'COM_VERWALTUNG_DBQUERY_BACK_TO_QUERY');
        $this->toolbar->cancel('dbquery.cancel', 'COM_VERWALTUNG_DBQUERY_BACK_TO_LIST');
        $this->toolbar->standardButton('clipboard', 'COM_VERWALTUNG_COPY_TO_CLIPBOARD')
            ->onclick('copySqlResult()');
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     */
    public function display($tpl = null) {
        try {
            $this->items = $this->get('Items');
        }
        catch(\RuntimeException $e) {
            Factory::getApplication()->enqueueMessage($e->getMessage(), CMSApplicationInterface::MSG_ERROR);
            $id = Factory::getApplication()->input->getInt('id');
            if ($id) {
                Factory::getApplication()->redirect('index.php?option=com_verwaltung&view=query&layout=edit&id=' . $id);
            } else {
                Factory::getApplication()->redirect('index.php?option=com_verwaltung&view=queries');
            }
            return false;
        }
        if ($this->items === false) {
            return;
        }
        $copy = [];
        if (!empty($this->getModel()->search)) {
            foreach ($this->items as $item) {
                foreach (get_object_vars($item) as $title=>$value) {
                    $found = stripos($value, $this->getModel()->search) !== false;
                    if ($found) {
                        $copy[] = $item;
                        break;
                    }
                }
            }
        }
        if (!empty($copy)) {
            $this->items = $copy;
        }
        $this->state = $this->get('State');
        $l = $this->state['list.limit'];
        if (count($this->items) > $l) {
            $this->pagination = $this->get('Pagination');
        }
        $this->header = Extensions::getListHeader('iwfactionicon-database-dark-xxl-blue', Text::_('COM_VERWALTUNG_DBQUERY_RESULT'), '"' . $this->getModel()->queryTitle . '"', 'style="font-weight:200"');
        $this->filterForm = $this->get('FilterForm');
        $this->activeFilters = $this->get('ActiveFilters');
        Text::script('COM_VERWALTUNG_DBQUERY_SQL_COPIED');
        $this->addToolBar();
        parent::display($tpl);
    }

}
