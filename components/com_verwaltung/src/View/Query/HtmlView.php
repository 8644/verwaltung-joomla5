<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace IWF\Component\Verwaltung\Site\View\Query;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\Toolbar;
use Joomla\CMS\Language\Text;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package IWF\Component\Verwaltung\Site\View\Issuedkey */
class HtmlView extends BaseHtmlView
{

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        $this->isNew = ($this->item == false);
        IwfToolbarHelper::title($this->isNew ? Text::_('COM_VERWALTUNG_QUERY_CREATING') : Text::_('COM_VERWALTUNG_QUERY_EDITING'), 'abfrage');
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if ($this->isNew) {
            $this->header = Extensions::getListHeader('iwfactionicon-database-dark-xxl-blue', Text::_('COM_VERWALTUNG_QUERY_CREATING'));
            if (Extensions::isAllowed('manage.hw')) {
                $this->toolbar->apply('query.apply', 'JAPPLY');
            }
        } else {
            $this->header = Extensions::getListHeader('iwfactionicon-database-dark-xxl-blue', Extensions::isAllowed('manage.hw') ? Text::_('COM_VERWALTUNG_QUERY_EDITING') : Text::_('COM_VERWALTUNG_QUERY_VIEWING'));
            if (Extensions::isAllowed('manage.hw')) {
                $this->toolbar->standardButton('sql', 'COM_VERWALTUNG_JTOOLBAR_QUERY', 'query.sqlexec');
                $this->toolbar->save('query.apply', 'JAPPLY');
                $saveGroup = $this->toolbar->dropdownButton('save-group');
                $saveGroup->configure(
                    function (Toolbar $childBar) {
                        $childBar->save('query.save', 'JSAVEANDCLOSE');
                        $childBar->save2copy('query.save2copy', 'JSAVEASCOPY');
                    }
                );
            }
        }
        $this->toolbar->cancel('query.cancel', 'JCANCEL');
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws InvalidArgumentException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws UnexpectedValueException 
     */
    function display($tpl = null)
    {
        $this->item  = $this->get('Item');
        $this->form = $this->get('Form');
        $this->addToolBar();
        parent::display($tpl);
    }
}
