<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace IWF\Component\Verwaltung\Site\View\Desks;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Error;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package IWF\Component\Verwaltung\Site\View\Desks */
class HtmlView extends BaseHtmlView
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     * @throws Error 
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
    }

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function addToolBar()
    {
        IwfToolbarHelper::title(Text::_('COM_VERWALTUNG_RESERVIERUNG_SCHREIBTISCH_LIST'), 'schreibtisch');
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if (Extensions::isAllowed('manage.abteilung')) {
            $this->toolbar->addNew('desk.add', 'JNEW');
            $this->toolbar->delete('desks.delete', 'JACTION_DELETE')
                ->message('JGLOBAL_CONFIRM_DELETE')
                ->listCheck(true);
        }
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    public function display($tpl = null)
    {
        $this->header = Extensions::getListHeader('iwfactionicon-desk-dark-xxl-blue', 'COM_VERWALTUNG_RESERVIERUNG_SCHREIBTISCH_LIST');
        $this->userId = Factory::getApplication()->getIdentity()->id;
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');
        $this->state = $this->get('State');
        $this->addToolBar();
        parent::display($tpl);
    }
}
