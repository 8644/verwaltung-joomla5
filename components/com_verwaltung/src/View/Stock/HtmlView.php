<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\View\Stock;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\Toolbar;
use Joomla\CMS\Language\Text;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

/** @package Iwf\Component\Verwaltung\Site\View\Stock */
class HtmlView extends BaseHtmlView
{

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        $this->isNew = ($this->item == false);
        IwfToolbarHelper::title($this->isNew ? Text::_('COM_VERWALTUNG_INVENTAR_CREATING') : Text::_('COM_VERWALTUNG_INVENTAR_EDITING'), 'inventar');
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if ($this->isNew) {
            $this->header = Extensions::getListHeader('iwfactionicon-inventar-dark-xxl-blue', Text::_('COM_VERWALTUNG_INVENTAR_CREATING'));
            if (Extensions::isAllowed('manage.inventar')) {
                $this->toolbar->apply('stock.apply', 'JAPPLY');
                $this->toolbar->apply('stock.save', 'JSAVEANDCLOSE');
            }
        } else {
            $this->header = Extensions::getListHeader('iwfactionicon-inventar-dark-xxl-blue', !Extensions::isAllowed('manage.inventar') ? Text::_('COM_VERWALTUNG_INVENTAR_VIEWING') : Text::_('COM_VERWALTUNG_INVENTAR_EDITING'));
            if (Extensions::isAllowed('manage.inventar')) {
                $this->toolbar->save('stock.apply', 'JAPPLY');
                $saveGroup = $this->toolbar->dropdownButton('save-group');
                $saveGroup->configure(
                    function (Toolbar $childBar) {
                        $childBar->save('stock.save', 'JSAVEANDCLOSE');
                        $childBar->save2copy('stock.save2copy', 'JSAVEASCOPY');
                    }
                );
            }
        }
        $this->toolbar->cancel('stock.cancel', 'JCANCEL');
        $this->toolbar->inlinehelp();
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws InvalidArgumentException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    function display($tpl = null)
    {
        Text::script('COM_VERWALTUNG_VERWALTUNG_ERROR_UNACCEPTABLE');
        Text::script('COMBOBOXINITSTRING');
        $this->item  = $this->get('Item');
        $this->form = $this->get('Form');
        $returnFlag = Factory::getApplication()->getUserState('com_verwaltung.stock.return', 0);
        switch ($returnFlag) {
            case 0: // Standard
            case 1: // von Ma-Formular - nur einmal setzen (bei speichern verhindern)
                Extensions::setReturnRoute();
                Factory::getApplication()->setUserState('com_verwaltung.stock.return', 2);
                break;
            defaut:
            break;
        }
        if (!Extensions::isAllowed('manage.inventar')) {
            foreach ($this->form->getFieldset('inventar_fields') as $field) {
                $this->form->setFieldAttribute($field->fieldname, 'readonly', 'true');
                $this->form->setFieldAttribute($field->fieldname, 'disabled', 'true');
            }
        }
        $this->addToolBar();
        parent::display($tpl);
    }
}
