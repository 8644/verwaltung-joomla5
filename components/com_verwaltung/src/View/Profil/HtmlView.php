<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace IWF\Component\Verwaltung\Site\View\Profil;

defined('_JEXEC') or die;

use Exception;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\DI\Exception\KeyNotFoundException;

/** @package IWF\Component\Verwaltung\Site\View\Profil */
class HtmlView extends BaseHtmlView
{

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        IwfToolbarHelper::title(Text::_('COM_VERWALTUNG_PROFIL_EDITING'), 'profil');
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        $this->toolbar->save('profil.apply', 'JSAVEANDCLOSE');
        $this->toolbar->cancel('profil.cancel', 'JCANCEL');
        $this->toolbar->inlinehelp();
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function display($tpl = null)
    {
        $person = Person::getInstance();
        $this->header = Extensions::getListHeader('iwfactionicon-tools-dark-xxl-blue listheader', Text::sprintf('COM_VERWALTUNG_PROFIL_EDITING', $person->name));
        $this->form = $this->get('Form');
        $this->addToolBar();
        $this->setLayout('edit');
        parent::display($tpl);
    }
}
