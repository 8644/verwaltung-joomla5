<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\View\Publication;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Error;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\Toolbar;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\View\Publication */
class HtmlView extends BaseHtmlView
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     * @throws Error 
     */
    public function __construct($config = [])
    {
        $this->readonly = !Extensions::isAllowed('manage.literatur');
        parent::__construct($config);
    }

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        $this->isNew = ($this->item == false);
        IwfToolbarHelper::title($this->isNew ? Text::_('COM_VERWALTUNG_PUBLIKATION_CREATING') : Text::_('COM_VERWALTUNG_PUBLIKATION_EDITING'), 'publikation');
        $hasTyp = count($this->form->getFieldset()) > 3; // nachdem Publikationstyp gewählt wurde
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if (!$this->readonly) {
            $this->header = Extensions::getListHeader('iwfactionicon-publication-dark-xxl-blue', $this->isNew ? Text::_('COM_VERWALTUNG_PUBLIKATION_CREATING') : Text::_('COM_VERWALTUNG_PUBLIKATION_EDITING'));
            if ($this->isNew) {
                if ($hasTyp) {
                    $this->toolbar->apply('publication.apply', 'JAPPLY');
                    $this->toolbar->apply('publication.save', 'JSAVEANDCLOSE');
                }
            } else {
                $this->toolbar->save('publication.apply', 'JAPPLY');
                $saveGroup = $this->toolbar->dropdownButton('save-group');
                $saveGroup->configure(
                    function (Toolbar $childBar) {
                        $childBar->save('publication.save', 'JSAVEANDCLOSE');
                        $childBar->save2copy('publication.save2copy', 'JSAVEASCOPY');
                    }
                );
            }
        }
        $this->toolbar->cancel('publication.cancel', 'JCANCEL');
        $this->toolbar->inlinehelp();
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function display($tpl = null)
    {
        $this->item = $this->get('Item');
        $this->form = $this->get('Form');
        if ($this->readonly) {
            foreach ($this->form->getFieldset('ptyp_fields') as $field) {
                if ($this->form->getFieldAttribute($field->fieldname, 'hide', 'false') == 'true') {
                    $this->form->removeField($field->fieldname);
                } else {
                    $this->form->setFieldAttribute($field->fieldname, 'readonly', 'true');
                    $this->form->setFieldAttribute($field->fieldname, 'disabled', 'true');
                    $this->form->setFieldAttribute($field->fieldname, 'class', 'readonly');
                }
            }
        }
        $this->addToolBar();
        parent::display($tpl);
    }
}
