<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace IWF\Component\Verwaltung\Site\View\Printorder;

defined('_JEXEC') or die;

use Exception;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\DI\Exception\KeyNotFoundException;
use stdClass;

/** @package IWF\Component\Verwaltung\Site\View\Printorder */
class HtmlView extends BaseHtmlView
{

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function display($tpl = null)
    {
        $this->form = $this->get('Form');
        $this->item = $this->get('Item');
        $this->projekt = $this->getModel()->get('projekt');
        $anbote = array('anbot1' => $this->item->preis1, 'anbot2' => (float) $this->item->preis2 == 0 ? 1E10 + 1 : (float) $this->item->preis2, 'anbot3' => (float) $this->item->preis3 == 0 ? 1E10 + 2 : (float) $this->item->preis3);
        asort($anbote);
        $this->anbote = new stdClass;
        $i = 1;
        foreach ($anbote as $anbot => $preis) {
            if ($preis >= 1E10) {
                $preis = 0;
            }
            if ($preis > 0) {
                $this->preis = $preis;
            }
            $anbotx = "anbot$i";
            $preisx = "preis$i";
            $this->anbote->$anbotx = $this->item->$anbot;
            $this->anbote->$preisx = $preis == 0 ? "" : "EUR " . number_format($preis, 2, ',', '.');
            $i++;
        }
        $this->anbot = $this->getModel()->anbot;
        $this->person = Person::getPersonByMaId($this->item->ma_id);
        parent::display($tpl);
    }
}
