<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace IWF\Component\Verwaltung\Site\View\Mas;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Error;
use Iwf\Component\Verwaltung\Site\Helper\RouterHelper;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package IWF\Component\Verwaltung\Site\View\Mas */
class HtmlView extends BaseHtmlView
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     * @throws Error 
     */
    public function __construct($config = [])
    {
        $this->readonly = !Extensions::isAllowed('edit.ma');
        parent::__construct($config);
    }

    /**
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    protected function addToolBar()
    {
        IwfToolbarHelper::title(Text::_('COM_VERWALTUNG_MITARBEITER_LIST'), 'mitarbeiter');
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if (Extensions::isAllowed('create.ma')) {
            $this->toolbar->addNew('ma.add', 'JNEW');
        }
        if (Extensions::isAllowed('delete.ma')) {
            $this->toolbar->delete('mas.delete', 'JACTION_DELETE')
                ->message('JGLOBAL_CONFIRM_DELETE')
                ->listCheck(true);
        }
        if (Extensions::isAllowed(['admin', 'manage.abteilung'])) {
            $model = $this->getModel('mas');
            $search = $model->getState('filter.search');
            $state = $model->getState('filter.state');
            $dienstverhaeltnis = $model->getState('filter.dienstverhaeltnis');
            $abteilung = $model->getState('filter.abteilung');
            $eintritt = $model->getState('filter.eintritt');
            $zimmer = $model->getState('filter.zimmer');
            $abgelaufen = $model->getState('filter.abgelaufen');
            $url = RouterHelper::getPersonalListeRoute($state, $search, $dienstverhaeltnis, $abteilung, $eintritt, $zimmer, $abgelaufen);
            $this->toolbar->popupButton('print', 'COM_VERWALTUNG_JTOOLBAR_PERSONALLISTE')
                ->popupType('iframe')
                ->textHeader(Text::_('COM_VERWALTUNG_JTOOLBAR_PERSONALLISTE'))
                ->url($url)
                ->modalWidth('70%')
                ->modalHeight('950px')
                ->icon('icon-print');
        }
        if (Extensions::isAllowed('admin')) {
            $cmd = "startLoader();Joomla.submitbutton('mas.adsync');";
            $this->toolbar->standardButton('baramundi', 'COM_VERWALTUNG_JTOOLBAR_AD_SYNC')
                ->onclick($cmd);
        }
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    public function display($tpl = null)
    {
        Extensions::clearReturnRoute();
        $this->header = Extensions::getListHeader('iwfactionicon-malefemale-dark-xxl-blue', 'COM_VERWALTUNG_MITARBEITER_LIST');
        $this->userid = Factory::getApplication()->getMenu()->getActive()->id;
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');
        $this->state = $this->get('State');
        $this->filterForm = $this->get('FilterForm');
        $this->activeFilters = $this->get('ActiveFilters');
        $this->addToolBar();
        parent::display($tpl);
    }
}
