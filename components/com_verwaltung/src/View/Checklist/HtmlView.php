<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\View\Checklist;

defined('_JEXEC') or die;

use Exception;
use Iwf\Component\Verwaltung\Site\Helper\RouterHelper;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\DI\Exception\KeyNotFoundException;

/** @package Iwf\Component\Verwaltung\Site\View\Checklist */
class HtmlView extends BaseHtmlView
{

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        $this->isNew = ($this->item == false);
        IwfToolbarHelper::title($this->isNew ? Text::_('COM_VERWALTUNG_CHECKLIST_CREATING') : Text::_('COM_VERWALTUNG_CHECKLIST_EDITING'), 'liste');
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if ($this->isNew) {
            $this->header = Extensions::getListHeader('iwfactionicon-checklist-dark-xxl-blue', Text::_('COM_VERWALTUNG_CHECKLIST_CREATING'));
        } else {
            $this->header = Extensions::getListHeader('iwfactionicon-checkist-dark-xxl-blue', Text::_('COM_VERWALTUNG_CHECKLIST_EDITING'));
            $url = RouterHelper::getPrintChecklistformularRoute($this->item->id);
            $this->toolbar->popupButton('print', 'COM_VERWALTUNG_JTOOLBAR_PRINT')
                ->popupType('iframe')
                ->textHeader(Text::_('COM_VERWALTUNG_JTOOLBAR_PRINT'))
                ->modalWidth('1000px')
                ->modalHeight('900px')
                ->url($url)
                ->icon('icon-print');
        }
        $this->toolbar->apply('checklist.apply', 'JAPPLY');
        $this->toolbar->apply('checklist.save', 'JSAVEANDCLOSE');
        $this->toolbar->cancel('checklist.cancel', 'JCANCEL');
        $this->toolbar->inlinehelp();
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function display($tpl = null)
    {
        $app = Factory::getApplication();
        $this->readonly = $app->getUserState('com_verwaltung.checklist.readonly');
        $this->item = $this->get('Item');
        $this->form = $this->get('Form');
        $this->addToolBar();
        $returnFlag = Factory::getApplication()->getUserState('com_verwaltung.checklist.return', 0);
        switch ($returnFlag) {
            case 0: // Standard
            case 1: // von Ma-Formular - nur einmal setzen (bei speichern verhindern)
                Extensions::setReturnRoute();
                Factory::getApplication()->setUserState('com_verwaltung.checklist.return', 2);
                break;
            defaut:
            break;
        }
        if ($this->readonly) {
            foreach ($this->form->getFieldsets() as $fieldset) {
                foreach ($this->form->getFieldset($fieldset->name) as $field) {
                    $this->form->setFieldAttribute($field->fieldname, 'readonly', 'true');
                    $this->form->setFieldAttribute($field->fieldname, 'disabled', 'true');
                }
            }
        }
        parent::display($tpl);
    }
}
