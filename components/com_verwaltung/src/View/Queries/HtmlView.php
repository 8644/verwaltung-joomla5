<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace IWF\Component\Verwaltung\Site\View\Queries;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Site\Helper\RouterHelper;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package IWF\Component\Verwaltung\Site\View\Queries */
class HtmlView extends BaseHtmlView
{

    /**
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function addToolBar()
    {
        IwfToolbarHelper::title(Text::_('COM_VERWALTUNG_QUERY_LIST'), 'abfragen');
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        $url = RouterHelper::getWlanUserRoute();
        if (Extensions::isAllowed('manage.hw')) {
            $this->toolbar->popupButton('wlan', 'COM_VERWALTUNG_JTOOLBAR_WLANUSER')
                ->popupType('iframe')
                ->textHeader(Text::_('COM_VERWALTUNG_JTOOLBAR_WLANUSER'))
                ->url($url)
                ->iframeWidth('400px')
                ->iframeHeight('600px')
                ->icon('icon-eye-open')
                ->title(Text::_('COM_VERWALTUNG_JTOOLBAR_WLANUSER'));
            $this->toolbar->addNew('query.add', 'JNEW');
            $this->toolbar->delete('queries.delete', 'JACTION_DELETE')
                ->message('JGLOBAL_CONFIRM_DELETE')
                ->listCheck(true);
        }
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    public function display($tpl = null)
    {
        $this->header = Extensions::getListHeader('iwfactionicon-database-dark-xxl-blue', 'COM_VERWALTUNG_QUERY_LIST');
        $this->userId = Factory::getApplication()->getIdentity()->id;
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');
        $this->state = $this->get('State');
        $this->filterForm = $this->get('FilterForm');
        $this->activeFilters = $this->get('ActiveFilters');
        $this->addToolBar();
        parent::display($tpl);
    }
}
