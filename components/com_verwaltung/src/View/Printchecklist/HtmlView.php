<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace IWF\Component\Verwaltung\Site\View\Printchecklist;

defined('_JEXEC') or die;

use Exception;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;

/** @package IWF\Component\Verwaltung\Site\View\Printchecklist */
class HtmlView extends BaseHtmlView
{

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     */
    function display($tpl = null) {
        $this->item = $this->get('Item');
        $this->form = $this->get('form');
        parent::display($tpl);
    }

}
