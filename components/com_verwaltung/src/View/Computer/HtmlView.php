<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\View\Computer;

defined('_JEXEC') or die;

use Error;
use Exception;
use InvalidArgumentException;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\Toolbar;
use Joomla\CMS\Language\Text;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\View\Computer */
class HtmlView extends BaseHtmlView
{
    
    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     * @throws Error 
     */
    public function __construct($config = [])
    {
        $this->readonly = !Extensions::isAllowed('manage.hw');
        parent::__construct($config);
    }

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        $this->isNew = ($this->item == false);
        IwfToolbarHelper::title($this->isNew ? Text::_('COM_VERWALTUNG_RECHNER_CREATING') : Text::_('COM_VERWALTUNG_RECHNER_EDITING'), 'liste');
        $this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if ($this->isNew) {
            $this->header = Extensions::getListHeader('iwfactionicon-computer-dark-xxl-blue', Text::_('COM_VERWALTUNG_RECHNER_CREATING'));
            if (Extensions::isAllowed('manage.hw')) {
                $this->toolbar->apply('computer.apply', 'JAPPLY');
                $this->toolbar->apply('computer.save', 'JSAVEANDCLOSE');
            }
        } else {
            $this->header = Extensions::getListHeader('iwfactionicon-computer-dark-xxl-blue', $this->readonly ? Text::_('COM_VERWALTUNG_RECHNER_VIEWING') : Text::_('COM_VERWALTUNG_RECHNER_EDITING'));
            if (!$this->readonly) {
                if (Extensions::isAllowed('manage.sw')) {
                    $this->toolbar->standardButton('', 'COM_VERWALTUNG_JTOOLBAR_LIZENZ', 'license.lizenz')
                        ->icon('iwficon-swlizenz-blue');
                }
                $this->toolbar->save('computer.apply', 'JAPPLY');
                $saveGroup = $this->toolbar->dropdownButton('save-group');
                $saveGroup->configure(
                    function (Toolbar $childBar) {
                        $childBar->save('computer.save', 'JSAVEANDCLOSE');
                        $childBar->save2copy('computer.save2copy', 'JSAVEASCOPY');
                    }
                );
            }
        }
        $this->toolbar->cancel('computer.cancel', 'JCANCEL');
        $this->toolbar->inlinehelp();
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws InvalidArgumentException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    function display($tpl = null)
    {
        Text::script('COM_VERWALTUNG_VERWALTUNG_ERROR_UNACCEPTABLE');
        $this->item  = $this->get('Item');
        $this->form = $this->get('Form');
        if ($this->readonly) {
            foreach ($this->form->getFieldset('inventar_fields') as $field) {
                $this->form->setFieldAttribute($field->fieldname, 'readonly', 'true');
                $this->form->setFieldAttribute($field->fieldname, 'disabled', 'true');
            }
        }
        $returnFlag = Factory::getApplication()->getUserState('com_verwaltung.computer.return', 0);
        switch ($returnFlag) {
            case 0: // Standard
            case 1: // von Computer-Formular - nur einmal setzen (bei speichern verhindern)
                Extensions::setReturnRoute();
                Factory::getApplication()->setUserState('com_verwaltung.computer.return', 2);
                break;
            defaut:
            break;
        }
        $this->addToolBar();
        parent::display($tpl);
    }
}
