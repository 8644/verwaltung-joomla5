<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Service;

defined('_JEXEC') or die;

use Exception;
use Joomla\CMS\Component\Router\RouterView;
use Joomla\CMS\Component\Router\RouterViewConfiguration;
use Joomla\CMS\Component\Router\Rules\MenuRules;
use Joomla\CMS\Component\Router\Rules\NomenuRules;
use Joomla\CMS\Component\Router\Rules\StandardRules;
use Joomla\DI\Exception\ContainerNotFoundException;
use Joomla\DI\Exception\KeyNotFoundException;

/** @package Iwf\Component\Verwaltung\Site\Service */
class Router extends RouterView
{

    /**
     * @param mixed $app 
     * @param mixed $menu 
     * @return void 
     * @throws Exception 
     * @throws ContainerNotFoundException 
     * @throws KeyNotFoundException 
     */
    public function __construct($app = null, $menu = null)
    {
        $this->registerView(new RouterViewConfiguration('ma'));
        $this->registerView(new RouterViewConfiguration('mas'));

        parent::__construct($app, $menu);

        $this->attachRule(new MenuRules($this));
        $this->attachRule(new StandardRules($this));
        $this->attachRule(new NomenuRules($this));
    }
}
