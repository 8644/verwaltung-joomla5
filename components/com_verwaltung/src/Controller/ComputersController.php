<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Controller;

defined('_JEXEC') or die;

use Exception;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfAdminController;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\Model\BaseDatabaseModel;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\Controller */
class ComputersController extends IwfAdminController
{

    /**
     * @param string $name 
     * @param string $prefix 
     * @param array $config 
     * @return BaseDatabaseModel|bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     */
    public function getModel($name = 'Computer', $prefix = 'Site', $config = array('ignore_request' => true))
    {
        return parent::getModel($name, $prefix, $config);
    }

    /**
     * @return void 
     * @throws Exception 
     */
    public function wol()
    {
        $input = $this->app->input;
        $mac = (string) $input->get('mac', "00:00:00:00:00:00", 'string');
        $response = array('result' => 0);
        $response['error'] = "";
        if (!Extensions::wakeOnLan($mac, "255.255.255.255", $errno, $errstr)) {
            $response['error'] = sprintf("Could not send WOL packet, error# %s, error=%s", $errno, $errstr);
        }
        $response['message'] = "Sent WOL packet to MAC address " . $mac;
        self::sendResponse($response);
    }

    /**
     * @param mixed $data 
     * @return void 
     * @throws Exception 
     */
    private static function sendResponse($data)
    {
        $response = (object)[];
        foreach ($data as $key => $value) {
            $response->$key = $value;
        }
        echo json_encode($response);
        Factory::getApplication()->close();
    }
}
