<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Controller;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Administrator\Helper\LogHelper;
use Iwf\Component\Verwaltung\Administrator\Helper\MailHelper;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\ActiveDirectory;
use Iwf\Verwaltung\IwfAdminController;
use Joomla\CMS\Application\CMSApplicationInterface;
use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Model\BaseDatabaseModel;
use Joomla\CMS\Router\Route;
use Joomla\Database\DatabaseInterface;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\Controller */
class MasController extends IwfAdminController
{

	/**
	 * @param string $name 
	 * @param string $prefix 
	 * @param array $config 
	 * @return BaseDatabaseModel|bool 
	 * @throws Exception 
	 * @throws UnexpectedValueException 
	 */
	public function getModel($name = 'Ma', $prefix = 'Site', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}

	/**
	 * @return void 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public function delete()
	{
		if (!Extensions::runningOnSaturn()) {
			return;
		}
		$return = Route::_('index.php?option=' . $this->option . '&view=' . $this->view_list . $this->getRedirectToListAppend(), false);
		if (Extensions::isAllowed('delete.ma')) {
			$cid = $this->app->getInput()->get('cid', [], 'Array');
			if (count($cid) > 1) {
				$this->app->enqueueMessage(Text::_('COM_VERWALTUNG_MITARBEITER_DELETE_NOT_POSSIBLE'), CMSApplicationInterface::MSG_INFO);
				$this->setRedirect($return);
				return;
			}
			$params = ComponentHelper::getParams('com_verwaltung');
			$admin_user = $params->get('admin_user');
			if (empty($admin_user)) {
				$this->app->enqueueMessage(Text::_('COM_VERWALTUNG_MITARBEITER_DELETE_NO_ADMIN_USER'), CMSApplicationInterface::MSG_ERROR);
				$this->setRedirect($return);
				return;
			}
			$ersatzAdmin = Person::getIwfUserByUserName($admin_user);
			$ersatzAdminId = $ersatzAdmin->ma_id;
			if (!$ersatzAdmin && !$ersatzAdminId) {
				$this->app->enqueueMessage(Text::_('COM_VERWALTUNG_MITARBEITER_DELETE_NO_ADMIN_USER'), CMSApplicationInterface::MSG_ERROR);
				$this->setRedirect($return);
				return;
			}
			$db = Factory::getContainer()->get(DatabaseInterface::class);
			foreach ($cid as $id) {
				$person2delete = Person::getPersonByMaId($id);
				MailHelper::adjustMailmanSubscriptions($id);
				$query = $db->createQuery()
					->update($db->qn('#__iwf_bestellungen'))
					->set($db->qn('ma_id') . '=' . $ersatzAdminId)
					->where($db->qn('ma_id') . '=:maid')
					->bind(':maid', $id, ParameterType::INTEGER);
				if ($db->setQuery($query)->execute()) {
					$count = $db->getAffectedRows();
					if ($count) {
						LogHelper::add(sprintf("%d Datensätze in Tabelle #__iwf_bestellungen von Benutzer %s (id %d) auf Benutzer %s (id %d) geändert.", $count, $person2delete->nachname, $id, $ersatzAdmin->joomla_user->name, $ersatzAdmin->joomla_user->id));
					}
				}
				$query = $db->createQuery()
					->update($db->qn('#__iwf_inventar'))
					->set($db->qn('ma_id') . '=' . $ersatzAdminId)
					->where($db->qn('ma_id') . '=:maid')
					->bind(':maid', $id, ParameterType::INTEGER);
				if ($db->setQuery($query)->execute()) {
					$count = $db->getAffectedRows();
					if ($count) {
						LogHelper::add(sprintf("%d Datensätze in Tabelle #__iwf_inventar von Benutzer %s (id %d) auf Benutzer %s (id %d) geändert.", $count, $person2delete->nachname, $id, $ersatzAdmin->joomla_user->name, $ersatzAdmin->joomla_user->id));
					}
				}
				$query = $db->createQuery()
					->delete()
					->from($db->qn('#__iwf_schluessel'))
					->where($db->qn('ma_id') . '=:maid')
					->bind(':maid', $id, ParameterType::INTEGER);
				if ($db->setQuery($query)->execute()) {
					$count = $db->getAffectedRows();
					if ($count) {
						LogHelper::add(sprintf("%d Schlüsseleinträge von Benutzer %s %s (id %d) gelöscht.", $count, $person2delete->nachname, $person2delete->vorname, $id));
					}
				}
				LogHelper::add(sprintf("%s %s aus Tabelle #__iwf_mitarbeiter entfernt", $person2delete->nachname, $person2delete->vorname));
				$this->app->enqueueMessage(sprintf("%s %s inklusive aller Referenzen entfernt", $person2delete->nachname, $person2delete->vorname), CMSApplicationInterface::MSG_INFO);
			}
			parent::delete();
			$this->setRedirect($return);
		} else {
			$this->setRedirect($return);
		}
	}

	/**
	 * @return void 
	 * @throws KeyNotFoundException 
	 * @throws Exception 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws RuntimeException 
	 * @throws InvalidArgumentException 
	 */
	public function adsync()
	{
		if (!ActiveDirectory::syncUsersWithAD()) {
			$this->app->enqueueMessage('AD Synchronisierung fehlgeschlagen!', CMSApplicationInterface::MSG_ERROR);
		}
		$this->setRedirect(
			Route::_(
				'index.php?option=' . $this->option . '&view=' . $this->view_list
					. $this->getRedirectToListAppend(),
				false
			)
		);
	}

	/**
	 * @return void 
	 * @throws Exception 
	 */
	public function personalliste()
	{
		$this->setRedirect(
			Route::_(
				'index.php?option=' . $this->option . '&view=' . $this->view_list
					. $this->getRedirectToListAppend(),
				false
			)
		);
	}

	/**
	 * @param mixed $id 
	 * @param int $value 
	 * @param int $tag 
	 * @return void 
	 * @throws KeyNotFoundException 
	 */
	public function changeStatus($id, $value = 0, $tag = 0, $return = 0)
	{
		if ($tag == 0 && $value == 0) {
			if (!$this->unpublish($id)) {
				return false;
			}
		}
		$db = Factory::getContainer()->get(DatabaseInterface::class);
		$query = $db->createQuery()
			->update($db->qn('#__iwf_mitarbeiter'));
		switch ($tag) {
			case 0:
				$query->set($db->qn('state') . ' = :status');
				break;
			case 1:
				$query->set($db->qn('swb_unterzeichnet') . ' = :status');
				break;
			case 2:
				$query->set($db->qn('bsv_unterzeichnet') . ' = :status');
				break;
		}
		$query->where($db->qn('id') . '=:id')
			->bind(':status', $value, ParameterType::INTEGER)
			->bind(':id', $id, ParameterType::INTEGER);
		$db->setQuery($query)->execute();
		return true;
	}

	/**
	 * @param DatabaseInterface $db 
	 * @param mixed $id 
	 * @param mixed $value 
	 * @return void 
	 */
	private function unpublish($id) : bool {
		$db = Factory::getContainer()->get(DatabaseInterface::class);
		$query = $db->createQuery()
			->select('*')
			->from($db->qn('#__iwf_schluessel'))
			->where($db->qn('ma_id') . '=:id')
			->bind(':id', $id, ParameterType::INTEGER);
		if ($db->setQuery($query)->execute()) {
			if ($db->getAffectedRows() > 0) {
				$this->app->enqueueMessage(Text::_('COM_VERWALTUNG_SCHLUESSEL_VORHANDEN'), CMSApplicationInterface::MSG_ERROR);
				return false;
			}
		}
		$dv = DIENSTVERHAELTNIS_GAST;
		$query->clear()
			->select
			(
				[
					$db->qn('id'),
					$db->qn('eintritt'),
					$db->qn('vertragsende')
				]
			)
			->from($db->qn('#__iwf_mitarbeiter'))
			->where($db->qn('id') . '=:id')
			->where($db->qn('dienstverhaeltnis') . '=:dv')
			->bind(':id', $id, ParameterType::INTEGER)
			->bind(':dv', $dv, ParameterType::INTEGER);
		if ($item = $db->setQuery($query)->loadObject()) {
			$query->clear()
				->select('*')
				->from($db->qn('#__iwf_gasthistorie'))
				->where($db->qn('ma_id') . '=:id')
				->where($db->qn('eintritt') . '=:eintritt')
				->where($db->qn('vertragsende') . '=:vertragsende')
				->bind(':id', $item->id)
				->bind(':eintritt', $item->eintritt)
				->bind(':vertragsende', $item->vertragsende);
			if (!$db->setQuery($query)->loadObject()) {
				$query->clear()
					->insert($db->qn('#__iwf_gasthistorie'))
					->columns($db->qn('ma_id') . ',' . $db->qn('eintritt') . ',' . $db->qn('vertragsende'))
					->values($item->id . ',"' . $item->eintritt . '","' . $item->vertragsende . '"');
				$db->setQuery($query)->execute();
			}
		}
		return true;
	}
}
