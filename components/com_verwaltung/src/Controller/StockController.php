<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Controller;

defined('_JEXEC') or die;

use Error;
use Exception;
use InvalidArgumentException;
use Joomla\CMS\MVC\Controller\FormController;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Router\Route;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;
use RuntimeException;

/** @package Iwf\Component\Verwaltung\Site\Controller */
class StockController extends FormController
{

    /**
     * @param array $data 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    protected function allowAdd($data = array())
    {
        return Extensions::isAllowed('manage.inventar');
    }

    /**
     * @param array $data 
     * @param string $key 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    protected function allowEdit($data = array(), $key = 'id')
    {
        return Extensions::isAllowed('manage.inventar');
    }

    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws RuntimeException 
     */
    public function edit($key = null, $urlVar = null)
    {
        $model = $this->getModel();
        $table = $model->getTable();
        $key = $table->getKeyName();
        if (empty($urlVar)) {
            $urlVar = $key;
        }
        $cid = $this->app->getInput()->get('cid', [], 'Array');
        $recordId = (int) (count($cid) ? $cid[0] : $this->app->getInput()->getInt($key));
        $from = $this->app->input->get('from');
        if (in_array($from, ['ma', 'computers'])) {
            $this->app->setUserState('com_verwaltung.stock.return', 1);
        }
        if ($this->allowEdit([$key => $recordId], $key)) {
            $this->app->setUserState('com_verwaltung.stock.readonly', false);
            return parent::edit($key, $urlVar);
        } else {
            $this->app->setUserState('com_verwaltung.stock.readonly', true);
            $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=' . $this->view_item
                . $this->getRedirectToItemAppend($recordId, $key), false));
        }
    }

    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Error 
     */
    public function save($key = null, $urlVar = null) {
        parent::save($key, $urlVar);
        if ($this->getTask() == 'save') {
            $this->app->setUserState('com_verwaltung.issuedkey.return', 0);
            if ($return = Extensions::getReturnRoute()) {
                $this->app->redirect($return);
            }
        }
    }

    /**
     * @param string $key 
     * @return bool 
     * @throws InvalidArgumentException 
     */
    public function cancel($key = null)
    {
        $this->app->setUserState('com_verwaltung.stock.return', 0);
        $result = parent::cancel($key);
        if ($return = Extensions::getReturnRoute()) {
            $this->app->redirect($return);
        }
        return $result;
    }
}
