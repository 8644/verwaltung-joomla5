<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Controller;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\CMS\MVC\Controller\FormController;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Application\CMSWebApplicationInterface;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Session\Session;
use Joomla\Database\DatabaseInterface;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use stdClass;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\Controller */
class GastformController extends FormController
{
    
    /**
     * @param array $data 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowAdd($data = array())
    {
        return Extensions::isAllowed('edit.gastform');
    }

    /**
     * @param array $data 
     * @param string $key 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowEdit($data = array(), $key = 'id')
    {
        return Extensions::isAllowed('edit.gastform');
    }

    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     */
    public function edit($key = null, $urlVar = null)
    {
        parent::edit($key, $urlVar);
        $this->app->enqueueMessage(Text::_('COM_VERWALTUNG_GASTFORM_INFO') . '<br /></br />' . Text::_('COM_VERWALTUNG_GASTFORM_INFO2'), CMSWebApplicationInterface::MSG_INFO);

    }
    
    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function findGastData()
    {
        $formToken = Session::getFormToken();
        $input = $this->app->input;
        $data = $input->get('jform', array(), 'array');
        $response = array('result' => 0);
        $dataObject = new stdClass;
        $fieldsFound = 0;
        foreach ($data as $value) {
            if ($value["name"] == $formToken) {
                $dataObject->$formToken = $value["value"];
                $fieldsFound++;
            } else if (preg_match('/jform\[([^]]+)\]/', $value['name'], $treffer)) {
                switch ($treffer[1]) {
                    case 'ma_id':
                        $dataObject->{$treffer[1]} = $value['value'];
                        $fieldsFound++;
                        break;
                }
            }
            if ($fieldsFound == 2) {
                break;
            }
        }
        $tokenValid = false;
        if (isset($dataObject->$formToken)) {
            $tokenValid = $dataObject->$formToken == 1;
        }
        if (!$tokenValid) {
            $response['error'] = self::renderAjaxError(Text::_('COM_VERWALTUNG_PROGRAM_AJAX_TOKEN_FEHLER'), 'error');
        } else {
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $query = $db->createQuery()
                ->select
                (
                    [
                        $db->qn('a.id'),
                        $db->qn('a.vorname'),
                        $db->qn('a.nachname'),
                        $db->qn('a.geschlecht'),
                        $db->qn('a.geburtsdatum'),
                        $db->qn('a.gastland'),
                        $db->qn('a.gastinstitution'),
                        $db->qn('a.gastaufenthaltszweck'),
                    ]
                )
                ->from($db->qn('#__iwf_mitarbeiter', 'a'))
                ->where($db->qn('a.id') . '=:id')
                ->bind(':id', $dataObject->ma_id, ParameterType::INTEGER);
            $result = $db->setQuery($query)->loadObject();
            array_walk($result, function (&$item) {
                $item = strval($item);
            });  // null wird zu ""
            if ($result->geburtsdatum == '0000-00-00') {
                $result->geburtsdatum = null;
            }
            $response["message"] = json_encode($result);
        }
        self::sendResponse($response);
    }
    
    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function syncWithMa()
    {
        $formToken = Session::getFormToken();
        $input = $this->app->input;
        $data = $input->get('jform', array(), 'array');
        $response = array('result' => 0);

        $dataObject = new stdClass;
        foreach ($data as $value) {
            if ($value["name"] == $formToken) {
                $dataObject->$formToken = $value["value"];
            } else if (preg_match('/jform\[([^]]+)\]/', $value['name'], $treffer)) {
                $dataObject->{$treffer[1]} = $value['value'];
            }
        }
        if (empty($dataObject->finanziert_aus_projekt)) {
            $dataObject->finanziert_aus_projekt = 0;
        }
        $tokenValid = false;
        if (isset($dataObject->$formToken)) {
            $tokenValid = $dataObject->$formToken == 1;
        }
        if (!$tokenValid) {
            $response['error'] = self::renderAjaxError(Text::_('COM_VERWALTUNG_GASTFORM_AJAX_TOKEN_FEHLER'), 'error');
        } else {
            $response['message'] = self::renderAjaxError(Text::_('COM_VERWALTUNG_GASTFORM_AJAX_OK'));
            $response['insert'] = 0;
            $person = Person::getInstance();
            $date = Factory::getDate('now');
            $now = $date->toSql(true);
            $bereich = 0;
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $query = $db->createQuery()
                ->select
                (
                    [
                        $db->qn('a.id'),
                        $db->qn('a.abteilung', 'bereich'),
                    ]
                )
                ->from($db->qn('#__iwf_mitarbeiter', 'a'))
                ->where($db->qn('a.id') . '=:id')
                ->bind(':id', $dataObject->betreuer_id, ParameterType::INTEGER);
            $result = $db->setQuery($query)->loadObject();
            $bereich = $result->bereich;
            $vorname = '%' . $dataObject->vorname . '%';
            $query = $db->createQuery()
                ->select('a.*')
                ->from($db->qn('#__iwf_mitarbeiter', 'a'))
                ->where
                (
                    [
                        $db->qn('a.nachname') . ' LIKE :nn',
                        $db->qn('a.vorname') . ' LIKE :vn',
                    ],
                    'AND'
                )
                ->bind(':nn', $dataObject->nachname, ParameterType::STRING)
                ->bind(':vn', $vorname, ParameterType::STRING);
            $result = $db->setQuery($query)->loadObject();
            if ($result) {
                $dataObject->ma_id = $result->id;
                $response['insert'] = 1; //weil ma_id nachgetragen bei bereits existierendem Gast
                $query = $db->createQuery();
                $query->update($db->qn('#__iwf_gastformular'))
                    ->set($db->qn('ma_id') . '=:maid')
                    ->where($db->qn('id'). '=:id')
                    ->bind(':maid', $result->id, ParameterType::INTEGER)
                    ->bind(':id', $dataObject->id, ParameterType::INTEGER);
                if ($db->execute()) {
                    $response['insert'] = 1;
                }
            } else {
                $dataObject->ma_id = 0;
            }

            $dv = DIENSTVERHAELTNIS_GAST;
            if ($dataObject->ma_id) {
                // Gast bereits vorhanden -> aktualisieren
                $query = $db->createQuery()
                    ->update($db->qn('#__iwf_mitarbeiter'))
                    ->set($db->qn('dienstverhaeltnis') . '=:dienstverhaeltnis')
                    ->set($db->qn('eintritt') . '=:eintritt')
                    ->set($db->qn('vertragsende') . '=:vertragsende')
                    ->set($db->qn('abteilung') . '=:abteilung')
                    ->set($db->qn('gastinstitution') . '=:gastinstitution')
                    ->set($db->qn('email') . '=:email')
                    ->set($db->qn('gastaufenthaltszweck') . '=:gastaufenthaltszweck')
                    ->set($db->qn('gastort') . '=:gastort')
                    ->set($db->qn('gastland') . '=:gastland')
                    ->set($db->qn('gastverantwortlicher') . '=:gastverantwortlicher')
                    ->set($db->qn('gastfinanzierung') . '=:gastfinanzierung')
                    ->set($db->qn('gastprojekt') . '=:gastprojekt')
                    ->set($db->qn('zimmer') . '=:zimmer')
                    ->set($db->qn('modified_by') . '=:modified_by')
                    ->set($db->qn('modified_time') . '=:modified_time')
                    ->set($db->qn('state') . '=1')
                    ->bind(':dienstverhaeltnis', $dv, ParameterType::INTEGER)
                    ->bind(':eintritt', $dataObject->ankunft, ParameterType::STRING)
                    ->bind(':vertragsende', $dataObject->abreise, ParameterType::STRING)
                    ->bind(':abteilung', $bereich, ParameterType::INTEGER)
                    ->bind(':gastinstitution', $dataObject->institut, ParameterType::STRING)
                    ->bind(':email', $dataObject->email, ParameterType::STRING)
                    ->bind(':gastaufenthaltszweck', $dataObject->aufenthaltszweck, ParameterType::STRING)
                    ->bind(':gastort', $dataObject->ort, ParameterType::STRING)
                    ->bind(':gastland', $dataObject->nation, ParameterType::STRING)
                    ->bind(':gastverantwortlicher', $dataObject->betreuer_id, ParameterType::INTEGER)
                    ->bind(':gastfinanzierung', $dataObject->finanzierung, ParameterType::STRING)
                    ->bind(':gastprojekt', $dataObject->finanziert_aus_projekt, ParameterType::STRING)
                    ->bind(':zimmer', $dataObject->arbeitsplatz, ParameterType::INTEGER)
                    ->bind(':modified_by', $person->joomla_id, ParameterType::INTEGER)
                    ->bind(':modified_time', $now, ParameterType::STRING)
                    ->where($db->qn('id'). '=:id')
                    ->bind(':id', $dataObject->ma_id, ParameterType::INTEGER);
                $db->setQuery($query);
                if ($db->execute()) {
                    $response['message'] = self::renderAjaxError(Text::_('COM_VERWALTUNG_GASTFORM_AJAX_OK'));
                }
            } else {
                // Gast neu anlegen!
                $query = $db->createQuery()
                ->insert($db->qn('#__iwf_mitarbeiter'))
                ->set($db->qn('dienstverhaeltnis') . '=:dienstverhaeltnis')
                ->set($db->qn('geschlecht') . '=:geschlecht')
                ->set($db->qn('nachname') . '=:nachname')
                ->set($db->qn('vorname') . '=:vorname')
                ->set($db->qn('eintritt') . '=:eintritt')
                ->set($db->qn('geburtsdatum') . '=:geburtsdatum')
                ->set($db->qn('zimmer') . '=:zimmer')
                ->set($db->qn('email') . '=:email')
                ->set($db->qn('abteilung') . '=:abteilung')
                ->set($db->qn('vertragsende') . '=:vertragsende')
                ->set($db->qn('gastinstitution') . '=:gastinstitution')
                ->set($db->qn('gastaufenthaltszweck') . '=:gastaufenthaltszweck')
                ->set($db->qn('gastort') . '=:gastort')
                ->set($db->qn('gastland') . '=:gastland')
                ->set($db->qn('gastverantwortlicher') . '=:gastverantwortlicher')
                ->set($db->qn('gastfinanzierung') . '=:gastfinanzierung')
                ->set($db->qn('gastprojekt') . '=:gastprojekt')
                ->set($db->qn('state') . '=1')
                ->set($db->qn('created_by') . '=:created_by')
                ->set($db->qn('created_time') . '=:created_time')
                ->set($db->qn('modified_by') . '=:modified_by')
                ->set($db->qn('modified_time') . '=:modified_time')
                ->set($db->qn('modified_by_fzguser_id') . '=:modified_by_fzguser_id')
                ->bind(':dienstverhaeltnis', $dv, ParameterType::INTEGER)
                ->bind(':geschlecht', $dataObject->geschlecht, ParameterType::STRING)
                ->bind(':nachname', $dataObject->nachname, ParameterType::STRING)
                ->bind(':vorname', $dataObject->vorname, ParameterType::STRING)
                ->bind(':eintritt', $dataObject->ankunft, ParameterType::STRING)
                ->bind(':geburtsdatum', $dataObject->geburtsdatum, ParameterType::STRING)
                ->bind(':zimmer', $dataObject->arbeitsplatz, ParameterType::INTEGER)
                ->bind(':email', $dataObject->email, ParameterType::STRING)
                ->bind(':abteilung', $bereich, ParameterType::INTEGER)
                ->bind(':vertragsende', $dataObject->abreise, ParameterType::STRING)
                ->bind(':gastinstitution', $dataObject->institut, ParameterType::STRING)
                ->bind(':gastaufenthaltszweck', $dataObject->aufenthaltszweck, ParameterType::STRING)
                ->bind(':gastort', $dataObject->ort, ParameterType::STRING)
                ->bind(':gastland', $dataObject->nation, ParameterType::STRING)
                ->bind(':gastverantwortlicher', $dataObject->betreuer_id, ParameterType::INTEGER)
                ->bind(':gastfinanzierung', $dataObject->finanzierung, ParameterType::STRING)
                ->bind(':gastprojekt', $dataObject->finanziert_aus_projekt, ParameterType::STRING)
                ->bind(':created_by', $person->joomla_id, ParameterType::INTEGER)
                ->bind(':created_time', $now, ParameterType::STRING)
                ->bind(':modified_by', $person->joomla_id, ParameterType::INTEGER)
                ->bind(':modified_time', $now, ParameterType::STRING)
                ->bind(':modified_by_fzguser_id', $person->ma_id, ParameterType::STRING);
                $db->setQuery($query);
                if ($db->execute()) {
                    $lastId = $db->insertid();
                    if ($lastId) {
                        $query = $db->createQuery()
                            ->update($db->qn('#__iwf_gastformular'))
                            ->set($db->qn('ma_id') . '=:ma_id')
                            ->where($db->qn('id') . '=:id')
                            ->bind(':ma_id', $lastId, ParameterType::INTEGER)
                            ->bind(':id', $dataObject->id, ParameterType::INTEGER);
                        $db->setQuery($query);
                        if ($db->execute()) {
                            $response['insert'] = 1;
                        }
                    }
                    $response['message'] = self::renderAjaxError(Text::_('COM_VERWALTUNG_GASTFORM_AJAX_OK'));
                }
            }
        }
        self::sendResponse($response);
    }

    /**
     * @param array $data 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public static function sendResponse($data = array())
    {
        $response = new stdClass;
        foreach ($data as $key => $value) {
            $response->$key = $value;
        }
        $response->token = Session::getFormToken();
        echo json_encode($response);
        Factory::getApplication()->close();
    }
    
    /**
     * @param mixed $error 
     * @param string $type 
     * @return string 
     */
    public static function renderAjaxError($error, $type = 'info')
    {
        switch (strtolower($type)) {
            case 'info':
                $heading = 'Information';
                break;
            case 'success':
                $heading = 'Nachricht';
                break;
            case 'warning':
            case 'danger':
            case 'error':
                $heading = 'Warnung';
                break;
            default:
                $heading = 'Nachricht';
                break;
        }
        return "$heading: $error";
    }
    
}


