<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Controller;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\MVC\Controller\FormController;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Router\Route;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\Controller */
class QueryController extends FormController
{

    /**
     * @param array $data 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowAdd($data = array())
    {
        return Extensions::isAllowed('manage.hw');
    }

    /**
     * @param array $data 
     * @param string $key 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowEdit($data = array(), $key = 'id')
    {
        return Extensions::isAllowed('manage.hw');
    }

    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    public function edit($key = null, $urlVar = null)
    {
        $model = $this->getModel();
        $table = $model->getTable();
        $key = $table->getKeyName();
        if (empty($urlVar)) {
            $urlVar = $key;
        }
        $cid = $this->app->getInput()->get('cid', [], 'Array');
        $recordId = (int) (count($cid) ? $cid[0] : $this->app->getInput()->getInt($key));
        $uri = $this->app->input->server->getString("HTTP_REFERER", null, 'string');
        if ($uri) {
            $this->app->setUserState('com_verwaltung.query.referer', $uri); // zurück zum Aufrufer - muss nicht die Liste sein!
        }
        if ($this->allowEdit([$key => $recordId], $key)) {
            return parent::edit($key, $urlVar);
        } else {
            $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=' . $this->view_item
                . $this->getRedirectToItemAppend($recordId, $key), false));
        }
    }

    /**
     * @return void 
     * @throws Exception 
     */
    public function sqlexec() {
        $query = $this->app->input->get('jform', array(), 'array');
        $q = base64_encode($query['query']);
        $recordId = $this->app->input->getInt('id', 0);
        $this->setRedirect(Route::_('index.php?option=com_verwaltung&view=dbquery&id=' . $recordId . '&query=' . $q, false));
    }
}