<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Controller;

defined('_JEXEC') or die;

use Error;
use Exception;
use InvalidArgumentException;
use Joomla\CMS\Factory;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfFormController;
use Joomla\CMS\Router\Route;
use Joomla\Database\DatabaseInterface;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use UnexpectedValueException;
use RuntimeException;

/** @package Iwf\Component\Verwaltung\Site\Controller */
class ComputerController extends IwfFormController
{

    /**
     * @param array $data 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    protected function allowAdd($data = array())
    {
        return Extensions::isAllowed('manage.edv');
    }

    /**
     * @param array $data 
     * @param string $key 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    protected function allowEdit($data = array(), $key = 'id')
    {
        return Extensions::isAllowed('manage.hw');
    }

    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws RuntimeException 
     */
    public function edit($key = null, $urlVar = null)
    {
        $model = $this->getModel();
        $table = $model->getTable();
        $key = $table->getKeyName();
        if (empty($urlVar)) {
            $urlVar = $key;
        }
        $cid = $this->app->getInput()->get('cid', [], 'Array');
        $recordId = (int) (count($cid) ? $cid[0] : $this->app->getInput()->getInt($key));
        $this->app->setUserState('com_verwaltung.computer.recordid', $recordId);
        $from = $this->app->input->get('from');
        if (in_array($from, ['ma', 'licenses'])) {
            $this->app->setUserState('com_verwaltung.computer.return', 1);
        }
        if ($this->allowEdit([$key => $recordId], $key)) {
            $this->app->setUserState('com_verwaltung.computer.readonly', false);
            return parent::edit($key, $urlVar);
        } else {
            $this->app->setUserState('com_verwaltung.computer.readonly', true);
            $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=' . $this->view_item
                . $this->getRedirectToItemAppend($recordId, $key), false));
        }
    }

    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Error 
     */
    public function save($key = null, $urlVar = null)
    {
        parent::save($key, $urlVar);
        if ($this->getTask() == 'save') {
            $this->app->setUserState('com_verwaltung.computer.return', 0);
            if ($return = Extensions::getReturnRoute()) {
                $this->app->redirect($return);
            }
        }
    }

    /**
     * @param string $key 
     * @return bool 
     * @throws InvalidArgumentException 
     */
    public function cancel($key = null)
    {
        $this->app->setUserState('com_verwaltung.stock.return', 0);
        $result = parent::cancel($key);
        if ($return = Extensions::getReturnRoute()) {
            $this->app->redirect($return);
        }
        return $result;
    }

    /**
     * @param mixed $id 
     * @param int $value 
     * @param int $tag 
     * @return void 
     * @throws KeyNotFoundException 
     */
    public function changeStatus($id, $value = 0, $tag = 0, $return = 0)
    {
        if ($id > 0) {
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $query = $db->createQuery();
            switch ($tag) {
                case 0:
                    $query->delete($db->qn('#__iwf_lizenzen'))
                        ->where($db->qn('id') . '=:id')
                        ->bind(':id', $id, ParameterType::INTEGER);
                    $db->setQuery($query)->execute();
                    break;
            }
        }
        $recordId = $this->app->getUserState('com_verwaltung.computer.recordid');
        if ($recordId) {
            $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend($recordId, 'id'), false));
        }
        return true;
    }
}
