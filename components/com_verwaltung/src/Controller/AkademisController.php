<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Controller;

use Exception;
use Joomla\CMS\MVC\Controller\FormController;
use Joomla\CMS\Factory;
use Joomla\CMS\Router\Route;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Controller */
class AkademisController extends FormController
{

    /**
     * @return void 
     * @throws Exception 
     */
    public function start() {
        $input = $this->app->input;
        $data = base64_encode(json_encode($input->get('jform', '', 'array')));
        // Daten mittels GET übergeben
        $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=publications&layout=akademis&akademis=1&d=' . $data));
    }

}