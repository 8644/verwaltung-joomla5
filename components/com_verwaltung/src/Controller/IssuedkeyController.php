<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Controller;

defined('_JEXEC') or die;

use Error;
use Exception;
use InvalidArgumentException;
use Joomla\CMS\MVC\Controller\FormController;
use Iwf\Verwaltung\Extensions;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\Controller */
class IssuedkeyController extends FormController
{

    /**
     * @param array $data 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowAdd($data = array())
    {
        return Extensions::isAllowed('manage.schluessel');
    }

    /**
     * @param array $data 
     * @param string $key 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowEdit($data = array(), $key = 'id')
    {
        return Extensions::isAllowed('manage.schluessel');
    }

    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     */
    public function edit($key = null, $urlVar = null) {
        $from = $this->app->input->get('from');
        if (in_array($from, ['ma'])) {
            $this->app->setUserState('com_verwaltung.stock.return', 1);
        }
        parent::edit();

    }

    public function key() {
        $this->app->input->set('id', 0);
        $this->app->setUserState('com_verwaltung.issuedkey.ma_id', $this->app->input->get->getInt('id'));
        $this->app->setUserState('com_verwaltung.issuedkey.return', 1);
        $this->edit();
    }

    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Error 
     */
    public function save($key = null, $urlVar = null)
    {
        parent::save($key, $urlVar);
        if ($this->getTask() == 'save') {
            $this->app->setUserState('com_verwaltung.issuedkey.return', 0);
            if ($return = Extensions::getReturnRoute()) {
                $this->app->redirect($return);
            }
        }
    }

    /**
     * @param string $key 
     * @return bool 
     * @throws InvalidArgumentException 
     */
    public function cancel($key = null)
    {
        $this->app->setUserState('com_verwaltung.issuedkey.return', 0);
        $result = parent::cancel($key);
        if ($return = Extensions::getReturnRoute()) {
            $this->app->redirect($return);
        }
        return $result;
    }
}
