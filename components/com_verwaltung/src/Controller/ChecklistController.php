<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Controller;

defined('_JEXEC') or die;

use Error;
use Exception;
use InvalidArgumentException;
use Joomla\CMS\MVC\Controller\FormController;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Factory;
use Joomla\CMS\Router\Router;
use Joomla\Database\DatabaseInterface;
use UnexpectedValueException;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\Exception\UnsupportedAdapterException;
use Joomla\Database\ParameterType;

/** @package Iwf\Component\Verwaltung\Site\Controller */
class ChecklistController extends FormController
{
    
    var $ma_id = 0;

    /**
     * @param array $data 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowAdd($data = [])
    {
        return Extensions::isAllowed(['manage.abteilung','manage.schluessel','manage.edv']);
    }

    /**
     * @param array $data 
     * @param string $key 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowEdit($data = [], $key = 'id')
    {
        return Extensions::isAllowed(['manage.abteilung','manage.schluessel','manage.edv']);
    }
    
    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws KeyNotFoundException 
     * @throws UnsupportedAdapterException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws RuntimeException 
     */
    public function edit($key = null, $urlVar = null) {
        // Hierher kommen wir nur über den Gasteintrag der Mitarbeitertabelle. Die zugehörige Checkliste
        // über die ma_id ermitteln und mit den Daten der letzten Version laden
        $this->app->allowCache(false);
        $model = $this->getModel();
        $table = $model->getTable();
        $key = $table->getKeyName();
        $urlVar = $key;
        // merken für späteres Editieren der Checkliste
        $this->app->setUserState('com_verwaltung.checklist.ma_id', $this->ma_id);
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select($db->qn('id'))
            ->from($db->qn('#__iwf_gastchecklisten'))
            ->where($db->qn('ma_id') . '=:id LIMIT 1')
            ->bind(':id', $this->ma_id, ParameterType::INTEGER);
        $checklistId = 0;  // Vorgabe für Checkliste: neuer Eintrag. Falls eine Checkliste existiert, wird diese geladen
        if ($result = $db->setQuery($query)->loadObject()) {
            // Dies ist die Id des Checklisten-Datensatzes
            $checklistId = $result->id;
        }
        if ($this->allowEdit(array($key => $checklistId), $key)) {
            $this->app->setUserState('com_verwaltung.checklist.readonly', false);
            $this->app->input->set($key, $checklistId);
            return parent::edit($key, $urlVar);
        } else {
            $this->app->setUserState('com_verwaltung.checklist.readonly', true);
            $this->setRedirect(Router::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend($checklistId, $key), false));
        }
        return true;
    }

    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Error 
     */
    public function save($key = null, $urlVar = null) {
        $input = $this->app->input;
        parent::save($key, $urlVar);
        if ($this->getTask() == 'save') {
            $this->app->setUserState('com_verwaltung.license.return', 0);
            if ($return = Extensions::getReturnRoute()) {
                $this->app->redirect($return);
            }
        }
    }

    /** @return void  */
    public function check() {
        $this->app->input->set('id', 0);
        $this->ma_id = $this->app->input->get->getInt('id');
        $this->app->setUserState('com_verwaltung.checklist.ma_id', $this->ma_id);
        $this->app->setUserState('com_verwaltung.checklist.return', 1);
        $this->edit();
    }

    /**
     * @param string $key 
     * @return bool 
     * @throws InvalidArgumentException 
     */
    public function cancel($key = null)
    {
        $this->app->setUserState('com_verwaltung.checklist.return', 0);
        $result = parent::cancel($key);
        if ($return = Extensions::getReturnRoute()) {
            $this->app->redirect($return);
        }
        return $result;
    }

}

