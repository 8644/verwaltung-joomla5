<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Controller;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\MVC\Controller\FormController;
use Joomla\CMS\Router\Route;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\Controller */
class PublicationController extends FormController
{

    /**
     * @param array $data 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowAdd($data = [])
    {
        return Extensions::isAllowed('manage.literatur');
    }

    /**
     * @param array $data 
     * @param string $key 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowEdit($data = [], $key = 'id')
    {
        return Extensions::isAllowed('manage.literatur');
    }

    /**
     * @return bool 
     * @throws Exception 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws KeyNotFoundException 
     */
    public function add() {
        $this->app->setUserState('com_verwaltung.edit.publication.newdata', []);
        parent::add();
    }

    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    public function edit($key = null, $urlVar = null)
    {
        $model = $this->getModel();
        $table = $model->getTable();
        $key = $table->getKeyName();
        if (empty($urlVar)) {
            $urlVar = $key;
        }
        $cid = $this->app->getInput()->get('cid', [], 'Array');
        $this->app->setUserState('com_verwaltung.edit.publication.newdata', []);
        $recordId = (int) (count($cid) ? $cid[0] : $this->app->getInput()->getInt($key));
        if ($recordId == 0) {
            $data = $this->input->post->get('jform', [], 'array');
            $this->app->setUserState('com_verwaltung.edit.publication.newdata', $data); // neuer Ma
        }
        if ($this->allowEdit([$key => $recordId], $key)) {
            $this->app->setUserState('com_verwaltung.publication.readonly', false);
            return parent::edit($key, $urlVar);
        } else {
            $this->app->setUserState('com_verwaltung.publication.readonly', true);
            $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=' . $this->view_item
                . $this->getRedirectToItemAppend($recordId, $key), false));
        }
    }

    /**
     * @param string $key 
     * @return bool 
     * @throws Exception 
     */
    public function cancel($key = null)
    {
        $this->setRedirect(Route::_('index.php?option=com_verwaltung&view=publications'));
    }

}
 