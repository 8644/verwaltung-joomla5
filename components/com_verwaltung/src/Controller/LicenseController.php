<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Controller;

defined('_JEXEC') or die;

use Error;
use Exception;
use InvalidArgumentException;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfFormController;
use Joomla\CMS\Factory;
use Joomla\CMS\Router\Route;
use Joomla\Database\DatabaseInterface;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\Controller */
class LicenseController extends IwfFormController
{

    /**
     * @param array $data 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowAdd($data = array())
    {
        return Extensions::isAllowed('manage.sw');
    }

    /**
     * @param array $data 
     * @param string $key 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowEdit($data = array(), $key = 'id')
    {
        return Extensions::isAllowed('manage.sw');
    }

    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     */
    public function edit($key = null, $urlVar = null)
    {
        $this->app->allowCache(false);
        $model = $this->getModel();
        $cid = $this->app->getInput()->get('cid', [], 'Array');
        if (empty($key)) {
            $key = $model->getTable()->getKeyName();
        }
        if (empty($urlVar)) {
            $urlVar = $key;
        }
        $recordId = (int) (count($cid) ? $cid[0] : $this->app->getInput()->getInt($key));
        $this->app->setUserState('com_verwaltung.license.rechner_id', 0); // zurück zum Aufrufer - muss nicht die Liste sein!
        if ($this->app->input->get('view') == 'computer') {  // wir kommen von einem Mitarbeiter -> neuer Eintrag
            $this->app->input->set('id', 0);
            $this->app->setUserState('com_verwaltung.license.rechner_id', $this->app->input->get->getInt('id'));
        }
        if ($this->allowEdit([$key => $recordId], $key)) {
            return parent::edit($key, $urlVar);
        } else {
            $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend($recordId, $key), false));
        }
        return true;
    }

    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Error 
     */
    public function save($key = null, $urlVar = null)
    {
        parent::save($key, $urlVar);
        if ($this->getTask() == 'save') {
            $this->app->setUserState('com_verwaltung.license.return', 0);
            if ($return = Extensions::getReturnRoute()) {
                $this->app->redirect($return);
            }
        }
    }

    /**
     * @return void 
     * @throws Exception 
     */
    public function lizenz() {
        $this->app->input->set('id', 0);
        $this->app->setUserState('com_verwaltung.license.ma_id', $this->app->input->get->getInt('id'));
        $this->app->setUserState('com_verwaltung.license.return', 1);
        $this->edit();
    }
    
    /**
     * @param string $key 
     * @return bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws KeyNotFoundException 
     * @throws Error 
     * @throws InvalidArgumentException 
     */
    public function cancel($key = null)
    {
        $this->app->setUserState('com_verwaltung.license.return', 0);
        $result = parent::cancel($key);
        if ($return = Extensions::getReturnRoute()) {
            $this->app->redirect($return);
        }
        return $result;
    }

    /**
     * @param mixed $id 
     * @param int $value 
     * @param int $tag 
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     */
    public function changeStatus($id, $value = 0, $tag = 0, $return = 0)
    {
        if ($id > 0) {
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $query = $db->createQuery();
            switch ($tag) {
                case 0:
                    $query->delete($db->qn('#__iwf_lizenzen'))
                        ->where($db->qn('id') . '=:id')
                        ->bind(':id', $id, ParameterType::INTEGER);
                    $db->setQuery($query)->execute();
                    break;
            }
        }
        if ($return) {
            $url = Extensions::getReturnRoute();
            if ($url) {
                $this->setRedirect($url);
                return true;
            }        
        }
        $recordId = $this->app->getUserState('com_verwaltung.computer.recordid');
        if ($recordId) {
            $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend($recordId, 'id'), false));
        }
        return true;
    }
}
