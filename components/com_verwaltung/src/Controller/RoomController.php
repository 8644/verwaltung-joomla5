<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Controller;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\MVC\Controller\FormController;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Router\Route;
use UnexpectedValueException;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

/** @package Iwf\Component\Verwaltung\Site\Controller */
class RoomController extends FormController
{
    
    /**
     * @param array $data 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowAdd($data = array())
    {
        return Extensions::isAllowed('create.reservierungen');
    }

    /**
     * @param array $data 
     * @param string $key 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowEdit($data = array(), $key = 'id')
    {
        $result = Extensions::isAllowed('manage.abteilung');
        if (count($data) && !$result) {
            if (Extensions::isAllowed(['edit.own', 'create.reservierungen'])) {
                if (!$data[$key]) {
                    return true;  // neuer Datensatz
                }
                $record = $this->getModel()->getItem($data[$key]);
                if ($record) {
                    $user = $this->app->getIdentity();
                    $result |= ($user->id == $record->created_by);
                }
            }
        }
        return $result;
    }
    
    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    public function edit($key = null, $urlVar = null)
    {
        $model = $this->getModel();
        $table = $model->getTable();
        $key = $table->getKeyName();
        if (empty($urlVar)) {
            $urlVar = $key;
        }
        $cid = $this->app->getInput()->get('cid', [], 'Array');
        $recordId = (int) (count($cid) ? $cid[0] : $this->app->getInput()->getInt($key));
        if ($this->allowEdit([$key => $recordId], $key)) {
            $this->app->setUserState('com_verwaltung.room.readonly', false);
            return parent::edit($key, $urlVar);
        } else {
            $this->app->setUserState('com_verwaltung.room.readonly', true);
            $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=' . $this->view_item
                . $this->getRedirectToItemAppend($recordId, $key), false));
        }
    }

    /**
     * @param string $key 
     * @return bool 
     * @throws Exception 
     */
    public function cancel($key = null) {
        $this->setRedirect(Route::_('index.php?option=com_verwaltung&view=rooms'));
        return true;
    }
}

