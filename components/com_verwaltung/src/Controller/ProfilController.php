<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Controller;

defined('_JEXEC') or die;

use Exception;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\Controller\FormController;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\Database\DatabaseInterface;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Registry\Format\Json;

/** @package Iwf\Component\Verwaltung\Site\Controller */
class ProfilController extends FormController
{
    
    /**
     * @param string $key 
     * @return bool 
     * @throws Exception 
     */
    public function cancel($key = null)
    {
        $this->setRedirect(Route::_('index.php?option=com_verwaltung&view=mas'));
    }

    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function Save($key = null, $urlVar = null)
    {
        $person = Person::getInstance();
        if ($person->ma_id) {
            $data = $this->app->getInput()->get('jform', array(), 'array');
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $json = new Json();
            $jsonData = $json->objectToString($data);
            $query = $db->createQuery()
                ->update($db->qn('#__iwf_mitarbeiter'))
                ->set($db->qn('profil') . '=:json')
                ->where($db->qn('id') . '=:id')
                ->bind(':json', $jsonData, ParameterType::STRING)
                ->bind(':id', $person->ma_id, ParameterType::INTEGER);
            if ($db->setQuery($query)->execute()) {
                $this->setMessage(Text::_('COM_VERWALTUNG_PROFIL_SAVE_SUCCESSFUL'));
            } else {
                $this->setMessage(Text::_('COM_VERWALTUNG_PROFIL_SAVE_UNSUCCESSFUL'), 'error');
            }
        }
        $this->setRedirect(Route::_('index.php?option=com_verwaltung&view=mas'));
    }
}
