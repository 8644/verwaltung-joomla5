<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Controller;

defined('_JEXEC') or die;

use Exception;
use Iwf\Component\Verwaltung\Administrator\Helper\LogHelper;
use Iwf\Verwaltung\IwfAdminController;
use Joomla\CMS\Factory;
use Joomla\Database\DatabaseInterface;
use Joomla\Database\ParameterType;
use Joomla\CMS\Log\Log;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Application\CMSApplicationInterface;
use Joomla\CMS\MVC\Model\BaseDatabaseModel;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\Controller */
class StocksController extends IwfAdminController
{

    /**
     * @param string $name 
     * @param string $prefix 
     * @param array $config 
     * @return BaseDatabaseModel|bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     */
    public function getModel($name = 'Stock', $prefix = 'Site', $config = array('ignore_request' => true))
    {
        return parent::getModel($name, $prefix, $config);
    }

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     */
    public function delete()
    {
        $cid = $this->app->getInput()->get('cid', [], 'array');
        $toDelete = [];
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        foreach ($cid as $id) {
            $query = $db->createQuery()
                ->select('*')
                ->from($db->qn('#__iwf_rechner'))
                ->where($db->qn('inventar_id') . '= :id')
                ->bind(':id', $id, ParameterType::INTEGER)
                ->setLimit(1);
            $item = $db->setQuery($query)->loadObject();
            if ($item == null) {
                $toDelete[] = $id;
            } else {
                $this->app->enqueueMessage("Inventar mit Id $id hat noch einen verknüpften Rechner", CMSApplicationInterface::MSG_WARNING);
            }
        }
        foreach ($toDelete as $id) {
            $query = $db->createQuery()
                ->delete()
                ->from($db->qn('#__iwf_inventar'))
                ->where($db->qn('id') . '=:id')
                ->bind(':id', $id, ParameterType::INTEGER);
            if ($db->setQuery($query)->execute()) {
                LogHelper::add("Inventar mit Id $id aus der Tabelle iwf23_iwf_inventar gelöscht", Log::INFO, 'delete');
            }
        }
        $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=' . $this->view_list, false));
    }
}
