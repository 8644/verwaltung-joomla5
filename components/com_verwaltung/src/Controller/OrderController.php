<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Controller;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Site\Helper\PdfHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\Controller\FormController;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Router\Route;
use Joomla\Database\DatabaseInterface;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\Controller */
class OrderController extends FormController
{

    /**
     * @param array $data 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowAdd($data = array())
    {
        return Extensions::isAllowed(['admin', 'create.bestellungen']);
    }

    /**
     * @param array $data 
     * @param string $key 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowEdit($data = array(), $key = 'id')
    {
        $result = Extensions::isAllowed('manage.abteilung');
        if ($data && !$result) {
            if (Extensions::isAllowed(['edit.own', 'create.bestellungen'])) {
                if (!$data[$key]) {
                    return true; // neuer Datensatz
                }
                $record = $this->getModel()->getItem($data[$key]);
                if ($record) {
                    $user = $this->app->getIdentity();
                    $result |= ($user->id == $record->created_by);
                }
            }
        }
        return $result;
    }

    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    public function edit($key = null, $urlVar = null)
    {
        $this->app->allowCache(false);
        $model = $this->getModel();
        $table = $model->getTable();
        $cid = $this->input->post->get('cid', array(), 'array');
        if (empty($key)) {
            $key = $table->getKeyName();
        }
        if (empty($urlVar)) {
            $urlVar = $key;
        }
        $app = $this->app;
        $recordId = (int) (count($cid) ? $cid[0] : $app->input->getInt($key));
        if ($this->allowEdit(array($key => $recordId), $key)) {
            $app->setUserState('com_verwaltung.order.readonly', false);
            return parent::edit($key, $urlVar);
        } else {
            $app->setUserState('com_verwaltung.order.readonly', true);
            $this->setRedirect(
                Route::_(
                    'index.php?option=' . $this->option . '&view=' . $this->view_item
                        . $this->getRedirectToItemAppend($recordId, $key),
                    false
                )
            );
        }
        return true;
    }

    public function createpdf() {

        $model = $this->getModel();
        $table = $model->getTable();
        if (empty($key)) {
            $key = $table->getKeyName();
        }
        if (empty($urlVar)) {
            $urlVar = $key;
        }
        $recordId = (int) $this->app->input->getInt($urlVar);
        $data = $this->app->input->post->get('jform', array(), 'array');
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery();
        $query->select($query->concatenate([$db->qn('nachname'), $db->qn('vorname')], ' ') . ' AS ma_name')
            ->from($db->qn('#__iwf_mitarbeiter'))
            ->where($db->qn('id') . '=:id')
            ->bind(':id', $data['ma_id'], ParameterType::INTEGER);
        $item = $db->setQuery($query)->loadObject();
        $name = $item->name;
        $query->clear();
        $query->select('CONCAT(' . $db->qn('projekt') . ', " (", ' . $db->qn('projektbez') . ', ")") AS projektname')
            ->from($db->qn('#__iwf_projekte'))
            ->where('id=' . $data['projekt']);
        $item = $db->setQuery($query)->loadObject();
        $projekt = $item->projektname;
        $bestellnummer = str_replace("/", "_", $data['bestellnummer']);
        $filename = $bestellnummer . '.pdf';
        $html[] = '<div>';
        $html[] = '<h3 style="text-align:center;">Bestellung ' . $bestellnummer . '</h3>';
        $html[] = '<p>&nbsp;</p><p>Bestelldatum: ' . $data['bestelldatum'] . '</p>';
        $html[] = '<p>Besteller: ' .$name . '</p>';
        $html[] = '<p>Beschreibung: ' .$data['beschreibung'] . '</p>';
        $html[] = '<p>Projekt: ' .$projekt . '</p>';
        $html[] = '<p>Firma: ' .$data['anbot1'] . '</p>';
        if (!empty($data['preis1'])) {
            $html[] = '<p>Preis (brutto): ' .$data['preis1'] . 'EUR</p>';
        }
        if ($data['eigenerklaerung'] != 'N/A') {
            $html[] = '<p>Eigenerklärung: ' .$data['eigenerklaerung'] . '</p>';
        }
        $html[] = '</div>';
        PdfHelper::bestellung2Pdf($filename , implode('', $html));
    }

}
