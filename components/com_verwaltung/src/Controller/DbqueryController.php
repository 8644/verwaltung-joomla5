<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Controller;

use Exception;
use Joomla\CMS\MVC\Controller\FormController;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Controller */
class DbqueryController extends FormController
{

    /**
     * @param string $key 
     * @return bool 
     */
    public function cancel($key = null)
    {
        $this->setRedirect('index.php?option=com_verwaltung&view=queries');
    }

    /**
     * @return void 
     * @throws Exception 
     */
    public function back()
    {
        $id = $this->app->input->getInt('id');
        if ($id) {
            $this->setRedirect('index.php?option=com_verwaltung&view=query&layout=edit&id=' . $id);
        } else {
            $this->setRedirect('index.php?option=com_verwaltung&view=queries');
        }
    }
}