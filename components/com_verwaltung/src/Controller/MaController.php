<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Site\Controller;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Error;
use Joomla\CMS\Factory;
use Iwf\Verwaltung\Extensions;
use Iwf\Component\Verwaltung\Administrator\Helper\MailHelper;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Iwf\Verwaltung\IwfFormController;
use Joomla\CMS\Application\CMSWebApplicationInterface;
use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;
use Joomla\Database\DatabaseInterface;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use UnexpectedValueException;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;

/** @package Iwf\Component\Verwaltung\Site\Controller */
class MaController extends IwfFormController
{

    /**
     * @param array $data 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    protected function allowAdd($data = array())
    {
        return Extensions::isAllowed('create.ma');
    }

    /**
     * @param array $data 
     * @param string $key 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    protected function allowEdit($data = array(), $key = 'id')
    {
        return Extensions::isAllowed('edit.ma');
    }

    /**
     * @return bool 
     * @throws Exception 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws KeyNotFoundException 
     */
    public function add()
    {
        $this->app->setUserState('com_verwaltung.edit.ma.newdata', []);
        parent::add();
    }

    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    public function edit($key = null, $urlVar = null)
    {
        $this->app->allowCache(false);
        $model = $this->getModel();
        $table = $model->getTable();
        $cid = $this->input->post->get('cid', array(), 'array');
        if (empty($key)) {
            $key = $table->getKeyName();
        }
        if (empty($urlVar)) {
            $urlVar = $key;
        }
        $this->app->setUserState('com_verwaltung.edit.ma.newdata', []);
        $this->app->setUserState('com_verwaltung.edit.ma.dv', null); 
        $recordId = (int) (count($cid) ? $cid[0] : $this->app->input->getInt($key));
        $data = $this->input->post->get('jform', [], 'array');
        if ($recordId == 0) { // neuer Ma
            $model->setState($this->getName() . '.id', 0);
            $this->app->setUserState('com_verwaltung.edit.ma.newdata', $data); // neuer Ma
        } else {
            $this->app->setUserState('com_verwaltung.edit.ma.dv', $data['dienstverhaeltnis']); // neuer Ma
        }
        $this->app->setUserState('com_verwaltung.ma.recordid', $recordId);
        if ($this->allowEdit(array($key => $recordId), $key)) {
            $this->app->setUserState('com_verwaltung.ma.readonly', false);
            return parent::edit($key, $urlVar);
        } else {
            $this->app->setUserState('com_verwaltung.ma.readonly', true);
            $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend($recordId, $key), false));
        }
        return true;
    }

    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws Error 
     */
    public function save($key = null, $urlVar = null)
    {
        $task = $this->getTask();
        $data = $this->input->post->get('jform', [], 'array');
        if ($task == 'save2copy') {
            $data['nachname'] = $data['nachname'] . '_kopie';
            $data['email'] = '';
            $data['samaccountname'] = '';
        }
        $model = $this->getModel();
        $isNew = isset($data['new']) || (isset($data['id']) && $data['id'] == 0);
        $haveData = !isset($data['new']);
        if (Extensions::isAllowed('edit.ma')) {
            $new_mailman_lists = [];
            $old_mailman_lists = [];
            if ($data['state'] > 0 && isset($data['abteilung']) && in_array($data['abteilung'], explode(",", MAILMAN_ABTEILUNGEN))) {
                if (isset($data['mailabteilung']) && count($data['mailabteilung'])) {
                    $new_mailman_lists = MailHelper::getMailmanListen($data['mailabteilung']);
                    $data['mailabteilung'] = implode(',', $data['mailabteilung']);
                } else {
                    $data['mailabteilung'] = ""; // muss "" gesetzt werden, damit Eintrag gespeichert wird
                }
                // falls keine neuen Listen, bleiben auch alte Listen leer - es erfolg dann Löschung aus allen Listen
                if ($new_mailman_lists) {
                    $oldData = $model->getItem($data['id']);
                    $old_mailman_lists = MailHelper::getMailmanListen($oldData->mailabteilung);
                }
            } else {
                $data['mailabteilung'] = "";
            }
        }
        $this->app->input->post->set('jform', $data);
        $result = parent::save($key, $urlVar);
        if ($result) {
            MailHelper::adjustMailmanSubscriptions($data['id'], $old_mailman_lists, $new_mailman_lists);
            if ($isNew && $haveData) {
                $params = ComponentHelper::getParams('com_verwaltung');
                $edvEmail = $params->get('edv_email');
                if ($edvEmail) {
                    $dv = 'unknown';
                    $db = Factory::getContainer()->get(DatabaseInterface::class);
                    $query = $db->createQuery()
                        ->select($db->qn('inhalt'))
                        ->from($db->qn('#__iwf_listen'))
                        ->where($db->qn('id') . '=:dv')
                        ->bind(':dv', $data['dienstverhaeltnis'], ParameterType::INTEGER);
                    $item = $db->setQuery($query)->loadObject();
                    $dv = $item->inhalt;
                    $person = Person::getInstance();
                    $message = 'Nur zur Information - einfach löschen, falls kein Bedarf :)' . CRLF . CRLF;
                    $message .= Text::sprintf('Neuer Personaleintrag: %s %s%s', $data['nachname'], $data['vorname'], CRLF);
                    $message .= Text::sprintf('Angelegt von: %s%s', $person->name, CRLF);
                    $message .= Text::sprintf('Dienstverhältnis: %s%s', $dv, CRLF);
                    MailHelper::sendMail($edvEmail, 'Neue Person wurde angelegt', $message);
                }
            }
        } else {
            if ($isNew) {
                $this->app->getMessageQueue(true);
            }
        }
        return $result;
    }

    /**
     * @param mixed $id 
     * @param int $value 
     * @param int $tag 
     * @return void 
     * @throws KeyNotFoundException 
     */
    public function changeStatus($id, $value = 0, $tag = 0, $return = 0)
    {
        if ($id > 0) {
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $query = $db->createQuery();
            switch ($tag) {
                case 0:
                    $query->delete($db->qn('#__iwf_rechner'))
                        ->where($db->qn('id') . '=:id')
                        ->bind(':id', $id, ParameterType::INTEGER);
                    $db->setQuery($query)->execute();
                    $query->clear()
                        ->delete($db->qn('#__iwf_lizenzen'))
                        ->where($db->qn('rechner_id') . '=:r_id')
                        ->bind(':r_id', $id, ParameterType::INTEGER);
                    $db->setQuery($query)->execute();
                    $this->app->enqueueMessage('Rechner mit zugehörigen Lizenzen entfernt.', CMSWebApplicationInterface::MSG_INFO);
                    break;
                case 1:
                    $query->delete($db->qn('#__iwf_lizenzen'))
                        ->where($db->qn('id') . '=:id')
                        ->bind(':id', $id, ParameterType::INTEGER);
                    $db->setQuery($query)->execute();
                    break;
                case 2:
                    $query->delete($db->qn('#__iwf_schluessel'))
                        ->where($db->qn('id') . '=:id')
                        ->bind(':id', $id, ParameterType::INTEGER);
                    $db->setQuery($query)->execute();
                    break;
            }
        }
        $recordId = $this->app->getUserState('com_verwaltung.ma.recordid');
        if ($recordId) {
            $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=' . $this->view_item . $this->getRedirectToItemAppend($recordId, 'id'), false));
        }
        return true;
    }
}
