<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Iwf\Verwaltung\IwfPrintButton;
use Joomla\CMS\HTML\HTMLHelper;

$editIcon = '<span class="fa fa-print" aria-hidden="true"></span>';
?>
<?php echo (new IwfPrintButton('print', 'printform();', $editIcon, 'row'))->render(); ?>
<div class="row">
    <table class="print">
        <thead class="print">
            <th colspan="3">
                Bestellformular - <?php echo $this->person->abteilung ?>
            </th>
        </thead>
        <tbody class="print">
            <tr>
                <td class="line1">Bestellzeichen:</td>
                <td class="line2" colspan="2">
                    <?php echo $this->item->bestellnummer ?>
                </td>
            </tr>
            <tr>
                <td class="line1">Besteller:</td>
                <td class="line2" width="76%" valign="top" colspan="2">
                    <?php echo $this->person->name ?>
                </td>
            </tr>
            <tr>
                <td class="line1">Bestelldatum:</td>
                <td class="line2" colspan="2">
                    <?php echo HTMLHelper::_('date', $this->item->bestelldatum, 'd.m.Y'); ?>
                </td>
            </tr>
            <tr>
                <td class="line1">Anbot Bestbieter:</td>
                <td class="line2" colspan="2">
                    <?php echo $this->anbote->anbot1 ?>
                </td>
            </tr>
            <tr>
                <td class="preis">vorauss. Preis:</td>
                <td class="line2" colspan="2">
                    <?php echo $this->anbote->preis1 ?>
                </td>
            </tr>
            <?php if (!empty($this->anbote->anbot2)):?>
            <tr>
                <td class="line1">Zweitgereihtes Anbot*:</td>
                <td class="line2" colspan="2">
                    <?php echo $this->anbote->anbot2 ?>
                </td>
            </tr>
            <tr>
                <td class="preis">vorauss. Preis:</td>
                <td class="line2" colspan="2">
                    <?php echo $this->anbote->preis2 ?>
                </td>
            </tr>
            <?php endif;?>
            <?php if (!empty($this->anbote->anbot3)):?>
            <tr>
                <td class="line1">Drittgereihtes Anbot*:</td>
                <td class="line2" colspan="2">
                    <?php echo $this->anbote->anbot3 ?>
                </td>
            </tr>
            <tr>
                <td class="preis">vorauss. Preis:</td>
                <td class="line2" colspan="2">
                    <?php echo $this->anbote->preis3 ?>
                </td>
            </tr>
            <?php endif;?>
            <?php if (!empty($this->anbote->anbot2) || !empty($this->anbote->anbot3)):?>
            <tr id="anbot">
                <td class="line1">gewähltes Anbot:</td>
                <td class="line2" colspan="2">
                    <?php echo $this->anbot ?>
                </td>
            </tr>
            <?php endif; ?>
            <tr>
                <td class="line1">
                    Begründung, falls
                    <br />nicht Bestbieter:
                </td>
                <td class="line2" colspan="2">
                    <?php echo $this->item->anbotbegruendung ?>
                </td>
            </tr>
            <?php if ($this->preis >= 5000): ?>
                <tr>
                    <td class="line1">Eigenerklärung ab EUR 5000 brutto:</td>
                    <td class="line2" colspan="2">
                        <?php echo $this->item->eigenerklaerung ?>
                    </td>
                </tr>
            <?php endif; ?>
            <tr>
                <td class="line1">Ansprechpartner:</td>
                <td class="line2" colspan="2">
                    <?php echo $this->item->kontaktperson ?>
                </td>
            </tr>
            <tr>
                <td class="line1">Artikel:</td>
                <td class="line2" colspan="2">
                    <?php echo $this->item->beschreibung ?>
                </td>
            </tr>
            <tr>
                <td class="line1" style="border-bottom:2px solid black;">Finanzierung:</td>
                <td class="line2" colspan="2" style="border-bottom: 2px solid black;">
                    <?php echo $this->projekt->projekt ?>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <span id="nb">* die Anbote liegen zur Einsicht auf</span>
                </td>
            </tr>
            <tr>
                <td class="genehmigung" width="30%">
                    Genehmigt
                    <br />vom Projektleiter:
                </td>
                <td class="genehmigung" width="28%">&nbsp;</td>
                <td class="genehmigung" width="48%">
                    Genehmigt vom
                    <br />Abteilungsleiter:
                </td>
            </tr>
            <tr>
                <td class="spacer" colspan="3">&nbsp;</td>
            </tr>
            <tr>
                <td class="wichtig" colspan="3">
                    Ab einer Auftrags- /Bestellsumme con €5.000,- sind zumindest zwei und ab €25.000,- zumindest drei (Vergleichs-)Angebote einzuholen.
                    <b>Die Angebote sind jeweils vollständig zu dokumentieren.</b>
                </td>
            </tr>
        </tbody>
    </table>
</div>