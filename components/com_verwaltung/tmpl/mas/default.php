<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Uri\Uri;
use Iwf\Verwaltung\IwfActionButton;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\Plugin\Actionlog\Joomla\Extension\Joomla;
use Iwf\Component\Verwaltung\Site\Helper\RouterHelper;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfSpinningWheel;

/** @var \Joomla\CMS\WebAsset\WebAssetManager $wa */
$wa = $this->document->getWebAssetManager();
$wa->useScript('table.columns')
    ->useScript('multiselect')
    ->useScript('actionbutton');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
$editIcon = '<span class="fa fa-pen-square me-2" aria-hidden="true"></span>';
?>
<?php echo IwfToolbarHelper::render($this->toolbar); ?>
<?php echo $this->header; ?>
<form action="<?php echo htmlspecialchars(Uri::getInstance()->toString()); ?>" method="post" name="adminForm" id="adminForm">
    <?php echo (new IwfSpinningWheel('loader'))->render('Synchronisiere User mit AD...');?>
    <div class="row">
        <div class="col-md-12">
            <div id="j-main-container" class="j-main-container">
                <?php echo LayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
                <?php if (empty($this->items)) : ?>
                    <div class="alert alert-info">
                        <span class="icon-info-circle" aria-hidden="true"></span>
                        <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                    </div>
                <?php else : ?>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col" class="w-1 d-none d-md-table-cell" <?php if (!Extensions::isAllowed('delete.ma')) : ?>style="display:none !important" <?php endif; ?>>
                                    <?php echo HTMLHelper::_('grid.checkall'); ?>
                                </th>
                                <th scope="col" class="w-10 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_MITARBEITER_HEADING_NAME', 'm.nachname', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_MITARBEITER_HEADING_ZIMMER', 'z.zimmer', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-20 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_MITARBEITER_HEADING_ABTEILUNG', 'a.abteilung', $listDirn, $listOrder) ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_MITARBEITER_HEADING_TEL', 'm.tel', $listDirn, $listOrder) ?>
                                </th>
                                <?php if (Extensions::isAllowed(['edit.ma'])) : ?>
                                    <th scope="col" class="w-2 d-none d-md-table-cell">
                                        <?php echo Text::_('COM_VERWALTUNG_MITARBEITER_HEADING_PUBLISHED') ?>
                                    </th>
                                    <th scope="col" class="w-2 d-none d-md-table-cell">
                                        <?php echo Text::_('COM_VERWALTUNG_MITARBEITER_HEADING_SWB') ?>
                                    </th>
                                    <th scope="col" class="w-2 d-none d-md-table-cell">
                                        <?php echo Text::_('COM_VERWALTUNG_MITARBEITER_HEADING_BSV') ?>
                                    </th>
                                    <th scope="col" class="w-2 d-none d-md-table-cell">
                                        <?php echo Text::_('COM_VERWALTUNG_MITARBEITER_HEADING_HASKEY') ?>
                                    </th>
                                    <th scope="col" class="w-2 d-none d-md-table-cell">
                                        <?php echo Text::_('COM_VERWALTUNG_MITARBEITER_HEADING_DELETED') ?>
                                    </th>
                                <?php endif; ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($this->items as $i => $item) :
                                $expired = "";
                                $title = "";
                                if (in_array($item->dv, explode(',', ECHTE_DIENSTVERHAELTNISSE))) {
                                    if ($item->vertrag_abgelaufen && $item->state) {
                                        $expired = "contract_expired";
                                        $title = 'Vertrag abgelaufen';
                                    }
                                    if (!$item->state && !$item->deleted) {
                                        $expired = "user_inactive";
                                        $title = 'User deaktiviert';
                                    }
                                    if ($item->deleted) {
                                        $expired = "missing_ad";
                                        $title = 'Kein AD Konto';
                                    }
                                }
                            ?>
                                <tr class="<?php echo $expired ?>">
                                    <td class="d-none d-md-table-cell" <?php if (!Extensions::isAllowed('delete.ma')) : ?>style="display:none !important" <?php endif; ?>>
                                        <?php echo HTMLHelper::_('grid.id', $i, $item->id, false, 'cid', 'cb'); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo RouterHelper::getMaEditRoute($item->id, $item->name, $title, $editIcon); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->zimmer; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->abteilung; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->tel; ?>
                                    </td>
                                    <?php if (Extensions::isAllowed(['edit.ma'])) : ?>
                                        <td class="d-none d-md-table-cell">
                                            <?php echo (new IwfActionButton($this, 0))->render((int) $item->state, $i, true, ['id' => $item->id, 'task_prefix' => 'mas.', 'tip' => false, 'disabled' => "$this->readonly"]); ?>
                                        </td>
                                        <td class="d-none d-md-table-cell">
                                            <?php echo (new IwfActionButton($this, 1, null, null, 'COM_VERWALTUNG_MAS_SWB_0', 'COM_VERWALTUNG_MAS_SWB_1'))->render((int) $item->swb, $i, true, ['id' => $item->id, 'task_prefix' => 'mas.', 'disabled' => "$this->readonly"]); ?>
                                        </td>
                                        <td class="d-none d-md-table-cell">
                                            <?php echo (new IwfActionButton($this, 2, null, null, 'COM_VERWALTUNG_MAS_BSV_0', 'COM_VERWALTUNG_MAS_BSV_1'))->render((int) $item->bsv, $i, true, ['id' => $item->id, 'task_prefix' => 'mas.', 'disabled' => "$this->readonly"]); ?>
                                        </td>
                                        <td class="d-none d-md-table-cell">
                                            <?php echo (new IwfActionButton($this, 3, 'key-disable', 'key-success', "COM_VERWALTUNG_MAS_NOKEY", "$item->haskey" . Text::_('COM_VERWALTUNG_MAS_KEYS')))->render((int) $item->haskey, $i, true, ['id' => $item->id, 'tip' => false, 'disabled' => 'true']); ?>
                                        </td>
                                        <td class="d-none d-md-table-cell">
                                            <?php echo (new IwfActionButton($this, 4, 'empty', 'sad-success', "", "COM_VERWALTUNG_MAS_DELETED"))->render((int) $item->deleted, $i, true, ['id' => $item->id, 'disabled' => 'true']); ?>
                                        </td>
                                    <?php endif; ?>

                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
                <?php echo $this->pagination->getListFooter(); ?>
                <input type="hidden" name="actionButton" id="actionButton" value="">
                <input type="hidden" name="task" value="">
                <input type="hidden" name="boxchecked" value="0">
                <?php echo HTMLHelper::_('form.token'); ?>
            </div>
        </div>
    </div>
</form>