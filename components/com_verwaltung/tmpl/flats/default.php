<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Iwf\Component\Verwaltung\Site\Helper\RouterHelper;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Uri\Uri;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\Plugin\Actionlog\Joomla\Extension\Joomla;

/** @var \Joomla\CMS\WebAsset\WebAssetManager $wa */
$wa = $this->document->getWebAssetManager();
$wa->useScript('table.columns')
    ->useScript('multiselect');

$editIcon = '<span class="fa fa-pen-square me-2" aria-hidden="true"></span>';
?>
<?php echo IwfToolbarHelper::render($this->toolbar); ?>
<?php echo $this->header; ?>
<form action="<?php echo htmlspecialchars(Uri::getInstance()->toString()); ?>" method="post" name="adminForm" id="adminForm">
    <div class="row">
        <div class="col-md-12">
            <div id="j-main-container" class="j-main-container">
                <?php if (empty($this->items)) : ?>
                    <div class="alert alert-info">
                        <span class="icon-info-circle" aria-hidden="true"></span>
                        <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                    </div>
                <?php else : ?>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col" class="w-1 d-none d-md-table-cell" <?php if (!Extensions::isAllowed('manage.abteilung')) : ?>style="display:none !important" <?php endif; ?>>
                                    <?php echo HTMLHelper::_('grid.checkall'); ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_RESERVIERUNG_HEADING_WOHNUNG'); ?>
                                </th>
                                <th scope="col" class="w-3 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_RESERVIERUNG_HEADING_VON'); ?>
                                </th>
                                <th scope="col" class="w-3 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_RESERVIERUNG_HEADING_BIS'); ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_RESERVIERUNG_HEADING_GAST'); ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_RESERVIERUNG_HEADING_EINLADENDER') ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_RESERVIERUNG_HEADING_RESERIERT_VON') ?>
                                </th>
                                <th scope="col" class="w-20 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_RESERVIERUNG_HEADING_VORMERK') ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $n = count($this->items);
                            foreach ($this->items as $i => $reservierung) : ?>
                                <?php $title = sprintf (Text::_('COM_VERWALTUNG_RESERVIERUNG_TITLE_RESEVIERTVON'), $reservierung->name, HTMLHelper::_('date', $reservierung->vorgemerkt, 'd.m.Y')); ?>
                                <tr class="row<?php echo $i % 2; ?>">
                                    <td class="d-none d-md-table-cell" <?php if (!Extensions::isAllowed('manage.abteilung')) : ?>style="display:none !important" <?php endif; ?>>
                                        <?php if (Extensions::isAllowed('manage.abteilung')) : ?>
                                        <?php echo HTMLHelper::_('grid.id', $i, $reservierung->id); ?>
                                        <?php endif; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell nobreak" title="<?php echo $title ?>">
                                        <?php echo RouterHelper::getFlatEditRoute($reservierung->id, $reservierung->wohnung, $title, $editIcon); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo HTMLHelper::_('date', $reservierung->datumvon, 'd.m.Y');?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo HTMLHelper::_('date', $reservierung->datumbis, 'd.m.Y');?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $reservierung->gastname; if ($reservierung->gast2) { echo ' / ' . $reservierung->gast2; } ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $reservierung->name; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell nobreak">
                                        <?php echo Person::getJoomlaUserById($reservierung->owner)->name; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo HTMLHelper::_('date', $reservierung->vorgemerkt, 'd.m.Y'); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
                <?php echo $this->pagination->getListFooter(); ?>
                <input type="hidden" name="task" value="">
                <input type="hidden" name="boxchecked" value="0">
                <?php echo HTMLHelper::_('form.token'); ?>
            </div>
        </div>
    </div>
</form>