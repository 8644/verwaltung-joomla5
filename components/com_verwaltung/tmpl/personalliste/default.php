<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

use Iwf\Verwaltung\IwfPrintButton;
use Joomla\CMS\Language\Text;

defined('_JEXEC') or die('Restricted Access');

$editIcon = '<span class="fa fa-print" aria-hidden="true"></span>';
?>
<div class="row">
    <div class="col-md-12">
        <div id="j-main-container" class="j-main-container">
            <?php if (empty($this->items)) : ?>
                <div class="alert alert-info">
                    <span class="icon-info-circle" aria-hidden="true"></span>
                    <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                </div>
            <?php else : ?>
                <?php echo (new IwfPrintButton('print', 'printform();', $editIcon, 'row', 'landscape'))->render(); ?>
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="w-10 d-none d-md-table-cell">
                                <?php echo Text::_('COM_VERWALTUNG_PERSONALLISTE_HEADING_NACHNAME'); ?>
                            </th>
                            <th class="w-10 d-none d-md-table-cell">
                                <?php echo Text::_('COM_VERWALTUNG_PERSONALLISTE_HEADING_VORNAME'); ?>
                            </th>
                            <th class="w-20 d-none d-md-table-cell">
                                <?php echo Text::_('COM_VERWALTUNG_PERSONALLISTE_HEADING_ABTEILUNG'); ?>
                            </th>
                            <th class="w-15 d-none d-md-table-cell">
                                <?php echo Text::_('COM_VERWALTUNG_PERSONALLISTE_HEADING_DIENSTVERHAELTNIS'); ?>
                            </th>
                            <th class="w-5 d-none d-md-table-cell">
                                <?php echo Text::_('COM_VERWALTUNG_PERSONALLISTE_HEADING_ZIMMER'); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($this->items as $i => $person) : ?>
                            <tr class="row<?php echo $i % 2; ?>">
                                <td class="d-none d-md-table-cell">
                                    <?php echo $person->nachname; ?>
                                </td>
                                <td class="d-none d-md-table-cell">
                                    <?php echo $person->vorname; ?>
                                </td>
                                <td class="d-none d-md-table-cell">
                                    <?php echo $person->abteilung ?>
                                </td>
                                <td class="d-none d-md-table-cell">
                                    <?php echo $person->dienstverhaeltnis ?>
                                </td>
                                <td class="d-none d-md-table-cell">
                                    <?php echo $person->zimmer ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>
