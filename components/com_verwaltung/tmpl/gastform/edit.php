<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 * 
 * 
 */

defined('_JEXEC') or die('Restricted Access');

use IWF\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;

$wa = $this->getDocument()->getWebAssetManager();
$wa->useScript('keepalive')
    ->useScript('form.validate')
    ->useScript('syncguest');
$id = $this->item->id ?? 0;
?>
<?php echo $this->header; ?>
<?php echo IwfToolbarHelper::render($this->toolbar); ?>
<form action="<?php echo Route::_('index.php?option=com_verwaltung&layout=edit&id=' . (int) $id); ?>" method="post" name="adminForm" id="item-form" class="form-validate">
    <div class="main-card">
        <?php echo HTMLHelper::_('uitab.startTabSet', 'myTab', ['active' => 'details', 'recall' => true, 'breakpoint' => 768]); ?>
        <?php echo HTMLHelper::_('uitab.addTab', 'myTab', 'details', Text::_('COM_VERWALTUNG_GASTFORM_DETAILS')); ?>
        <div class="row">
            <div class="col-lg-6">
                <fieldset class="options-form">
                    <legend><?php echo Text::_('COM_VERWALTUNG_GASTFORM_DETAILS'); ?></legend>
                    <div>
                        <?php echo $this->form->renderFieldset('gf_fields'); ?>
                        <?php if (Extensions::isAllowed('manage.institut')): ?>
                            <div class="control-group">
                                <div class="control-label">
                                    <label class="required" id="jform_updatema">
                                        <span class="star">
                                            <?php echo Text::_('COM_VERWALTUNG_GASTFORM_SYNC'); ?></span>
                                    </label>
                                </div>
                                <div class="controls">
                                    <input type="button" class="btn btn-primary" id="sync_ma" value="<?php echo Text::_('COM_VERWALTUNG_GASTFORM_SYNC_BUTTON'); ?>" />
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </fieldset>
            </div>
        </div>
        <?php echo HTMLHelper::_('uitab.endTab'); ?>
        <?php echo HTMLHelper::_('uitab.addTab', 'myTab', 'publishing', Text::_('COM_VERWALTUNG_ADDITIONAL_INFOS', true)); ?>
        <div class="row">
            <div class="col-lg-6">
                <fieldset class="options-form">
                    <legend><?php echo Text::_('COM_VERWALTUNG_ADDITIONAL_INFOS'); ?></legend>
                    <div>
                        <?php echo $this->form->renderFieldset('gf_fields2'); ?>
                    </div>
                </fieldset>
            </div>
        </div>
        <?php echo HTMLHelper::_('uitab.endTab'); ?>
        <?php echo HTMLHelper::_('uitab.endTabSet'); ?>
        <input type="hidden" name="task" value="" />
        <?php echo HTMLHelper::_('form.token'); ?>
    </div>
</form>