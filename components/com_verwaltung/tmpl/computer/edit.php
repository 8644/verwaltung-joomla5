<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Iwf\Component\Verwaltung\Site\Helper\MitarbeiterHelper;
use Iwf\Component\Verwaltung\Site\Helper\RouterHelper;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfActionButton;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;
use Iwf\Verwaltung\IwfToolbarHelper;

$wa = $this->getDocument()->getWebAssetManager()
  ->useScript('keepalive')
  ->useScript('form.validate')
  ->useScript('actionbutton');
$editIcon = '<span class="fa fa-pen-square me-2" aria-hidden="true"></span>';
$id = $this->item->id ?? 0;
?>
<?php echo $this->header; ?>
<?php echo  IwfToolbarHelper::render($this->toolbar); ?>
<form action="<?php echo Route::_('index.php?option=com_verwaltung&view=computer&layout=edit&id=' . (int) $id); ?>" method="post" name="adminForm" id="item-form" class="form-validate">
  <div class="main-card">
    <?php echo HTMLHelper::_('uitab.startTabSet', 'myTab', ['active' => 'details', 'recall' => true, 'breakpoint' => 768]); ?>
    <?php echo HTMLHelper::_('uitab.addTab', 'myTab', 'details', Text::_('COM_VERWALTUNG_RECHNER_DETAILS')); ?>
    <div class="row">
      <div class="col-lg-6">
        <fieldset class="options-form">
          <legend><?php echo Text::_('COM_VERWALTUNG_RECHNER_DETAILS'); ?></legend>
          <div>
            <?php echo $this->form->renderFieldset('rechner_fields'); ?>
          </div>
        </fieldset>
      </div>
    </div>
    <?php echo HTMLHelper::_('uitab.endTab'); ?>
    <?php echo HTMLHelper::_('uitab.addTab', 'myTab', 'computer', Text::_('COM_VERWALTUNG_MITARBEITER_LIZENZEN')); ?>
    <div class="row">
      <div class="col-lg-6">
        <fieldset class="options-form">
          <legend><?php echo Text::_('COM_VERWALTUNG_MITARBEITER_LIZENZEN'); ?></legend>
          <div>
            <?php $licenses = MitarbeiterHelper::getLicenses($this->item->id); ?>
            <?php if (empty($licenses)) : ?>
              <div class="alert alert-info">
                <span class="icon-info-circle" aria-hidden="true"></span>
                <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
              </div>
            <?php else : ?>
              <table class="table table-striped table-hover">
                <thead>
                  <th scope="col" class="w-1 d-md-table-cell" style="display:none !important"></th>
                  <th class="w-50 d-md-table-cell"><?php echo Text::_('COM_VERWALTUNG_LIZENZEN_FIELD_SOFTWAREID_LABEL'); ?></th>
                  <th class="w-50 d-md-table-cell"></th>
                </thead>
                <tbody>
                  <?php foreach ($licenses as $i => $license) : ?>
                    <tr>
                      <td class="d-none d-md-table-cell" style="display:none !important">
                        <?php echo HTMLHelper::_('grid.id', $i, $item->id, false, 'cid', 'cb'); ?>
                      </td>
                      <td class="d-none d-md-table-cell">
                        <?php echo RouterHelper::getLicenseEditRoute($license->id, $license->produkt, "", $editIcon); ?>
                      </td>
                      <td class="d-none d-md-table-cell">
                        <?php if (Extensions::isAllowed('manage.sw')) : ?>
                          <?php echo (new IwfActionButton($this, 0, 'unpublish-success', 'unpublish-success'))->render(1, $i, true, ['id' => $license->id, 'task_prefix' => 'license.', 'return' => true, 'message' => Text::_('COM_VERWALTUNG_RECHNER_DELETE_WARNING')]); ?>
                        <?php else: ?>
                          &nbsp;
                        <?php endif; ?>
                      </td>
                    </tr>
                  <?php endforeach; ?>
                </tbody>
              </table>
            <?php endif; ?>
          </div>
        </fieldset>
      </div>
    </div>
    <?php echo HTMLHelper::_('uitab.endTab'); ?>
    <?php echo HTMLHelper::_('uitab.endTabSet'); ?>
    <input type="hidden" name="task" value="" />
    <input type="hidden" name="actionButton" id="actionButton" value="" askpermission="1">
    <input type="hidden" name="boxchecked" value="0">
    <?php echo HTMLHelper::_('form.token'); ?>
  </div>
</form>