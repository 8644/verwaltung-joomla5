<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;
use Iwf\Verwaltung\IwfToolbarHelper;

$wa = $this->getDocument()->getWebAssetManager();
$wa->useScript('keepalive');
$wa->useScript('form.validate');
$wa->useScript('editablecombobox');
$id = $this->item->id ?? 0;
?>
<?php echo $this->header; ?>
<?php echo  IwfToolbarHelper::render($this->toolbar); ?>
<form action="<?php echo Route::_('index.php?option=com_verwaltung&layout=edit&id=' . (int) $id); ?>" method="post" name="adminForm" id="item-form" class="form-validate">
  <div class="main-card">
    <?php echo HTMLHelper::_('uitab.startTabSet', 'myTab', ['active' => 'details', 'recall' => true, 'breakpoint' => 768]); ?>
    <?php echo HTMLHelper::_('uitab.addTab', 'myTab', 'details', Text::_('COM_VERWALTUNG_INVENTAR_DETAILS')); ?>
    <div class="row">
      <div class="col-lg-6">
        <fieldset class="options-form">
          <legend><?php echo Text::_('COM_VERWALTUNG_INVENTAR_DETAILS'); ?></legend>
          <div>
            <?php echo $this->form->renderFieldset('inventar_fields'); ?>
          </div>
        </fieldset>
      </div>
    </div>
    <?php echo HTMLHelper::_('uitab.endTab'); ?>
    <?php echo HTMLHelper::_('uitab.endTabSet'); ?>
    <input type="hidden" name="task" value="" />
    <?php echo HTMLHelper::_('form.token'); ?>
  </div>
</form>