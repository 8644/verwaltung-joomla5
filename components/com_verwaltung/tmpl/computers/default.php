<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Uri\Uri;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\Plugin\Actionlog\Joomla\Extension\Joomla;
use Iwf\Component\Verwaltung\Site\Helper\RouterHelper;
use Iwf\Verwaltung\Extensions;

/** @var \Joomla\CMS\WebAsset\WebAssetManager $wa */
$wa = $this->document->getWebAssetManager();
$wa->useScript('table.columns')
    ->useScript('multiselect');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
$editIcon = '<span class="fa fa-pen-square me-2" aria-hidden="true"></span>';
?>
<?php echo IwfToolbarHelper::render($this->toolbar); ?>
<?php echo $this->header; ?>
<form action="<?php echo htmlspecialchars(Uri::getInstance()->toString()); ?>" method="post" name="adminForm" id="adminForm">
    <div class="row">
        <div class="col-md-12">
            <div id="j-main-container" class="j-main-container">
                <?php echo LayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
                <?php if (empty($this->items)) : ?>
                    <div class="alert alert-info">
                        <span class="icon-info-circle" aria-hidden="true"></span>
                        <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                    </div>
                <?php else : ?>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col" class="w-1 d-none d-md-table-cell" <?php if (!Extensions::isAllowed('manage.hw')) : ?>style="display:none !important" <?php endif; ?>>
                                    <?php echo HTMLHelper::_('grid.checkall'); ?>
                                </th>
                                <th scope="col" class="w-4 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_RECHNER_HEADING_HOSTNAME', 'd.hostname', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_RECHNER_HEADING_WNAME', 'd.wname', $listDirn, $listOrder); ?>
                                </th>
                                <?php if (Extensions::isAllowed('manage.hw')): ?>
                                    <th scope="col" class="w-10 d-none d-md-table-cell">
                                        <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_RECHNER_HEADING_BESITZER', 'm.nachname', $listDirn, $listOrder) ?>
                                    </th>
                                <?php endif; ?>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_RECHNER_HEADING_IPNUMMER', 'd.ipnummer', $listDirn, $listOrder) ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_RECHNER_HEADING_MAC', 'd.mac', $listDirn, $listOrder) ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_RECHNER_HEADING_STANDORT', 'z.zimmer', $listDirn, $listOrder) ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_RECHNER_HEADING_INVENTARTYP', 'l.inhalt', $listDirn, $listOrder) ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_RECHNER_HEADING_OS', 'a.os', $listDirn, $listOrder) ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_RECHNER_HEADING_INVENTARNUMMER', 'i.inventarnummer', $listDirn, $listOrder) ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_RECHNER_HEADING_NUM_LICENSES', 'lizenzen', $listDirn, $listOrder) ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($this->items as $i => $item) :
                                $title = $item->titel;
                                if (!is_null($item->baramundi) && $item->baramundi != '0000-00-00 00:00:00') {
                                    $title .= ', ' . HTMLHelper::date($item->baramundi, Text::_('d.m.Y'));
                                }
                            ?>
                                <tr>
                                    <td class="d-none d-md-table-cell" <?php if (!Extensions::isAllowed('manage.hw')) : ?>style="display:none !important" <?php endif; ?>>
                                        <?php echo HTMLHelper::_('grid.id', $i, $item->id, false, 'cid', 'cb'); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo RouterHelper::getComputerEditRoute($item->id, $item->hostname, $title, $editIcon); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo strtoupper($item->wname); ?>
                                    </td>
                                    <?php if (Extensions::isAllowed('manage.hw')) : ?>
                                        <td class="d-none d-md-table-cell">
                                            <?php echo $item->name; ?>
                                        </td>
                                    <?php endif; ?>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->ipnummer; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php if ($this->person->nachname === $item->nachname || Extensions::isAllowed('manage.hw')) : ?>
                                            <a href="#" id="macaddress_<?php echo $item->name; ?>" title="Click to wake up" style="color:blue; cursor:pointer"><?php echo $item->mac; ?></a>
                                        <?php else : ?>
                                            <?php echo $item->mac; ?>
                                        <?php endif; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->zimmer; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->inventartyp; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell" title="<?php echo $item->name ?>">
                                        <?php echo substr($item->os, 0, 23); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo RouterHelper::getInventarEditRoute($item->inventar_id, $item->inventarnummer, $title, $editIcon, 'computers'); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->lizenzen; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
                <?php echo $this->pagination->getListFooter(); ?>
                <input type="hidden" name="actionButton" id="actionButton" value="">
                <input type="hidden" name="task" value="">
                <input type="hidden" name="boxchecked" value="0">
                <?php echo HTMLHelper::_('form.token'); ?>
            </div>
        </div>
    </div>
</form>