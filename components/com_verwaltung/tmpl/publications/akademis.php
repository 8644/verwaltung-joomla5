<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

use Iwf\Verwaltung\IwfToolbarHelper;

defined('_JEXEC') or die('Restricted Access');

?>
<?php echo IwfToolbarHelper::render($this->toolbar); ?>

<div class="main-card">
    <div class="row">
        <div class="col-lg-12">

            <fieldset class="options-form">
                <div class="form-grid">
                    <?php foreach ($this->items as $item) : ?>
                        <?php echo $item; ?>
                    <?php endforeach; ?>
                </div>
            </fieldset>
        </div>
    </div>
</div>
