<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Iwf\Component\Verwaltung\Site\Helper\RouterHelper;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Uri\Uri;
use Iwf\Verwaltung\IwfToolbarHelper;

$wa = $this->document->getWebAssetManager();
$wa->useScript('table.columns')
    ->useScript('multiselect');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
$editIcon = '<span class="fa fa-pen-square me-2" aria-hidden="true"></span>';
?>
<?php echo IwfToolbarHelper::render($this->toolbar); ?>
<?php echo $this->header; ?>
<form action="<?php echo htmlspecialchars(Uri::getInstance()->toString()); ?>" method="post" name="adminForm" id="adminForm">
    <div class="row">
        <div class="col-md-12">
            <div id="j-main-container" class="j-main-container">
                <?php echo LayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
                <?php if (empty($this->items)) : ?>
                    <div class="alert alert-info">
                        <span class="icon-info-circle" aria-hidden="true"></span>
                        <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                    </div>
                <?php else : ?>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col" class="w-1 d-none d-md-table-cell" <?php if (!Extensions::isAllowed('manage.inventar')) : ?>style="display:none !important"<?php endif;?>>
                                    <?php echo HTMLHelper::_('grid.checkall'); ?>
                                </th>
                                <th scope="col" class="w-1 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_INVENTAR_HEADING_INVENTARNUMMER', 'a.inventarnummer', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-15 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_INVENTAR_HEADING_BESCHREIBUNG', 'a.beschreibung', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-2 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_INVENTAR_HEADING_TYP', 'a.inventartyp', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_INVENTAR_HEADING_FIRMA', 'a.firma', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-2 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_INVENTAR_HEADING_STANDORT', 'z.zimmer', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-2 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_INVENTAR_HEADING_RECHNER', 'd.hostname', $listDirn, $listOrder) ?>
                                </th>
                                <th scope="col" class="w-2 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_INVENTAR_HEADING_BESTELLNUMMER', 'b.bestellnummer', $listDirn, $listOrder) ?>
                                </th>
                                <th scope="col" class="w-2 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_INVENTAR_HEADING_ANLAGEDATUM', 'a.created_time', $listDirn, $listOrder); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($this->items as $i => $item) :
                                $title = $item->name;
                                if ($item->bestellnummer) {
                                    $title .= ' (' . $item->bestellnummer . ', ' . $item->bestelldatum . ')';
                                }
                                $beschreibung = "";
                                if ($item->deinventarisiert) {
                                    if (strlen($item->beschreibung) > 38) {
                                        $beschreibung = $item->beschreibung . ', deinventarisiert am ' . HTMLHelper::_('date', $item->deinventarisierungsdatum, 'd.m.Y');
                                    } else {
                                        $beschreibung = 'Deinventarisiert am ' . HTMLHelper::_('date', $item->deinventarisierungsdatum, 'd.m.Y');
                                    }
                                } else if (strlen($item->beschreibung) > 38) {
                                    $beschreibung = $item->beschreibung;
                                }
                                ?>
                                <tr>
                                    <td class="d-none d-md-table-cell" <?php if (!Extensions::isAllowed('manage.inventar')) : ?>style="display:none !important"<?php endif;?>>
                                        <?php echo HTMLHelper::_('grid.id', $i, $item->id, false, 'cid', 'cb'); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo RouterHelper::getInventarEditRoute($item->id, $item->inventarnummer, $title, $editIcon); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell" title="<?php echo $beschreibung ?>">
                                        <?php echo $item->beschreibung; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->inhalt; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->firma; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->zimmer; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo RouterHelper::getComputerEditRoute($item->rechner_id, $item->hostname, "", $editIcon); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->bestellnummer; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo HTMLHelper::_('date', $item->created_time, 'd.m.Y'); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                    </table>
                <?php endif; ?>
                <?php echo $this->pagination->getListFooter(); ?>
                <input type="hidden" name="task" value="">
                <input type="hidden" name="boxchecked" value="0">
                <?php echo HTMLHelper::_('form.token'); ?>
            </div>
        </div>
    </div>
</form>