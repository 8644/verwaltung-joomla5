<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Iwf\Component\Verwaltung\Site\Helper\RouterHelper;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Uri\Uri;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\Plugin\Actionlog\Joomla\Extension\Joomla;
use Joomla\CMS\Factory;

/** @var \Joomla\CMS\WebAsset\WebAssetManager $wa */
$wa = $this->document->getWebAssetManager();
$wa->useScript('table.columns')
    ->useScript('multiselect');

$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
$editIcon = '<span class="fa fa-pen-square me-2" aria-hidden="true"></span>';
?>
<?php echo IwfToolbarHelper::render($this->toolbar); ?>
<?php echo $this->header; ?>
<form action="<?php echo htmlspecialchars(Uri::getInstance()->toString()); ?>" method="post" name="adminForm" id="adminForm">
    <div class="row">
        <div class="col-md-12">
            <div id="j-main-container" class="j-main-container">
                <?php echo LayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
                <?php if (empty($this->items)) : ?>
                    <div class="alert alert-info">
                        <span class="icon-info-circle" aria-hidden="true"></span>
                        <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                    </div>
                <?php else : ?>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col" class="w-1 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('grid.checkall'); ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_RESERVIERUNG_HEADING_VERANSTALTUNG', 'a.veranstaltung', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-3 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_RESERVIERUNG_HEADING_RAUM', 'l.inhalt', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_RESERVIERUNG_HEADING_DATUM', 'a.datum', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-1 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_RESERVIERUNG_HEADING_BEGINN'); ?>
                                </th>
                                <th scope="col" class="w-1 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_RESERVIERUNG_HEADING_ENDE') ?>
                                </th>
                                <th scope="col" class="w-8 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_RESERVIERUNG_HEADING_ABTEILUNG', 'm.abteilung', $listDirn, $listOrder) ?>
                                </th>
                                <th scope="col" class="w-20 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_RESERVIERUNG_HEADING_PERIODIC', 'a.wiederholungsintervall', $listDirn, $listOrder) ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $n = count($this->items);
                            foreach ($this->items as $i => $reservierung) : ?>
                                <?php $title = sprintf (Text::_('COM_VERWALTUNG_RESERVIERUNG_TITLE_RESEVIERTVON'), $reservierung->name, HTMLHelper::_('date', $reservierung->vorgemerkt, 'd.m.Y')); ?>
                                <tr class="row<?php echo $i % 2; ?>">
                                    <td class="d-none d-md-table-cell">
                                        <?php if (Extensions::isAllowed('manage.abteilung') || !$reservierung->readonly) : ?>
                                        <?php echo HTMLHelper::_('grid.id', $i, $reservierung->id); ?>
                                        <?php endif; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell nobreak" title="<?php echo $title ?>">
                                        <?php echo RouterHelper::getRoomEditRoute($reservierung->id, $reservierung->veranstaltung, $reservierung->readonly, $title, $editIcon); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell nobreak">
                                        <?php echo $reservierung->raum; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $this->wochentage[HTMLHelper::_('date', $reservierung->datum, 'w')];?>
                                        <?php echo HTMLHelper::_('date', $reservierung->datum, 'd.m.Y');?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo HTMLHelper::_('date', strtotime($reservierung->beginn.Factory::getApplication()->getConfig()->get('offset')), 'H:i') ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('date', strtotime($reservierung->ende.Factory::getApplication()->getConfig()->get('offset')), 'H:i') ?>
                                    </td>
                                    <td class="d-none d-md-table-cell nobreak">
                                        <?php echo $reservierung->abteilung; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php 
                                            if ($reservierung->wiederholungsintervall == 63) {
                                                if (Date('Y-m-d') < $reservierung->datum) {
                                                    echo sprintf( TEXT::_('COM_VERWALTUNG_RESERVIERUNG_HEADING_WEEKLY'), date('d.m.Y', strtotime($reservierung->datum)));
                                                }
                                                else {
                                                    echo sprintf( TEXT::_('COM_VERWALTUNG_RESERVIERUNG_HEADING_WEEKLY'), date('d.m.Y', strtotime($reservierung->datum . " +7 day")));
                                                }
                                            } else if ($reservierung->wiederholungsintervall == 348) {
                                                if (Date('Y-m-d') < $reservierung->datum) {
                                                    echo sprintf( TEXT::_('COM_VERWALTUNG_RESERVIERUNG_HEADING_WEEKLY'), date('d.m.Y', strtotime($reservierung->datum)));
                                                }
                                                else {
                                                    echo sprintf( TEXT::_('COM_VERWALTUNG_RESERVIERUNG_HEADING_BIWEEKLY'), date('d.m.Y', strtotime($reservierung->datum . " +14 day")));
                                                }
                                            }
                                        ?>
	                                </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
                <?php echo $this->pagination->getListFooter(); ?>
                <input type="hidden" name="task" value="">
                <input type="hidden" name="boxchecked" value="0">
                <?php echo HTMLHelper::_('form.token'); ?>
            </div>
        </div>
    </div>
</form>