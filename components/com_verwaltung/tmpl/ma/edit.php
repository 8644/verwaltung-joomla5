<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Iwf\Component\Verwaltung\Site\Helper\MitarbeiterHelper;
use Iwf\Component\Verwaltung\Site\Helper\RouterHelper;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfActionButton;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;
use Iwf\Verwaltung\IwfToolbarHelper;

$wa = $this->document->getWebAssetManager()
  ->useScript('keepalive')
  ->useScript('form.validate')
  ->useScript('actionbutton');
$editIcon = '<span class="fa fa-pen-square me-2" aria-hidden="true"></span>';
$id = $this->item->id ?? 0;
?>
<?php echo $this->header; ?>
<?php echo  IwfToolbarHelper::render($this->toolbar); ?>
<form action="<?php echo Route::_('index.php?option=com_verwaltung&view=ma&layout=edit&id=' . (int) $id); ?>" method="post" name="adminForm" id="item-form" class="form-validate">
  <div class="main-card">
    <?php echo HTMLHelper::_('uitab.startTabSet', 'myTab', ['active' => 'general', 'recall' => true, 'breakpoint' => 768]); ?>
    <?php echo HTMLHelper::_('uitab.addTab', 'myTab', 'general', Text::_('COM_VERWALTUNG_MITARBEITER_DETAILS')); ?>
    <div class="row">
      <div class="col-lg-6">
        <fieldset class="options-form">
          <legend><?php echo Text::_('COM_VERWALTUNG_MITARBEITER_DETAILS'); ?></legend>
          <div>
            <?php echo $this->form->renderFieldset('ma_fields'); ?>
          </div>
        </fieldset>
      </div>
    </div>
    <?php echo HTMLHelper::_('uitab.endTab'); ?>
    <?php if (!$this->isNew) : ?>
      <?php $items = MitarbeiterHelper::getComputer($this->item->id); ?>
      <?php if ($items): ?>
        <?php echo HTMLHelper::_('uitab.addTab', 'myTab', 'computer', Text::_('COM_VERWALTUNG_MITARBEITER_RECHNER') . '/' . Text::_('COM_VERWALTUNG_MITARBEITER_LIZENZEN')); ?>
        <div class="row">
          <div class="col-lg-7">
            <fieldset class="options-form">
              <legend><?php echo Text::_('COM_VERWALTUNG_MITARBEITER_RECHNER') . '/' . Text::_('COM_VERWALTUNG_MITARBEITER_LIZENZEN'); ?></legend>
              <div>
                <?php if (empty($items)) : ?>
                  <div class="alert alert-info">
                    <span class="icon-info-circle" aria-hidden="true"></span>
                    <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                  </div>
                <?php else : ?>
                  <table class="table table-disable">
                    <thead>
                      <th scope="col" class="w-1 d-md-table-cell" style="display:none !important"></th>
                      <th scope="col" class="w-20 d-md-table-cell">Rechner</th>
                      <th scope="col" class="w-20 d-md-table-cell">IP-Nummer</th>
                      <th scope="col" class="w-60 d-md-table-cell">Zimmer</th>
                      <th scope="col" class="d-md-table-cell"></th>
                    </thead>
                    <tbody>
                      <?php foreach ($items as $i => $item) : ?>
                        <tr>
                          <td class="d-none d-md-table-cell" style="display:none !important">
                            <?php echo HTMLHelper::_('grid.id', $i, $item->id, false, 'cid', 'cb'); ?>
                          </td>
                          <td class="d-none d-md-table-cell">
                            <?php echo RouterHelper::getComputerEditRoute($item->id, $item->wname, "", $editIcon, 'ma'); ?>
                          </td>
                          <td class="d-none d-md-table-cell">
                            <?php echo $item->ipnummer; ?>
                          </td>
                          <td class="d-none d-md-table-cell">
                            <?php echo $item->zimmer; ?>
                          </td>
                          <td class="d-none d-md-table-cell">
                            <?php if ($this->managehw) : ?>
                              <?php echo (new IwfActionButton($this, 0, 'purge-success', 'purge-success'))->render(1, $i, true, ['id' => $item->id, 'task_prefix' => 'ma.', 'message' => Text::_('COM_VERWALTUNG_RECHNER_DELETE_WARNING')]); ?>
                            <?php else: ?>
                              &nbsp;
                            <?php endif; ?>
                          </td>
                        </tr>
                        <?php $licenses = MitarbeiterHelper::getLicenses($item->id); ?>
                        <tr>
                          <?php if ($licenses) : ?>
                            <td colspan="4">
                              <table class="table table-light table-striped">
                                <thead>
                                  <th class="w-30 d-md-table-cell"></th>
                                  <th class="w-30 d-md-table-cell"></th>
                                </thead>
                                <tbody>
                                  <?php foreach ($licenses as $i => $license) : ?>
                                    <tr>
                                      <td class="d-none d-md-table-cell">
                                        <?php echo RouterHelper::getLicenseEditRoute($license->id, $license->produkt, "", $editIcon); ?>
                                      </td>
                                      <td class="d-none d-md-table-cell">
                                        <?php if ($this->managesw) : ?>
                                          <?php echo (new IwfActionButton($this, 1, 'unpublish-warning', 'unpublish-warning'))->render(1, $i, true, ['id' => $license->id, 'task_prefix' => 'ma.', 'message' => Text::_('COM_VERWALTUNG_RECHNER_DELETE_WARNING')]); ?>
                                        <?php else: ?>
                                          &nbsp;
                                        <?php endif; ?>
                                      </td>
                                    </tr>
                                  <?php endforeach; ?>
                                </tbody>
                              </table>
                            </td>
                        </tr>
                      <?php endif; ?>
                      </td>
                    <?php endforeach; ?>
                    </tbody>
                  </table>
                <?php endif; ?>
              </div>
            </fieldset>
          </div>
        </div>
        <?php echo HTMLHelper::_('uitab.endTab'); ?>
      <?php endif; ?>
      <?php $items = MitarbeiterHelper::getKeys($this->item->id); ?>
      <?php if ($items) : ?>
        <?php echo HTMLHelper::_('uitab.addTab', 'myTab', 'keys', Text::_('COM_VERWALTUNG_MITARBEITER_SCHLUESSEL')); ?>
        <div class="row">
          <div class="col-lg-6">
            <fieldset class="options-form">
              <legend><?php echo Text::_('COM_VERWALTUNG_MITARBEITER_SCHLUESSEL'); ?></legend>
              <div>
                <?php if (empty($items)) : ?>
                  <div class="alert alert-info">
                    <span class="icon-info-circle" aria-hidden="true"></span>
                    <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                  </div>
                <?php else : ?>
                  <table class="table table-striped table-hover">
                    <thead>
                      <tr>
                        <th scope="col" class="w-1 d-none d-md-table-cell" style="display:none !important">
                          <?php echo HTMLHelper::_('grid.checkall'); ?>
                        </th>
                        <th scope="col" class="w-2 d-none d-md-table-cell">
                          <?php echo Text::_('Schlüssel') ?>
                        </th>
                        <th scope="col" class="w-20 d-none d-md-table-cell">
                          &nbsp;
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($items as $i => $item) : ?>
                        <tr>
                          <td class="d-none d-md-table-cell" style="display:none !important">
                            <?php echo HTMLHelper::_('grid.id', $i, $item->id, false, 'cid', 'cb'); ?>
                          </td>
                          <td class="d-none d-md-table-cell">
                            <?php echo RouterHelper::getIssuedkeyEditRoute($item->id, $item->bezeichnung, "", $editIcon, 'ma'); ?>
                          </td>
                          <td>
                            <?php if ($this->manageschluessel) : ?>
                              <?php echo (new IwfActionButton($this, 2, 'purge-success', 'purge-success'))->render(1, $i, true, ['id' => $item->id, 'task_prefix' => 'ma.', 'message' => Text::_('COM_VERWALTUNG_RECHNER_DELETE_WARNING')]); ?>
                            <?php else: ?>
                              &nbsp;
                            <?php endif; ?>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                <?php endif; ?>
              </div>
            </fieldset>
          </div>
        </div>
        <?php echo HTMLHelper::_('uitab.endTab'); ?>
      <?php endif; ?>
      <?php $items = MitarbeiterHelper::getStock($this->item->id); ?>
      <?php if ($items) : ?>
        <?php echo HTMLHelper::_('uitab.addTab', 'myTab', 'stock', Text::_('COM_VERWALTUNG_MITARBEITER_INVENTAR')); ?>
        <div class="row">
          <div class="col-lg-6">
            <fieldset class="options-form">
              <legend><?php echo Text::_('COM_VERWALTUNG_MITARBEITER_INVENTAR'); ?></legend>
              <div>
                <?php if (empty($items)) : ?>
                  <div class="alert alert-info">
                    <span class="icon-info-circle" aria-hidden="true"></span>
                    <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                  </div>
                <?php else : ?>
                  <table class="table table-striped table-hover">
                    <thead>
                      <tr>
                        <th scope="col" class="w-2 d-none d-md-table-cell">
                          <?php echo Text::_('Inventarnummer') ?>
                        </th>
                        <th scope="col" class="w-20 d-none d-md-table-cell">
                          <?php echo Text::_('Beschreibung') ?>
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php foreach ($items as $i => $item) : ?>
                        <tr>
                          <td class="d-none d-md-table-cell">
                            <?php echo RouterHelper::getInventarEditRoute($item->id, $item->inventarnummer, "", $editIcon, 'ma'); ?>
                          </td>
                          <td class="d-none d-md-table-cell">
                            <?php echo $item->beschreibung; ?>
                          </td>
                        </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
                <?php endif; ?>
              </div>
            </fieldset>
          </div>
        </div>
        <?php echo HTMLHelper::_('uitab.endTab'); ?>
      <?php endif; ?>
      <?php if (Extensions::isAllowed('manage.abteilung')) : ?>
        <?php if ($this->form->getFieldset('ma_fields2')) : ?>
          <?php echo HTMLHelper::_('uitab.addTab', 'myTab', 'publishing', Text::_('COM_VERWALTUNG_ADDITIONAL_INFOS')); ?>
          <div class="row">
            <div class="col-lg-6">
              <fieldset class="options-form">
                <legend><?php echo Text::_('COM_VERWALTUNG_ADDITIONAL_INFOS'); ?></legend>
                <div>
                  <?php echo $this->form->renderFieldset('ma_fields2'); ?>
                </div>
              </fieldset>
            </div>
          </div>
          <?php echo HTMLHelper::_('uitab.endTab'); ?>
        <?php endif; ?>
      <?php endif; ?>
    <?php endif; ?>
    <?php echo HTMLHelper::_('uitab.endTabSet'); ?>
    <input type="hidden" name="task" value="" />
    <?php echo HTMLHelper::_('form.token'); ?>
    <input type="hidden" name="actionButton" id="actionButton" value="" askpermission="1">
    <input type="hidden" name="boxchecked" value="0">
  </div>
</form>