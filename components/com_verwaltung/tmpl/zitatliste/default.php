<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Joomla\CMS\HTML\HTMLHelper;

$typ = "";
if (!$this->items) {
    echo "Keine Zitate zu gewählten Kriterien gefunden.";
}
?>
<?php foreach ($this->items as $i => $item):
    if ($item->typ != $typ)  // nächster Publikationstyp
    {
        $typ = $item->typ;
?>
    <h2 class="zitat"><?php echo $typ; ?></h2>
    <?php
    }
    ?>
    <p class="zitat">
        <span class="zitat"><?php echo $item->zitat; ?></span>
    </p>
<?php
endforeach; ?>