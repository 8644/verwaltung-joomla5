<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

use Iwf\Verwaltung\IwfPrintButton;
use Joomla\CMS\Language\Text;

defined('_JEXEC') or die('Restricted Access');

$editIcon = '<span class="fa fa-print" aria-hidden="true"></span>';
?>
<style>
    body {font-family: "Courier New"}
    .table thead th {border-bottom-width: 0px !important;}
    .row {padding-left:10px;} 
    .fa-print {font-size:xx-large;padding: 20px 0;}
    td>fieldset, td>fieldset>div.btn-group {margin:0}
    .btn:disabled,.btn.disabled {display:none !important}
    tr:has(textarea:placeholder-shown) {display:none !important}
    tr:has(input:placeholder-shown) {display:none !important}
    .radio label {background-color: transparent !important;color: black !important;border: none;padding: 0;}
    select {border:none !important;padding:0 !important}
    input:read-only {border:none !important;background-color: transparent !important;padding:0 !important}
</style>
<div>
    <div class="col-md-12">
        <div id="j-main-container" class="j-main-container">
            <?php if (empty($this->item)) : ?>
                <div class="alert alert-info">
                    <span class="icon-info-circle" aria-hidden="true"></span>
                    <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                </div>
            <?php else : ?>
                <?php echo (new IwfPrintButton('print', 'printform();', $editIcon, 'row'))->render(); ?>
                <table class="table">
                    <thead>
                        <tr>
                            <th class="w-40 d-none d-md-table-cell"></th>
                            <th class="w-60 d-none d-md-table-cell"></th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php foreach($this->form->getFieldSets() as $fieldset): if ($fieldset->name=='hidden_fields') continue; ?>
                        <?php foreach($this->form->getFieldSet($fieldset->name) as $field): ?>
                            <?php if (!$field->hidden) : ?>
                            <tr style="border-color:white;">
                                <?php $field->readonly = true; ?>
                                <td>
                                    <?php echo $field->label; ?>
                                </td>
                                <td>
                                    <?php echo $field->input; ?>
                                </td>
                            </tr>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                    </tbody>
                </table>
            <?php endif; ?>
        </div>
    </div>
</div>
