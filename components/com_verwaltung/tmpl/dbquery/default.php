<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;

defined('_JEXEC') or die('Restricted Access');

$wa = $this->document->getWebAssetManager()
    ->useScript('sqlresult');
?>
<?php echo IwfToolbarHelper::render($this->toolbar); ?>
<?php echo $this->header; ?>
<form action="#" method="post" name="adminForm" id="adminForm">
    <div class="row">
        <div class="col-md-12">
            <div id="j-main-container" class="j-main-container">
                <?php echo LayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
                <div id="sqlresult">
                    <?php if (empty($this->items)) : ?>
                        <div class="alert alert-info">
                            <span class="icon-info-circle" aria-hidden="true"></span>
                            <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                        </div>
                    <?php else : ?>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <?php foreach (get_object_vars($this->items[0]) as $title => $value): ?>
                                        <th scope="col" class="w-1 d-none d-md-table-cell">
                                            <?php echo ucfirst($title); ?>
                                        </th>
                                    <?php endforeach; ?>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($this->items as $i => $item): ?>
                                    <tr class="d row<?php echo $i % 2; ?>">
                                        <?php foreach (get_object_vars($item) as $title => $value): ?>
                                            <td class="d-none d-md-table-cell">
                                                <?php echo htmlentities($value) ?>
                                            </td>
                                        <?php endforeach; ?>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    <?php endif; ?>
                </div>
                <?php if ($this->pagination) {
                    echo $this->pagination->getListFooter();
                } ?>
                <input type="hidden" name="task" value="">
                <?php echo HTMLHelper::_('form.token'); ?>
            </div>
        </div>
    </div>
</form>