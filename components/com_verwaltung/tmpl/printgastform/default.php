<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Iwf\Verwaltung\IwfPrintButton;
use Joomla\CMS\HTML\HTMLHelper;

$editIcon = '<span class="fa fa-print" aria-hidden="true"></span>';
?>
<?php echo (new IwfPrintButton('print', 'printform();', $editIcon, 'row'))->render(); ?>
<div class="row">
    <table>
        <thead>
            <tr>
                <th style="font-size:x-large;padding-bottom:1em;text-align:center" colspan="2">
                    Gastformular<br />
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    Gast:
                </td>
                <td>
                    <?php echo sprintf("%s %s", $this->item->titel, $this->item->name); ?>
                </td>
            </tr>
            <tr>
                <td>
                    Akademischer Titel:
                </td>
                <td>
                    <?php echo (strlen($this->item->titel) == 0)  ? "-" : $this->item->titel; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Geburtsdatum:
                </td>
                <td>
                    <?php echo HTMLHelper::_('date', $this->item->geburtsdatum, 'd.m.Y'); ?>
                </td>
            </tr>
            <tr>
                <td>
                    Nationalität:
                </td>
                <td>
                    <?php echo $this->item->nation; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Geschlecht:
                </td>
                <td>
                    <?php echo $this->item->geschlecht; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Institut:
                </td>
                <td>
                    <?php echo sprintf('%s (%s, %s)', $this->item->institut, $this->item->ort, $this->item->land); ?>
                </td>
            </tr>
            <tr>
                <td>
                    Privatadresse:
                </td>
                <td>
                    <?php echo $this->item->privatadresse; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Aufenthalt:
                </td>
                <td>
                    <?php echo sprintf('%s - %s', HTMLHelper::_('date', $this->item->ankunft, 'd.m.Y'), HTMLHelper::_('date', $this->item->abreise, 'd.m.Y')); ?>
                </td>
            </tr>
            <tr>
                <td>
                    Aufenthaltszweck:
                </td>
                <td>
                    <?php echo $this->item->aufenthaltszweck; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Email:
                </td>
                <td>
                    <?php echo $this->item->email; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Finanzierung:
                </td>
                <td>
                    <?php echo $this->item->finanzierung; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Finanziert aus Projekt:
                </td>
                <td>
                    <?php echo $this->item->finanziert_aus_projekt; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Braucht Schlüssel:
                </td>
                <td>
                    <?php echo $this->item->braucht_schluessel; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Computer:
                </td>
                <td>
                    <?php echo $this->item->computer; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Arbeitsplatz:
                </td>
                <td>
                    <?php echo $this->item->arbeitsplatz; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Betreuer:
                </td>
                <td>
                    <?php echo $this->item->betreuer; ?>
                </td>
            </tr>
            <tr>
                <td>
                    Wohnung:
                </td>
                <td>
                    <?php echo $this->item->wohnung; ?>
                </td>
            </tr>
            <tr>
                <td style="padding-top:40px">
                    Unterschrift:
                </td>
                <td style="border-bottom: solid 1px black">&nbsp;</td>
            </tr>
        </tbody>
    </table>
</div>