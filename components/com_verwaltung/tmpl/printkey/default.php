<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Iwf\Verwaltung\IwfPrintButton;
use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;

$editIcon = '<span class="fa fa-print" aria-hidden="true"></span>';
?>
<style>
    html, body {
        height: auto;
    }
    table.print {
        border-collapse: collapse;
        font-family: arial;
        font-size: 14px;
        width: 100%;
    }
    table.print th {
        font-size: x-large;
        height: 150px;
        vertical-align: middle;
    }
    table.print td {
        border-style: none
    }
    table.print th {
        border-style: none
    }
    table.keys {
        font-size: 12px;
        border: 1px black dotted;
        border-collapse: collapse;
        font-family: arial;
        margin: 20px auto 0 auto;
    }
    table.keys td {
        border: 1px dotted black;
        padding: 10px;
    }
</style>
<?php echo (new IwfPrintButton('print', 'printform();', $editIcon, 'row'))->render(); ?>
<div class="row print">
    <h2 style="text-align:center;margin-bottom:2rem">Quittung für Schlüssel</h2>
    <table class="print">
        <tbody>
            <tr>
                <td style="valign:baseline" colspan="2"><?php echo ($this->mitarbeiter->geschlecht == "M" ? "Herr" : "Frau"); ?>
                    &nbsp;
                    <b>
                        <font size="3"><?php echo $this->mitarbeiter->name ?></font>
                    </b>,
                    &nbsp;
                    <?php if ($this->mitarbeiter->eintritt != '0000-00-00') echo 'Eintritt am: ' . HTMLHelper::_('date', $this->mitarbeiter->eintritt, 'd.m.Y') . ',';
                    if ($this->mitarbeiter->vertragsende != '0000-00-00') echo ' Vertragsende: ' . HTMLHelper::_('date', $this->mitarbeiter->vertragsende, 'd.m.Y') . ',';
                    ?>
                    <br><br>
                    bestätigt mit <?php echo ($this->mitarbeiter->geschlecht == "M" ? "seiner" : "ihrer"); ?> Unterschrift die Übernahme folgender Schlüssel:
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table class="keys" align="center" cellpadding="4">
                        <?php foreach ($this->schluesselliste as $key): ?>
                            <tr>
                                <td><?php echo $key->bezeichnung ?></td>
                                <td><?php echo HTMLHelper::_('date', $key->ausgabedatum, 'd.m.Y'); ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </td>
            </tr>
            <tr>
                <td height="60px" valign="middle" colspan=2><?php if ($this->verantwortlicher) echo 'Verantwortlicher: ' . $this->verantwortlicher->vorname . ' ' . $this->verantwortlicher->nachname; ?></td>
            </tr>
            <tr>
                <td height="60px">Graz, am <?php echo HTMLHelper::_('date', Factory::getDate(), 'd.m.Y'); ?></td>
                <td align="right">
                    Unterschrift: ____________________________________________&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td height="60px" colspan=2><br>Rückgabedatum:</td>
            </tr>
            <tr>
                <td height="40px" valign="bottom" colspan=2>Mit dieser Quittung sind alle früher datierten Quittungen ungültig.</td>
            </tr>
            <tr>
                <td height="30px" valign="bottom" colspan=2 style="font-size:smaller;font-weight:bold">Der Verlust eines Schlüssels muss umgehend dem Verantwortlichen gemeldet werden!</td>
            </tr>
        </tbody>
    </table>
</div>