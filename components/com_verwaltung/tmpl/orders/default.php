<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Uri\Uri;
use Iwf\Verwaltung\IwfActionButton;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\Plugin\Actionlog\Joomla\Extension\Joomla;
use Iwf\Component\Verwaltung\Site\Helper\RouterHelper;
use Iwf\Verwaltung\Extensions;

/** @var \Joomla\CMS\WebAsset\WebAssetManager $wa */
$wa = $this->document->getWebAssetManager();
$wa->useScript('table.columns')
    ->useScript('multiselect')
    ->useScript('actionbutton');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
$editIcon = '<span class="fa fa-pen-square me-2" aria-hidden="true"></span>';
?>
<?php echo IwfToolbarHelper::render($this->toolbar); ?>
<?php echo $this->header; ?>
<form action="<?php echo htmlspecialchars(Uri::getInstance()->toString()); ?>" method="post" name="adminForm" id="adminForm">
    <div class="row">
        <div class="col-md-12">
            <div id="j-main-container" class="j-main-container">
                <?php echo LayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
                <?php if (empty($this->items)) : ?>
                    <div class="alert alert-info">
                        <span class="icon-info-circle" aria-hidden="true"></span>
                        <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                    </div>
                <?php else : ?>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col" class="w-1 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('grid.checkall'); ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_BESTELLUNGEN_HEADING_BESTELLNUMMER', 'a.bestellnummer', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-20 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_BESTELLUNGEN_HEADING_BESCHREIBUNG', 'a.beschreibung', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-3 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_BESTELLUNGEN_HEADING_BESTELLDATUM', 'a.bestelldatum', $listDirn, $listOrder) ?>
                                </th>
                                <th scope="col" class="w-7 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_BESTELLUNGEN_HEADING_BESTELLER', 'm.nachname', $listDirn, $listOrder) ?>
                                </th>
                                <th scope="col" class="w-2 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_BESTELLUNGEN_HEADING_PO') ?>
                                </th>
                                <th scope="col" class="w-2 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_BESTELLUNGEN_HEADING_PREIS') ?>
                                </th>
                                <?php if (Extensions::isAllowed('manage.abteilung')) : ?>
                                    <th scope="col" class="w-5 d-none d-md-table-cell">
                                        <?php echo Text::_('COM_VERWALTUNG_BESTELLUNGEN_HEADING_BEZAHLT') ?>
                                    </th>
                                <?php endif; ?>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $n = count($this->items);
                            foreach ($this->items as $i => $bestellung) : 
                                $readonly = !Extensions::isAllowed('manage.abteilung') && $bestellung->readonly;
                            ?>
                                <tr class="row<?php echo $i % 2; ?>">
                                    <td class="d-none d-md-table-cell">
                                        <?php if (!$readonly) : ?>
                                            <?php echo HTMLHelper::_('grid.id', $i, $bestellung->id); ?>
                                        <?php endif; ?>
                                    </td>
                                    <td class="nobreak d-none d-md-table-cell">
                                        <?php echo RouterHelper::getOrderEditRoute($this->person, $bestellung, "", $editIcon); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $bestellung->beschreibung; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo HTMLHelper::_('date', $bestellung->bestelldatum, 'd.m.Y'); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell nobreak">
                                        <?php echo $bestellung->name; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $bestellung->sap; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo number_format($bestellung->preis, 2) ?>
                                    </td>
                                    <?php if (Extensions::isAllowed('manage.abteilung')) : ?>
                                        <td class="d-none d-md-table-cell">
                                            <?php echo (new IwfActionButton($this, 0, null, null, 'COM_VERWALTUNG_BESTELLUNG_BEZAHLT_0', 'COM_VERWALTUNG_BESTELLUNG_BEZAHLT_1'))->render((int) $bestellung->bezahlt, $i, true, ['id' => $bestellung->id, 'task_prefix' => 'orders.', 'disabled' => (string)$readonly]); ?>
                                        </td>
                                    <?php endif; ?>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
                <?php echo $this->pagination->getListFooter(); ?>
                <input type="hidden" name="actionButton" id="actionButton" value="">
                <input type="hidden" name="task" value="">
                <input type="hidden" name="boxchecked" value="0">
                <?php echo HTMLHelper::_('form.token'); ?>
            </div>
        </div>
    </div>
</form>