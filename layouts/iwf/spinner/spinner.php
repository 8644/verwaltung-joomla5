<?php
/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

use Joomla\CMS\Factory;

\defined('_JEXEC') or die;

extract($displayData, EXTR_OVERWRITE);
$script = $displayData['script'];
$title = $displayData['title'];

if (!empty($script)) {
  $wa = Factory::getApplication()->getDocument()->getWebAssetManager()
      ->useScript($script);
}
?>
<style>
#loader {
    display:none;  /*initial state*/
    text-align:center !important;
    color: #212529;
    width: 100%;
    height: 30px;
    margin-top: 15rem;
    font-weight: 600;
    font-size: larger;
}

#loader p {
  margin-top: 100px !important
}

#spinner {
    position: relative;
    left: 50%;
    top: 50%;
    z-index: 1;
    width: 120px;
    height: 120px;
    margin: -76px 0 0 -76px;
    border: 16px solid #bcf831;
    border-radius: 50%;
    border-top: 16px solid blue;
    border-bottom: 16px solid blue;
    -webkit-animation: spin 2s linear infinite;
    animation: spin 2s linear infinite;
  }
  
  @-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
  }
  
  @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
  }
  
  #loadready {
    display: flex; /*initial state*/
  }
</style>

<div id="loader">
  <div id="spinner"></div>
  <p><?php echo $title; ?></p>
</div>
