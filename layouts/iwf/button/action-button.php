<?php
/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;

\defined('_JEXEC') or die;

extract($displayData, EXTR_OVERWRITE);

Factory::getApplication()->getDocument()->getWebAssetManager()->useScript('list-view');

$disabled = !empty($options['disabled']);
$taskPrefix = $options['task_prefix'];
$checkboxName = $options['checkbox_name'];
$id = $options['id'];
$title = $displayData['title'];
$tipTitle = $options['tip_title'];
if (empty($title)) {
    $title = $tipTitle;
}
if (empty($title)) {
    $tip = false;
} else {
    $tip = $options['tip'];
}
$tag = $options['tag'];
$message = $options['message'] ?? '';
$return = isset($options['return']) ? (boolval($options['return']) ? "1" : "0") : "0";
$onclick = sprintf("getActionButton(this,event,'%s')", $message);
?>
<button type="button"
    onmousedown="<?php echo $onclick; ?>"
    id="<?php echo $id . '-' . $tag. '-' . $return?>"
    class="js-grid-item-action tbody-icon data-state-<?php echo $this->escape($value ?? ''); ?>"
    aria-labelledby="<?php echo $tag; ?>"
    <?php if (!empty($title)) echo 'title="' . $title . '"'?>
    <?php echo $disabled ? 'disabled' : ''; ?>
    data-item-id="<?php echo $checkboxName . $this->escape($row ?? ''); ?>"
    data-item-task="<?php echo $this->escape(isset($task) ? $taskPrefix . $task : ''); ?>"
>
    <span class="<?php echo $this->escape($icon ?? ''); ?>" aria-hidden="true"></span>
</button>
<?php if ($tip): ?>
<div id="<?php echo $id; ?>" role="tooltip">
    <?php echo HTMLHelper::_('tooltipText', $tipTitle ?: $title, $title, 0, false); ?>
</div>
<?php endif; ?>
