<?php
/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

use Joomla\CMS\Factory;

\defined('_JEXEC') or die;

extract($displayData, EXTR_OVERWRITE);
$script = $displayData['script'];
$command = $displayData['command'];
$icon = $displayData['icon'];
$class = $displayData['class'];
$orientation = $displayData['orientation'];

if (!empty($script)) {
    $wa = Factory::getApplication()->getDocument()->getWebAssetManager()
        ->useScript($script);
}
?>
<style>
    .row { padding-left: 30px; }
    .fa-print { font-size: xx-large; padding: 20px 0; }
    th, td { padding: 5px 10px; }
    @page { size: A4 <?php echo $orientation ?>; margin: 1cm; }    
</style>

<div class="<?php echo $class; ?>" id="printicon">
    <a href="#" onclick="<?php echo $command; ?>"><?php echo $icon; ?></a>
</div>

