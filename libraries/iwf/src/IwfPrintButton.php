<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Verwaltung;

use Joomla\CMS\Layout\LayoutHelper;

\defined('_JEXEC') or die;

/** @package Iwf\Verwaltung */
class IwfPrintButton
{

    public $script = '';

    public $command = 'window.print();';

    public $icon = '<span class="fa fa-print" aria-hidden="true"></span>';

    public $class = 'row';

    public $orientation = 'portrait';

    protected $layout = 'iwf.button.print-button';

    /**
     * @param string $script javascript defined in joomla.asset.json
     * @param string $command javascript command as defined in the print script that is added to the onclick event. If not supplied, window.print() is used.
     * @param string $icon print icon to display
     * @param string $class class for the print div
     * @return void 
     */
    public function __construct($script = '', $command = '', $icon = '', $class = '', $orientation = '')
    {
        if (!empty($script)) {
            $this->script = $script;
        }
        if (!empty($command)) {
            $this->command = $command;
        }
        if (!empty($icon)) {
            $this->icon = $icon;
        }
        if (!empty($class)) {
            $this->class = $class;
        }
        if (!empty($orientation)) {
            $this->orientation = $orientation;
        }
        $this->preprocess();
    }

    /**
     * Configure this object.
     *
     * @return  void
     */
    protected function preprocess()
    {
        // Implement this method.
    }

    /** @return string  */
    public function render(): string
    {
        $data = [];
        $data['script'] = $this->script;
        $data['command'] = $this->command;
        $data['icon'] = $this->icon;
        $data['class'] = $this->class;
        $data['orientation'] = $this->orientation;

        return LayoutHelper::render($this->layout, $data);
    }

    /**
     * Render to string.
     *
     * @return  string
     */
    public function __toString(): string
    {
        try {
            return $this->render();
        } catch (\Throwable $e) {
            return (string) $e;
        }
    }

    /**
     * Method to get property layout.
     *
     * @return  string
     */
    public function getLayout(): string
    {
        return $this->layout;
    }

    /**
     * Method to set property template.
     *
     * @param   string  $layout  The layout path.
     *
     * @return  static  Return self to support chaining.
     */
    public function setLayout(string $layout): self
    {
        $this->layout = $layout;

        return $this;
    }

}
