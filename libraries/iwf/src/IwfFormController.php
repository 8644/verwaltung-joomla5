<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Verwaltung;

\defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\MVC\Factory\MVCFactoryInterface;
use Joomla\CMS\Factory;
use Joomla\CMS\Application\CMSWebApplicationInterface;
use Joomla\CMS\Router\Route;
use Joomla\Utilities\ArrayHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Controller\FormController;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;
use Joomla\Filesystem\Exception\FilesystemException;

/** @package Iwf\Verwaltung */
class IwfFormController extends FormController
{

	/**
	 * @param array $config 
	 * @param MVCFactoryInterface|null $factory 
	 * @param mixed $app 
	 * @param mixed $input 
	 * @return void 
	 * @throws Exception 
	 * @throws UnexpectedValueException 
	 * @throws FilesystemException 
	 * @throws KeyNotFoundException 
	 */
	public function __construct($config = [], MVCFactoryInterface $factory = null, $app = null, $input = null)
	{
		parent::__construct($config, $factory, $app, $input);
		// die vorgegebenen enable und disable Tasks werden auf die Funktion setStatus() gesetzt
		// enable bzw. disable werden beim Klicken auf einen ActionButton ausgelöst.
		$this->registerTask('disable', 'setStatus');
		$this->registerTask('enable', 'setStatus');
		$this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=' . $this->view_list . $this->getRedirectToListAppend(), false));
	}

	/**
	 * Ändert den Status eines On/Off-Buttons
	 * @param mixed $id Id des Datensatzes
	 * @param int $value neuer Wert für den Button (0 oder 1 für off bzw. on)
	 * @param int $tag Id des Buttons, falls mehrere in einer Reihe
	 * @return void 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 */
	protected function changeStatus($id, $value = 0, $tag = 0)
	{
		Factory::getApplication()->enqueueMessage(Text::_('COM_VERWALTUNG_STATUS_METHOD_MISSING'), CMSWebApplicationInterface::MSG_WARNING);
	}

	/**
	 * Wird beim Klicken auf einen IwfActionButton aufgerufen (warum? - siehe Konstruktor)
	 * Erwartet im vererbten Controller die Funktion changeStatus($id, $value = 0, $tag = 0)
	 * @return void 
	 * @throws Exception 
	 * @throws InvalidArgumentException 
	 */
	public function setStatus()
	{
		$button = $this->input->get('actionButton');
		// Wert von $button hat die Form 'id-tag-return'
		$pattern = '/(\d+)-(\d+)-(\d+)/';
		if (preg_match($pattern, $button, $matches)) {
			$id = $matches[1];
			$tag = $matches[2];
			$return = $matches[3];
			if ($return) {
				Extensions::setReturnRoute();
			}
			$this->checkToken();
			$values = ['enable' => 1, 'disable' => 0];
			$task = $this->getTask();
			$value = ArrayHelper::getValue($values, $task, 0, 'int');
			$this->changeStatus($id, $value, $tag, $return);
		} 
	}
}
