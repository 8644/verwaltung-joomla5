<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Verwaltung;

use Exception;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Toolbar\Toolbar;
use Joomla\CMS\Layout\FileLayout;

\defined('_JEXEC') or die;

/** @package Iwf\Verwaltung */
class IwfToolbarHelper
{

    /**
     * Loads the iwf.toolbar.title layout
     * @param mixed $title 
     * @param string $icon 
     * @return void 
     */
    public static function title($title, $icon = 'generic.png')
    {
        $layout = new FileLayout('iwf.toolbar.title');
        $html   = $layout->render(['title' => $title, 'icon' => 'icon-' . $icon]);

        $app                  = Factory::getApplication();
        $app->JComponentTitle = $html;
        $title                = strip_tags($title) . ' - ' . $app->get('sitename');

        if ($app->isClient('administrator')) {
            $title .= ' - ' . Text::_('JADMINISTRATION');
        }
        $app->getDocument()->setTitle($title);
    }

    /**
     * @param null|Toolbar $toolbar 
     * @return string 
     * @throws Exception 
     */
    public static function render(?Toolbar $toolbar)
    {
        if ($toolbar) {
            $toolbarHtml = $toolbar->render();
            if (!$toolbarHtml) {
                return '';
            }
            $result = '<div id="subhead-container" class="subhead mb-3"><div class="row"><div class="col-md-12">' .
                $toolbarHtml .
                '</div></div></div>';
            return $result;
        } else {
            return '';
        }
    }
}
