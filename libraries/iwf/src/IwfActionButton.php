<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Verwaltung;

use Exception;
use Joomla\CMS\Button\ActionButton;
use Joomla\CMS\Language\Text;
use Joomla\DI\Exception\KeyNotFoundException;

\defined('_JEXEC') or die;

/** @package Iwf\Verwaltung */
class IwfActionButton extends ActionButton
{
    protected $offTitle;
    protected $onTitle;
    protected $offIcon;
    protected $onIcon;
    protected $offTip;
    protected $onTip;
    public $tag;
    public $id = -1;
    protected $options;
    protected $view;
    public static $actionButtons = [];

    /**
     * Erzeugt einen ActionButton, der einen enable/disable task ausführt. Die enable Funktion muss im Controller definiert werden
     * @param mixed $view 
     * @param int $tag Ein int Wert zur Unterscheidung, falls sich mehrere Buttons im Formular befinden. Er wird
     * in der changeStatus Funktion des entsprechenden Controllers ausgewertet, um den richtigen Tabellenwert zu ändern.
     * Zusätzlich werden im Listentemplate folgende Felder benötigt:
     *   <input type="hidden" name="actionButton" id="actionButton" value="">
     *   <input type="hidden" name="boxchecked" value="0"> 
     * @param mixed $offIcon 
     * @param mixed $onIcon 
     * @param mixed $offTitle 
     * @param mixed $onTitle 
     * @param mixed $offTip 
     * @param mixed $onTip 
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * of/onIcon ohne "icon-"-Präfix
     */
    public function __construct($view, $tag = 0, $offIcon = null, $onIcon = null, $offTitle = null, $onTitle = null, $offTip = null, $onTip = null)
    {
        $this->view = $view;
        $this->tag = $tag;
        $this->offIcon = $offIcon ?: 'unpublish-disable';
        $this->onIcon = $onIcon ?: 'publish-success';
        $this->offTitle = Text::_($offTitle);
        $this->onTitle = Text::_($onTitle);
        $this->offTip = Text::_($offTip);
        $this->onTip = Text::_($onTip);
        $this->options = [];
        parent::__construct();
    }

    /** @return void  */
    protected function preprocess()
    {
        $this->addState(
            0,
            'enable',
            $this->offIcon,
            $this->offTitle,
            ['tip_title' => $this->offTip, 'tip' => (empty($this->offTip) ? false : true)]
        );
        $this->addState(
            1,
            'disable',
            $this->onIcon,
            $this->onTitle,
            ['tip_title' => $this->onTip, 'tip' => (empty($this->onTip) ? false : true)]
        );
    }

    /**
     * Erzeugt die HTML-Ausgabe
     * @param int $value 0 oder > 0 für disabled oder enabled icon 
     * @param int $row Nummer der Zeile der Tabelle, in der sich der Button befindet
     * @param bool $customButton Fals true, wird Pfad zu com_component/layouts/button/action-button gesetzt
     * @param array $options weitere Optionen für Darstellung
     */
    public function render(?int $value = null, ?int $row = null, $customButton = true, array $options = []): string
    {
        if ($customButton) {
            $this->setLayout('iwf.button.action-button');
        }
        if (isset($options['id'])) {
            $this->id = $options['id'];
        }
        $options['tag'] = $this->tag;
        if (empty($value)) {
            $value = 0;
        }
        if ($value > 1) {
            $value = 1;
        }
        return parent::render($value, $row, $options);
    }

    /**
     * @param string $identifier 
     * @return string 
     */
    public function fetchIconClass(string $identifier): string
    {
        if (str_starts_with($identifier, 'icon-')) {
            return $identifier;
        }
        return 'iwfactionicon-' . $identifier;
    }
}
