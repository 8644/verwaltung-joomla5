<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Verwaltung;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Date\Date;
use Joomla\CMS\Factory;
use Joomla\CMS\Plugin\PluginHelper;
use Joomla\Database\ParameterType;
use Joomla\Database\DatabaseInterface;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Registry\Registry;
use RuntimeException;

class ActiveDirectory
{

    /**
     * @return false|void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     */
    public static function syncUsersWithAD()
    {
        if (!Extensions::runningOnSaturn()) {
            return true;
        }
        $configparams = ComponentHelper::getParams('com_verwaltung');
        $now = (new Date('now'))->toSql(true);
        $adAdmin = $configparams->get('ad_admin');
        $adAdminPw = $configparams->get('ad_adminpw');
        if (empty($adAdmin) || empty($adAdminPw)) {
            return false;
        }
        $plugin = PluginHelper::getPlugin('authentication', 'ldap');
        $params = new Registry($plugin->params);
        if ($ldap = new LdapClient($params)) {
            if ($ldap->connect()) {
                $bindtest = $ldap->bind($adAdmin, $adAdminPw, 1);
                if ($bindtest) {
                    $baseDn = $configparams->get('ldap_basedn', '');
                    $db = Factory::getContainer()->get(DatabaseInterface::class);
                    $query = $db->createQuery()
                        ->select('*')->from($db->qn('#__iwf_mitarbeiter'))
                        ->where($db->qn('dienstverhaeltnis') . '<>114')
                        ->whereIn($db->qn('abteilung'), explode(',', ABTEILUNGEN)); // Dienste auslassen
                    $mas = $db->setQuery($query)->loadObjectList();
                    foreach ($mas as $ma) {
                        $search = array("dn=*$ma->nachname*", "mail=$ma->email");
                        $binddata = $ldap->search($search, $baseDn, array('dn', 'sAMAccountName', 'uid', 'employeeNumber'));
                        $query = $db->createQuery();
                        if (count($binddata) == 0) { // Löschflag setzen
                            $query->update($db->qn('#__iwf_mitarbeiter'))
                                ->set($db->qn('deleted') . '=1')
                                ->set($db->qn('deleted_time') . '= "' . $now . '"')
                                ->where($db->qn('id') . '= :maid')
                                ->where($db->qn('deleted') . '=0')
                                ->bind(':maid', $ma->id, ParameterType::INTEGER);
                            $db->setQuery($query)->execute();
                        } else {
                            $query->update($db->qn('#__iwf_mitarbeiter'))
                                ->set($db->qn('deleted') . '=0')
                                ->set($db->qn('deleted_time') . '="0000-00-00 00:00:00"') //0000-00-00
                                ->where($db->qn('id') . '=:maid')
                                ->bind(':maid', $ma->id, ParameterType::INTEGER);
                            $db->setQuery($query)->execute();
                            if (isset($binddata[0]['sAMAccountName']) && (isset($binddata[0]['uid']) || isset($binddata[0]['employeeNumber'])) & isset($binddata[0]['dn'])) {
                                $samaccountname = $binddata[0]['sAMAccountName'][0];
                                $query = $db->createQuery()
                                    ->update($db->qn('#__iwf_mitarbeiter'))
                                    ->set($db->qn('samaccountname') . '=:sam')
                                    ->bind(':sam', $samaccountname, ParameterType::STRING);
                                $employeeNumber = 0;
                                if (isset($binddata[0]['uid'])) {
                                    $employeeNumber = $binddata[0]['uid'][0];
                                } else if (isset($binddata[0]['employeeNumber'])) {
                                    $employeeNumber = $binddata[0]['employeeNumber'][0];
                                }
                                $query->set($db->qn('personalnummer') . '=:empl')
                                    ->where($db->qn('id') . '= :maid')
                                    ->bind(':empl', $employeeNumber, ParameterType::INTEGER)
                                    ->bind(':maid', $ma->id, ParameterType::INTEGER);
                                $db->setQuery($query)->execute();
                            }
                        }
                    }
                }
            }
            $ldap->close();
            return true;
        } else {
            return false;
        }
    }

    public static function updateDhcpSettings() {
        $configparams = ComponentHelper::getParams('com_verwaltung');
        $filePath = JPATH_ROOT;
        $dhcpFilename = $configparams->get("dhcp_conf_filename", "dhcp.txt");
        if (!str_ends_with($dhcpFilename, DIRECTORY_SEPARATOR)) {
            $filePath .= DIRECTORY_SEPARATOR . $dhcpFilename;
        } else {
            $filePath .= $dhcpFilename;
        }
        if (!file_exists($filePath)) {
            return;
        }
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select($db->qn('dhcpchecksum'))
            ->from('#__iwf_cron');
        $md5alt = $db->setQuery($query)->loadObject();


        $content = file_get_contents($filePath);
        unlink($filePath);

        $md5neu = md5($content);
        if ($md5alt->dhcpchecksum == $md5neu) {
            return;
        }
        $query->clear()
            ->update('#__iwf_cron')->set($db->qn('dhcpchecksum') . '=:cs')
            ->bind(':cs', $md5neu, ParameterType::STRING);
        $db->setQuery($query)
            ->execute();

        $adAdmin = $configparams->get('ad_admin');
        $adAdminPw = $configparams->get('ad_adminpw');
        if (!($adAdmin && $adAdminPw)) {
            return;
        }
        $plugin = PluginHelper::getPlugin('authentication', 'ldap');
        $params = new Registry($plugin->params);
        $ipTemplate = $db->qn('ipnummer') . '=:ip';
        $macTemplate = $db->qn('mac') . '=:mac';
        if ($ldap = new LdapClient($params)) {
            if ($ldap->connect()) {
                $bindtest = $ldap->bind($adAdmin, $adAdminPw, 1);
                if ($bindtest) {
                    $baseDn = $configparams->get('ldap_basedn', '');
                    $pattern = "/#\s+([\w\s\d\.;,:\(\)\/\\?\[\]äöüÄÖÜß!-]+)^host[{\w\s\d\.\"\/;,:\(\)\/\\?#äöüÄÖÜß!-]+]*}/m";
                    $macPattern = "/\w{2}:\w{2}:\w{2}:\w{2}:\w{2}:\w{2}/";
                    $ipPattern = "/fixed-address\s+(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/";
                    if (preg_match_all($pattern, $content, $matches, PREG_SET_ORDER + PREG_OFFSET_CAPTURE)) {
                        foreach ($matches as $object) {
                            $entry = $object[0][0];
                            $title = preg_replace("/\r|\n/", "", $object[1][0]);
                            $macAddress = null;
                            $ipAddress = null;
                            if (preg_match($macPattern, $entry, $mac)) {
                                $macAddress = $mac[0];
                            }
                            if (preg_match($ipPattern, $entry, $ip)) {
                                $ipAddress = $ip[1];
                            }
                            if ($macAddress && $ipAddress) {
                                // suche in fzg_dns die ip-Adresse und den dazugehörigen Netbios-Namen (wname)
                                $query->clear()
                                    ->select
                                        (
                                            [
                                                $db->qn('wname'), 
                                                $db->qn('ipnummer')
                                            ]
                                        )
                                    ->from($db->qn('#__iwf_dns'))
                                    ->where($db->qn('ipnummer') . '=:ip')
                                    ->bind(':ip', $ipAddress, ParameterType::STRING);
                                $item = $db->setQuery($query)->loadObject();
                                if (isset($item)) {
                                    $search = array("cn=$item->wname");
                                    $binddata = $ldap->search($search, $baseDn, array('description'));
                                    if (count($binddata)) {
                                        if (isset($binddata[0])) {
                                            $description = [];
                                            $description['description'][0] = $title;
                                            $ldap->replace($binddata[0]['dn'], $description);
                                        }
                                    }
                                    // ändere auch die Beschreibung (=titel) in fzg_dns
                                    $query->clear()
                                        ->update($db->qn('#__iwf_dns'))
                                            ->set($macTemplate)
                                            ->set($db->qn('titel') . '=:title')
                                            ->where($ipTemplate)
                                            ->bind(':mac', $macAddress, ParameterType::STRING)
                                            ->bind(':title', $title, ParameterType::STRING)
                                            ->bind(':ip', $ipAddress, ParameterType::STRING);
                                    $db->setQuery($query)
                                        ->execute();
                                }
                            }
                        }
                    }
                }
            }
        }


        /*$ldap = new JClientLdap($params);
        $ipTemplate = "ipnummer='%s'";
        $macTemplate = "mac='%s'";
        if ($ldap) {
            if ($ldap->connect()) {
                $bindtest = $ldap->bind($adAdmin, $adAdminPw, 1);
                if ($bindtest) {
                    $baseDn = $configparams->get('ldap_basedn', '');

                    $pattern = "/#\s+([\w\s\d\.;,:\(\)\/\\?\[\]äöüÄÖÜß!-]+)^host[{\w\s\d\.\"\/;,:\(\)\/\\?#äöüÄÖÜß!-]+]*}/m";
                    $macPattern = "/\w{2}:\w{2}:\w{2}:\w{2}:\w{2}:\w{2}/";
                    $ipPattern = "/fixed-address\s+(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})/";
                    if (preg_match_all($pattern, $content, $matches, PREG_SET_ORDER + PREG_OFFSET_CAPTURE)) {
                        foreach ($matches as $object) {
                            $entry = $object[0][0];
                            $title = preg_replace("/\r|\n/", "", $object[1][0]);
                            $macAddress = null;
                            $ipAddress = null;
                            if (preg_match($macPattern, $entry, $mac)) {
                                $macAddress = $mac[0];
                            }
                            if (preg_match($ipPattern, $entry, $ip)) {
                                $ipAddress = $ip[1];
                            }
                            if ($macAddress && $ipAddress) {
                                // suche in fzg_dns die ip-Adresse und den dazugehörigen Netbios-Namen (wname)
                                $query->clear();
                                $query->select("*")->from("fzg_dns")->where("ipnummer='$ipAddress'");
                                $db->setQuery($query);
                                $item = $db->loadObject();
                                if ($item->wname) {
                                    $search = array("cn=$item->wname");
                                    $binddata = $ldap->search($search, $baseDn, array('description'));
                                    if (count($binddata)) {
                                        if (isset($binddata[0])) {
                                            $description = [];
                                            $description['description'][0] = $title;
                                            $ldap->replace($binddata[0]['dn'], $description);
                                        }
                                    }
                                    // ändere auch die Beschreibung (=titel) in fzg_dns
                                    $query->clear();
                                    $query->update('fzg_dns')
                                            ->set(sprintf($macTemplate, $macAddress))
                                            ->set("titel='$title'")
                                            ->where(sprintf($ipTemplate, $ipAddress));
                                    $db->setQuery($query);
                                    $db->execute();
                                }
                            }
                        }
                    }
                }
            }
        }*/

    }
    
}
