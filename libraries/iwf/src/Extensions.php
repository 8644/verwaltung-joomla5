<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Verwaltung;

use Joomla\CMS\Helper\ContentHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\Database\ParameterType;
use Joomla\Database\DatabaseInterface;
use Doctrine\Inflector\InflectorFactory;
use Exception;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use InvalidArgumentException;
use Joomla\CMS\Application\CMSApplicationInterface;
use stdClass;
use UnexpectedValueException;

class Extensions
{
    protected static $accessList = null;

    private static function initAccess() {
        if (!self::$accessList) {
            $user = Factory::getApplication()->getIdentity();
            self::$accessList = new stdClass;
            $accessItems = ContentHelper::getActions('com_verwaltung')->getProperties();
            if ($user->id && $user->email) {
                foreach ($accessItems as $accessItem=>$value) {
                    $allowed = $user->authorise($accessItem, 'com_verwaltung');
                    self::$accessList->$accessItem = $allowed;
                }
            }
        }
    }

    /**
     * @param mixed $accessList 
     * @param string $glue 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    public static function isAllowed($accessList = null, $glue = "OR"): bool
    {
        $user = Factory::getApplication()->getIdentity();
        if ($accessList == null) {
            return false;
        }
        if (!is_array($accessList)) {
            $accessList = preg_split("/[\s,;]+/", $accessList);
        }
        $or = strcasecmp($glue, "AND") != 0;
        $result = !$or;
        foreach ($accessList as $access) {
            if (strncmp($access, "core.", 5) != 0) {
                $access = "core." . $access;
            }
            if ($or) {
                $result = $result || $user->authorise($access, 'com_verwaltung');
                if ($result) {
                    break;
                }
            } else {
                $result = $result && $user->authorise($access, 'com_verwaltung');
            }
        }
        return $result;
    }

    /**
     * Header für Listen und Editierformulare
     * @param mixed $iconClass 
     * @param mixed $text 
     * @return string 
     * @throws KeyNotFoundException 
     * @throws Exception 
     */
    public static function getListHeader($iconClass, $text, $text2 = null, $style = ''): string
    {
        if ($text2) {
            return '<h1 class="' . $iconClass . ' listheader"><span>&nbsp;' . Text::_($text) . '</span><span ' . $style . '>&nbsp;' . Text::_($text2) . '</span></h1>';
        } else {
            return '<h1 class="' . $iconClass . ' listheader"><span>&nbsp;' . Text::_($text) . '</span></h1>';
        }
    }

    /**
     * Holt einen Formulardateinamen, falls mehrere zur Auswahl stehen; vor allem für Dienstverhältnisse
     * @param mixed $id 
     * @param mixed $kategorie 
     * @param string $default 
     * @return string 
     * @throws KeyNotFoundException 
     */
    public static function getForm($id, $kategorie, $default = 'ma'): string
    {
        $result = null;
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('id'),
                    $db->qn('form')
                ]
            )
            ->from($db->qn('#__iwf_listen'))
            ->where($db->qn('kategorie') . ' LIKE :kat')
            ->where($db->qn('id') . '=' . $id)
            ->bind(':kat', $kategorie, ParameterType::STRING);
        $result = $db->setQuery($query)->loadObject();
        if (!empty($result)) {
            return $result->form;
        }
        return $default;
    }

    /**
     * @param bool $skip If true, function returns true (simulates running on saturn)
     * @return bool 
     * @throws Exception 
     */
    public static function runningOnSaturn($skip = false) {
        $backtrace = debug_backtrace();
        $result = $skip || strcasecmp(gethostname(), 'saturn.iwf.oeaw.ac.at') == 0;
        if (!$result) {
            Factory::getApplication()->enqueueMessage(sprintf('%s.%s(): Funktion wird nur aufgerufen, wenn Website auf saturn läuft', basename($backtrace[1]['class']), $backtrace[1]['function']), CMSApplicationInterface::MSG_INFO);
        }
        return $result;
    }

    /**
     * @param mixed $key 
     * @param mixed $value 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public static function setPermanent($key, $value): void
    {
        $userId = Factory::getApplication()->getIdentity()->id;
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select('*')
            ->from($db->qn('#__user_profiles'))
            ->where($db->qn('profile_key') . '= :key')
            ->where($db->qn('user_id') . '= :userid')
            ->bind(':key', $key,  ParameterType::STRING)
            ->bind(':userid', $userId, ParameterType::INTEGER);
        $result = $db->setQuery($query)->loadObject();
        $object = (object) [
            'user_id'       => $userId,
            'profile_key'   => $key,
            'profile_value' => json_encode($value),
            'ordering'      => 1,
        ];
        $query->clear();
        if (!empty($result->profile_key)) {
            $db->updateObject('#__user_profiles', $object, ['user_id', 'profile_key']);
        } else {
            $db->insertObject('#__user_profiles', $object);
        }
    }

    /**
     * @param mixed $key 
     * @param mixed $default 
     * @return mixed 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public static function getPermanent($key, $default = null): mixed
    {
        $userId = Factory::getApplication()->getIdentity()->id;
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select('*')
            ->from($db->qn('#__user_profiles'))
            ->where($db->qn('profile_key') . '= :key')
            ->bind(':key', $key,  ParameterType::STRING);
        $result = $db->setQuery($query)->loadObject();
        if ($result) {
            return json_decode($result->profile_value);
        }
        return $default;
    }

    /**
     * @param mixed $word 
     * @return string 
     * @throws InvalidArgumentException 
     */
    public static function getPluralOf($word): string
    {
        return InflectorFactory::create()->build()->pluralize($word);
    }

    /**
     * @param mixed $word 
     * @return string 
     * @throws InvalidArgumentException 
     */
    public static function getSingularOf($word): string
    {
        return InflectorFactory::create()->build()->singularize($word);
    }

    /**
     * Entfernt Akzente aus $str und ersetzt sie laut $transliterationstabelle.
     * @param mixed $str 
     * @return string 
     */
    public static function unaccent($str): string
    {
        $transliteration = array(
            'Ĳ' => 'I',
            'Ö' => 'O',
            'Œ' => 'O',
            'Ü' => 'U',
            'ä' => 'a',
            'æ' => 'a',
            'ĳ' => 'i',
            'ö' => 'o',
            'œ' => 'o',
            'ü' => 'u',
            'ß' => 's',
            'ſ' => 's',
            'À' => 'A',
            'Á' => 'A',
            'Â' => 'A',
            'Ã' => 'A',
            'Ä' => 'A',
            'Å' => 'A',
            'Æ' => 'A',
            'Ā' => 'A',
            'Ą' => 'A',
            'Ă' => 'A',
            'Ç' => 'C',
            'Ć' => 'C',
            'Č' => 'C',
            'Ĉ' => 'C',
            'Ċ' => 'C',
            'Ď' => 'D',
            'Đ' => 'D',
            'È' => 'E',
            'É' => 'E',
            'Ê' => 'E',
            'Ë' => 'E',
            'Ē' => 'E',
            'Ę' => 'E',
            'Ě' => 'E',
            'Ĕ' => 'E',
            'Ė' => 'E',
            'Ĝ' => 'G',
            'Ğ' => 'G',
            'Ġ' => 'G',
            'Ģ' => 'G',
            'Ĥ' => 'H',
            'Ħ' => 'H',
            'Ì' => 'I',
            'Í' => 'I',
            'Î' => 'I',
            'Ï' => 'I',
            'Ī' => 'I',
            'Ĩ' => 'I',
            'Ĭ' => 'I',
            'Į' => 'I',
            'İ' => 'I',
            'Ĵ' => 'J',
            'Ķ' => 'K',
            'Ľ' => 'K',
            'Ĺ' => 'K',
            'Ļ' => 'K',
            'Ŀ' => 'K',
            'Ł' => 'L',
            'Ñ' => 'N',
            'Ń' => 'N',
            'Ň' => 'N',
            'Ņ' => 'N',
            'Ŋ' => 'N',
            'Ò' => 'O',
            'Ó' => 'O',
            'Ô' => 'O',
            'Õ' => 'O',
            'Ø' => 'O',
            'Ō' => 'O',
            'Ő' => 'O',
            'Ŏ' => 'O',
            'Ŕ' => 'R',
            'Ř' => 'R',
            'Ŗ' => 'R',
            'Ś' => 'S',
            'Ş' => 'S',
            'Ŝ' => 'S',
            'Ș' => 'S',
            'Š' => 'S',
            'Ť' => 'T',
            'Ţ' => 'T',
            'Ŧ' => 'T',
            'Ț' => 'T',
            'Ù' => 'U',
            'Ú' => 'U',
            'Û' => 'U',
            'Ū' => 'U',
            'Ů' => 'U',
            'Ű' => 'U',
            'Ŭ' => 'U',
            'Ũ' => 'U',
            'Ų' => 'U',
            'Ŵ' => 'W',
            'Ŷ' => 'Y',
            'Ÿ' => 'Y',
            'Ý' => 'Y',
            'Ź' => 'Z',
            'Ż' => 'Z',
            'Ž' => 'Z',
            'à' => 'a',
            'á' => 'a',
            'â' => 'a',
            'ã' => 'a',
            'ā' => 'a',
            'ą' => 'a',
            'ă' => 'a',
            'å' => 'a',
            'ç' => 'c',
            'ć' => 'c',
            'č' => 'c',
            'ĉ' => 'c',
            'ċ' => 'c',
            'ď' => 'd',
            'đ' => 'd',
            'è' => 'e',
            'é' => 'e',
            'ê' => 'e',
            'ë' => 'e',
            'ē' => 'e',
            'ę' => 'e',
            'ě' => 'e',
            'ĕ' => 'e',
            'ė' => 'e',
            'ƒ' => 'f',
            'ĝ' => 'g',
            'ğ' => 'g',
            'ġ' => 'g',
            'ģ' => 'g',
            'ĥ' => 'h',
            'ħ' => 'h',
            'ì' => 'i',
            'í' => 'i',
            'î' => 'i',
            'ï' => 'i',
            'ī' => 'i',
            'ĩ' => 'i',
            'ĭ' => 'i',
            'į' => 'i',
            'ı' => 'i',
            'ĵ' => 'j',
            'ķ' => 'k',
            'ĸ' => 'k',
            'ł' => 'l',
            'ľ' => 'l',
            'ĺ' => 'l',
            'ļ' => 'l',
            'ŀ' => 'l',
            'ñ' => 'n',
            'ń' => 'n',
            'ň' => 'n',
            'ņ' => 'n',
            'ŉ' => 'n',
            'ŋ' => 'n',
            'ò' => 'o',
            'ó' => 'o',
            'ô' => 'o',
            'õ' => 'o',
            'ø' => 'o',
            'ō' => 'o',
            'ő' => 'o',
            'ŏ' => 'o',
            'ŕ' => 'r',
            'ř' => 'r',
            'ŗ' => 'r',
            'ś' => 's',
            'š' => 's',
            'ť' => 't',
            'ù' => 'u',
            'ú' => 'u',
            'û' => 'u',
            'ū' => 'u',
            'ů' => 'u',
            'ű' => 'u',
            'ŭ' => 'u',
            'ũ' => 'u',
            'ų' => 'u',
            'ŵ' => 'w',
            'ÿ' => 'y',
            'ý' => 'y',
            'ŷ' => 'y',
            'ż' => 'z',
            'ź' => 'z',
            'ž' => 'z',
            'Α' => 'A',
            'Ά' => 'A',
            'Ἀ' => 'A',
            'Ἁ' => 'A',
            'Ἂ' => 'A',
            'Ἃ' => 'A',
            'Ἄ' => 'A',
            'Ἅ' => 'A',
            'Ἆ' => 'A',
            'Ἇ' => 'A',
            'ᾈ' => 'A',
            'ᾉ' => 'A',
            'ᾊ' => 'A',
            'ᾋ' => 'A',
            'ᾌ' => 'A',
            'ᾍ' => 'A',
            'ᾎ' => 'A',
            'ᾏ' => 'A',
            'Ᾰ' => 'A',
            'Ᾱ' => 'A',
            'Ὰ' => 'A',
            'ᾼ' => 'A',
            'Β' => 'B',
            'Γ' => 'G',
            'Δ' => 'D',
            'Ε' => 'E',
            'Έ' => 'E',
            'Ἐ' => 'E',
            'Ἑ' => 'E',
            'Ἒ' => 'E',
            'Ἓ' => 'E',
            'Ἔ' => 'E',
            'Ἕ' => 'E',
            'Ὲ' => 'E',
            'Ζ' => 'Z',
            'Η' => 'I',
            'Ή' => 'I',
            'Ἠ' => 'I',
            'Ἡ' => 'I',
            'Ἢ' => 'I',
            'Ἣ' => 'I',
            'Ἤ' => 'I',
            'Ἥ' => 'I',
            'Ἦ' => 'I',
            'Ἧ' => 'I',
            'ᾘ' => 'I',
            'ᾙ' => 'I',
            'ᾚ' => 'I',
            'ᾛ' => 'I',
            'ᾜ' => 'I',
            'ᾝ' => 'I',
            'ᾞ' => 'I',
            'ᾟ' => 'I',
            'Ὴ' => 'I',
            'ῌ' => 'I',
            'Θ' => 'T',
            'Ι' => 'I',
            'Ί' => 'I',
            'Ϊ' => 'I',
            'Ἰ' => 'I',
            'Ἱ' => 'I',
            'Ἲ' => 'I',
            'Ἳ' => 'I',
            'Ἴ' => 'I',
            'Ἵ' => 'I',
            'Ἶ' => 'I',
            'Ἷ' => 'I',
            'Ῐ' => 'I',
            'Ῑ' => 'I',
            'Ὶ' => 'I',
            'Κ' => 'K',
            'Λ' => 'L',
            'Μ' => 'M',
            'Ν' => 'N',
            'Ξ' => 'K',
            'Ο' => 'O',
            'Ό' => 'O',
            'Ὀ' => 'O',
            'Ὁ' => 'O',
            'Ὂ' => 'O',
            'Ὃ' => 'O',
            'Ὄ' => 'O',
            'Ὅ' => 'O',
            'Ὸ' => 'O',
            'Π' => 'P',
            'Ρ' => 'R',
            'Ῥ' => 'R',
            'Σ' => 'S',
            'Τ' => 'T',
            'Υ' => 'Y',
            'Ύ' => 'Y',
            'Ϋ' => 'Y',
            'Ὑ' => 'Y',
            'Ὓ' => 'Y',
            'Ὕ' => 'Y',
            'Ὗ' => 'Y',
            'Ῠ' => 'Y',
            'Ῡ' => 'Y',
            'Ὺ' => 'Y',
            'Φ' => 'F',
            'Χ' => 'X',
            'Ψ' => 'P',
            'Ω' => 'O',
            'Ώ' => 'O',
            'Ὠ' => 'O',
            'Ὡ' => 'O',
            'Ὢ' => 'O',
            'Ὣ' => 'O',
            'Ὤ' => 'O',
            'Ὥ' => 'O',
            'Ὦ' => 'O',
            'Ὧ' => 'O',
            'ᾨ' => 'O',
            'ᾩ' => 'O',
            'ᾪ' => 'O',
            'ᾫ' => 'O',
            'ᾬ' => 'O',
            'ᾭ' => 'O',
            'ᾮ' => 'O',
            'ᾯ' => 'O',
            'Ὼ' => 'O',
            'ῼ' => 'O',
            'α' => 'a',
            'ά' => 'a',
            'ἀ' => 'a',
            'ἁ' => 'a',
            'ἂ' => 'a',
            'ἃ' => 'a',
            'ἄ' => 'a',
            'ἅ' => 'a',
            'ἆ' => 'a',
            'ἇ' => 'a',
            'ᾀ' => 'a',
            'ᾁ' => 'a',
            'ᾂ' => 'a',
            'ᾃ' => 'a',
            'ᾄ' => 'a',
            'ᾅ' => 'a',
            'ᾆ' => 'a',
            'ᾇ' => 'a',
            'ὰ' => 'a',
            'ᾰ' => 'a',
            'ᾱ' => 'a',
            'ᾲ' => 'a',
            'ᾳ' => 'a',
            'ᾴ' => 'a',
            'ᾶ' => 'a',
            'ᾷ' => 'a',
            'β' => 'b',
            'γ' => 'g',
            'δ' => 'd',
            'ε' => 'e',
            'έ' => 'e',
            'ἐ' => 'e',
            'ἑ' => 'e',
            'ἒ' => 'e',
            'ἓ' => 'e',
            'ἔ' => 'e',
            'ἕ' => 'e',
            'ὲ' => 'e',
            'ζ' => 'z',
            'η' => 'i',
            'ή' => 'i',
            'ἠ' => 'i',
            'ἡ' => 'i',
            'ἢ' => 'i',
            'ἣ' => 'i',
            'ἤ' => 'i',
            'ἥ' => 'i',
            'ἦ' => 'i',
            'ἧ' => 'i',
            'ᾐ' => 'i',
            'ᾑ' => 'i',
            'ᾒ' => 'i',
            'ᾓ' => 'i',
            'ᾔ' => 'i',
            'ᾕ' => 'i',
            'ᾖ' => 'i',
            'ᾗ' => 'i',
            'ὴ' => 'i',
            'ῂ' => 'i',
            'ῃ' => 'i',
            'ῄ' => 'i',
            'ῆ' => 'i',
            'ῇ' => 'i',
            'θ' => 't',
            'ι' => 'i',
            'ί' => 'i',
            'ϊ' => 'i',
            'ΐ' => 'i',
            'ἰ' => 'i',
            'ἱ' => 'i',
            'ἲ' => 'i',
            'ἳ' => 'i',
            'ἴ' => 'i',
            'ἵ' => 'i',
            'ἶ' => 'i',
            'ἷ' => 'i',
            'ὶ' => 'i',
            'ῐ' => 'i',
            'ῑ' => 'i',
            'ῒ' => 'i',
            'ῖ' => 'i',
            'ῗ' => 'i',
            'κ' => 'k',
            'λ' => 'l',
            'μ' => 'm',
            'ν' => 'n',
            'ξ' => 'k',
            'ο' => 'o',
            'ό' => 'o',
            'ὀ' => 'o',
            'ὁ' => 'o',
            'ὂ' => 'o',
            'ὃ' => 'o',
            'ὄ' => 'o',
            'ὅ' => 'o',
            'ὸ' => 'o',
            'π' => 'p',
            'ρ' => 'r',
            'ῤ' => 'r',
            'ῥ' => 'r',
            'σ' => 's',
            'ς' => 's',
            'τ' => 't',
            'υ' => 'y',
            'ύ' => 'y',
            'ϋ' => 'y',
            'ΰ' => 'y',
            'ὐ' => 'y',
            'ὑ' => 'y',
            'ὒ' => 'y',
            'ὓ' => 'y',
            'ὔ' => 'y',
            'ὕ' => 'y',
            'ὖ' => 'y',
            'ὗ' => 'y',
            'ὺ' => 'y',
            'ῠ' => 'y',
            'ῡ' => 'y',
            'ῢ' => 'y',
            'ῦ' => 'y',
            'ῧ' => 'y',
            'φ' => 'f',
            'χ' => 'x',
            'ψ' => 'p',
            'ω' => 'o',
            'ώ' => 'o',
            'ὠ' => 'o',
            'ὡ' => 'o',
            'ὢ' => 'o',
            'ὣ' => 'o',
            'ὤ' => 'o',
            'ὥ' => 'o',
            'ὦ' => 'o',
            'ὧ' => 'o',
            'ᾠ' => 'o',
            'ᾡ' => 'o',
            'ᾢ' => 'o',
            'ᾣ' => 'o',
            'ᾤ' => 'o',
            'ᾥ' => 'o',
            'ᾦ' => 'o',
            'ᾧ' => 'o',
            'ὼ' => 'o',
            'ῲ' => 'o',
            'ῳ' => 'o',
            'ῴ' => 'o',
            'ῶ' => 'o',
            'ῷ' => 'o',
            'А' => 'A',
            'Б' => 'B',
            'В' => 'V',
            'Г' => 'G',
            'Д' => 'D',
            'Е' => 'E',
            'Ё' => 'E',
            'Ж' => 'Z',
            'З' => 'Z',
            'И' => 'I',
            'Й' => 'I',
            'К' => 'K',
            'Л' => 'L',
            'М' => 'M',
            'Н' => 'N',
            'О' => 'O',
            'П' => 'P',
            'Р' => 'R',
            'С' => 'S',
            'Т' => 'T',
            'У' => 'U',
            'Ф' => 'F',
            'Х' => 'K',
            'Ц' => 'T',
            'Ч' => 'C',
            'Ш' => 'S',
            'Щ' => 'S',
            'Ы' => 'Y',
            'Э' => 'E',
            'Ю' => 'Y',
            'Я' => 'Y',
            'а' => 'A',
            'б' => 'B',
            'в' => 'V',
            'г' => 'G',
            'д' => 'D',
            'е' => 'E',
            'ё' => 'E',
            'ж' => 'Z',
            'з' => 'Z',
            'и' => 'I',
            'й' => 'I',
            'к' => 'K',
            'л' => 'L',
            'м' => 'M',
            'н' => 'N',
            'о' => 'O',
            'п' => 'P',
            'р' => 'R',
            'с' => 'S',
            'т' => 'T',
            'у' => 'U',
            'ф' => 'F',
            'х' => 'K',
            'ц' => 'T',
            'ч' => 'C',
            'ш' => 'S',
            'щ' => 'S',
            'ы' => 'Y',
            'э' => 'E',
            'ю' => 'Y',
            'я' => 'Y',
            'ð' => 'd',
            'Ð' => 'D',
            'þ' => 't',
            'Þ' => 'T',
            'ა' => 'a',
            'ბ' => 'b',
            'გ' => 'g',
            'დ' => 'd',
            'ე' => 'e',
            'ვ' => 'v',
            'ზ' => 'z',
            'თ' => 't',
            'ი' => 'i',
            'კ' => 'k',
            'ლ' => 'l',
            'მ' => 'm',
            'ნ' => 'n',
            'ო' => 'o',
            'პ' => 'p',
            'ჟ' => 'z',
            'რ' => 'r',
            'ს' => 's',
            'ტ' => 't',
            'უ' => 'u',
            'ფ' => 'p',
            'ქ' => 'k',
            'ღ' => 'g',
            'ყ' => 'q',
            'შ' => 's',
            'ჩ' => 'c',
            'ც' => 't',
            'ძ' => 'd',
            'წ' => 't',
            'ჭ' => 'c',
            'ხ' => 'k',
            'ჯ' => 'j',
            'ჰ' => 'h'
        );
        $str = str_replace(
            array_keys($transliteration),
            array_values($transliteration),
            $str
        );
        return $str;
    }

    /**
     * @param mixed $mac_addr 
     * @param string $broadcast 
     * @param string|null &$error_code 
     * @param string|null &$error_message 
     * @return false|int 
     */
    public static function wakeOnLan($mac_addr, $broadcast = '255.255.255.255', string &$error_code = null, string &$error_message = null)
    {
        $mac_hex = preg_replace('=[^a-f0-9]=i', '', $mac_addr);
        $mac_bin = pack('H12', $mac_hex);
        $data = str_repeat("\xFF", 6) . str_repeat($mac_bin, 16);
        $e = false;
        $s = socket_create(AF_INET, SOCK_DGRAM, SOL_UDP);
        if ($s === false) {
            $s = null;
        }
        if ($s != null) {
            $error_code = socket_last_error($s);
            $error_message = socket_strerror($error_code);
            $opt_ret = socket_set_option($s, SOL_SOCKET, SO_BROADCAST, 1);
            if ($opt_ret < 0) {
                $error_code = $opt_ret;
                $error_message = socket_strerror($error_code);
                return false;
            }
            $e = socket_sendto($s, $data, strlen($data), 0, $broadcast, 2050);
            if ($e == false) {
                $error_code = socket_last_error($s);
                $error_message = socket_strerror($error_code);
            }
            socket_close($s);
        }
        return $e;
    }

    /**
     * @return void 
     * @throws Exception 
     */
    public static function clearReturnRoute() {
        Factory::getApplication()->setUserState('com_verwaltung.returntree', []);
    }

    /**
     * @return mixed 
     */
    public static function setReturnRoute() {
        $referer = $_SERVER['HTTP_REFERER'];
        $returns = Factory::getApplication()->getUserState('com_verwaltung.returntree');
        if (empty($returns) || !is_array($returns)) {
            $returns = [];
        }
        $returns[] = base64_encode($referer);
        Factory::getApplication()->setUserState('com_verwaltung.returntree', $returns);
	}

    /**
     * @return string|false 
     * @throws Exception 
     */
    public static function getReturnRoute() {
        $return = '';
        // letzten Eintrag suchen
        $returns = Factory::getApplication()->getUserState('com_verwaltung.returntree');
        if (is_array($returns) && !empty($returns)) {
            $return = base64_decode(end($returns));
            array_pop($returns);
            Factory::getApplication()->setUserState('com_verwaltung.returntree', $returns);
        }
        return $return;
    }
}
