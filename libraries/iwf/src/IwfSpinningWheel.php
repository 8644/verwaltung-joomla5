<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Verwaltung;

use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;

\defined('_JEXEC') or die;

/** @package Iwf\Verwaltung */
class IwfSpinningWheel
{
    public $script = 'loader';

    public $title = 'Work in progress...';

    protected $layout = 'iwf.spinner.spinner';

    public function __construct($script = '')
    {
        if (!empty($script)) {
            $this->script = $script;
        }
        $this->preprocess();
    }

    /**
     * Configure this object.
     *
     * @return  void
     */
    protected function preprocess()
    {
        // Implement this method.
    }

    /** @return string  */
    public function render(string $title = ''): string
    {
        $data['script'] = $this->script;
        if (!empty($title)) {
            $this->title = Text::_($title);
        }
        $data['title'] = $this->title;

        return LayoutHelper::render($this->layout, $data);
    }

    /**
     * Render to string.
     *
     * @return  string
     */
    public function __toString(): string
    {
        try {
            return $this->render();
        } catch (\Throwable $e) {
            return (string) $e;
        }
    }

    /**
     * Method to get property layout.
     *
     * @return  string
     */
    public function getLayout(): string
    {
        return $this->layout;
    }

    /**
     * Method to set property template.
     *
     * @param   string  $layout  The layout path.
     *
     * @return  static  Return self to support chaining.
     */
    public function setLayout(string $layout): self
    {
        $this->layout = $layout;

        return $this;
    }

}
