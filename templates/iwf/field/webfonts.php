<?php
/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die;

use Joomla\CMS\Form\Field\ListField;

class JFormFieldWebfonts extends ListField {
    
    protected $type = 'webfonts';

    public function getOptions() {
        $fontFile = JPATH_SITE .'/media/templates/site/iwf/css/global/googlefonts.css';
        if (!file_exists($fontFile)) {
            return parent::getOptions();
        }
        $fonts = [];
        $family = "";
        $style = "";
        $weight = "";
        $fontDescriptor = "";
        $fh = fopen($fontFile, 'r');
        while ($line = fgets($fh)) {
            if (preg_match_all("/^(?<descriptor>\/[^\/]+\/)/", $line, $matches)) {
                $fontDescriptor = $matches['descriptor'][0];
                $fontDescriptor = str_replace("/*", "", $fontDescriptor);
                $fontDescriptor = str_replace("*/", "", $fontDescriptor);
                $fontDescriptor = trim($fontDescriptor, " ");
            }
            if (preg_match("/font-family/", $line)) {
                preg_match_all("/(font-family:\s)+(?<family>'[^']+')/", $line, $matches);
                $family = str_replace("'", "", $matches['family'][0]);
            }
            if (preg_match("/font-style/", $line)) {
                preg_match_all("/font-style:\s(?<style>[^;]*)/", $line, $matches);
                $style = $matches['style'][0];
            }
            if (preg_match("/font-weight/", $line)) {
                preg_match_all("/font-weight:\s(?<weight>[^;]*)/", $line, $matches);
                $weight = $matches['weight'][0];
            }
            if (strlen($fontDescriptor) && strlen($family) && strlen($style) && strlen($weight)) {
                $font = sprintf("%s|%s|%s", $family, $style, $weight);
                $fonts[] = [$fontDescriptor, $font];
                $family = "";
                $style = "";
                $weight = "";
                $fontDescriptor = "";
            }
        }
        $options = [];
        usort($fonts, function ($a, $b) {
            return strcmp($a[1], $b[1]);
        });
        foreach ($fonts as $f) {
            $options[] = JHtml::_('select.option', $f[1], $f[0]);
        }
        fclose($fh);
        return array_merge(parent::getOptions(), $options);
    }
   
}
