<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die;

use Joomla\CMS\Factory;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;

/** @var Joomla\CMS\Document\HtmlDocument $this */

$app   = Factory::getApplication();
$input = $app->getInput();
$wa    = $this->getWebAssetManager();

// Browsers support SVG favicons
//$this->addHeadLink(HTMLHelper::_('image', 'joomla-favicon.svg', '', [], true, 1), 'icon', 'rel', ['type' => 'image/svg+xml']);
$this->addHeadLink(HTMLHelper::_('image', '/templates/iwf/favicon.ico', '', [], false, 1), 'alternate icon', 'rel', ['type' => 'image/vnd.microsoft.icon']);
//$this->addHeadLink(HTMLHelper::_('image', 'joomla-favicon-pinned.svg', '', [], true, 1), 'mask-icon', 'rel', ['color' => '#000']);

// Detecting Active Variables
$option   = $input->getCmd('option', '');
$view     = $input->getCmd('view', '');
$layout   = $input->getCmd('layout', '');
$task     = $input->getCmd('task', '');
$itemid   = $input->getCmd('Itemid', '');
$sitename = htmlspecialchars($app->get('sitename'), ENT_QUOTES, 'UTF-8');
$menu     = $app->getMenu()->getActive();
$hideMenu   = $app->getInput()->get('hidemainmenu');
$pageclass = $menu !== null ? $menu->getParams()->get('pageclass_sfx', '') : '';

// Color Theme
$paramsColorName = $this->params->get('colorName', 'colors_standard');
$assetColorName  = 'theme.' . $paramsColorName;
$wa->registerAndUseStyle($assetColorName, 'global/' . $paramsColorName . '.css');

// Use a font scheme if set in the template style options
$paramsFontScheme = $this->params->get('useFontScheme', false);
$fontStyles       = '';

if ($paramsFontScheme) {
    if (stripos($paramsFontScheme, 'https://') === 0) {
        $this->getPreloadManager()->preconnect('https://fonts.googleapis.com/', ['crossorigin' => 'anonymous']);
        $this->getPreloadManager()->preconnect('https://fonts.gstatic.com/', ['crossorigin' => 'anonymous']);
        $this->getPreloadManager()->preload($paramsFontScheme, ['as' => 'style', 'crossorigin' => 'anonymous']);
        $wa->registerAndUseStyle('fontscheme.current', $paramsFontScheme, [], ['media' => 'print', 'rel' => 'lazy-stylesheet', 'onload' => 'this.media=\'all\'', 'crossorigin' => 'anonymous']);

        if (preg_match_all('/family=([^?:]*):/i', $paramsFontScheme, $matches) > 0) {
            $fontStyles = '--iwf-font-family-body: "' . str_replace('+', ' ', $matches[1][0]) . '", sans-serif;
			--iwf-font-family-headings: "' . str_replace('+', ' ', $matches[1][1] ?? $matches[1][0]) . '", sans-serif;
			--iwf-font-weight-normal: 400;
			--iwf-font-weight-headings: 700;';
        }
    } elseif ($paramsFontScheme === 'system') {
        $fontStylesBody    = $this->params->get('systemFontBody', '');
        $fontStylesHeading = $this->params->get('systemFontHeading', '');

        if ($fontStylesBody) {
            $fontStyles = '--iwf-font-family-body: ' . $fontStylesBody . ';
            --iwf-font-weight-normal: 400;';
        }
        if ($fontStylesHeading) {
            $fontStyles .= '--iwf-font-family-headings: ' . $fontStylesHeading . ';
    		--iwf-font-weight-headings: 700;';
        }
    } elseif ($paramsFontScheme === 'googlefonts') {
        $font = $this->params->get('googlefontsbody', '');
        $f = explode("|", $font);
        $fontStylesBody = $f[0];
        $fontStyle = $f[1];
        $fontWeight = $f[2];
        $fontStylesBody;
        if ($fontStylesBody) {
            $fontStyles = '--iwf-font-family-body: ' . $fontStylesBody . ';
        --iwf-font-weight-' . $fontStyle . ': ' . $fontWeight . ';';
        }
        $fontStylesHeading = $this->params->get('googlefontsheading', '');
        if ($fontStylesHeading) {
            $font = $fontStylesHeading;
            $f = explode("|", $font);
            $fontStylesHeading = $f[0];
            $fontStyle = $f[1];
            $fontWeight = $f[2];
            $fontStyles .= '
        --iwf-font-family-headings: ' . $fontStylesHeading . ';
    	--iwf-font-weight-headings: 700;';
        }
        $wa->useStyle('template.googlefonts');
    } else {
        $wa->registerAndUseStyle('fontscheme.current', $paramsFontScheme, ['version' => 'auto'], ['media' => 'print', 'rel' => 'lazy-stylesheet', 'onload' => 'this.media=\'all\'']);
        $this->getPreloadManager()->preload($wa->getAsset('style', 'fontscheme.current')->getUri() . '?' . $this->getMediaVersion(), ['as' => 'style']);
    }
}

// Enable assets
$colorsIwf = "";
$colorFile = JPATH_SITE . '/media/templates/site/iwf/css/global/colors_iwf.css';
if (file_exists($colorFile)) {
    $colorsIwf = file_get_contents($colorFile);
}
$verwcss = "";
$verwaltungcss = JPATH_SITE . '/media/com_verwaltung/css/verwaltung.css';
if (file_exists($verwaltungcss)) {
    $verwcss = file_get_contents($verwaltungcss);
}

$wa->usePreset('template.iwf.' . ($this->direction === 'rtl' ? 'rtl' : 'ltr'))
    ->useStyle('template.active.language')
    ->useStyle('template.icomoon')
    ->useStyle('template.user')
    ->useScript('template.user')
    ->addInlineStyle(":root {
		--hue: 214;
		--template-bg-light: #f0f4fb;
		--template-text-dark: #495057;
		--template-text-light: #ffffff;
		--template-link-color: var(--link-color);
		--template-special-color: #001B4C;
	    $fontStyles
        $colorsIwf
        $verwcss
	}");

// Override 'template.active' asset to set correct ltr/rtl dependency
$wa->registerStyle('template.active', '', [], [], ['template.iwf.' . ($this->direction === 'rtl' ? 'rtl' : 'ltr')]);

// Logo file or site title param

$showLogo = $this->params->get('brand');
$logofile = $this->params->get('logofile', '');
$gridCharPos = strpos($logofile, '#');
if ($gridCharPos) {
    $logofile = substr($logofile, 0, $gridCharPos);
}
if ($logofile) {
    $logoUri = htmlspecialchars($logofile, ENT_COMPAT, 'UTF-8');
    $logo = HTMLHelper::_('image', $logoUri, $sitename, ['loading' => 'eager', 'decoding' => 'async'], false, 0);
}
$logoIsImage = $this->params->get('logoisimage') && $logofile;
$stickyHeader =  $this->params->get('stickyheader') && !$logoIsImage;
$headerHeight =  $this->params->get('headerheight');

if ($logoIsImage) {
    $logoClass = ' imagelogo';
} else {
    $logoClass = ' backgroundlogo';
    $logoClass .= $stickyHeader ? ' stickyheader' : '';
    $logoStyle = ' style="background-image: url(' . $logoUri . ')"';
}

$hasClass = '';

if ($this->countModules('sidebar-left', true)) {
    $hasClass .= ' has-sidebar-left';
}

if ($this->countModules('sidebar-right', true)) {
    $hasClass .= ' has-sidebar-right';
}

// Container
$wrapper = $this->params->get('fluidContainer') ? 'wrapper-fluid' : 'wrapper-static';

$this->setMetaData('viewport', 'width=device-width, initial-scale=1');

?>
<!DOCTYPE html>
<html lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">

<head>
    <jdoc:include type="metas" />
    <jdoc:include type="styles" />
    <jdoc:include type="scripts" />
    <style>
        .backgroundlogo {
            height: <?php echo $headerHeight ?>px;
        }

        <?php if ($logoIsImage) : ?>.subhead {
            top: 0 !important;
        }

        <?php else : ?>.subhead {
            top: <?php echo $headerHeight; ?>px !important;
        }

        <?php endif; ?>
    </style>
</head>

<body class="site <?php echo $option
                        . ' ' . $wrapper
                        . ($layout ? ' layout-' . $layout : ' no-layout')
                        . ($task ? ' task-' . $task : ' no-task')
                        . ($itemid ? ' itemid-' . $itemid : '')
                        . ($pageclass ? ' ' . $pageclass : '')
                        . $hasClass
                        . ($this->direction == 'rtl' ? ' rtl' : '');
                    ?>">
    <header class="header container-header full-width<?php echo $logoClass; ?>" <?php echo $logoStyle ?>>
        <div class="page-title"></div>
        <?php if ($this->countModules('topbar')) : ?>
            <div class="container-topbar">
                <jdoc:include type="modules" name="topbar" style="none" />
            </div>
        <?php endif; ?>

        <?php if ($this->countModules('below-top')) : ?>
            <div class="grid-child container-below-top">
                <jdoc:include type="modules" name="below-top" style="none" />
            </div>
        <?php endif; ?>

        <?php if ($showLogo && $logoIsImage) : ?>
            <div class="grid-child">
                <div class="navbar-brand" style="margin:0 auto;">
                    <a class="brand-logo" href="<?php echo $this->baseurl; ?>/">
                        <?php echo $logo; ?>
                    </a>
                    <?php if ($this->params->get('siteDescription')) : ?>
                        <div class="site-description"><?php echo htmlspecialchars($this->params->get('siteDescription')); ?></div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>

        <?php if ($this->countModules('menu', true) || $this->countModules('search', true)) : ?>
            <div class="grid-child container-nav">
                <?php if ($this->countModules('menu', true)) : ?>
                    <jdoc:include type="modules" name="menu" style="none" />
                <?php endif; ?>
                <?php if ($this->countModules('search', true)) : ?>
                    <div class="container-search">
                        <jdoc:include type="modules" name="search" style="none" />
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </header>

    <div class="site-grid">
        <?php if ($this->countModules('banner', true)) : ?>
            <div class="container-banner full-width">
                <jdoc:include type="modules" name="banner" style="none" />
            </div>
        <?php endif; ?>

        <?php if ($this->countModules('top-a', true)) : ?>
            <div class="grid-child container-top-a">
                <jdoc:include type="modules" name="top-a" style="card" />
            </div>
        <?php endif; ?>

        <?php if ($this->countModules('top-b', true)) : ?>
            <div class="grid-child container-top-b">
                <jdoc:include type="modules" name="top-b" style="card" />
            </div>
        <?php endif; ?>

        <?php if ($this->countModules('sidebar-left', true)) : ?>
            <div class="grid-child container-sidebar-left">
                <jdoc:include type="modules" name="sidebar-left" style="card" />
            </div>
        <?php endif; ?>

        <div class="grid-child container-component">
            <jdoc:include type="modules" name="breadcrumbs" style="none" />
            <?php if (!$hideMenu) : ?>
                <jdoc:include type="modules" name="main-top" style="card" />
            <?php endif; ?>
            <jdoc:include type="message" />
            <main>
                <jdoc:include type="component" />
            </main>
            <jdoc:include type="modules" name="main-bottom" style="card" />
        </div>

        <?php if (!$hideMenu && $this->countModules('sidebar-right', true)) : ?>
            <div class="grid-child container-sidebar-right">
                <jdoc:include type="modules" name="sidebar-right" style="card" />
            </div>
        <?php endif; ?>

        <?php if ($this->countModules('bottom-a', true)) : ?>
            <div class="grid-child container-bottom-a">
                <jdoc:include type="modules" name="bottom-a" style="card" />
            </div>
        <?php endif; ?>

        <?php if ($this->countModules('bottom-b', true)) : ?>
            <div class="grid-child container-bottom-b">
                <jdoc:include type="modules" name="bottom-b" style="card" />
            </div>
        <?php endif; ?>
    </div>

    <?php if ($this->countModules('footer', true)) : ?>
        <footer class="container-footer footer full-width">
            <div class="grid-child">
                <jdoc:include type="modules" name="footer" style="none" />
            </div>
        </footer>
    <?php endif; ?>

    <?php if ($this->params->get('backTop') == 1) : ?>
        <a href="#top" id="back-top" class="back-to-top-link" aria-label="<?php echo Text::_('TPL_IWF_BACKTOTOP'); ?>">
            <span class="icomoon-top icon-fw" aria-hidden="true"></span>
        </a>
    <?php endif; ?>

    <jdoc:include type="modules" name="debug" style="none" />
</body>

</html>