/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

jQuery(document).ready(function () {
    fetch_licenses(null);
    jQuery('#installed_licenses_button').click(function (event) {
        fetch_licenses(event);
    });

    function fetch_licenses(event) {
        // eventType: 0 = mouse, 1 = keyboard
        if (event) {
            event.preventDefault();
        }

        var formData = jQuery("#item-form").serializeArray();
        var path = 'index.php?option=com_verwaltung&task=program.findlicenses';

        jQuery.ajax({
            url: path,
            type: "POST",
            dataType: "json",
            data: { "jform": formData },
            async: false,
            processData: true,
            /*beforeSend: function () {
                jQuery('#keytek-finder-tag-submit').hide();
                jQuery('#keytek-finder-tag-spinner').show();
            },*/
            success: done,
            error: failure,
            complete: function (xhr, status) {
                /*jQuery('#keytek-finder-tag-spinner').hide();
                jQuery('#keytek-finder-tag-submit').show();*/
            }
        });
    }

    function done(data, textStatus, jqXHR) {
        if (data.message) {
            jQuery('#licenses_html').html(data.message);
            jQuery('#license_ajax_result').slideDown("slow", function () { });
        }
        else {
            jQuery('#licenses_html').html('');
        }
        if (data.error) {
            alert(data.error);
        }
    }

    function failure(jqXHR, textStatus, errorThrown) {
        jQuery('#licenses_html').html(jqXHR.responseText);
        jQuery('#license_ajax_result').slideDown("slow", function () { });
    }
  });
  