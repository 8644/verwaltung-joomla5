/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

jQuery(document).ready(function () {
  $("#adminForm").find('.row').addClass('loadready');
});

function startLoader() {
  $("#adminForm").find('.row').css("display", "none");
  $("#loader").css("display", "block");
}

function stopLoader() {
  $("#adminForm").find('.row').css("display", "flex");
  $("#loader").css("display", "none");
}
