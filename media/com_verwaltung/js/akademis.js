/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */
jQuery(document).ready(function () {
    document.formvalidator.setHandler('name',
            function (value) {
                regex = /^[^6-9]+$/;
                return regex.test(value);
            });
    document.formvalidator.setHandler('nowhitespace',
            function (value) {
                regex = /^[^0-9_ ]+$/;
                return regex.test(value);
            });
    document.formvalidator.setHandler('float',
            function (value) {
                regex = /^[0-9\,\.]+$/;
                return regex.test(value);
            });
    document.formvalidator.setHandler('time',
            function (value) {
                regex = /^([0-9]{2})[:]+([0-9]{2})[:]?([0-9])*$/;
                return regex.test(value);
            });
    document.formvalidator.setHandler('date',
            function (value) {
                regex = /^([0-9]{4})[-]{1}([0-9]{2})[-]{1}([0-9]{2})$/;
                var res = regex.test(value);
                if (!res) {
                    alert('Datumsformat: YYYY-MM-DD');
                }
                return res;
            });
    document.formvalidator.setHandler('raumreservierung',
            function (value) {
                regex = /^[0-9:\-]*$/;
                return regex.test(value);
            });
    document.formvalidator.setHandler('autoren',
            function (value) {
                return validAutor(value, 'autoren');
            });
    document.formvalidator.setHandler('literaturtitel',
            function (value) {
                regex = /^([?]+)$/;
                return !regex.test(value);
            });
    document.formvalidator.setHandler('herausgeber',
            function (value) {
                return validAutor(value, 'herausgeber');
            });
    document.formvalidator.setHandler('betreuer',
            function (value) {
                return validAutor(value, 'betreuer');
            });

    if (jQuery('#jform_dienstverhaeltnis')) {
        jQuery('#jform_dienstverhaeltnis').change(function () {
            return Joomla.submitform('ma.edit', document.getElementById('item-form'), true);
        });
    }
    if (jQuery('#jform_publikationstyp')) {
        jQuery('#jform_publikationstyp').change(function () {
            return Joomla.submitform('publication.edit', document.getElementById('item-form'), true);
        });
    }
});

function validAutor(value, field) {
    // doppelte Leerzeichen entfernen
    re = /[\x20\xc2\xa0]{2,}/;
    while (re.exec(value)) {
        value = value.replace(re, " ");
    }
    // andere Sonderleerzeichen entfernen
    re = /[\xc2\xa0]{1,}/;
    while (re.exec(value)) {
        value = value.replace(re, "");
    }
    // fehlende Leerzeichen nach Komma einfügen
    re = /(,[\u003a-\u0300]{1})/g;
    while (true)
    {
        m = re.exec(value);
        if (m === null) {
            break;
        }
        value = value.substring(0, m.index + 1) + ' ' + value.substr(m.index + 1);
    }
    // fehlende Leerzeichen nach Punkt und darauffolgendem Nachnamen einfügen
    re = /[\u003a-\u0300]\.[\u003a-\u0300]{2,}/;
    while (true)
    {
        m = re.exec(value);
        if (m === null) {
            break;
        }
        value = value.substring(0, m.index + 2) + ' ' + value.substr(m.index + 2);
    }
    // führende Leerzeichen vor Komma entfernen
    re = /\x20,/;
    while (re.exec(value)) {
        value = value.replace(re, ',');
    }
    // fehlenden Punkt nach Initiale und Leerzeichen einfügen
    re = /\b[\u003a-\u0300]{1}\x20/;
    while (true) {
        m = re.exec(value);
        if (m === null) {
            break;
        }
        value = value.substring(0, m.index + 1) + '.' + value.substr(m.index + 1);
    }
    // fehlenden Punkt nach Initiale und Komma einfügen
    /*re = /\b[\u003a-\u0300]{1},/;
     while (m = re.exec(value)) {
     value = value.substring(0, m.index + 1) + '.' + value.substr(m.index + 1);
     }*/
    // fehlenden Punkt nach Initiale und Bindestrich einfügen
    re = /\b[\u003a-\u0300]{1}-/;
    while (true) {
        m = re.exec(value);
        if (m === null) {
            break;
        }
        value = value.substring(0, m.index + 1) + '.' + value.substr(m.index + 1);
    }
    // Leerzeichen vor Bindestrich (C. - M. => C.-M.)
    re = /(\x20-)/;
    while (true) {
        m = re.exec(value);
        if (m === null) {
            break;
        }
        value = value.substring(0, m.index) + value.substr(m.index + 1);
    }
    // Leerzeichen nach Bindestrich (C.- M. => C.-M.)
    re = /(-\x20)/;
    while (true) {
        m = re.exec(value);
        if (m === null) {
            break;
        }
        value = value.substring(0, m.index + 1) + value.substr(m.index + 2);
    }
    // Zeilenenden entfernen
    re = /[\x0d]+/;
    while (re.exec(value)) {
        value = value.replace(re, "");
    }
    re = /[\x0a]+/;
    while (re.exec(value)) {
        value = value.replace(re, "");
    }
    if (value === "" || value === null) {
        return false;
    }
    value = value.trim();
    //Komma am Ende entfernen
    re = /[,]$/g;
    var match = re.exec(value);
    if (match !== null) {
        value = value.substring(0, match.index);
    }
    // Leerzeichen zwischen 2 Namensabkürzungen entfernen
    re = /([\u003a-\u0300]{1,2}\.([ ]+[-]?([\u003a-\u0300]{1,2}\.))+)/;
    while (true) {
        m = re.exec(value);
        if (m === null) {
            break;
        }
        tmp = m[0].replace(" ", "");
        value = value.substring(0, m.index) + tmp + value.substr(m.index + m[0].length);
    }
    //Erstautorformat prüfen
    // Achtung: \w enthält nur Zeichen a-zA-Z0-9, daher \u... verwenden
    re = /^([\u0027\u003a-\u0300 -]{2,})(, )([\u003a-\u0300]{1,2}\.((-)?([\u003a-\u0300]{1,2}\.))?)/g;
    matches = re.exec(value);
    if (matches === null) {
        alert("Falsches Erstautor-Format. Richtig: Nachname, V.[X.]\nNur ein Leerzeichen zw. Nach- und Vorname, kein Leerzeichen zwischen Vornamen!\nVorname(n) nur mit 1.Buchstaben abk\u00FCrzen!");
        return false;
    }
    jQuery('#jform_' + field).val(value);
    return true;
}
