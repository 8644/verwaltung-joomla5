/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

(function ($) {
    $(function ($, undefined) { // attach callback to run onready
        // code to run onready
        $('#jform_gaeste').change(function (event) {
            var sel = $('#jform_gaeste :selected').val();
            if (sel) {
                fillFields(event);
            }
        });
        $('#sync_ma').click(function (event) {
            if (confirm(Joomla.JText._('COM_VERWALTUNG_GASTFORM_CONFIRM_SYNC', ''))) {
                syncWithMa(event);
            }
        });

        function fillFields(event) {
            if (event) {
                event.preventDefault();
            }
            var formData = $("#item-form").serializeArray();
            var path = 'index.php?option=com_verwaltung&task=gastform.findGastData';
            $.ajax({
                url: path,
                type: "POST",
                dataType: "json",
                data: { "jform": formData },
                async: false,
                processData: true,
                success: done,
                error: failure,
                complete: function (xhr, status) {
                }
            });

            function done(data, textStatus, jqXHR) {
                if (data.message) {
                    var maData = JSON.parse(data.message);
                    $('#jform_vorname').val(maData.vorname);
                    $('#jform_nachname').val(maData.nachname);
                    $('#jform_geschlecht').val(maData.geschlecht);
                    $('#jform_geburtsdatum').val(maData.geburtsdatum);
                    $('#jform_institut').val(maData.gastinstitution);
                    $('#jform_nation').val(maData.gastland).change();
                    if (maData.gastaufenthaltszweck) {
                        $('#jform_aufenthaltszweck').val(maData.gastaufenthaltszweck).change();
                    }
                }
                if (data.error) {
                    alert(data.error);
                }
            }

            function failure(jqXHR, textStatus, errorThrown) {
                alert(errorThrown + jqXHR.responseText);
            }
        }

        function syncWithMa(event) {
            if (event) {
                event.preventDefault();
            }
            var formData = $("#item-form").serializeArray();
            var path = 'index.php?option=com_verwaltung&task=gastform.syncWithMa';
            $.ajax({
                url: path,
                type: "POST",
                dataType: "json",
                data: { "jform": formData },
                async: false,
                processData: true,
                success: done,
                error: failure,
                complete: function (xhr, status) {
                }
            });

            function done(data, textStatus, jqXHR) {
                if (data.message) {
                    Joomla.renderMessages({ 'message': [data.message] });
                    if (data.insert == 1) {
                        Joomla.submitform('gastform.edit', document.getElementById('item-form'), true);
                    }
                }
                if (data.error) {
                    Joomla.renderMessages({ 'error': [data.error] });
                }
            }

            function failure(jqXHR, textStatus, errorThrown) {
                alert(jqXHR.responseText);
            }
        }

    });

})(jQuery);