/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */
 
function getActionButton(button, event, message) {
    if (event.button != 0) { // nur linke Maustaste
        return;
    }
    if (message) {} else {message='Wirklich weiter?';}
    var id = (document.getElementById('actionButton'));
    if (id) {
        id.value = button['id'];
        if (id['attributes'].askpermission) {
            if (!confirm(message)) {
                id.value = '0-0-0';
            } else {
                button.click();
            }
        } else {
            button.click();
        }
    }
}
