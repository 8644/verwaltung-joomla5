/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */
function copySqlResult() {
    var result = document.getElementById("sqlresult");
    if (result) {
        var copyText = result.innerHTML;
        navigator.clipboard.writeText(copyText);
        alert(Joomla.JText._('COM_VERWALTUNG_DBQUERY_SQL_COPIED', 'Table in clipboard - can be copied into Excel'));
    }
}
