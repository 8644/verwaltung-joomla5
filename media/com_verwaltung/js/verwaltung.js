/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

(function ($) {
    $(document).ready(function () {
        if ($('#jform_dienstverhaeltnis')) {
            $('#jform_dienstverhaeltnis').change(function () {
                return Joomla.submitform('ma.edit', document.getElementById('item-form'), false);
            });
        }
        if ($('#jform_publikationstyp')) {
            $('#jform_publikationstyp').change(function () {
                return Joomla.submitform('publication.edit', document.getElementById('item-form'), false);
            });
        }
    });

    $(function ($, undefined) {
        $('[id^=macaddress_]').click(function (event) {
            if (confirm(Joomla.Text._('COM_VERWALTUNG_COMPUTER_CONFIRM_WAKEUP', ''))) {
                wakeup(event);
            }
        });
    });

    function wakeup(event) {
        if (event) {
            event.preventDefault();
        }
        var mac = event.target.innerHTML.trim();
        var path = 'index.php?option=com_verwaltung&tmpl=component&task=computers.wol';
        $.ajax({
            url: path,
            type: "POST",
            dataType: "json",
            data: { "mac": mac },
            async: false,
            processData: true,
            success: done,
            error: failure,
            complete: function (xhr, status) {
            }
        });
        
        function done(data, textStatus, jqXHR) {
            if (data.message) {
                Joomla.renderMessages({ 'message': [data.message] });
                if (data.insert == 1) {
                    Joomla.submitform();
                }
            }
            if (data.error) {
                Joomla.renderMessages({ 'error': [data.error] });
            }
        }

        function failure(jqXHR, textStatus, errorThrown) {
            alert(jqXHR.responseText);
        }
    }
})(jQuery);
