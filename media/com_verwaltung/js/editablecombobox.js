/**
 * @package		Joomla.JavaScript
 * @copyright	Copyright (C) 2005 - 2014 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */

// Only define the Joomla namespace if not defined.
if (typeof(Joomla) === 'undefined') {
	var Joomla = {};
}

/**
* JCombobox JavaScript behavior.
*
* Inspired by: Subrata Chakrabarty <http://chakrabarty.com/editable_dropdown_samplecode.html>
*
* @package		Joomla.JavaScript
* @since		1.6
*/

Joomla.combobox = {};
Joomla.combobox.transform = function(el, options)
{
    // Add the editable option to the select.
    var option = jQuery("<option>", { class: "mycombobox", text: Joomla.JText._('ComboBoxInitString', 'type custom...'), id: "mycombobox" });
    el.prepend(option);
    el.prop('changeType', 'manual');

	// Add a key press event handler.
	el.keypress(function(e, o){
	    e.preventDefault();
	    target = e.target;
	    if (o) {
	        target = o.target;
	    }

		// The change in selected option was browser behavior.
	    if ((this.options.selectedIndex != 0) && (target.changeType == 'auto'))
		{
			this.options.selectedIndex = 0;
			target.changeType = 'manual';
		}

		// Check to see if the character is valid.
		if ((e.charCode > 47 && e.charCode < 59) || (e.charCode > 62 && e.charCode < 127) || (e.charCode == 32)) {
			var validChar = true;
		} else {
			var validChar = false;
		}

		// If the editable option is selected, proceed.
		if (this.options.selectedIndex == 0)
		{
			// Get the custom string for editing.
			var customString = this.options[0].value;

			// If the string is being edited for the first time, nullify it.
			if ((validChar == true) || (e.keyCode == 8))
			{
				if (customString == Joomla.JText._('ComboBoxInitString', 'type custom...')) {
					customString = '';
				}
			}

			// If the backspace key was used, remove a character from the end of the string.
			if (o && o.keyCode == 8)
			{
				customString = customString.substring(0, customString.length - 1);
				if (customString == '') {
					customString = Joomla.JText._('ComboBoxInitString', 'type custom...');
				}

				// Indicate that the change event was manually initiated.
				target.changeType = 'manual';
			}

			// Handle valid characters to add to the editable option.
			if (validChar == true)
			{
				// Concatenate the new character to the custom string.
			    customString += String.fromCharCode(e.keyCode);
			}

			// Set the new custom string into the editable select option.
			this.options.selectedIndex = 0;
			this.options[0].text = customString;
			this.options[0].value = customString;
			return false;
		}
	});

	// Add a change event handler.
	el.change( function(e){

		// The change in selected option was browser behavior.
		if ((this.options.selectedIndex != 0) && (e.target.changeType == 'auto'))
		{
			this.options.selectedIndex = 0;
			e.target.changeType = 'manual';
		}
	});

	// Add a keydown event handler.
	el.keydown(function(e){
	    // Stop the backspace key from firing the back button of the browser.
	    if (e.keyCode == 8 || e.keyCode == 127) {
	        e.charCode = e.keyCode;
			// Stopping the keydown event in WebKit stops the keypress event as well.
	        if (jQuery.browser.webkit) {
	            e.preventDefault();

	            jQuery('#' + e.target.id).trigger('keypress', e, e.keyCode);
	            return;
			}
		}

		if (this.options.selectedIndex == 0)
		{
			/*
			 * In some browsers a feature exists to automatically jump to select options which
			 * have the same letter typed as the first letter of the option.  The following
			 * section is designed to mitigate this issue when editing the custom option.
			 *
			 * Compare the entered character with the first character of all non-editable
			 * select options.  If they match, then we assume the change happened because of
			 * the browser trying to auto-change for the given character.
			 */
		    var character = String.fromCharCode(e.keyCode).toLowerCase();
			for (var i = 1; i < this.options.length; i++)
			{
				// Get the first character from the select option.
				var FirstChar = this.options[i].value.charAt(0).toLowerCase();

				// If the first character matches the entered character, the change was automatic.
				if (FirstChar == character) {
					this.options.selectedIndex = 0;
					e.target.changeType = 'auto';
					return;
				}
			}
		}
	});

	// Add a keyup event handler.
	el.keyup(function(e){

		// If the left or right arrow keys are pressed, return to the editable option.
	    if ((e.keyCode == 37) || (e.keyCode == 39)) {
			this.options.selectedIndex = 0;
		}

		// The change in selected option was browser behavior.
		if ((this.options.selectedIndex != 0) && (this.target.changeType == 'auto'))
		{
			this.options.selectedIndex = 0;
			this.target.changeType = 'manual';
		}
	});

	el.blur(function (e) {
	    if (this.options[0].value == Joomla.JText._('ComboBoxInitString', 'type custom...')) {
	        this.options[0].value = "";
	    }
	});
};

jQuery(document).ready(function(){
    var elements = $(".combobox");
    if (elements["length"] == 1) {
        var id = elements[0].id;
        Joomla.combobox.transform(jQuery('#' + id));
    }
});
