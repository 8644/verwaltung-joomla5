<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Field */
class SchluesseltypField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $typ = "schluesseltyp";
        $query->select('a.*')
            ->from($db->qn('#__iwf_listen', 'a'))
            ->where($db->qn('a.kategorie') . '=:schluesseltyp')
            ->bind(':schluesseltyp', $typ, ParameterType::STRING)
            ->order('a.reihenfolge');
        $options = [];
        $db->setQuery($query);
        $items = $db->loadObjectList();
        if (count($items)) {
            foreach ($items as $item) {
                $options[] = HTMLHelper::_('select.option', $item->value, $item->inhalt);
            }
        }
        return array_merge(parent::getOptions(), $options);
    }
}
