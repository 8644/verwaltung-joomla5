<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

//In old File FormFieldCombo protected function getInput und protected getOptions
namespace Iwf\Component\Verwaltung\Administrator\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\ParameterType;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\CMS\Factory;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Field */
class ReihenfolgeField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $id = Factory::getApplication()->input->getInt('id');
        $kategorien = 1;
        if ($id == null) {
            $id = 0;
        }
        $query->select($db->qn('a.kategorie', 'kategorie'))
            ->from($db->qn('#__iwf_listen', 'a'))
            ->where($db->qn('a.id') . '=:id')
            ->bind(':id', $id, ParameterType::INTEGER);
        if ($item = $db->setQuery($query)->loadObject()) {
            $kategorie = $item->kategorie;
            $query = $db->createQuery();
            $query->select('*')
                ->from($db->qn('#__iwf_listen', 'a'))
                ->where($db->qn('a.kategorie') . '=:kategorie')
                ->bind(':kategorie', $kategorie, ParameterType::STRING)
                ->where($db->qn('a.aktiv') . '=1');
            $kategorien = count($db->setQuery($query)->loadObjectList());
        }
        $options = [];
        $i = 0;
        for ($i = 1; $i < $kategorien + 1; $i++) {
            $options[] = HTMLHelper::_('select.option', $i, $i);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
