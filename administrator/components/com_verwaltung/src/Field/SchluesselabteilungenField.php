<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 * 
 * 
 * name abteilung typ:schluesselabteilungen
 */

namespace Iwf\Component\Verwaltung\Administrator\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\CheckboxesField;
use Joomla\CMS\HTML\HTMLHelper;
use Iwf\Component\Verwaltung\Administrator\Helper\VerwaltungHelper;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Field */
class SchluesselabteilungenField extends CheckboxesField
{

    /**
     * @return object[] 
     * @throws KeyNotFoundException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        if (is_string($this->value)) {
            $this->value = explode(',', $this->value);
        }
        $options = [];
        $abteilungen = VerwaltungHelper::getAbteilungen();
        foreach ($abteilungen as $abteilung) {
            $tmp = HTMLHelper::_('select.option', $abteilung->id, $abteilung->abteilung);
            $tmp->checked = (in_array($abteilung->id, $this->value) ? '1' : '0');
            $options[] = $tmp;
        }
        return array_merge(parent::getOptions(), $options);
    }
}
