<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\ComboField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Field */
class GekauftbeiField extends ComboField
{
    /**
     * @return string 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getInput()
    {
        $html = array();
        $attr = '';
        $attr .= $this->element['class'] ? ' class="combobox ' . (string) $this->element['class'] . '"' : ' class="combobox form-select valid"';
        $attr .= ' aria-describedby="jform_standort-desc"';
        $attr .= ' aria-invalid="false"';
        $attr .= ((string) $this->element['readonly'] == 'true') ? ' readonly="readonly"' : '';
        $attr .= ((string) $this->element['disabled'] == 'true') ? ' disabled="disabled"' : '';
        $attr .= $this->element['size'] ? ' size="' . (int) $this->element['size'] . '"' : '';
        $attr .= $this->element['onchange'] ? ' onchange="' . (string) $this->element['onchange'] . '"' : '';
        $options = $this->getOptions();
        $html[] = HTMLHelper::_('select.genericlist', $options, $this->name, trim($attr), 'value', 'text', $this->value, $this->id);
        return implode($html);
    }

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select('DISTINCT ' . $db->qn('a.gekauftbei', 'gekauftbei'))
            ->from($db->qn('#__iwf_software', 'a'))
            ->order($db->qn('a.gekauftbei'));
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $options[] = HTMLHelper::_('select.option', $item->gekauftbei, $item->gekauftbei);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
