<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 * 
 * name="berechtigungen"
 * type="schluesselberechtigungen"
 */

namespace Iwf\Component\Verwaltung\Administrator\Field;

use InvalidArgumentException;
use Joomla\CMS\Form\Field\CheckboxesField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Field */
class SchluesselberechtigungenField extends CheckboxesField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    protected function getOptions()
    {
        if (is_string($this->value)) {
            $this->value = explode(',', $this->value);
        }
        asort($this->value);
        $db = $this->getDatabase();
        $query = $db->getQuery(true)
            ->select(
                [
                    'a.id AS id',
                    'a.zimmer AS zimmer',
                    'a.raumbezeichnung AS raumbezeichnung',
                ]
            )

            ->from($db->qn('#__iwf_zimmer', 'a'))
            ->where($db->qn('a.schlosstyp') . '=' . '"T"');  //Türen mit transponderzylinder $db->qn('#__iwf_bestellungen')
        $options = [];
        $db->setQuery((string) $query);

        if ($db->execute()) {
            $items = $db->loadObjectList();
            if (count($items)) {
                foreach ($items as $zimmer) {
                    $tmp = HTMLHelper::_('select.option', $zimmer->id, $zimmer->zimmer . '(' .  $zimmer->raumbezeichnung . ')');
                    $tmp->checked = (in_array($zimmer->id, $this->value) ? '1' : '0');
                    $options[] = $tmp;
                }
            }
        }
        return $options;
    }
}
