<?php
/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Field;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Factory;
use Joomla\Database\Exception\DatabaseNotFoundException;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Field */
class BaramundiversionField extends ListField {

    /**
     * @return object[] 
     * @throws Exception 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions() {
        $app = Factory::getApplication();
		$baramundi_product = $app->getUserState('com_verwaltung.baramundi.product');
        if (is_string($this->value)) {
            $this->value = explode(',', $this->value);
        }
        $options = [];
        if (!empty($baramundi_product)) {
            $db = $this->getDatabase();
            $query = $db->createQuery()
                ->select
                (
                    [
                        'CONCAT( ' . $db->qn('a.version') . ', " {", ' . $db->qn('a.product_name') . ', "}") AS name',
                        $db->qn('a.version')
                    ]
                )
                ->from($db->qn('#__iwf_baramundi_sw', 'a'));
            $baramundi_products = preg_split("/[,;]/", $baramundi_product);
            foreach ($baramundi_products as $product) {
                $product = '%' . trim($product) . '%';
                $query->where($db->qn('a.product_name') . " LIKE \"$product\"", 'OR');
            }
            $query->order($db->qn('a.product_name'));
            $items = $db->setQuery($query)->loadObjectList();
            foreach ($items as $item) {
                $sel = !(array_search($item->version, $this->value) === false);
                $opt =  HTMLHelper::_('select.option', $item->version, $item->name);
                if ($sel) {
                    $opt->optionattr = ['selected'=>'selected'];
                }
                $options[] = $opt;
            }
        }
        return array_merge(parent::getOptions(), $options);
    }
}
