<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Field */
class DomainField extends ListField
{

    /**
     * @return object[] 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $kategorie = 'domains';
        $query->select
            (
                [
                    $db->qn('a.id', 'id'),
                    $db->qn('a.inhalt', 'domain'),
                ]
            )
            ->from($db->qn('#__iwf_listen', 'a'))
            ->where($db->qn('a.kategorie') . '=:domain')
            ->bind(':domain', $kategorie, ParameterType::STRING)
            ->where('a.aktiv=1')
            ->order('reihenfolge');
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $options[] = HTMLHelper::_('select.option', $item->domain, $item->domain);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
