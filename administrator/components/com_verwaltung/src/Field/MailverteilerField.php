<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Field */
class MailverteilerField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select('a.inhalt as mailverteiler')
            ->from('#__iwf_listen as a')
            ->where('(a.kategorie="mailverteiler")')->where('(a.aktiv=1)');
        $options = [];
        if ($db->setQuery($query)->execute()) {
            $items = $db->loadObjectList();
            if ($items) {
                foreach ($items as $item) {
                    $options[] = HTMLHelper::_('select.option', $item->mailverteiler, $item->mailverteiler);
                }
            }
        }
        return array_merge(parent::getOptions(), $options);
    }
}
