<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\DI\Exception\KeyNotFoundException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Field */
class AbteilungField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select(
            [
                $db->qn('a.id', 'id'),
                $query->concatenate([$db->qn('a.institut'), $db->qn('a.abteilung')], '-') . ' AS abteilung'
            ]
            )
            ->from($db->qn('#__iwf_abteilungen', 'a'))
            ->where($db->qn('a.state') . '=1')
            ->order($db->qn('a.reihenfolge'));
        $items = $db->setQuery($query)->loadObjectList();
        $options = [];
        foreach ($items as $item) {
            $options[] = HTMLHelper::_('select.option', $item->id, $item->abteilung);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
