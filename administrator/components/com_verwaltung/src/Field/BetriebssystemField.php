<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use RuntimeException;
use Joomla\DI\Exception\KeyNotFoundException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Field */
class BetriebssystemField extends ListField
{

    /**
     * @return object[] 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->getQuery(true);
        $kategorie = 'os';
        $query->select
            (
                [
                    $db->qn('a.id', 'id'),
                    $db->qn('a.inhalt', 'name'),
                ]
            )
            ->from($db->qn('#__iwf_listen', 'a'))
            ->where($db->qn('a.kategorie') . '=:kategorie')
            ->bind(':kategorie', $kategorie, ParameterType::STRING)
            ->order('a.inhalt');
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $options[] = HTMLHelper::_('select.option', $item->id, $item->name);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
