<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Field;

use InvalidArgumentException;
use Exception;
use Joomla\CMS\Form\Field\ListField;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\UnsupportedAdapterException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use RuntimeException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Field */
class MitarbeiterField extends ListField
{

    /**
     * @return object[] 
     * @throws KeyNotFoundException 
     * @throws UnsupportedAdapterException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     */
    protected function getOptions()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $dv = (int)DIENSTVERHAELTNIS_OEAW;
        $query->select(
                [
                    $db->qn('a.id', 'id'),
                    $query->concatenate([$db->qn('a.nachname'), $db->qn('a.vorname')], ' ') . ' AS name'
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter', 'a'))
            ->where($db->qn('a.dienstverhaeltnis') . '=:dv')
            ->where($db->qn('a.state') . '=1')
            ->where($db->qn('a.nachname') . ' NOT LIKE "EDV%"')
            ->bind(':dv', $dv, ParameterType::INTEGER)
            ->order($db->qn('a.nachname'));
        $options = [];
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $options[] = HTMLHelper::_('select.option', $item->id, $item->name);
        }
        return array_merge(parent::getOptions(), $options);
    }
}
