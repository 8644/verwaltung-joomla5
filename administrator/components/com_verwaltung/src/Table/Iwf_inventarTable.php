<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Table;

defined('_JEXEC') or die;

use Exception;
use BadMethodCallException;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\CMS\Factory;
use Joomla\CMS\Table\Table;
use Joomla\Database\DatabaseDriver;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\Table */
class Iwf_inventarTable extends Table
{
    
    /**
     * @param DatabaseDriver $db 
     * @return void 
     * @throws UnexpectedValueException 
     * @throws Exception 
     * @throws BadMethodCallException 
     */
    function __construct(DatabaseDriver $db)
    {
        parent::__construct('#__iwf_inventar', 'id', $db);
    }

    /**
     * @param bool $updateNulls 
     * @return bool 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws BadMethodCallException 
     * @throws UnexpectedValueException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    public function store($updateNulls = true)
    {
        $date   = Factory::getDate('now')->toSql();
        $userId = Factory::getApplication()->getIdentity()->id;
        $person = Person::getInstance();
        if ($this->rueckgefordert) {
            if ((int)$this->rueckgefordert_time == 0) {
                $this->rueckgefordert_time = $date;
            }
        } else {
            $this->rueckgefordert_time = '0000-00-00 00:00:00';
            $this->rueckgefordert_mail = 0;
        }
        if (!(int) $this->created_time) {
            $this->created_time = $date;
        }
        if ($this->id) {
            $this->modified_by = $userId;
            $this->modified_time = $date;
            $this->modified_by_fzguser_id = $person->ma_id;
        } else {
            if (empty($this->created_by)) {
                $this->created_by = $userId;
            }

            if (!(int) $this->modified_time) {
                $this->modified_time = $date;
            }

            if (empty($this->modified_by)) {
                $this->modified_by = $userId;
            }
        }
        if (empty($this->deinventarisierungsdatum)) {
            $this->deinventarisierungsdatum = '0000-00-00 00:00:00';
        }
        if (empty($this->letztkontrolle)) {
            $this->letztkontrolle = '0000-00-00 00:00:00';
        }
        if (empty($this->bestellung)) {
            $this->bestellung = 0;
        }
        return parent::store($updateNulls);
    }
}
