<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Table;

defined('_JEXEC') or die;
define("COM_VERWALTUNG_KEYS_LESS_ITEMS_LEFT", "Schlüssel mit Id=%s (%s/%s) noch in Verwendung - Löschen (auch eventuell weiterer selektierter Schlüssel) abgebrochen!");

use Exception;
use BadMethodCallException;
use InvalidArgumentException;
use Joomla\CMS\Application\CMSWebApplicationInterface;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Table\Table;
use Joomla\Database\DatabaseDriver;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\Exception\UnsupportedAdapterException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\Table */
class Iwf_schliessanlageTable extends Table
{
    
    /**
     * @param DatabaseDriver $db 
     * @return void 
     * @throws UnexpectedValueException 
     * @throws Exception 
     * @throws BadMethodCallException 
     */
    function __construct(DatabaseDriver $db)
    {
        parent::__construct('#__iwf_schliessanlage', 'id', $db);
    }

    /**
     * @param bool $updateNulls 
     * @return bool 
     * @throws BadMethodCallException 
     * @throws UnexpectedValueException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     */
    public function store($updateNulls = true)
    {
        if (is_array($this->abteilung)) {
            $this->abteilung = implode(',', $this->abteilung);
        }
        if (is_array($this->berechtigungen)) {
            $this->berechtigungen = implode(',', $this->berechtigungen);
        } else {
            $this->berechtigungen = "";
        }
        return parent::store($updateNulls);
    }

    /**
     * @param mixed $pk 
     * @return bool 
     * @throws UnsupportedAdapterException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws RuntimeException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function delete($pk = null) {
        $db = $this->getDbo();
        $query = $db->createQuery();
        $query->select('*')
            ->from($db->qn('#__iwf_schluessel'))
            ->where($db->qn('schluessel') . '=:id')
            ->bind(':id', $this->id, ParameterType::INTEGER);
        if ($db->setQuery($query)->loadObject()) {
            Factory::getApplication()->enqueueMessage(sprintf(Text::_(COM_VERWALTUNG_KEYS_LESS_ITEMS_LEFT), $this->id, $this->bezeichnung, $this->raum_gruppe), CMSWebApplicationInterface::MSG_WARNING);
            return false; // Schlüssel noch in Verwendung - nicht löschen
        }
        return parent::delete($pk);
    }
}
