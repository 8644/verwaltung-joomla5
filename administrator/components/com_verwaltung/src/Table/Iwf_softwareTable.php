<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Table;

defined('_JEXEC') or die;

use Exception;
use BadMethodCallException;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\CMS\Factory;
use Joomla\CMS\Table\Table;
use Joomla\Database\DatabaseDriver;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\Table */
class Iwf_softwareTable extends Table
{

    /**
     * @param DatabaseDriver $db 
     * @return void 
     * @throws UnexpectedValueException 
     * @throws Exception 
     * @throws BadMethodCallException 
     */
    function __construct(DatabaseDriver $db)
    {
        parent::__construct('#__iwf_software', 'id', $db);
    }

    /**
     * @param bool $updateNulls 
     * @return bool 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws BadMethodCallException 
     * @throws UnexpectedValueException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    public function store($updateNulls = true)
    {
        $date   = Factory::getDate()->toSql();
        $userId = Factory::getApplication()->getIdentity()->id;
        $person = Person::getInstance();
        if (!(int) $this->created_time) {
            $this->created_time = $date;
        }
        if ($this->id) {
            $this->modified_by = $userId;
            $this->modified_time = $date;
            $this->modified_by_fzguser_id = $person->ma_id;
        } else {
            if (empty($this->created_by)) {
                $this->created_by = $userId;
            }
            if (!(int) $this->modified_time) {
                $this->modified_time = $date;
            }
            if (empty($this->modified_by)) {
                $this->modified_by = $userId;
            }
        }
        if ($this->baramundi_product) {
            Factory::getApplication()->setUserState('com_verwaltung.baramundi.product', $this->baramundi_product);
        } else {
            Factory::getApplication()->setUserState('com_verwaltung.baramundi.product', '');
        }
        if (is_array($this->baramundi_version)) {
            $this->baramundi_version = implode(',', $this->baramundi_version);
        } else {
            $this->baramundi_version = '';
        }
        return parent::store($updateNulls);
    }
}
