<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Table;

defined('_JEXEC') or die;

use BadMethodCallException;
use InvalidArgumentException;
use Exception;
use Joomla\CMS\Table\Table;
use Joomla\Database\DatabaseDriver;
use UnexpectedValueException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

/** @package Iwf\Component\Verwaltung\Administrator\Table */
class Iwf_rechnerTable extends Table
{
    
    /**
     * @param DatabaseDriver $db 
     * @return void 
     * @throws UnexpectedValueException 
     * @throws Exception 
     * @throws BadMethodCallException 
     */
    function __construct(DatabaseDriver $db)
    {
        parent::__construct('#__iwf_rechner', 'id', $db);
    }

    /**
     * @param bool $updateNulls 
     * @return bool 
     * @throws BadMethodCallException 
     * @throws UnexpectedValueException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     */
    public function store($updateNulls = true)
    {
        if (empty($this->baramundi_sync)) {
            $this->baramundi_sync = '0000-00-00 00:00:00';
        }
        return parent::store($updateNulls);
    }
}
