<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Table;

defined('_JEXEC') or die;

use Exception;
use BadMethodCallException;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Administrator\Helper\MailHelper;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\CMS\Application\CMSApplicationInterface;
use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Date\Date;
use Joomla\CMS\Factory;
use Joomla\CMS\Table\Table;
use Joomla\CMS\User\CurrentUserTrait;
use Joomla\Database\DatabaseDriver;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\Table */
class Iwf_reservierungenTable extends Table
{
    use CurrentUserTrait;

    /**
     * @param DatabaseDriver $db 
     * @return void 
     * @throws UnexpectedValueException 
     * @throws Exception 
     * @throws BadMethodCallException 
     */
    function __construct(DatabaseDriver $db)
    {
        parent::__construct('#__iwf_reservierungen', 'id', $db);
    }

    /**
     * @param bool $updateNulls 
     * @return bool 
     * @throws BadMethodCallException 
     * @throws UnexpectedValueException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     */
    public function store($updateNulls = true)
    {
        $date   = Factory::getDate()->toSql();
        $userId = $this->getCurrentUser()->id;
        $person = Person::getInstance();
        if (!(int) $this->created_time) {
            $this->created_time = $date;
        }
        if ($this->id) {
            $this->modified_by = $userId;
            $this->modified_time = $date;
            $this->modified_by_fzguser_id = $person->ma_id;
        } else {
            if (empty($this->created_by)) {
                $this->created_by = $userId;
            }

            if (!(int) $this->modified_time) {
                $this->modified_time = $date;
            }
            if (empty($this->modified_by)) {
                $this->modified_by = $userId;
            }
        }
        if ($this->ende < $this->beginn) {
            throw new \Exception('Beginn muss vor Ende liegen!', 0);
            return false;
        }
        $data = Factory::getApplication()->getInput()->get('jform', array(), 'array');
        $dauer = (int)$data['dauer']; // weil nicht in db
        $wochenende = $data['wochenende']; // weil nicht in db
        $mehrtaegig = $this->wiederholungsintervall == RESERVIERUNG_MEHRTAEGIG && $dauer > 1;
        if ($mehrtaegig) {
            $this->netz = 1;
        }
        if (empty($this->enddatum)) {
            $this->enddatum = null;
        }
        $this->wiederholungsintervall == RESERVIERUNG_EINMALIG; // zurücksetzen!
        if ($this->betreuung) {
            $task = Factory::getApplication()->getInput()->get('task');
            if (in_array($task, ['apply', 'save'])) {
                $params = ComponentHelper::getParams('com_verwaltung');
                $recipient = $params->get('edv_email');
                if ($recipient) {
                    $beginn = (new Date($this->datum))->format('d.m.Y');
                    $message = 'Für die Veranstaltung "' . $this->veranstaltung . '" wurde eine Betreuung angefordert' . CRLF;
                    $message .= 'Falls jemand vom EDV-Team Zeit hat, bitte um entsprechende Einweisung für den Veranstalter' . CRLF . CRLF;
                    $message .= 'Datum: ' . $beginn . CRLF;
                    $db = $this->getDbo();
                    $query = $db->createQuery()
                        ->select($db->qn('l.inhalt', 'value'))
                        ->from($db->qn('#__iwf_listen', 'l'))
                        ->where($db->qn('l.id'). '=:id')
                        ->bind(':id', $this->text, ParameterType::INTEGER);
                    if ($item = $db->setQuery($query)->loadObject()) {
                        $message .= 'Ort: ' . $item->value . CRLF;
                        $message .= 'Beginn: ' . $this->beginn . CRLF;
                        $message .= 'Ende: ' . $this->ende . CRLF . CRLF;
                        if ($this->videobeamer) {
                            $message .= '+Beamer' . CRLF;
                        }
                        if ($this->mikrofon) {
                            $message .= '+Mikrofon' . CRLF;
                        }
                        if ($this->laptop) {
                            $message .= '+Notebook' . CRLF . CRLF;
                        }
                        if ($this->created_by) {
                            $person = Person::getPersonByJoomlaId($this->created_by);
                            $message .= 'Reservierungseintrag stammt von ' . $person->name;
                        }
                        MailHelper::sendMail($recipient,'Raumreservierung', $message);
                    }
                }
            }
        }
        $result = parent::store($updateNulls);
        if ($mehrtaegig) { // ich verwende netz, um festzustellen, ob die zusätzlichen Tage bereits gespeichert wurden!
            $i = 1;
            $numDays = $dauer;
            $datum = date_create($this->datum);
            while ($i < $numDays) {
                date_add($datum, date_interval_create_from_date_string("1 days"));
                $dow = (int) date('w', strtotime(date_format($datum, "Y-m-d")));
                $addRecord = !((int)$wochenende == 1 && in_array($dow, array(0, 6)));
                if ($addRecord) {
                    $this->id = 0;
                    $this->wiederholungsintervall = RESERVIERUNG_EINMALIG;
                    $this->datum = date_format($datum, "Y-m-d");
                    $this->netz = 1;
                    $result &= parent::store($updateNulls);
                    $i++;
                }
            }
            Factory::getApplication()->enqueueMessage('Mehrtägige Veranstaltung wurde gespeichert und entsprechend dupliziert.', CMSApplicationInterface::MSG_INFO);
            Factory::getApplication()->redirect('index.php?option=com_verwaltung&view=rooms');
        }
        return $result;
    }
}
