<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Table;

defined('_JEXEC') or die;

use Exception;
use BadMethodCallException;
use DateTime;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\CMS\Factory;
use Joomla\CMS\Table\Table;
use Joomla\Database\DatabaseDriver;
use Joomla\Database\DatabaseInterface;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\Table */
class Iwf_bestellungenTable extends Table
{
    
    /**
     * @param DatabaseDriver $db 
     * @return void 
     * @throws UnexpectedValueException 
     * @throws Exception 
     * @throws BadMethodCallException 
     */
    function __construct(DatabaseDriver $db)
    {
        parent::__construct('#__iwf_bestellungen', 'id', $db);
    }

    /**
     * @param bool $updateNulls 
     * @return bool 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws BadMethodCallException 
     * @throws UnexpectedValueException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    public function store($updateNulls = true)
    {
        if ($this->id == 0) {
            $year = Factory::getDate()->format('Y');
            $person = Person::getPersonByMaId($this->ma_id);
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $query = $db->createQuery();
            $query->select('COUNT(' . $db->qn('a.bestellnummer') . ')+1 AS ' . $db->qn('next'))
                ->from($db->qn('#__iwf_bestellungen', 'a'))
                ->where('YEAR(' . $db->qn('a.bestelldatum') . ')=:jahr')
                ->bind(':jahr', $year, ParameterType::STRING);
            $next = $db->setQuery($query)->loadResult();
            if ($next) {
                $this->bestellnummer = sprintf("%03d-%s /%s%s", $next, $year, substr(strtoupper($person->vorname), 0, 1), substr(strtoupper($person->nachname), 0, 4));
            }
        }
        $bestelldatum = new DateTime($this->bestelldatum);
        $this->bestelldatum = $bestelldatum->format('Y-m-d');
        if (empty($this->rechnungsdatum)) {
            $this->rechnungsdatum = null;
        }
        $date = Factory::getDate()->toSql();
        $userId = Factory::getApplication()->getIdentity()->id;
        $person = Person::getInstance();
        if (!(int) $this->created_time) {
            $this->created_time = $date;
        }
        if ($this->id) {
            $this->modified_by = $userId;
            $this->modified_time = $date;
            $this->modified_by_fzguser_id = $person->ma_id;
        } else {
            if (empty($this->created_by)) {
                $this->created_by = $userId;
            }
            if (!(int) $this->modified_time) {
                $this->modified_time = $date;
            }
            if (empty($this->modified_by)) {
                $this->modified_by = $userId;
            }
            if (empty($this->bezahldatum)) {
                $this->bezahldatum = '0000-00-00 00:00:00';
            }
        }
        return parent::store($updateNulls);
    }
}
