<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Table;

defined('_JEXEC') or die;

use Exception;
use BadMethodCallException;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Administrator\Helper\MailHelper;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Table\Table;
use Joomla\CMS\User\CurrentUserTrait;
use Joomla\Database\DatabaseDriver;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\Table */
class Iwf_gastformularTable extends Table
{
    use CurrentUserTrait;

    /**
     * @param DatabaseDriver $db 
     * @return void 
     * @throws UnexpectedValueException 
     * @throws Exception 
     * @throws BadMethodCallException 
     */
    function __construct(DatabaseDriver $db)
    {
        parent::__construct('#__iwf_gastformular', 'id', $db);
    }

    /**
     * @param bool $updateNulls 
     * @return bool 
     * @throws BadMethodCallException 
     * @throws UnexpectedValueException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     */
    public function store($updateNulls = true)
    {
        $date   = Factory::getDate()->toSql();
        $userId = $this->getCurrentUser()->id;
        $person = Person::getInstance();
        if (!(int) $this->created_time) {
            $this->created_time = $date;
        }
        if ($this->id) {
            $this->modified_by = $userId;
            $this->modified_time = $date;
            $this->modified_by_fzguser_id = $person->ma_id;
        } else {
            if (empty($this->created_by)) {
                $this->created_by = $userId;
            }

            if (!(int) $this->modified_time) {
                $this->modified_time = $date;
            }
            if (empty($this->modified_by)) {
                $this->modified_by = $userId;
            }
        }
        if (empty($this->ma_id)) {
            $this->ma_id = 0;
        }
        if (empty($this->finanziert_aus_projekt)) {
            $this->finanziert_aus_projekt = null;
        }
        if (empty($this->wohnung)) {
            $this->wohnung = null;
        }
        if (empty($this->erfasst)) {
            $params = ComponentHelper::getParams('com_verwaltung');
            if ($params->get('sekretariat_email')) {
                $person = Person::getInstance();
                $db = $this->getDbo();
                $query = $db->createQuery();
                $query->select($query->concatenate([$db->qn('a.nachname'), $db->qn('a.vorname')], ' ') . ' AS ' . $db->qn('name'))
                    ->from($db->qn('#__iwf_mitarbeiter', 'a'))
                    ->where($db->qn('id'). '=:id')
                    ->bind(':id', $person->ma_id, ParameterType::INTEGER);
                if ($result = $db->setQuery($query)->loadObject()) {
                    $message = 'Erfasst von: ' . $result->name . CRLF;
                    $message .= 'Gast: ' . $this->nachname . ' ' . $this->vorname . CRLF . CRLF;
                    $message .= 'Bitte um Kontrolle und weitere Bearbeitung.';
                    if ($params->get('sekretariat_email')) {
                        if (MailHelper::sendMail($params->get('sekretariat_email'), 'Neuer Gast wurde erfasst', $message)) {
                            $this->erfasst = 1;
                        }
                    }
                }
            }
        }
        return parent::store($updateNulls);
    }
}
