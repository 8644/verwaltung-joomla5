<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Table;

defined('_JEXEC') or die;

use Exception;
use BadMethodCallException;
use InvalidArgumentException;
use Joomla\CMS\Factory;
use Joomla\CMS\Table\Table;
use Joomla\Database\DatabaseDriver;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\Table */
class Iwf_lizenzenTable extends Table
{
    
    /**
     * @param DatabaseDriver $db 
     * @return void 
     * @throws UnexpectedValueException 
     * @throws Exception 
     * @throws BadMethodCallException 
     */
    function __construct(DatabaseDriver $db)
    {
        parent::__construct('#__iwf_lizenzen', 'id', $db);
    }

    /**
     * @param bool $updateNulls 
     * @return bool 
     * @throws KeyNotFoundException 
     * @throws BadMethodCallException 
     * @throws UnexpectedValueException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws Exception 
     */
    public function store($updateNulls = true)
    {
        if (empty($this->installationsdatum)) {
            if ($this->id == 0) {
                $this->installationsdatum = Factory::getDate('now')->toSql();
            } else {
                $this->installationsdatum = '0000-00-00 00:00:00';
            }
        }
        return parent::store($updateNulls);
    }

}