<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Table;

defined('_JEXEC') or die;

use Exception;
use BadMethodCallException;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Factory;
use Joomla\CMS\Table\Table;
use Joomla\Database\DatabaseDriver;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\Table */
class Iwf_gastchecklistenTable extends Table
{
    
    /**
     * @param DatabaseDriver $db 
     * @return void 
     * @throws UnexpectedValueException 
     * @throws Exception 
     * @throws BadMethodCallException 
     */
    function __construct(DatabaseDriver $db)
    {
        parent::__construct('#__iwf_gastchecklisten', 'id', $db);
    }

    /**
     * @param bool $updateNulls 
     * @return bool 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws BadMethodCallException 
     * @throws UnexpectedValueException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    public function store($updateNulls = true)
    {
        $ruecksetzen = Factory::getApplication()->getInput()->get('jform', [], 'array')['checkliste_ruecksetzen'];
        if ($this->id && $ruecksetzen) {
            $db = $this->getDbo();
            $query = $db->createQuery()
                ->delete($db->qn('#__iwf_gastchecklisten'))
                ->where($db->qn('id') . '=:id')
                ->bind(':id', $this->id, ParameterType::INTEGER);
            $db->setQuery($query)->execute();
            Factory::getApplication()->redirect(Extensions::getReturnRoute());
            return;
        }

        $date   = Factory::getDate('now')->toSql();
        $userId = Factory::getApplication()->getIdentity()->id;
        $person = Person::getInstance();
        if (!(int) $this->created_time) {
            $this->created_time = $date;
        }
        if ($this->id) {
            $this->modified_by = $userId;
            $this->modified_time = $date;
            $this->modified_by_fzguser_id = $person->ma_id;
        } else {
            if (empty($this->created_by)) {
                $this->created_by = $userId;
            }
            if (!(int) $this->modified_time) {
                $this->modified_time = $date;
            }
            if (empty($this->modified_by)) {
                $this->modified_by = $userId;
            }
        }
        return parent::store($updateNulls);
    }
}
