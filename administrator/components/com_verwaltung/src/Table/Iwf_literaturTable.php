<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Table;

defined('_JEXEC') or die;

use Exception;
use BadMethodCallException;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\CMS\Factory;
use Joomla\CMS\Table\Table;
use Joomla\Database\DatabaseDriver;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\Exception\UnsupportedAdapterException;
use Joomla\Database\ParameterType;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\Table */
class Iwf_literaturTable extends Table
{
    
    /**
     * @param DatabaseDriver $db 
     * @return void 
     * @throws UnexpectedValueException 
     * @throws Exception 
     * @throws BadMethodCallException 
     */
    function __construct(DatabaseDriver $db)
    {
        parent::__construct('#__iwf_literatur', 'id', $db);
    }

    /**
     * @param bool $updateNulls 
     * @return bool 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws BadMethodCallException 
     * @throws UnexpectedValueException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     */
    public function store($updateNulls = true)
    {
        $date   = Factory::getDate('now')->toSql();
        $userId = Factory::getApplication()->getIdentity()->id;
        $person = Person::getInstance();
        if (!(int) $this->created_time) {
            $this->created_time = $date;
        }
        if ($this->id) {
            $this->modified_by = $userId;
            $this->modified_time = $date;
            $this->modified_by_fzguser_id = $person->ma_id;
        } else {
            if (empty($this->created_by)) {
                $this->created_by = $userId;
            }

            if (!(int) $this->modified_time) {
                $this->modified_time = $date;
            }

            if (empty($this->modified_by)) {
                $this->modified_by = $userId;
            }
        }
        if (isset($this->tagungsdatum)) {
            if (preg_match('/\d+/', $this->tagungsdatum, $treffer)) {
                $this->erscheinungsjahr = $treffer[0];
            }
        }
        $task = Factory::getApplication()->getInput()->get('task');
        if ($task == 'save2copy') {
            $this->titel = uniqid("?????") . ' hier neuer Titel';  // damit die Prüfung auf doppelten Titel nicht anspricht
        }
        $this->setZitat();
        return parent::store($updateNulls);
    }

    /**
     * @return void 
     * @throws UnsupportedAdapterException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws RuntimeException 
     */
    private function setZitat() {
        $db = $this->getDbo();
        $query = $db->createQuery();
        $I_Open = "<I>";
        $I_Close = "</I>";
        $B_Open = "<B>";
        $B_Close = "</B>";
        $res = "";
        $typ = $this->publikationstyp;
        //hier das Zitat erstellen
        $doi = "";
        switch ($typ) {
            case P_TYP_ZEITSCHRIFT_REFERIERT:  // Zeitschriften(ref)
                $doi = strlen(trim($this->doi)) > 0 ? sprintf(", doi:<a class=\"doi\" target=\"_blank\" href=\"http://dx.doi.org/%s\">%s</a>", trim($this->doi), trim($this->doi)) : "";
                $res = sprintf("%s: %s, " . $I_Open . "%s" . $I_Close . ", " . $B_Open . "%s" . $B_Close . ", %s%s, %s.",
                        trim($this->autoren), $this->getLink($this->titel, $this->link), trim($this->zeitschrift), trim($this->band), trim($this->seitenzahlen), $doi, trim($this->erscheinungsjahr));
                break;
            case P_TYP_ZEITSCHRIFT_NICHT_REFERIERT:  // zeitschriften(n.ref.)
                $doi = strlen(trim($this->doi)) > 0 ? sprintf(", doi:<a class=\"doi\" target=\"_blank\" href=\"http://dx.doi.org/%s\">%s</a>", trim($this->doi), trim($this->doi)) : "";
                $res = sprintf("%s: %s, " . $I_Open . "%s" . $I_Close . ", " . $B_Open . "%s" . $B_Close . ", %s%s, %s.",
                        trim($this->autoren), $this->getLink($this->titel, $this->link), trim($this->zeitschrift), trim($this->band), trim($this->seitenzahlen), $doi, trim($this->erscheinungsjahr));
                break;
            case P_TYP_IWF_BERICHT: // Institutsbericht
                $res = sprintf("%s: %s, %s, %s pages, %s.",
                        trim($this->autoren), $this->getLink($this->titel, $this->link), "IWF-Report " . trim($this->berichtnummer), trim($this->seitenzahlen), trim($this->erscheinungsjahr));
                break;
            case P_TYP_BUCHBEITRAG: // Buchbeitrag,Proceeding
                $tmp = trim($this->gesamttitel);
                $tmp1 = "";
                if ($this->herausgeber != "") {
                    if (strpos($this->herausgeber, ".,") > 0) {
                        $tmp1 = "Eds. " . $this->herausgeber . ", ";
                    } else {
                        $tmp1 = "Ed. " . $this->herausgeber . ", ";
                    }
                }
                if (strlen($tmp) == 0) {
                    $tmp = $this->untertitel;
                }
                $doi = strlen(trim($this->doi)) > 0 ? sprintf(", doi:<a class=\"doi\" target=\"_blank\" href=\"http://dx.doi.org/%s\">%s</a>", trim($this->doi), trim($this->doi)) : "";
                $res = sprintf("%s: %s. In: $I_Open%s$I_Close, %s%s, %s, %s%s, %s.",
                        trim($this->autoren), $this->getLink($this->titel, $this->link), $tmp, $tmp1, trim($this->verlag),
                        trim($this->verlagsort), trim($this->seitenzahlen), $doi, trim($this->erscheinungsjahr));
                break;
            case P_TYP_BUCH:  // Buch
                $tmp = trim($this->autoren);
                if (strlen($tmp) == 0) {
                    $tmp = trim($this->herausgeber) . " (Ed.)";
                }
                $res = sprintf("%s: %s, %s, %s, %s pages (%s)",
                        $tmp, $this->getLink($this->titel, $this->link), trim($this->verlag), trim($this->verlagsort),
                        trim($this->seitenzahlen), trim($this->erscheinungsjahr));
                break;
            case P_TYP_BUCH_HERAUSGABE:  // Buchherausgabe
                if (strpos($this->herausgeber, ".,") > 0) {
                    $tmp1 = "Eds.";
                } else {
                    $tmp1 = "Ed.";
                }
                $res = sprintf("%s ($tmp1): %s, %s, %s, %s pages (%s)",
                        trim($this->herausgeber), $this->getLink($this->titel, $this->link), trim($this->verlag), trim($this->verlagsort),
                        trim($this->seitenzahlen), trim($this->erscheinungsjahr));
                break;
            case P_TYP_DIPLOM_BAKK_ARBEIT:  // Diplomarbeit
                $typ = 'arbeitstyp';
                $query->select($db->qn('a.inhalt', 'arbeitstyp'))
                        ->from($db->qn('#__iwf_aka_listen', 'a'))
                        ->where($db->qn('a.kategorie') . '=:typ')
                        ->where($db->qn('a.value') . '=:atyp')
                        ->bind(':typ', $typ, ParameterType::STRING)
                        ->bind(':atyp', $this->arbeitstyp, ParameterType::INTEGER);
                $db->setQuery($query);
                $arbeitstyp = 'Diploma Thesis';
                if ($result = $db->setQuery($query)->loadObject()) {
                    $arbeitstyp = $result->arbeitstyp;
                }
                $res = sprintf("%s: %s, $arbeitstyp, %s, %s pages (%s)",
                        trim($this->autoren), $this->getLink($this->titel, $this->link), trim($this->institut),
                        trim($this->seitenzahlen), trim($this->erscheinungsjahr));
                break;
            case P_TYP_DISSERTATION:  // Dissertation
                $res = sprintf("%s: %s, Dissertation, %s, %s pages (%s)",
                        trim($this->autoren), $this->getLink($this->titel, $this->link), trim($this->institut),
                        trim($this->seitenzahlen), trim($this->erscheinungsjahr));
                break;
            case P_TYP_VORTRAG_BEI_TAGUNGEN:  // Vortrag bei Tagungen
                $tmp = "";
                if ($this->eingeladen == 1) {
                    $tmp = " (Invited)";
                }
                $res = sprintf("%s: %s%s, $I_Open%s$I_Close, %s, %s.",
                        trim($this->autoren), $this->getLink($this->titel, $this->link), $tmp,
                        trim($this->tagungstitel), trim($this->tagungsort), trim($this->tagungsdatum));
                break;
            case P_TYP_POSTER:  // Poster
                $res = sprintf("%s: %s, $I_Open%s$I_Close, %s, %s.",
                        trim($this->autoren), $this->getLink($this->titel, $this->link), trim($this->tagungstitel),
                        trim($this->tagungsort), trim($this->tagungsdatum));
                break;
            case P_TYP_PROPOSAL:  // Proposal
                $tmp = trim($this->dokumentreferenz);
                if (strlen($tmp) > 0) {
                    $tmp .= ", ";
                }
                $res = sprintf("%s: %s, Proposal to %s, %s%s pages (%s)",
                        trim($this->autoren), $this->getLink($this->titel, $this->link), trim($this->weltraumagentur), $tmp,
                        trim($this->seitenzahlen), trim($this->erscheinungsjahr));
                break;
            case P_TYP_BERICHT_ALLGEMEIN:  // Bericht
                $tmp = trim($this->dokumentreferenz);
                if (strlen($tmp) === 0) {
                    $tmp = trim($this->berichttyp);
                }
                if (strlen($tmp) > 0) {
                    $tmp .= ", ";
                }
                $res = sprintf("%s: %s, %s%s pages (%s)",
                        trim($this->autoren), $this->getLink($this->titel, $this->link), $tmp,
                        trim($this->seitenzahlen), trim($this->erscheinungsjahr));
                break;
            case P_TYP_VORTRAG_BEI_INSTITUTION:  // Vortrag bei Institutionen
                $res = sprintf("%s: %s, %s, %s, %s.",
                        trim($this->autoren), $this->getLink($this->titel, $this->link), trim($this->institut),
                        trim($this->tagungsort), trim($this->tagungsdatum));
                break;
            case P_TYP_HABILITATION:  // Habilitation
                $res = sprintf("%s: %s, Habilitation, %s, %s pages (%s)",
                        trim($this->autoren), $this->getLink($this->titel, $this->link), trim($this->institut),
                        trim($this->seitenzahlen), trim($this->erscheinungsjahr));
                break;
            case P_TYP_ANDERER_VORTRAG:  // Andere Praesentationen, anderer Vortrag
                $res = sprintf("%s: %s, %s, %s, %s.",
                        trim($this->autoren), $this->getLink($this->titel, $this->link), trim($this->institut),
                        trim($this->tagungsort), trim($this->tagungsdatum));
                break;
            case P_TYP_VORTRAG_EINES_GASTES:  //  Gastvortrag
                $res = sprintf("%s: %s, %s, %s.",
                        trim($this->autoren), $this->getLink($this->titel, $this->link), trim($this->institut),
                        trim($this->tagungsdatum));
                break;
            case P_TYP_VORTRAG_BEI_PROJEKTMEETING:  // Vortrag bei Projektmeeting
                $res = sprintf("%s: %s, $I_Open%s$I_Close, %s, %s.",
                        trim($this->autoren), $this->getLink($this->titel, $this->link), trim($this->tagungstitel),
                        trim($this->tagungsort), trim($this->tagungsdatum));
                break;
        }
        $this->zitat = $res;
    }

    /**
     * @param mixed $title 
     * @param mixed $link 
     * @return mixed 
     */
    private function getLink($title, $link) {
        if (strlen($link) > 0) {
            return "<a class=\"sprint\" href=\"" . $link . "\">" . $title . "</a>";
        } else {
            return $title;
        }
    }
}
