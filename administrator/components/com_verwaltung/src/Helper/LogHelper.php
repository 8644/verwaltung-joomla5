<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Helper;

use Exception;
use Joomla\CMS\Factory;
use Joomla\CMS\Log\Log;
use Joomla\CMS\Log\LogEntry;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Helper */
class LogHelper
{

    /**
     * @param mixed $message 
     * @param mixed $priority 
     * @param string $category 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     */
    public static function add($message, $priority = Log::INFO, $category = 'verwaltung')
    {
        $user = Factory::getApplication()->getIdentity();
        $userId = $user->id;
        $userName = $user->name;
        $entry = new LogEntry("$userName ($userId) -- " . $message, $priority, $category, Factory::getDate('now'));
        Log::add($entry);
    }
}
