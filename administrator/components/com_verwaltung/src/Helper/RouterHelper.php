<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Helper;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Multilanguage;
use Joomla\CMS\Router\Route;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Site\Helper */
abstract class RouterHelper
{
	/**
	 * @return string 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 */
	private static function addLanguageTag()
	{
		$langTag = "";
		if (Multilanguage::isEnabled()) {
			$lang = Factory::getApplication()->getLanguage();
			$langTag = '&lang=' . substr($lang->getTag(), 0, 2);
		}
		return $langTag;
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return string 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getDepartmentEditRoute($id, $text, $title = "", $icon = ""): string
	{
		if (Extensions::isAllowed('manage.abteilung')) {
			$route = 'index.php?option=com_verwaltung&task=department.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
            return $text;
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return string 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getRoomEditRoute($id, $text, $title = "", $icon = ""): string
	{
		if (Extensions::isAllowed('manage.fzg')) {
			$route = 'index.php?option=com_verwaltung&task=room.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
            return $text;
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return string 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getProjectEditRoute($id, $text, $title = "", $icon = ""): string
	{
		if (Extensions::isAllowed('manage.abteilung')) {
			$route = 'index.php?option=com_verwaltung&task=project.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
            return $text;
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return string 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getHostEditRoute($id, $text, $title = "", $icon = ""): string
	{
		if (Extensions::isAllowed('manage.hw')) {
			$route = 'index.php?option=com_verwaltung&task=host.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
            return $text;
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return string 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getKeyEditRoute($id, $text, $title = "", $icon = ""): string
	{
		if (Extensions::isAllowed('manage.schluessel')) {
			$route = 'index.php?option=com_verwaltung&task=key.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
            return $text;
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return string 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getListEditRoute($id, $text, $title = "", $icon = ""): string
	{
		if (Extensions::isAllowed('admin')) {
			$route = 'index.php?option=com_verwaltung&task=list.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
            return $text;
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return string 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getMediumEditRoute($id, $text, $title = "", $icon = ""): string
	{
		if (Extensions::isAllowed('manage.sw')) {
			$route = 'index.php?option=com_verwaltung&task=medium.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
            return $text;
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return string 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 */
	public static function getProgramEditRoute($id, $text, $title = "", $icon = ""): string
	{
		if (Extensions::isAllowed('manage.sw')) {
			$route = 'index.php?option=com_verwaltung&task=program.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
            return $text;
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return string 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getAkalistEditRoute($id, $text, $title = "", $icon = ""): string
	{
		if (Extensions::isAllowed('manage.literatur')) {
			$route = 'index.php?option=com_verwaltung&task=akalist.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
            return $text;
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return string 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getLitprojectEditRoute($id, $text, $title = "", $icon = ""): string
	{
		if (Extensions::isAllowed('manage.literatur')) {
			$route = 'index.php?option=com_verwaltung&task=litproject.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
            return $text;
		}
	}

	/**
	 * @param mixed $id 
	 * @param mixed $text 
	 * @param string $title 
	 * @param string $icon 
	 * @return string 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public static function getMagazineEditRoute($id, $text, $title = "", $icon = ""): string
	{
		if (Extensions::isAllowed('admin')) {
			$route = 'index.php?option=com_verwaltung&task=magazine.edit&id=' . $id . self::addLanguageTag();
			return '<a href="' . Route::_($route) . '" title="' . $title . '">' . $icon . $text . '</a>';
		} else {
            return $text;
		}
	}
}
