<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Helper;

use Exception;
use Joomla\CMS\Factory;
use Joomla\CMS\Log\Log;
use Joomla\Database\DatabaseInterface;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Helper */
class LicenseHelper
{

    /**
     * @param mixed $controller 
     * @return void 
     * @throws KeyNotFoundException 
     */
    public static function abdeckung($controller) {
        // Hier nun die Abdeckung für die angezeigten Softwareelemente berechnen und die software-Tabelle aktualisieren
        // zuerst Abdeckung zurücksetzen:
        $db = Factory::getContainer()->get(DatabaseInterface::class);
		$query = $db->createQuery()
            ->update($db->qn('#__iwf_software'))
            ->set($db->qn('abdeckung') . '=0');
        $db->setQuery($query)->execute();
        $model = $controller->getModel('Licensestate', 'Administrator', []);
        $items = $model->getLicenseOverview();
        foreach ($items as $item) {
            if ($item->info) {
                foreach ($item->info as $produkt) {
                    $sw_id = $produkt->software_id;
                    $abdeckung = ($produkt->abgedeckt || $produkt->basisgekauftfloat > 0) ? 1 : 0;
                    if (!$produkt->lizenzpflichtig || $produkt->isfloat) {
                        $abdeckung = 1;
                    }
                    $query->clear()
                        ->update($db->qn('#__iwf_software'))
                        ->set($db->qn('abdeckung') . '=:abdeckung')
                        ->where($db->qn('id') . '=:swid')
                        ->bind(':abdeckung', $abdeckung, ParameterType::INTEGER)
                        ->bind(':swid', $sw_id, ParameterType::INTEGER);
                    $db->setQuery($query)->execute();
                }
            }
            // jetzt die restlichen Produkte dieser Gruppe, die keine Lizenzen verwenden, finden und Abdeckung auf 1 setzen:
            $query->clear()
                ->update($db->qn('#__iwf_software', 's'))
                ->leftJoin($db->qn('#__iwf_lizenzen', 'l'), $db->qn('s.id') . '=' . $db->qn('l.software_id'))
                ->set($db->qn('s.abdeckung') . '=1')
                ->where($db->qn('l.software_id') . ' IS NULL')
                ->where($db->qn('s.produktgruppe') . ' LIKE :pgruppe')
                ->bind(':pgruppe', $item->name, ParameterType::STRING);
            $db->setQuery($query)->execute();
        }
    }

    /**
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     */
    public static function syncLicenses() {
        
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery();
        $endpointsSoftware = BaramundiHelper::getEndpointsSoftware();
        $installed = [];
        foreach ($endpointsSoftware as $endpoint) {
            foreach ($endpoint->software as $program) {
                $query->clear()
                    ->select(
                        [
                            $db->qn('id'),
                            $db->qn('baramundi_product'),
                            $db->qn('baramundi_version')
                        ]
                    )
                    ->from($db->qn('#__iwf_software'))
                    ->where('NOT (NULLIF(' . $db->qn('baramundi_product') . ', "") IS NULL)')
                    ->where('NOT (NULLIF(' . $db->qn('baramundi_version') . ', "") IS NULL)')
                    ->where('POSITION(' . $db->qn('baramundi_product') . ' IN :pname) > 0');
                if (!empty($program->Version)) {
                    $query->where('POSITION(:pversion IN ' . $db->qn('baramundi_version') . ') > 0')
                        ->bind(':pversion', $program->Version, ParameterType::STRING);
                } else {
                    $query->where('POSITION(md5(:pname) IN ' . $db->qn('baramundi_version') . ') > 0');
                }
                $query->bind(':pname', $program->ProductName, ParameterType::STRING)
                    ->setLimit(1);

                try {
                    if ($result = $db->setQuery($query)->loadObject()) {
                        $query->clear()
                            ->select('*')
                            ->from($db->qn('#__iwf_lizenzen'))
                            ->where(
                                [
                                    $db->qn('software_id') . '=:swid',
                                    $db->qn('rechner_id') . '=:rid',
                                ]
                            )
                            ->bind(':swid', $result->id, ParameterType::INTEGER)
                            ->bind(':rid', $endpoint->rechner_id, ParameterType::INTEGER);
                        $items = $db->setQuery($query)->loadObjectList();
                        if (empty($items)) {
                            $date = Factory::getDate('now')->format('Y-m-d', true);
                            $query->clear()
                                ->insert($db->qn('#__iwf_lizenzen'))
                                ->columns(
                                    [
                                        $db->qn('software_id'),
                                        $db->qn('rechner_id'),
                                        $db->qn('version'),
                                        $db->qn('installationsdatum'),
                                        $db->qn('bybaramundi'),
                                    ]
                                )
                                ->values(':swid, :rid, :vers, :datum, 1')
                                ->bind(':swid', $result->id, ParameterType::INTEGER)
                                ->bind(':rid', $endpoint->rechner_id, ParameterType::INTEGER)
                                ->bind(':vers', $program->Version, ParameterType::STRING)
                                ->bind(':datum', $date, ParameterType::STRING);
                            if ($db->setQuery($query)->execute()) {
                                $log = sprintf("Lizenz zu Software [%s], Id=%s, für Rechner %s eingetragen", $program->ProductName, $result->id, $endpoint->hostname);
                                LogHelper::add($log, Log::INFO, 'baramundi');
                                $installed[] = $log;
                            }
                        }

                    }
                }
                catch (Exception $e) {}
            }
        }
        if ($installed) {
            $mail = Factory::getApplication()->getIdentity()->email;
            MailHelper::sendMail($mail, 'Ergebnis der baramundi Aktion', implode("\r\n", $installed));
        }
    }

    /**
     * @return void 
     * @throws KeyNotFoundException 
     */
    public static function cleanLicenses() {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('wname'),
                    $db->qn('baramundi_id'),
                ]
            )
            ->from($db->qn('#__iwf_dns'))
            ->where('NOT NULLIF(' . $db->qn('baramundi_id') . ', "") IS NULL');
        $iwf_endpoints = $db->setQuery($query)->loadObjectList();
        if (empty($iwf_endpoints)) {
            return;
        }
        $removed = [];
        foreach ($iwf_endpoints as $iwf_endpoint) {
            $query->clear()
                ->select(
                    [
                        $db->qn('d.wname'),
                        $db->qn('s.baramundi_version'),
                        $db->qn('s.baramundi_product'),
                        $db->qn('l.id', 'lizenz_id')
                    ]
                )
                ->from($db->qn('#__iwf_rechner', 'r'))
                ->leftJoin($db->qn('#__iwf_dns', 'd'), $db->qn('r.dns_id') . '=' . $db->qn('d.id'))
                ->leftJoin($db->qn('#__iwf_lizenzen', 'l'), $db->qn('l.rechner_id') . '=' . $db->qn('r.id'))
                ->leftJoin($db->qn('#__iwf_software', 's'), $db->qn('l.software_id') . '=' . $db->qn('s.id'))
                ->where('NOT NULLIF(' . $db->qn('d.wname') . ', "") IS NULL')
                ->where('NOT NULLIF(' . $db->qn('s.baramundi_version') . ', "") IS NULL')
                ->where('NOT NULLIF(' . $db->qn('s.baramundi_product') . ', "") IS NULL')
                ->where($db->qn('d.wname') . ' LIKE :wname')
                ->bind(':wname', $iwf_endpoint->wname, ParameterType::STRING);
            $installed_programs = $db->setQuery($query)->loadObjectList();
            if (empty($installed_programs)) {
                continue;
            }
            $baramundi_programs = BaramundiHelper::getEndpointSoftware($iwf_endpoint->baramundi_id);
            if (empty($baramundi_programs)) {
                continue;
            }
            foreach ($installed_programs as $program) {
                $license_found = self::findLicence($program, $baramundi_programs);
                if (!$license_found) {
                    $query->clear()
                        ->delete($db->qn('#__iwf_lizenzen'))
                        ->where($db->qn('id') . '=:id')
                        ->bind(':id', $program->lizenz_id, ParameterType::INTEGER);
                    if ($db->setQuery($query)->execute()) {
                        $removed[] = sprintf("%s: Nicht installierte Lizenz für Program '%s' gelöscht", $program->wname, $program->baramundi_product);
                    }

                }
            }
        }
        if (count($removed)) {
            $userEmail = Factory::getApplication()->getIdentity()->email;
            MailHelper::sendMail($userEmail, "Ergebnis der baramundi Aktion", implode("\r\n", $removed));
        }
    }

    /**
     * @param mixed $iwf_program 
     * @param mixed $baramundi_programs 
     * @return bool 
     */
    private static function findLicence($iwf_program, $baramundi_programs) {
        foreach ($baramundi_programs as $baramundi_program) {
            if (self::inString($baramundi_program->ProductName, $iwf_program->baramundi_product)) {
                if (self::inString($iwf_program->baramundi_version, $baramundi_program->Version)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @param mixed $haystack 
     * @param mixed $needles 
     * @return false|int 
     */
    private static function inString($haystack, $needles) {
        $result = false;
        $a = preg_split("/[,;]/", $needles);
        foreach ($a as $needle) {
            $result |= !(stripos($haystack, $needle) === false);
        }
        return $result;
    }

}
