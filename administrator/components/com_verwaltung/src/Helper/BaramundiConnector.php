<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Helper;

use Exception;
use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Log\Log;
use Joomla\CMS\Log\LogEntry;
use Joomla\Database\DatabaseInterface;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Helper */
class BaramundiConnector
{

    static $requestHeader = [
        'Authorization: Basic %s',
        'client-request-id: %s',
    ];
    
    static $patchHeader = [
        'Authorization: Basic %s',
        'client-request-id: %s',
        'Content-type: application/xml'
    ];

    static $patchComment = 
        '<Endpoint xmlns:i="http://www.w3.org/2001/XMLSchema-instance"'
        . ' xmlns="http://schemas.datacontract.org/2004/07/Baramundi.Bms.Server.Connector.Rest.BConnect.DataModel">'
        . '<Comments>%s</Comments>'
        . '</Endpoint>';

    static $patchCustomStateText = 
        '<Endpoint xmlns:i="http://www.w3.org/2001/XMLSchema-instance"'
        . ' xmlns="http://schemas.datacontract.org/2004/07/Baramundi.Bms.Server.Connector.Rest.BConnect.DataModel">'
        . '<CustomStateText>%s</CustomStateText>'
        . '</Endpoint>';

        /**
         * @return void 
         * @throws KeyNotFoundException 
         */
        function __construct() {
            $params = ComponentHelper::getParams('com_verwaltung');
            $this->server = $params->get('baramundi_server');
            $this->user = $params->get('baramundi_user');
            $this->password = $params->get('baramundi_password');
            $this->data = "";
            $this->baseUrl = sprintf("https://%s/bConnect/v1.1/", $params->get('baramundi_server'));
            $this->standardGroupsId = $params->get('baramundi_standard_groups_id');
        }
        
        /** @return array{0: 'Authorization: Basic %s', 1: 'client-request-id: %s'}  */
        private function getRequestHeader() {
            $headerTemplate = self::$requestHeader;
            $func = function (&$v, $k, $d) {
                $v = sprintf($v, $d[$k]);
            };
            $items[0] = base64_encode($this->user . ':' . $this->password);
            $items[1] = uniqid();
            array_walk($headerTemplate, $func, $items);
            return $headerTemplate;
        }
        
        /**
         * @param mixed $GET 
         * @return string|bool 
         */
        private function getRequest($GET) {
            $header = $this->getRequestHeader();
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => $this->baseUrl . $GET,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_HEADER => 1,
                CURLOPT_HTTPHEADER => $header
            ]);
            $result = curl_exec($curl);
            curl_close($curl);
            return $result;
        }
    
        /** @return array{0: 'Authorization: Basic %s', 1: 'client-request-id: %s', 2: 'Content-type: application/xml'}  */
        private function getPatchHeader() {
            $headerTemplate = self::$patchHeader;
            $func = function (&$v, $k, $d) {
                $v = sprintf($v, $d[$k]);
            };
            $items[0] = base64_encode($this->user . ':' . $this->password);
            $items[1] = uniqid();
            array_walk($headerTemplate, $func, $items);
            return $headerTemplate;
        }
    
        /**
         * @param mixed $PATCH 
         * @param mixed $data 
         * @return void 
         */
        private function patchRequest($PATCH, $data) {
            $header = $this->getPatchHeader();
            $curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_URL => $this->baseUrl . $PATCH,
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => 0,
                //CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_HEADER => 1,
                CURLOPT_HTTPHEADER => $header,
                CURLOPT_CUSTOMREQUEST=>"PATCH",
                CURLOPT_POSTFIELDS=>$data
            ]);
            curl_exec($curl);
            curl_close($curl);
        }
        
        /**
         * @param mixed $text 
         * @return mixed 
         */
        private function json2Object($text) {
            if ($text === false) {
                return json_decode("{}");
            }
            $jsonStart = strpos($text, "\r\n[{"); // array of json objects
            if ($jsonStart == 0 || $jsonStart === false){
                $jsonStart = strpos($text, "\r\n{"); // single json object
            }
            return json_decode(substr($text, $jsonStart + 2));
        }
        
        /** @return mixed  */
        public function getStandardGroups() {
            return $this->json2Object($this->getRequest("OrgUnits?OrgUnit=" . $this->standardGroupsId));
        }
        
        /**
         * @param mixed $unit 
         * @param bool $windowsOnly 
         * @return object 
         */
        public function getEndpoints($unit, $windowsOnly = true) {
            $endpoints = $this->json2Object($this->getRequest("Endpoints?OrgUnit=".$unit));
            $result = [];
            foreach ($endpoints as $endpoint) {
                if ($windowsOnly) {
                    if (stripos($endpoint->OperatingSystem, "Windows") !== false) {
                        $result[] = (object)$endpoint;
                    }
                } else {
                    $result[] = (object)$endpoint;
                }
            }
            return (Object)$result;
        }
    
        /** @return mixed  */
        public function getAllSoftware() {
            return $this->json2Object($this->getRequest("InventoryDataRegistryScans"));
        }
        
        /**
         * @param mixed $endpoint 
         * @return mixed 
         */
        public function getSoftware($endpoint) {
            return $this->json2Object($this->getRequest("InventoryDataRegistryScans?EndpointId=".$endpoint));
        }
    
        /**
         * @param mixed $endpoint 
         * @return mixed 
         */
        public function getWmiData($endpoint) {
            return $this->json2Object($this->getRequest("InventoryDataWMIScans?EndpointId=".$endpoint));
        }
        
        /** @return mixed  */
        public function getAllApplications() {
            return $this->json2Object($this->getRequest("Applications"));
        }
    
        /**
         * @param mixed $endpoint 
         * @return mixed 
         */
        public function getApplications($endpoint) {
            return $this->json2Object($this->getRequest("Applications?EndpointID=".$endpoint));
        }
    
        /**
         * @param mixed $endpoint 
         * @return mixed 
         */
        public function getApps($endpoint) {
            return $this->json2Object($this->getRequest("Apps?EndpointID=".$endpoint));
        }
    
        /**
         * @param mixed $endpoint 
         * @param mixed $comment 
         * @return void 
         */
        public function patchEndpointComment($endpoint, $comment) {
            $this->patchRequest("Endpoints?ID=".$endpoint, sprintf(self::$patchComment, $comment));
        }
    
        /**
         * @param mixed $endpoint 
         * @param mixed $customStateText 
         * @return void 
         */
        public function patchEndpointCustomStateText($endpoint, $customStateText) {
            $this->patchRequest("Endpoints?ID=".$endpoint, sprintf(self::$patchCustomStateText, $customStateText));
        }
    }