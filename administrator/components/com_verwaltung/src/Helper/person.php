<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Helper;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Access\Access;
use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Factory;
use Joomla\Database\ParameterType;
use Joomla\Registry\Format\Json;
use Joomla\Registry\Registry;
use Joomla\CMS\Language\Text;
use Joomla\CMS\User\UserFactoryInterface;
use Joomla\Database\DatabaseInterface;
use Iwf\Component\Verwaltung\Administrator\Helper\MailHelper;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Helper\ContentHelper;
use Joomla\CMS\User\User;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\Exception\UnsupportedAdapterException;
use UnexpectedValueException;

defined('_JEXEC') or die;

/**
 * Gibt ein Person-Objekt zurück 
 * @package Iwf\Component\Verwaltung\Administrator\Helper 
*/
class Person
{
    protected static  ?Person $instance = null;
    public $ma_id = 0;
    public $anrede = "";
    public $vorname = null;
    public $nachname = null;
    public $name = null;
    public $joomla_user = null;
    public $joomla_id = null;
    public $email = "";
    public $bereich = "";
    public $institut = null;
    public $abteilung_id = 0;
    public $abteilung_bereich = null;
    public $abteilung_filter = null;
    public $abteilung = "fzg-allgemein";
    public $abteilung_kurz = "fzg-all";
    public $abteilung4filename = "fzg-allgemein";  // leerzeichen entfernt für Dateien (Bilder...)
    public $abteilung_titel = "Forschungszentrum Graz";
    public $geschlecht;
    public $dienstverhaeltnis = 0;
    public $aktiv = false;
    public $mailman_name = null;
    public $default_mailman_lists = array();
    public $profil = null;

    /**
     * @param mixed $user ein Joomla User-Objekt. Falls null, muss eine email vorhanden sein
     * @param mixed $email Normalerweise null, außer es wird eine Person über ihre Mitarbeiter-Id gesucht. In diesem Fall muss $user null sein!
     * @param int $ma_id 
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    public function __construct($user, $email = null, $ma_id = 0)
    {
        if (is_null($user)) {
            $user = self::getJoomlaUserByEmail($email);
            if (is_null($user)) {
                $user = (object)[];
                $user->id = 0;
                $user->email = $email;
            }
        }
        $this->joomla_user = $user;
        $this->joomla_id = $user->id;
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('m.id', 'user_id'),
                    $db->qn('m.geschlecht'),
                    $db->qn('m.dienstverhaeltnis'),
                    $db->qn('m.nachname'),
                    $db->qn('m.vorname'),
                    $db->qn('m.abteilung', 'abteilung_id'),
                    $db->qn('m.email'),
                    $db->qn('m.mailabteilung'),
                    $db->qn('m.profil'),
                    $db->qn('m.state', 'aktiv'),
                    $db->qn('a.institut'),
                    $db->qn('a.abteilung', 'bereich'),
                    $db->qn('a.mailman_liste'),
                    $db->qn('a.titel'),
                    'CONCAT(' . $db->qn('m.nachname') . '," ",' . $db->qn('m.vorname') . ') AS name',
                    'CONCAT(' . $db->qn('a.institut') . ',"-",' . $db->qn('a.abteilung') . ') AS abteilung_bereich',
                    'CONCAT(' . $db->qn('a.institut') . ',"-",' . $db->qn('a.kuerzel') . ') AS abteilung_kurz',
                ]
            )
            ->from($db->qn('#__iwf_mitarbeiter', 'm'))
            ->leftJoin($db->qn('#__iwf_abteilungen', 'a'), $db->qn('m.abteilung') . '=' . $db->qn('a.id'));
        $execQuery = false;
        if (!empty($user->email)) {
            $query->where($db->qn('email') . ' LIKE :email')
                ->bind(':email', $user->email, ParameterType::STRING);
            $execQuery = true;
        }
        if ($ma_id) {
            $query->where($db->qn('m.id') . '=:maid')
                ->bind(':maid', $ma_id, ParameterType::INTEGER);
            $execQuery = true;
        }
        if ($execQuery) {
            $person = $db->setQuery($query)->loadObject();
        }
        if ($person) {
            $this->initFields($person);
            $query->clear();
            $query->select(
                [
                    $db->qn('id'),
                    $db->qn('institut'),
                    $db->qn('abteilung')
                ]
            )
                ->from($db->qn('#__iwf_abteilungen'));
            if (Extensions::isAllowed(['admin', 'manage.fzg', 'manage.schluessel', 'manage.edv'])) {
                // alle Abteilungen sichtbar
            } else {
                if (Extensions::isAllowed(['manage.institut', 'manage.inventar', 'manage.hw', 'manage.literatur'])) {
                    $query->where($db->qn('institut') . '=:institut')
                        ->bind(':institut', $person->institut, ParameterType::INTEGER);
                } else if (Extensions::isAllowed(['manage.abteilung', 'manage.sw'])) {
                    $query->where($db->qn('abteilung') . '=:abteilung')
                        ->bind(':abteilung', $person->abteilung, ParameterType::STRING);
                } else {
                    $query->where($db->qn('id') . '=:id')
                        ->bind(':id', $person->abteilung_id, ParameterType::INTEGER);
                }
            }
            $list = [];
            $items = $db->setQuery($query)->loadObjectList();
            foreach ($items as $item) {
                $list[] = $item->id;
            }
            if ($list) {
                $this->abteilung_filter = '(%s IN (' . implode(',', $list) . '))';
            }
        }
    }

    /**
     * @param mixed $person 
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     * @throws UnsupportedAdapterException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    private function initFields($person): void
    {
        $this->ma_id = $person->user_id;
        $this->vorname = $person->vorname;
        $this->nachname = $person->nachname;
        $this->anrede = $person->geschlecht == "M" ? Text::_('COM_VERWALTUNG_ANREDE_M') : Text::_('COM_VERWALTUNG_ANREDE_W');
        $this->name = $person->name;
        $this->email = $person->email;
        $this->bereich = $person->bereich;
        $this->institut = $person->institut;
        $this->abteilung_bereich = $person->abteilung_bereich;
        $this->abteilung_id = $person->abteilung_id;
        $this->abteilung_kurz = strtolower($person->abteilung_kurz);
        $this->abteilung4filename = strtolower(str_replace(" ", "", $this->bereich));
        $this->abteilung_titel = $person->titel;
        $this->geschlecht = $person->geschlecht;
        $this->dienstverhaeltnis = $person->dienstverhaeltnis;
        $this->aktiv = $person->aktiv > 0;
        $this->default_mailman_lists[] = "fzg-all";
        $this->profil = $person->profil;
        if ($person->mailabteilung) {
            $this->default_mailman_lists = array_unique(array_merge($this->default_mailman_lists, MailHelper::getMailmanListen($person->mailabteilung)));
        }
        if (in_array($person->abteilung_id, explode(",", ABTEILUNGEN_IWF))) {
            $this->default_mailman_lists[] = "iwf-all";
        }
        $this->mailman_name = sprintf("%s (%s %s)", $this->email, $this->vorname, $this->nachname);
        if ($this->profil) {
            $json = new Json();
            $this->profil = $json->stringToObject($this->profil);
            if (isset($this->profil->abteilung_filter)) {
                if ($this->profil->abteilung_filter == 0) {
                    $this->profil->abteilung_filter = null;
                }
            }
        } else {
            $this->profil = new Registry();
            $this->profil->set('abteilung_filter', null);
            $this->profil->set('jahr_filter', '');
        }
        if (Factory::getApplication()->isClient('administrator')) {
            // Falls der gerade angemeldete Benutzer Admin-Rechte hat, wird jener Benutzer als Admin-User
            // geladen, der in der Konfiguration als solcher eingetragen ist.
            // Dieser wird nämlich eingesetzt, wenn Datensätze eines anderen Users gelöscht werden.
            if (Access::check($this->joomla_user->id, 'core.admin')) {
                $params = ComponentHelper::getParams('com_verwaltung');
                $admin_user = $params->get('admin_user');
                if (isset($admin_user) && $this->joomla_user->username != $admin_user) {
                    $user = Factory::getContainer()->get(UserFactoryInterface::class)->loadUserByUsername($admin_user);
                    if ($user->id != 0) {
                        $this->joomla_user = $user;
                    }
                }
            }
        }
    }

    /**
     * @param User $joomlaUser 
     * @return void 
     */
    public static function setInstance(User $joomlaUser): void
    {
        self::$instance = new Person($joomlaUser);
    }

    /**
     * @return Person 
     * @throws Exception 
     */
    public static function getInstance()
    {
        if (!self::$instance) {
            self::$instance = new Person(Factory::getApplication()->getIdentity());
        }
        return self::$instance;
    }

    /**
     * @param mixed $joomlaId 
     * @return object 
     * @throws KeyNotFoundException 
     */
    public static function getPersonByJoomlaId($joomlaId): object
    {
        if (self::$instance) {
            if (self::$instance->joomla_id == $joomlaId) {
                return self::$instance;
            }
        }
        $user = Factory::getContainer()->get(UserFactoryInterface::class)->loadUserById((int)$joomlaId);
        return new Person($user);
    }

    /**
     * @param mixed $maId 
     * @return null|object 
     * @throws KeyNotFoundException 
     */
    public static function getPersonByMaId($maId): ?object
    {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select([$db->quoteName('id'), $db->quoteName('email')])
            ->from($db->quoteName('#__iwf_mitarbeiter'))
            ->where($db->quoteName('id') . ' = :id')
            ->bind(':id', $maId)
            ->setLimit(1);
        $item = $db->setQuery($query)->loadObject();
        if ($item) {
            return new Person(null, $item->email, $maId);
        }
        return null;
    }

    /**
     * @param mixed $email 
     * @return null|object 
     * @throws KeyNotFoundException 
     */
    public static function getJoomlaUserByEmail($email): ?object
    {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select([$db->quoteName('id'), $db->quoteName('email')])
            ->from($db->quoteName('#__users'))
            ->where($db->quoteName('email') . ' = :email')
            ->bind(':email', $email)
            ->setLimit(1);
        $item = $db->setQuery($query)->loadObject();
        if ($item) {
            return Factory::getContainer()->get(UserFactoryInterface::class)->loadUserById($item->id);
        }
        return null;
    }

    /**
     * @param mixed $joomlaUserId 
     * @return null|object 
     * @throws KeyNotFoundException 
     */
    public static function getJoomlaUserById($joomlaUserId): ?object
    {
        return Factory::getContainer()->get(UserFactoryInterface::class)->loadUserById($joomlaUserId);
    }

    /**
     * @param mixed $userName 
     * @return object 
     * @throws KeyNotFoundException 
     */
    public static function getIwfUserByUserName($userName): object
    {
        $user = Factory::getContainer()->get(UserFactoryInterface::class)->loadUserByUsername($userName);
        if (!$user) {
            return null;
        }
        return self::getPersonByJoomlaId($user->id);
    }

    /**
     * @param mixed $userName 
     * @return object 
     * @throws KeyNotFoundException 
     */
    public static function getIwfUserIdByUserName($userName): object
    {
        $user = Factory::getContainer()->get(UserFactoryInterface::class)->loadUserByUsername($userName);
        if (!$user) {
            return 0;
        }
        return self::getPersonByJoomlaId($user->id)->ma_id;
    }
}
