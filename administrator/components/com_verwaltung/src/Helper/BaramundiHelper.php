<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Helper;

use Exception;
use Joomla\CMS\Factory;
use Joomla\Database\DatabaseInterface;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Helper */
class BaramundiHelper
{

    private static $diffQuery = "SELECT 'plus' AS `set`, r.* "
            . "FROM #__iwf_baramundi_sw r "
            . "WHERE ROW(r.product_name, r.company, r.version) NOT IN (SELECT product_name, company, `version` FROM #__iwf_baramundi_sw_alt) "
            . "UNION ALL "
            . "SELECT 'minus' AS `set`, t.* "
            . "FROM #__iwf_baramundi_sw_alt t "
            . "WHERE ROW(t.product_name, t.company, t.version) NOT IN (SELECT product_name, company, `version` FROM #__iwf_baramundi_sw)";

    /**
     * Hole alle inventarisierte Software aller Endpoins von baramundi
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     */
    public static function fetchEndpointsSoftware() {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $db->setQuery('CREATE TEMPORARY TABLE ' . $db->qn('#__iwf_baramundi_sw_alt') . ' SELECT * FROM ' . $db->qn('#__iwf_baramundi_sw') . ';');
        $tempTableCreated = $db->execute();
        $query = $db->createQuery()
            ->delete($db->qn('#__iwf_baramundi_sw'));
        $db->setQuery($query)->execute();
        $baramundiConnector = new BaramundiConnector();
        $groups = $baramundiConnector->getStandardGroups();
        $n = 0;
        $k = 0;
        foreach ($groups as $group) {
            $endpoints = $baramundiConnector->getEndpoints($group->Id);
            foreach ($endpoints as $endpoint) {
                $sw = $baramundiConnector->getSoftware($endpoint->Id);
                if ($sw->Scans[0]->Data) {
                    foreach ($sw->Scans[0]->Data as $swItem) {
                        $version = $db->escape(trim($swItem->Version));
                        $productName = $db->escape(trim($swItem->ProductName));
                        $company = $db->escape(trim($swItem->Company));
                        if (empty($version)) {
                            $version = md5($productName);
                        }
                        if (!empty($productName)) {
                            $query->clear()
                                ->insert($db->qn('#__iwf_baramundi_sw'))
                                ->columns
                                (
                                    [
                                        $db->qn('product_name'),
                                        $db->qn('company'),
                                        $db->qn('version')
                                    ]
                                )
                                ->values(':pname, :comp, :version')
                                ->bind(':pname', $productName, ParameterType::STRING)
                                ->bind(':comp', $company, ParameterType::STRING)
                                ->bind(':version', $version, ParameterType::STRING);
                            $db->setQuery($query);
                            try {
                                $db->execute();
                                $n++;
                            } catch (Exception $ex) {
                                // double entry - skip
                            }
                        }
                    }
                }
            }
        }
        if ($n) {
            if ($tempTableCreated) {
                $diffs = $db->setQuery(self::$diffQuery)->loadObjectList();
                foreach ($diffs as $diff) {
                    $product_name = $db->escape($diff->product_name);
                    $company = $db->escape($diff->company);
                    $version = $db->escape($diff->version);
                    $set = $diff->set;
                    $date = Factory::getDate('now')->format('Y-m-d', true);
                    $query->clear()
                        ->insert($db->qn('#__iwf_baramundi_sw_diff'))
                        ->columns
                        (
                            [
                                $db->qn('product_name'),
                                $db->qn('company'),
                                $db->qn('version'),
                                $db->qn('set'),
                                $db->qn('found'),
                            ]
                        )
                        ->values(':pname, :comp, :vers, :set, :found')
                        ->bind(':pname', $product_name, ParameterType::STRING)
                        ->bind(':comp', $company, ParameterType::STRING)
                        ->bind(':vers', $version, ParameterType::STRING)
                        ->bind(':set', $set, ParameterType::STRING)
                        ->bind(':found', $date, ParameterType::STRING);
                    if ($db->setQuery($query)->execute()) {
                        $k++;
                    }
                }
            }

            $user = Factory::getApplication()->getIdentity();
            $userEmail = $user->email;
            MailHelper::sendMail($userEmail, "Ergebnis der baramundi Aktion", sprintf("%s Programme in der baramundi Inventur gefunden und in baramundi S/W-Datenbank eingetragen (%s Unterschiede)", $n, $k));
        }
    }

    /**
     * @return mixed 
     * @throws KeyNotFoundException 
     */
    public static function getEndpointsSoftware() {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('d.wname', 'hostname'),
                    $db->qn('d.baramundi_id', 'endpointid'),
                    $db->qn('r.id', 'rechner_id'),
                    $db->qn('i.ma_id')
                ]
            )
            ->from($db->qn('#__iwf_dns', 'd'))
            ->leftJoin($db->qn('#__iwf_rechner', 'r'), $db->qn('d.id') . '=' . $db->qn('r.dns_id'))
            ->leftJoin($db->qn('#__iwf_inventar', 'i'), $db->qn('i.id') . '=' . $db->qn('r.inventar_id'))
            ->where('NOT (NULLIF(' . $db->qn('d.baramundi_id') . ', "") IS NULL)')
            ->where('NOT (NULLIF(' . $db->qn('i.ma_id') . ', "") IS NULL)');
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $endpointSoftware = self::getEndpointSoftware($item->endpointid);
            if (count($endpointSoftware)) {
                $item->software = $endpointSoftware;
            }
        }
        return $items;
    }

    /**
     * @param mixed $endpointId 
     * @return mixed 
     */
    public static function getEndpointSoftware($endpointId) {
        $connector = new BaramundiConnector();
        $endpointSoftware = $connector->getSoftware($endpointId);
        if ($endpointSoftware->Scans) {
            if (count($endpointSoftware->Scans)) {
                return $endpointSoftware->Scans[0]->Data;
            }
        }
        return [];
    }

    /**
     * Nachdem baramundi S/W neu geholt wurde, die verknüpfte IWF S/W zuordnen
     * @return void 
     * @throws KeyNotFoundException 
     */
    public static function iwfSw2BaramundiSw() {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select(
                [
                    'a.*',
                    $db->qn('b.id'),
                    $db->qn('b.produkt'),
                ]
            )
            ->from($db->qn('#__iwf_baramundi_sw', 'a'))
            ->innerJoin($db->qn('#__iwf_software', 'b'), 
                $db->qn('b.baramundi_product') . ' LIKE CONCAT("%",' . $db->qn('a.product_name') . ',"%") AND ' . $db->qn('b.baramundi_version') . ' LIKE CONCAT("%",' . $db->qn('a.version') . ',"%")')
            ->where($db->qn('b.baramundi_product') . '<> ""');
        $items = $db->setQuery($query)->loadObjectList();
        foreach ($items as $item) {
            $query->clear()
                ->update($db->qn('#__iwf_baramundi_sw'))
                ->set($db->qn('iwf_software') . '=:sw')
                ->where(
                    [
                        $db->qn('version') . '=:ver',
                        $db->qn('product_name') . '=:pname'
                    ],
                    'AND'
                )
                ->bind(':sw', $item->id, ParameterType::INTEGER)
                ->bind(':ver', $item->version, ParameterType::STRING)
                ->bind(':pname', $item->product_name, ParameterType::STRING);
            $db->setQuery($query)->execute();
        }
    }

    /**
     * Das Feld baramundi_id für alle hosts (nach wname) in fzg_dns wird mit den Endpoint-Ids aus der baramundi-Datenbank gesetzt
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     */
    public static function fetchEndpointIds() {
        $emptyStr = '';
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->update($db->qn('#__iwf_dns'))
            ->set($db->qn('baramundi_id') . '=:bid')
            ->set($db->qn('OS') . '=:os')
            ->bind(':bid', $emptyStr, ParameterType::STRING)
            ->bind(':os', $emptyStr, ParameterType::STRING);
        $db->setQuery($query)->execute();
        $baramundiConnector = new BaramundiConnector();
        $groups = $baramundiConnector->getStandardGroups();
        $n = 0;
        foreach ($groups as $group) {
            $endpoints = $baramundiConnector->getEndpoints($group->Id, false);
            foreach ($endpoints as $endpoint) {
                $query->clear()
                    ->select("*")
                    ->from($db->qn('#__iwf_dns'))
                    ->where($db->qn('wname') . ' LIKE :wname')
                    ->bind(':wname', $endpoint->HostName, ParameterType::STRING);
                $item = $db->setQuery($query)->loadObject();
                if ($item) {
                    $query->clear()
                        ->update($db->qn('#__iwf_dns'))
                        ->set($db->qn('baramundi_id') . '=:bid')
                        ->set($db->qn('OS') . '=:os')
                        ->bind(':bid', $endpoint->Id, ParameterType::STRING)
                        ->bind(':os', $endpoint->OperatingSystem, ParameterType::STRING);
                    if (!empty($endpoint->Comments)) {
                        $query->set($db->qn('anmerkungen'). '=:anm')
                            ->bind(':anm', $endpoint->Comments, ParameterType::STRING);
                    } else {
                        if (!empty($item->anmerkungen) && $item->anmerkungen != $endpoint->Comments) {
                            $baramundiConnector->patchEndpointComment($endpoint->Id, $item->anmerkungen);
                        }
                    }
                    if (!empty($item->titel) && $item->titel != $endpoint->CustomStateText) {
                        $baramundiConnector->patchEndpointCustomStateText($endpoint->Id, trim($item->titel));
                    }
                    $query->where($db->qn('id') . '=:id')
                        ->bind(':id', $item->id, ParameterType::INTEGER);
                    $db->setQuery($query)->execute();
                    $n++;
                    // einige Rechnerdaten aktualisieren
                    $festplatten = '';
                    foreach ($endpoint->StorageMedia as $i => $storage) {
                        $festplatten .= sprintf('%d: %d [GB], ', $i + 1, $storage->ByteSize / 1000000000);
                    }
                    $festplatten = trim($festplatten, ', ');
                    $query->clear()
                        ->update($db->qn('#__iwf_rechner'))
                        ->set($db->qn('prozessor') . '=:prozessor')
                        ->set($db->qn('takt') . '=:takt')
                        ->set($db->qn('speicher') . '=:speicher')
                        ->set($db->qn('festplatten') . '=:platte')
                        ->where($db->qn('dns_id') . '=:dns_id')
                        ->bind(':dns_id', $item->id)
                        ->bind(':prozessor', $endpoint->CPUType, ParameterType::STRING)
                        ->bind(':takt', $endpoint->CPUSpeed, ParameterType::INTEGER)
                        ->bind(':speicher', $endpoint->TotalMemory, ParameterType::STRING)
                        ->bind(':platte', $festplatten, ParameterType::STRING);
                    $db->setQuery($query)->execute();
                }
            }
        }
        if ($n) {
            $user = Factory::getApplication()->getIdentity();
            $userEmail = $user->email;
            MailHelper::sendMail($userEmail, "Ergebnis der baramundi Aktion", sprintf("$n Endpoints gefunden und in DNS-Tabelle eingetragen"));
        }
    }

}
