<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

\defined('_JEXEC') or die;

define("CRLF", chr(13) . chr(10));
define("ABTEILUNGEN", "1,2,3,4,5,6,7,10,11,12,13");
define("ABTEILUNGEN_IWF", "1,2,3,4,5,10,11,12,13");
define("ABTEILUNGEN_PH", "6");
define("MAILMAN_ABTEILUNGEN", "1,2,3,4,5,6,7,10,11,12,13");
define("ABTEILUNGEN_AKADEMIS_AUTOREN", "1,2,3,4,5,7,10,11,12,13");

define("EDVX", "82,83,826");
define("DIENSTVERHAELTNIS_OEAW", 112);
define("DIENSTVERHAELTNIS_GAST", 113);
define("DIENSTVERHAELTNIS_DIENSTE", 114);
define("DIENSTVERHAELTNIS_FREIERDIENSTVERTRAG", 115);
define("DIENSTVERHAELTNIS_ANDERERVERTRAG", 116);
define("DIENSTVERHAELTNIS_AUSBILDUNGOHNEVERTRAG", 186);
define("ECHTE_DIENSTVERHAELTNISSE", "112, 113, 115, 116, 186");
define("MAILMAN_DIENSTVERHAELTNISSE", "112, 113, 115");

define("INVENTARTYP_RECHNER", "50, 51, 53, 55, 333");

define("RESERVIERUNGSTYP_RAUM", 144);
define("RESERVIERUNGSTYP_WOHNUNG", 145);
define("RESERVIERUNGSTYP_SCHREIBTISCH", 235);
define("RESERVIERUNG_EINMALIG", 62);
define("RESERVIERUNG_WOECHENTLICH", 63);
define("RESERVIERUNG_ZWEIWOECHENTLICH", 348);
define("RESERVIERUNG_MEHRTAEGIG", 326);
define("WOHNUNG3", 71);

// folgende Konstanten für Gästechecklistenmanagement
define("DIENSTVERHAELTNISSE_GAST", "(113)");

define("P_TYP_ZEITSCHRIFT_REFERIERT", "1");
define("P_TYP_ZEITSCHRIFT_NICHT_REFERIERT", "2");
define("P_TYP_IWF_BERICHT", "3");
define("P_TYP_BUCHBEITRAG", "4");
define("P_TYP_BUCH", "5");
define("P_TYP_BUCH_HERAUSGABE", "6");
define("P_TYP_DIPLOM_BAKK_ARBEIT", "7");
define("P_TYP_DISSERTATION", "8");
define("P_TYP_VORTRAG_BEI_TAGUNGEN", "9");
define("P_TYP_POSTER", "10");
define("P_TYP_PROPOSAL", "11");
define("P_TYP_BERICHT_ALLGEMEIN", "12");
define("P_TYP_VORTRAG_BEI_INSTITUTION", "13");
define("P_TYP_HABILITATION", "14");
define("P_TYP_ANDERER_VORTRAG", "15");
define("P_TYP_VORTRAG_EINES_GASTES", "16");
define("P_TYP_VORTRAG_BEI_PROJEKTMEETING", "17");

define("BESTELLUNG_ANBOT1", 229);
define("BESTELLUNG_ANBOT2", 230);
define("BESTELLUNG_ANBOT3", 231);
