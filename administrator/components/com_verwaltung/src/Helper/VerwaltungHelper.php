<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Helper;

defined('_JEXEC') or die;

use DateTime;
use Exception;
use InvalidArgumentException;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Component\ComponentHelper;
use \Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Log\Log;
use Joomla\CMS\Mail\MailerFactoryInterface;
use Joomla\CMS\Plugin\PluginHelper;
use Joomla\CMS\Uri\Uri;
use Joomla\Database\DatabaseInterface;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Registry\Registry;
use RuntimeException;

if (!defined('VERWALTUNG_MEDIA_URI')) {
    define('VERWALTUNG_MEDIA_URI', Uri::root(true) . '/media/com_verwaltung');
}
if (!defined('VERWALTUNG_TOOLBAR_URI')) {
    // nötig für Frontend Toolbars!
    define('VERWALTUNG_TOOLBAR_URI', Uri::root(true) . '/media/system/js/joomla-toolbar-button.js');
}

/** @package Iwf\Component\Verwaltung\Administrator\Helper */
class VerwaltungHelper
{

    /**
     * Lade Sprachdateien, falls nicht geladen
     * @return void 
     * @throws Exception 
     * @throws InvalidArgumentException 
     */
    public static function loadLangugage()
    {
        $lang = Factory::getApplication()->getLanguage();
        $extension = 'com_verwaltung';
        $base_dir = JPATH_SITE . '/components/com_verwaltung';
        $language_tag = $lang->getTag();
        $reload = true;
        $lang->load($extension, $base_dir, $language_tag, $reload);
    }

    /** @return void  */
    public static function loadDefines(): void
    {
        if (file_exists(__DIR__ . '/defines.php')) {
            include_once __DIR__ . '/defines.php';
        }
    }

    /**
     * @return void 
     * @throws KeyNotFoundException 
     */
    public static function loadLogger(): void
    {
        $params = ComponentHelper::getParams('com_verwaltung');
        $logfile = $params->get('logfile', 'com_verwaltung_log');
        $logfile .= '.php';
        $logfileperiod = $params->get('logperiod', 1);
        $prefix = date('Y-W_');
        switch ($logfileperiod) {
            case 0: // täglich
                $prefix = date('Y-m-d_');
                break;
            case 1: // wöchentlich
                $prefix = date('Y-W_');
                break;
            case 2: // monatlich
                $prefix = date('Y-m_');
                break;
        }
        $options = array('text_file' => $prefix . $logfile, 'text_entry_format' => '{DATETIME}	{CLIENTIP}	{PRIORITY}	{CATEGORY}	{MESSAGE}');
        Log::addLogger($options, Log::ALL);
    }

    /**
     * @return void 
     * @throws Exception 
     */
    public static function loadVerwaltungCss(): void
    {
        $doc = Factory::getApplication()->getDocument();
        $doc->addStyleSheet(VERWALTUNG_MEDIA_URI . '/css/verwaltung.css');
    }

    /**
     * @return void 
     * @throws Exception 
     */
    public static function loadFrontendCss(): void
    {
        $doc = Factory::getApplication()->getDocument();
        $doc->addStyleSheet(VERWALTUNG_MEDIA_URI . '/css/frontend.css');
    }

    /**
     * @return void 
     * @throws Exception 
     */
    public static function loadVerwaltungScript(): void
    {
        $doc = Factory::getApplication()->getDocument();
        $doc->addScript(VERWALTUNG_MEDIA_URI . '/js/verwaltung.js');
    }

    /**
     * @return void 
     * @throws Exception 
     */
    public static function loadToolbarScript(): void
    {
        $doc = Factory::getApplication()->getDocument();
        $doc->addScript(VERWALTUNG_TOOLBAR_URI);
    }

    /**
     * @param mixed $site 
     * @return bool 
     * @throws KeyNotFoundException 
     */
    public static function isTimeForCronJob($site)
    {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $today = getDate();
        $query = $db->createQuery()
            ->select($db->qn($site))
            ->from($db->qn('#__iwf_cron'))
            ->where($db->qn('id') . '=1');
        $item = $db->setQuery($query)->loadObject();
        if (!$item) {
            $query->clear()
                ->insert($db->qn('#__iwf_cron'))
                ->columns(
                    [
                        $db->qn('id'),
                        $db->qn('site'),
                        $db->qn('administrator'),
                    ]
                )
                ->values('1, ADDDATE(NOW(),INTERVAL -1 DAY), ADDDATE(NOW(),INTERVAL -1 DAY)');
            $item = $db->setQuery($query)->loadObject();
        }
        if ($item->$site) {
            $lastRun = getDate(Factory::getDate($item->$site)->toUnix());
            if ($lastRun['yday'] < $today['yday'] || $lastRun['year'] < $today['year']) {
                $query->clear()
                    ->update($db->qn('#__iwf_cron'))
                    ->set($db->qn($site) . '=NOW()')
                    ->where($db->qn('id') . '=1');
                $db->setQuery($query)->execute();
                return true;
            }
        }
        return false;
    }

    /**
     * @return array 
     * @throws KeyNotFoundException 
     */
    public static function getAbteilungen(): array
    {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select('*')
            ->from($db->qn('#__iwf_abteilungen'))
            ->where($db->qn('state') . '=1')
            ->order($db->qn('reihenfolge'));
        return $db->setQuery($query)->loadObjectList();
    }

    /**
     * Konvertiert AD Zeit in Unix timestamp
     * @param mixed $ldapTime 
     * @return PHP_Token_BAD_CHARACTER 
     */
    public static function ldapTimeToUnixTime($ldapTime): int
    {
        $secsAfterADEpoch = $ldapTime / 10000000;
        $ADToUnixConverter = ((1970 - 1601) * 365 - 3 + round((1970 - 1601) / 4)) * 86400;
        return intval($secsAfterADEpoch - $ADToUnixConverter);
    }

    /**
     * @return void 
     * @throws Exception 
     * @throws InvalidArgumentException 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     */
    public static function updateDatabases()
    {
        self::loadLangugage();
        $date = date('Y-m-d');
        // abgelaufene Raumreservierungen
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->delete($db->qn('#__iwf_reservierungen'))
            ->where(
                [
                    $db->qn('datum') . ' < :datum',
                    $db->qn('reservierungstyp') . '=' . RESERVIERUNGSTYP_RAUM,
                ]
            )
            ->andWhere(
                [
                    $db->qn('wiederholungsintervall') . '=' . RESERVIERUNG_EINMALIG,
                    $db->qn('wiederholungsintervall') . '=' . RESERVIERUNG_MEHRTAEGIG,
                ],
                'OR'
            )
            ->bind(':datum', $date, ParameterType::STRING);
        if ($db->setQuery($query)->execute()) {
            if ($count = $db->getAffectedRows()) {
                LogHelper::add("$count abgelaufene Raumreservierungen gelöscht", 'cronjob');
            }
        }
        // abgelaufene Wohnungsreservierungen
        $query->clear()
            ->delete($db->qn('#__iwf_reservierungen'))
            ->where(
                [
                    $db->qn('datumbis') . ' < :datum',
                    $db->qn('reservierungstyp') . '=' . RESERVIERUNGSTYP_WOHNUNG,
                ]
            )
            ->bind(':datum', $date, ParameterType::STRING);
        if ($db->setQuery($query)->execute()) {
            if ($count = $db->getAffectedRows()) {
                LogHelper::add("$count abgelaufene Wohnungsreservierungen gelöscht", 'cronjob');
            }
        }
        // abgelaufene Schreibtischreservierungen
        $query->clear()
            ->delete($db->qn('#__iwf_reservierungen'))
            ->where(
                [
                    $db->qn('datumbis') . ' < :datum',
                    $db->qn('reservierungstyp') . '=' . RESERVIERUNGSTYP_SCHREIBTISCH,
                ]
            )
            ->bind(':datum', $date, ParameterType::STRING);
        if ($db->setQuery($query)->execute()) {
            if ($count = $db->getAffectedRows()) {
                LogHelper::add("$count abgelaufene Schreibtischreservierungen gelöscht", 'cronjob');
            }
        }
        // wöchentliche Veranstaltungen
        $query->clear()
            ->select(
                [
                    $db->qn('id'),
                    $db->qn('datum'),
                    $db->qn('veranstaltung'),
                ]
            )
            ->from($db->qn('#__iwf_reservierungen'))
            ->where(
                [
                    $db->qn('datum') . ' < :datum',
                    $db->qn('wiederholungsintervall') . '=' . RESERVIERUNG_WOECHENTLICH,
                ]
            )
            ->bind(':datum', $date, ParameterType::STRING);
        if ($items = $db->setQuery($query)->loadObjectList()) {
            $currentTime = (new DateTime())->getTimestamp();
            foreach ($items as $item) {
                $d1 = (new DateTime($item->datum))->getTimestamp();
                while ($d1 < $currentTime) {
                    $d1 += 7 * 60 * 60 * 24;
                }
                $d1 = date('Y-m-d', $d1);
                $query->clear()
                    ->update($db->qn('#__iwf_reservierungen'))
                    ->set($db->qn('datum') . '=:datum')
                    ->bind(':datum', $d1, ParameterType::STRING)
                    ->where($db->qn('id') . '=:id')
                    ->bind(':id', $item->id, ParameterType::INTEGER);
                if ($count = $db->setQuery($query)->execute()->getAffectedRows()) {
                    LogHelper::add("Datum der periodischen Veranstaltung '$item->veranstaltung' aktualisiert", 'cronjob');
                }
            }
        }
        // zweiwöchentliche Veranstaltungen
        $query->clear()
            ->select(
                [
                    $db->qn('id'),
                    $db->qn('datum'),
                    $db->qn('veranstaltung'),
                ]
            )
            ->from($db->qn('#__iwf_reservierungen'))
            ->where(
                [
                    $db->qn('datum') . ' < :datum',
                    $db->qn('wiederholungsintervall') . '=' . RESERVIERUNG_ZWEIWOECHENTLICH,
                ]
            )
            ->bind(':datum', $date, ParameterType::STRING);
        if ($items = $db->setQuery($query)->loadObjectList()) {
            $currentTime = (new DateTime())->getTimestamp();
            foreach ($items as $item) {
                $d1 = (new DateTime($item->datum))->getTimestamp();
                while ($d1 < $currentTime) {
                    $d1 += 14 * 60 * 60 * 24;
                }
                $d1 = date('Y-m-d', $d1);
                $query->clear()
                    ->update($db->qn('#__iwf_reservierungen'))
                    ->set($db->qn('datum') . '=:datum')
                    ->bind(':datum', $d1, ParameterType::STRING)
                    ->where($db->qn('id') . '=:id')
                    ->bind(':id', $item->id, ParameterType::INTEGER);
                if ($count = $db->setQuery($query)->execute()->getAffectedRows()) {
                    LogHelper::add("Datum der periodischen Veranstaltung '$item->veranstaltung' aktualisiert", 'cronjob');
                }
            }
        }
        //periodische Veranstaltungen nach Enddatum löschen
        /*$query->clear()
            ->select(
                [
                    $db->qn('id'),
                    $db->qn('enddatum'),
                    $db->qn('veranstaltung'),
                ]
            )
            ->from($db->qn('#__iwf_reservierungen'))
            ->where(
                [
                    $db->qn('enddatum') . ' < :datum',
                ]
            )
            ->andWhere(
                [
                    $db->qn('wiederholungsintervall') . '=' . RESERVIERUNG_WOECHENTLICH,
                    $db->qn('wiederholungsintervall') . '=' . RESERVIERUNG_ZWEIWOECHENTLICH,
                ],
                'OR'
            )
            ->bind(':datum', $date, ParameterType::STRING);
        if ($items = $db->setQuery($query)->loadObjectList()) {
            foreach ($items as $item) {
                $query->clear()
                    ->delete($db->qn('#__iwf_reservierungen'))
                    ->where($db->qn('id') . '=:id')
                    ->bind(':id', $item->id, ParameterType::INTEGER);

                if ($db->setQuery($query)->execute()) {
                    if ($count = $db->getAffectedRows()) {
                        LogHelper::add("$count abgelaufene periodische Raumreservierungen gelöscht", 'cronjob');
                    }
                }
            }
        }*/
        // rückgeforderte Rechner
        $query->clear()
            ->select(
                [
                    $db->qn('i.id'),
                    $db->qn('i.inventarnummer'),
                    $db->qn('i.rueckgefordert'),
                    $db->qn('i.rueckgefordert_time'),
                    $db->qn('i.rueckgefordert_mail'),
                    'DATEDIFF(NOW(),' . $db->qn('i.rueckgefordert_time') . ') AS ' . $db->qn('age'),
                    $db->qn('r.id', 'rid'),
                    $db->qn('d.hostname'),
                    $db->qn('m.email'),
                    $query->concatenate([$db->qn('m.vorname'), $db->qn('m.nachname')], ' ') . ' AS ' . $db->qn('name')
                ]
            )
            ->from($db->qn('#__iwf_inventar', 'i'))
            ->leftJoin($db->qn('#__iwf_rechner', 'r'), $db->qn('r.inventar_id') . '=' . $db->qn('i.id'))
            ->leftJoin($db->qn('#__iwf_dns', 'd'), $db->qn('r.dns_id') . '=' . $db->qn('d.id'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('i.ma_id') . '=' . $db->qn('m.id'))
            ->where(
                [
                    $db->qn('i.rueckgefordert') . '=1',
                    'DATEDIFF(NOW(),' . $db->qn('i.rueckgefordert_time') . ') > 90',
                    'NOT ISNULL(' . $db->qn('r.id') . ')',
                    'NOT ISNULL(' . $db->qn('d.hostname') . ')',
                    'NOT ISNULL(' . $db->qn('m.email') . ')',
                ]
            )
            ->andWhere(
                [
                    'NOT ISNULL(' . $db->qn('i.rueckgefordert_time') . ')',
                    'CAST(' . $db->qn('i.rueckgefordert_time') . ' AS INTEGER) != 0'
                ],
                'OR'
            );
        if ($items = $db->setQuery($query)->loadObjectList()) {
            $params = ComponentHelper::getParams('com_verwaltung');
            $edvmail = $params->get('edv_email');
            $mail = Factory::getContainer()->get(MailerFactoryInterface::class)->createMailer();
            foreach ($items as $item) {
                if (!$item->rueckgefordert_mail) {
                    $mail->setSubject(sprintf(Text::_('COM_VERWALTUNG_CRON_COMPUTER_SUBJECT'), $item->inventarnummer));
                    $mail->setBody(sprintf(Text::_('COM_VERWALTUNG_CRON_COMPUTER_BODY'),  $item->hostname . ' (' . $item->inventarnummer . ')', $item->name));
                    $mail->addRecipient($item->email);
                    $mail->addBCC($edvmail);
                    $mail->send();
                    $query->clear()
                        ->update($db->qn('#__iwf_inventar'))
                        ->set($db->qn('rueckgefordert_mail') . '=1')
                        ->where($db->qn('id') . '=:id')
                        ->bind(':id', $item->id, ParameterType::INTEGER);
                    $db->setQuery($query)->execute();
                } elseif ($item->rueckgefordert_mail == 1 && $item->age > 90) {
                    $mail->setSubject(sprintf(Text::_('COM_VERWALTUNG_CRON_COMPUTER_DISABLE_SUBJECT'), $item->inventarnummer));
                    $mail->setBody(sprintf(Text::_('COM_VERWALTUNG_CRON_COMPUTER_DISABLE_BODY'), $item->hostname . ' (' . $item->inventarnummer . ')', $item->name));
                    $mail->addRecipient($edvmail);
                    $mail->Send();
                    $query->clear()
                        ->update($db->qn('#__iwf_inventar'))
                        ->set($db->qn('rueckgefordert_mail') . '=2')
                        ->where($db->qn('id') . '=:id')
                        ->bind(':id', $item->id, ParameterType::INTEGER);
                    $db->setQuery($query)->execute();
                }
            }
        }
    }
}
