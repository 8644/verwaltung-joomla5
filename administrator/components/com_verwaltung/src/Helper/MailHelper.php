<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Helper;

defined('_JEXEC') or die;

use Exception;
use \Joomla\CMS\Factory;
use Joomla\Database\DatabaseInterface;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Log\Log;
use Joomla\CMS\Mail\MailerFactoryInterface;
use Joomla\CMS\Application\CMSApplicationInterface;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;

/** @package Iwf\Component\Verwaltung\Administrator\Helper */
abstract class MailHelper
{

    /**
     * @return array 
     * @throws KeyNotFoundException 
     */
    public static function getMailabteilungen(): array
    {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('id'),
                    $db->qn('mailverteiler'),
                ]
            )
            ->from($db->qn('#__iwf_abteilungen'))
            ->where($db->qn('state') . '=1')
            ->order($db->qn('reihenfolge'));
        return $db->setQuery($query)->loadObjectList();
    }

    /**
     * Gibt eine Liste mit allen Mailmanlisten zurück
     * @return array 
     * @throws KeyNotFoundException 
     */
    public static function getAllMailingLists(): array
    {
        $result = [];
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery()
            ->select($db->qn('mailman_liste'))
            ->from($db->qn('#__iwf_abteilungen'))
            ->where('NOT (' . $db->qn('mailman_liste') . ' IS NULL)')
            ->where($db->qn('mailman_liste') . '<> ""');
        $items = $db->setQuery($query)->loadRowList();
        foreach ($items as $item) {
            $result[] = $item[0];
        }
        return $result;
    }

    /**
     * Ermittelt den Namen der Mailing-Listen aus den Abteilungs-Ids
     * @param mixed $abteilung_ids 
     * @return array 
     * @throws KeyNotFoundException 
     */
    public static function getMailmanListen($abteilung_ids): array
    {
        $ids = [];
        if (is_string($abteilung_ids)) {
            $ids = explode(',', $abteilung_ids);
        } else if (is_object($abteilung_ids)) {
            $ids = (array)$abteilung_ids;
        } else {
            $ids = (array)$abteilung_ids;
        }
        if (count($ids)) {
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $query = $db->createQuery()
                ->select($db->qn('mailman_liste'))
                ->from($db->qn('#__iwf_abteilungen'))
                ->whereIn($db->qn('id'), $ids);
            $items = $db->setQuery($query)->loadObjectList();
            $result = [];
            foreach ($items as $item) {
                if (!in_array($item->mailman_liste, $result)) {
                    $result[] = $item->mailman_liste;
                }
            }
            return $result;
        }
        return [];
    }

    /**
     * @param mixed $id 
     * @param mixed $oldMailingLists 
     * @param mixed $newMailingLists 
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     */
    public static function adjustMailmanSubscriptions($id, $oldMailingLists = null, $newMailingLists = null): void
    {
        if (!$id) {
            return;
        }
        $person = Person::getPersonByMaId($id);
        $email = $person->mailman_name;
        if (preg_match('/[A-Za-z0-9.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,}/', $email)) { // kann sein, dass nur der Name enthalten ist
            $params = ComponentHelper::getParams('com_verwaltung');
            $mailman_pwd = $params->get('mailman_password');
            if ($mailman_pwd) {
                if ($oldMailingLists || $newMailingLists) {
                    $remove = array_diff($oldMailingLists, $newMailingLists);
                    foreach ($remove as $list) {
                        self::mailmanUnsubscribe($list, $email, $mailman_pwd);
                    }
                    $add = array_unique(array_merge(array_diff($newMailingLists, $oldMailingLists), $person->default_mailman_lists));
                    foreach ($add as $list) {
                        self::mailmanSubscribe($list, $email, $mailman_pwd);
                    }
                } else {
                    // von allen Listen entfernen
                    foreach (self::getAllMailingLists() as $list) {
                        self::mailmanUnsubscribe($list, $email, $mailman_pwd);
                    }
                }
            }
        }
    }

    /**
     * @param mixed $list 
     * @param mixed $email 
     * @param mixed $adminPw 
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     */
    public static function mailmanSubscribe($list, $email, $adminPw): void
    {
        $params = ComponentHelper::getParams('com_verwaltung');
        if (!$params->get('mailman_enable')) {
            return;
        }
        $mm = Mailman::getInstance($list, $adminPw);
        $result = (int) $mm->subscribe($email);
        if ($result == 200) {
            LogHelper::add(sprintf("Subscription to list %s successful for %s", $list, urldecode($email)), Log::INFO, 'mailman');
        } else {
            LogHelper::add(sprintf("Subscription to list %s failed with HTTP error %d for %s", $list, $result, urldecode($email)), Log::ERROR, 'mailman');
        }
    }

    /**
     * @param mixed $list 
     * @param mixed $email 
     * @param mixed $adminPw 
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     */
    public static function mailmanUnsubscribe($list, $email, $adminPw): void
    {
        $params = ComponentHelper::getParams('com_verwaltung');
        if (!$params->get('mailman_enable')) {
            return;
        }
        $mm = Mailman::getInstance($list, $adminPw);
        $result = (int) $mm->unsubscribe($email);
        if ($result == 200) {
            LogHelper::add(sprintf("Unsubscription from list %s successful for %s", $list, urldecode($email)), Log::INFO, 'mailman');
        } else {
            LogHelper::add(sprintf("Unsubscription from list %s failed with HTTP error %d for %s", $list, $result, urldecode($email)), Log::ERROR, 'mailman');
        }
    }

    /**
     * @param mixed $to 
     * @param mixed $subject 
     * @param mixed $body 
     * @param mixed $from 
     * @param mixed $attachment 
     * @return bool 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     */
    public static function sendMail($to, $subject, $body, $from = null, $attachment = null): bool
    {
        if (!Extensions::runningOnSaturn()) {
            return false;
        }
        if (!Factory::getApplication()->get('mailonline', 1)) {
            Factory::getApplication()->enqueueMessage('Mailfunktion disabled!', CMSApplicationInterface::MSG_INFO);
            return false;
        }
        $mail = Factory::getContainer()->get(MailerFactoryInterface::class)->createMailer();
        $mail->XMailer = 'PHP/' . phpversion();
        if (is_null($from)) {
            $mail->setFrom("no-reply@saturn.iwf.oeaw.ac.at", "IWF-Verwaltung");
        } else {
            $from_ = preg_split("/[\s,;]+/", $from);
            if (count($from_) == 1) {
                $from_[] = "Verwaltung";
            }
            $mail->setFrom($from_[0], $from_[1]);
        }
        $mail->isHTML(true);
        $recipients = preg_split("/[\s,;]+/", $to);
        foreach ($recipients as $recipient) {
            $mail->addAddress($recipient);
        }
        $mail->Subject = $subject;
        if (str_contains($mail->ContentType, "html")) {
            $body = str_replace(CRLF, "<br />", $body);
        }
        $mail->Body = $body;
        if (is_array($attachment) && count($attachment) == 2) {
            $mail->addStringAttachment($attachment[0], $attachment[1]);
        }
        if ($mail->send()) {
            Factory::getApplication()->enqueueMessage(Text::_('COM_VERWALTUNG_MAILHELPER_SUCCESS'), CMSApplicationInterface::MSG_INFO);
            LogHelper::add(Text::sprintf("Email erfolgeich gesendet an %s", $to), Log::INFO, "person");
            return true;
        } else {
            Factory::getApplication()->enqueueMessage($mail->ErrorInfo, CMSApplicationInterface::MSG_ERROR);
            LogHelper::add(Text::sprintf("Email an %s konnte nicht gesendet werden (%s)", $to, $mail->ErrorInfo), Log::WARNING, 'person');
            return false;
        }
    }
}
