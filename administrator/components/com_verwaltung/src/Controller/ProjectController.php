<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Controller;

defined('_JEXEC') or die;

use Error;
use Exception;
use InvalidArgumentException;
use Joomla\CMS\MVC\Controller\FormController;
use Iwf\Verwaltung\Extensions;
use Joomla\Database\DatabaseInterface;
use Joomla\CMS\Factory;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\Controller */
class ProjectController extends FormController
{

    /**
     * @param array $data 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowAdd($data = array())
    {
        return Extensions::isAllowed('manage.abteilung');
    }

    /**
     * @param array $data 
     * @param string $key 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowEdit($data = array(), $key = 'id')
    {
        return Extensions::isAllowed('manage.abteilung');
    }

    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     * @throws Error 
     */
    public function save($key = null, $urlVar = null)
    {
        $data = $this->app->input->post->get('form', array(), 'array ');
        $id = (int) $data['ma_id'];
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery();
        $query->select($db->qn('a.abteilung'))
            ->from($db->qn('#__iwf_mitarbeiter', 'a'))
            ->where($db->qn('a.id') . '=:id')
            ->bind(':id', $id, ParameterType::INTEGER);
        $items = $db->setQuery($query)->loadObjectList();
        if (count($items)) {
            $abteilung = $items[0]->abteilung;
            if ($abteilung != $data['abteilung']) {
                $data['abteilung'] = $abteilung;
                $this->app->input->post->set('jform', $data);
            }
        }
        return parent::save($key, $urlVar);
    }
}
