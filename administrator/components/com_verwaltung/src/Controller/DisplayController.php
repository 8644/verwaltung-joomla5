<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Controller;

defined('_JEXEC') or die;

use Exception;
use Joomla\CMS\MVC\Controller\BaseController;
use Joomla\CMS\MVC\Factory\MVCFactoryInterface;
use Joomla\CMS\Application\CMSApplicationInterface;
use Joomla\Input\Input;
use Iwf\Component\Verwaltung\Administrator\Helper\VerwaltungHelper;
use Iwf\Component\Verwaltung\Administrator\Helper\Person;
use Joomla\CMS\Application\CMSWebApplicationInterface;
use Joomla\CMS\Language\Text;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;
use Joomla\Filesystem\Exception\FilesystemException;

/** @package Iwf\Component\Verwaltung\Administrator\Controller */
class DisplayController extends BaseController
{
    protected $default_view = 'departments';

    /**
     * @param array $config 
     * @param MVCFactoryInterface|null $factory 
     * @param null|CMSApplicationInterface $app 
     * @param null|Input $input 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     * @throws FilesystemException 
     */
    public function __construct($config = [], MVCFactoryInterface $factory = null, ?CMSApplicationInterface $app = null, ?Input $input = null)
    {
        $person = Person::getInstance();
        if ($person->ma_id == 0) {
            $this->app->enqueueMessage(Text::_('COM_VERWALTUNG_ACCESS_FORBIDDEN'), CMSWebApplicationInterface::MSG_WARNING);
            $this->app->redirect('index.php');
        } else {
            VerwaltungHelper::loadVerwaltungCss();
            parent::__construct($config, $factory, $app, $input);
        }
    }
}
