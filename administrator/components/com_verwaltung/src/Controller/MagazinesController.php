<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 * 
 * Zeitschriften
 */

namespace Iwf\Component\Verwaltung\Administrator\Controller;

defined('_JEXEC') or die;

use Exception;
use Iwf\Verwaltung\IwfAdminController;
use Joomla\CMS\MVC\Model\BaseDatabaseModel;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\Controller */
class MagazinesController extends IwfAdminController
{

	/**
	 * @param string $name 
	 * @param string $prefix 
	 * @param array $config 
	 * @return BaseDatabaseModel|bool 
	 * @throws Exception 
	 * @throws UnexpectedValueException 
	 */
	public function getModel($name = 'Magazine', $prefix = 'Administrator', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}
}
