<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Controller;

defined('_JEXEC') or die;

use Exception;
use Iwf\Component\Verwaltung\Administrator\Helper\BaramundiHelper;
use Joomla\CMS\MVC\Controller\FormController;
use Joomla\CMS\Router\Route;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;

/** @package Iwf\Component\Verwaltung\Administrator\Controller */
class BaramundiprogramsController extends FormController
{
    
    /**
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     */
    public function baramundisoftware()
    {
        BaramundiHelper::fetchEndpointsSoftware();
        $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=baramundiprograms', false));
    }

    /**
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     */
    public function iwfsoftware2baramundi()
    {
        BaramundiHelper::iwfSw2BaramundiSw();
        $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=baramundiprograms', false));
    }
}