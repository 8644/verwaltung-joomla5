<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Controller;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Iwf\Component\Verwaltung\Administrator\Helper\LicenseHelper;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfAdminController;
use Joomla\CMS\Application\CMSApplicationInterface;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Model\BaseDatabaseModel;
use Joomla\CMS\Router\Route;
use UnexpectedValueException;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;

/** @package Iwf\Component\Verwaltung\Administrator\Controller */
class LicensestateController extends IwfAdminController
{

    protected $default_view = 'licensestate';

    /**
     * @param string $name 
     * @param string $prefix 
     * @param array $config 
     * @return BaseDatabaseModel 
     * @throws Exception 
     * @throws UnexpectedValueException 
     */
    public function getModel($name = 'Licensestate', $prefix = 'Administrator', $config = []) {
        return parent::getModel($name, $prefix, $config);
    }

    /**
     * @return void 
     * @throws InvalidArgumentException 
     */
    public function cancel() {
        if ($return = Extensions::getReturnRoute()) {
            $this->app->redirect($return);
        } else {
            $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=programs', false));
        }
    }

    /**
     * @return false|void 
     * @throws Exception 
     */
    public function statistik() {
        $itemid = $this->app->input->getInt('id');
        if (!$itemid) {
            // Auswahl aus Liste
            $cid = $this->app->input->get('cid', array(), '', 'array');
            $itemid = $cid[0];
        }
        if (!$itemid) {
            $this->app->enqueueMessage(Text::_($this->text_prefix . '_NO_ITEM_SELECTED'), CMSApplicationInterface::MSG_WARNING);
            $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=programs', false));
            return false;
        }
        $this->app->setUserState('com_verwaltung.licensestate.program', $itemid);
        $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=licensestate&layout=default', false));
    }

    /**
     * @return void 
     * @throws Exception 
     */
    public function gesamtuebersicht() {
        $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=licensestate&layout=overview', false));
    }

    /**
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws UnexpectedValueException 
     */
    public function abdeckung() {
        LicenseHelper::abdeckung($this);
        $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=programs', false));
    }

    /**
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     */
    public function baramundisync() {
        LicenseHelper::syncLicenses();
        $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=programs', false));
    }

    /**
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     */
    public function cleanlicenses() {
        LicenseHelper::cleanLicenses();
        $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=programs', false));
    }

}
