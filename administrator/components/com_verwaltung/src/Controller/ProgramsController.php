<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Controller;

defined('_JEXEC') or die;

use Exception;
use Iwf\Verwaltung\IwfAdminController;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\Model\BaseDatabaseModel;
use Joomla\Database\DatabaseInterface;
use Joomla\Database\ParameterType;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\Controller */
class ProgramsController extends IwfAdminController
{

	/**
	 * @param string $name 
	 * @param string $prefix 
	 * @param array $config 
	 * @return BaseDatabaseModel|bool 
	 * @throws Exception 
	 * @throws UnexpectedValueException 
	 */
	public function getModel($name = 'Program', $prefix = 'Administrator', $config = array('ignore_request' => true))	//media->medium
	{
		return parent::getModel($name, $prefix, $config);
	}

    /**
     * @param mixed $id 
     * @param int $value 
     * @param int $tag 
     * @return void 
     */
    public function changeStatus($id, $value = 0, $tag = 0, $return = 0)
	{
		$db = Factory::getContainer()->get(DatabaseInterface::class);
		$query = $db->createQuery()
			->update($db->qn('#__iwf_software'));
            switch ($tag) {
                case 0:
                    $query->set($db->qn('aktuell') . ' = :status');
                    break;
                case 1:
                    $query->set($db->qn('abdeckung') . ' = :status');
                    break;
            }
			$query->where($db->qn('id') . '=:id')
			->bind(':status', $value, ParameterType::INTEGER)
			->bind(':id', $id, ParameterType::INTEGER);
		$db->setQuery($query)->execute();
	}
}
