<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Controller;

use Exception;
use Joomla\CMS\Factory;
use Joomla\Utilities\ArrayHelper;
use Joomla\Database\ParameterType;
use Joomla\Database\DatabaseInterface;

defined('_JEXEC') or die;

use Iwf\Verwaltung\IwfAdminController;
use Joomla\CMS\MVC\Model\BaseDatabaseModel;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\Controller */
class ProjectsController extends IwfAdminController
{

	/**
	 * @param string $name 
	 * @param string $prefix 
	 * @param array $config 
	 * @return BaseDatabaseModel|bool 
	 * @throws Exception 
	 * @throws UnexpectedValueException 
	 */
	public function getModel($name = 'Project', $prefix = 'Administrator', $config = array('ignore_request' => true))
	{
		return parent::getModel($name, $prefix, $config);
	}

	/**
	 * @param mixed $pks 
	 * @param int $value 
	 * @param int $tag 
	 * @return void 
	 * @throws KeyNotFoundException 
	 */
	public function changeStatus($pks, $value = 0, $tag = 0)
	{
		$pks = (array) $pks;
		$pks = ArrayHelper::toInteger($pks);
		$value = (int) $value;
		$db = Factory::getContainer()->get(DatabaseInterface::class);
		$query = $db->createQuery()
			->update($db->qn('#__iwf_projekte'))
			->set($db->qn('aktuell') . ' = :status')
			->whereIn($db->qn('id'), $pks)
			->bind(':status', $value, ParameterType::INTEGER);
		$db->setQuery($query)->execute();
	}
}
