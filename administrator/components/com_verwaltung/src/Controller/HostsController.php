<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 * 
 * DNS
 */

namespace Iwf\Component\Verwaltung\Administrator\Controller;


defined('_JEXEC') or die;

use Exception;
use Iwf\Verwaltung\IwfAdminController;
use Joomla\CMS\Application\CMSWebApplicationInterface;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\Model\BaseDatabaseModel;
use Joomla\Database\DatabaseInterface;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\Controller */
class HostsController extends IwfAdminController
{

    /**
     * @param string $name 
     * @param string $prefix 
     * @param array $config 
     * @return BaseDatabaseModel|bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     */
    public function getModel($name = 'Host', $prefix = 'Administrator', $config = array('ignore_request' => true))
    {
        return parent::getModel($name, $prefix, $config);
    }

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function delete()
    {
        $input = $this->app->input;
        $cid = $input->post->get('cid', array(), 'array');
        $orgCount = count($cid);
        $deleted_hosts = "";
        $nodelete = [];
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        foreach ($cid as $id) {
            $query = $db->createQuery();
            $query->select
                (
                    [
                        $db->qn('a.dns_id'),
                        $db->qn('b.hostname', 'hostname'),
                    ]
                )
                ->from($db->qn('#__iwf_rechner', 'a'))
                ->leftJoin($db->qn('#__iwf_dns', 'b'), $db->qn('a.dns_id') . '=' . $db->qn('b.id'))
                ->where($db->qn('a.dns_id') . '=:id')
                ->bind(':id', $id, ParameterType::INTEGER);
            $items = $db->setQuery($query)->loadObjectList();
            foreach ($items as $item) {
                $deleted_hosts .= $item->hostname . ", ";
                $nodelete[] = $item->id;
            }
        }
        foreach ($nodelete as $nodelete_id) {
            unset($cid[array_search($nodelete_id, $cid)]);
        }
        if (count($cid) && count($cid) < $orgCount) {
            $this->app->enqueueMessage(sprintf(Text::_("COM_VERWALTUNG_HOSTS_LESS_ITEMS_LEFT"), rtrim($deleted_hosts, ", ")), CMSWebApplicationInterface::MSG_WARNING);
        } else if (count($cid) < 1) {
            $this->app->enqueueMessage(Text::_("COM_VERWALTUNG_HOSTS_NO_ITEMS_LEFT"), CMSWebApplicationInterface::MSG_WARNING);
        }
        $input->set('cid', $cid, true);
        parent::delete();
    }
}
