<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Controller;

defined('_JEXEC') or die;

use Exception;
use Iwf\Verwaltung\IwfAdminController;
use Joomla\CMS\Factory;
use Joomla\Database\DatabaseInterface;
use Joomla\CMS\Router\Route;
use Joomla\DI\Exception\KeyNotFoundException;

/** @package Iwf\Component\Verwaltung\Administrator\Controller */
class BaramundiprogramsdiffController extends IwfAdminController
{

	/**
	 * @return void 
	 * @throws KeyNotFoundException 
	 * @throws Exception 
	 */
	public function deleteall()
	{
		$db = Factory::getContainer()->get(DatabaseInterface::class);
		$query = $db->createQuery()
			->delete($db->qn('#__iwf_baramundi_sw_diff'));
		$db->setQuery($query);
        $db->execute();
        $this->setRedirect(Route::_('index.php?option=' . $this->option . '&view=baramundiprogramsdiff', false));
	}
}
