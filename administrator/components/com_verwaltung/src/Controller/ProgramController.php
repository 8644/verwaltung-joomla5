<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Controller;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\MVC\Controller\FormController;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Session\Session;
use Joomla\Database\DatabaseInterface;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use stdClass;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\Controller */
class ProgramController extends FormController
{
    
    /**
     * @param array $data 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowAdd($data = array())
    {
        return Extensions::isAllowed('manage.sw');
    }

    /**
     * @param array $data 
     * @param string $key 
     * @return bool 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function allowEdit($data = array(), $key = 'id')
    {
        return Extensions::isAllowed('manage.sw');
    }

    /**
     * @param string $key 
     * @param string $urlVar 
     * @return bool 
     * @throws Exception 
     * @throws UnexpectedValueException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     */
    public function edit($key = null, $urlVar = null)
    {
        $model = $this->getModel();
        $table = $model->getTable();
        if (empty($key)) {
            $key = $table->getKeyName();
        }
        if (empty($urlVar)) {
            $urlVar = $key;
        }
        $id = $this->app->getInput()->getInt($key);
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery();
        $query->select($db->qn('baramundi_product'))
            ->from($db->qn('#__iwf_software'))
            ->where($db->qn('id') . '=:id')
            ->bind(':id', $id, ParameterType::INTEGER);
        $baramundi_product = $db->setQuery($query)->loadObject()->baramundi_product;
        if(!empty($baramundi_product))
        {
            $this->app->setUserState('com_verwaltung.baramundi.product', $baramundi_product); // zurück zum Aufrufer - muss nicht die Liste sein!
        }
        else
        {
            $this->app->setUserState('com_verwaltung.baramundi.product', '');
        }
        return parent ::edit($key, $urlVar);
    }
    
    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function findlicenses() {
        $formToken = Session::getFormToken();
        $input = $this->app->input;
        $data = $input->get('jform', array(), 'array');
        $response = array('result' => 0);
        $dataObject = new stdClass();
        $fieldsFound = 0;
        foreach ($data as $value) {
            if ($value["name"] == "jform[id]") {
                $dataObject->id = intval($value["value"]);
                $fieldsFound++;
            } else if ($value["name"]=="jform[installed_licenses_since_date]") {
                if (!empty($value["value"])) {
                    $dataObject->installed_licenses_since_date = $value["value"];
                }
            } else if ($value["name"]=="jform[which_licenses]") {
                $dataObject->which_licenses = $value["value"];
            } else if ($value["name"] == $formToken) {
                $dataObject->$formToken = $value["value"];
                $fieldsFound++;
                break;
            }
        }
        $tokenValid = false;
        if (isset($dataObject->$formToken)) {
            $tokenValid = $dataObject->$formToken == 1;
        }
        if (!$tokenValid) {
            $response['error'] = self::renderAjaxError(Text::_('COM_VERWALTUNG_PROGRAM_AJAX_TOKEN_FEHLER'), 'error');
        } else {
            $db = Factory::getContainer()->get(DatabaseInterface::class);
            $query = $db->createQuery();
            $query->select
                (
                    [
                        $db->qn('a.produkt'),
                        $db->qn('l.installationsdatum'),
                        $db->qn('l.bybaramundi'),
                        $db->qn('l.version'),

                        $db->qn('d.ipnummer'),
                        $db->qn('z.zimmer'),
                        $query->concatenate([$db->qn('m.nachname'), $db->qn('m.vorname')], ' ') . ' AS ma_name',
                        'CASE WHEN ' . $db->qn('d.wname') . '<> "" THEN ' . $db->qn('d.wname') . ' ELSE ' . $db->qn('d.hostname') . ' END AS host'
                        ]
                )
                ->from($db->qn('#__iwf_lizenzen', 'l'))
                ->leftJoin($db->qn('#__iwf_software', 'a'), $db->qn('l.software_id') . '=' . $db->qn('a.id'))
                ->leftJoin($db->qn('#__iwf_rechner', 'r'), $db->qn('l.rechner_id') . '=' . $db->qn('r.id'))
                ->leftJoin($db->qn('#__iwf_inventar', 'i'), $db->qn('i.id') . '=' . $db->qn('r.inventar_id'))
                ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('m.id') . '=' . $db->qn('i.ma_id'))
                ->leftJoin($db->qn('#__iwf_dns', 'd'), $db->qn('d.id') . '=' . $db->qn('r.dns_id'))
                ->leftJoin($db->qn('#__iwf_zimmer', 'z'), $db->qn('z.id') . '=' . $db->qn('i.standort'))
                ->where($db->qn('a.id') . '=:id')
                ->bind(':id', $dataObject->id, ParameterType::INTEGER);
            if (isset($dataObject->installed_licenses_since_date)) {
                $query->where($db->qn('l.installationsdatum') . '>=' . $query->quote($dataObject->installed_licenses_since_date));
            }
            if ($dataObject->which_licenses) {
                $query->where($db->qn('l.bybaramundi') . '=1');
            }
            $query->order($db->qn('l.installationsdatum') . ' ASC');
            $html = '<h2>Keine Datensätze gefunden!</h2>';
            $result = $db->setQuery($query)->loadObjectList();
            if (count($result) > 0) {
                $html = '<h3>Produkt <u>' . $result[0]->produkt . '</u> ist ' . count($result) . 'x installiert:</h3>';
                $html .= '<table class="table table-striped table-hover">';
                $html .= '<thead>';
                $html .= '<th scope="col" class="w-30 d-none d-md-table-cell">Name</th>';
                $html .= '<th scope="col" class="w-10 d-none d-md-table-cell">Rechner</th>';
                $html .= '<th scope="col" class="w-15 d-none d-md-table-cell">IP-Nummer</th>';
                $html .= '<th scope="col" class="w-15 d-none d-md-table-cell">Standort</th>';
                $html .= '<th scope="col" class="w-5 d-none d-md-table-cell">Installiert am</th>';
                $html .= '<th scope="col" class="w-5 d-none d-md-table-cell">Version</th>';
                $html .= '<th scope="col" class="w-25 d-none d-md-table-cell">via baramundi</th>';
                $html .= '</thead>';
                $html .= '<tbody>';
                foreach ($result as $i => $item) {
                    $html .= '<tr class="row<?php echo $i % 2; ?>">';
                    $html .= '<td class="d-none d-md-table-cell">' . $item->ma_name . '</td>';
                    $html .= '<td class="d-none d-md-table-cell">' . $item->host . '</td>';
                    $html .= '<td class="d-none d-md-table-cell">' . $item->ipnummer . '</td>';
                    $html .= '<td class="d-none d-md-table-cell">' . $item->zimmer . '</td>';
                    $html .= '<td class="d-none d-md-table-cell">' . $item->installationsdatum . '</td>';
                    $html .= '<td class="d-none d-md-table-cell">' . $item->version . '</td>';
                    $html .= '<td class="d-none d-md-table-cell">' . $item->bybaramundi . '</td>';
                    $html .= '</tr>';
                }
                $html .= '</tbody></table>';
            }
            $response['message'] = $html;
        }
        self::sendResponse($response);
    }
    
    /**
     * @param array $data 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    private static function sendResponse($data = array()) {
        $response = new stdClass();
        foreach ($data as $key => $value) {
            $response->$key = $value;
        }
        $response->token = Session::getFormToken();
        echo json_encode($response);
        Factory::getApplication()->close();
    }

    /**
     * @param mixed $error 
     * @param string $type 
     * @return string 
     */
    private static function renderAjaxError($error, $type = 'info') {
        switch (strtolower($type)) {
            case 'info':
                $heading = 'Information';
                break;
            case 'success':
                $heading = 'Nachricht';
                break;
            case 'warning':
            case 'danger':
            case 'error':
                $heading = 'Warnung';
                $type = 'error';
                break;
            default:
                $heading = 'Nachricht';
                break;
        }
        return "$heading: [$type] $error";
    }
}
