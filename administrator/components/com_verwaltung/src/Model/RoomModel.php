<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Model;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\MVC\Model\AdminModel;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Table\Table;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;

/** @package Iwf\Component\Verwaltung\Administrator\Model */
class RoomModel extends AdminModel
{

	/**
	 * @param string $name 
	 * @param string $prefix 
	 * @param array $config 
	 * @return Table 
	 * @throws DatabaseNotFoundException 
	 * @throws UnexpectedValueException 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 */
	public function getTable($name = '', $prefix = '', $config = array())
	{
		$name = 'iwf_zimmer';
		$prefix = 'VerwaltungTable';
		if ($table = $this->_createTable($name, $prefix, $config)) {
			return $table;
		}
	}

	/**
	 * @param array $data 
	 * @param bool $loadData 
	 * @return Form 
	 * @throws Exception 
	 */
	public function getForm($data = array(), $loadData = true)
	{
		$form = $this->loadForm('com_verwaltung.room', 'zimmer', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		return $form;
	}

	/**
	 * @return array 
	 * @throws Exception 
	 * @throws InvalidArgumentException 
	 * @throws RuntimeException 
	 * @throws UnexpectedValueException 
	 * @throws KeyNotFoundException 
	 * @throws QueryTypeAlreadyDefinedException 
	 */
	protected function loadFormData()
	{
		$app = Factory::getApplication();
		$data = $app->getUserState('com_verwaltung.edit.room.data', array());
		if (empty($data)) {
			$data = $this->getItem();
		}
		return $data;
	}
}
