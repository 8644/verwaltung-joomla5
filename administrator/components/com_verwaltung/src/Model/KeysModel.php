<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 * 
 * 
 */

namespace Iwf\Component\Verwaltung\Administrator\Model;

use Exception;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Model */
class KeysModel extends ListModel
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'schluessel',
                'a.schluessel',
                'verfuegbar',
                'bestand',
                'a.bestand',
                'a.bezeichnung',
                'raum_gruppe',
                'a.raum_gruppe',
                'tuer',
                'a.tuer',
            );
        }
        parent::__construct($config);
    }

    /**
     * @param string $ordering 
     * @param string $direction 
     * @return void 
     * @throws Exception 
     */
    protected function populateState($ordering = 'a.bezeichnung', $direction = 'asc')
    {
        $search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);
        parent::populateState($ordering, $direction);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     */
    protected function getListQuery()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select
            (
                [
                    $db->qn('b.schluessel_id'),
                    $db->qn('b.Anzahl'),
                    $db->qn('a.id'),
                    $db->qn('a.bezeichnung'),
                    $db->qn('a.bestand'),
                    $db->qn('a.raum_gruppe'),
                    $db->qn('a.tuer'),
                    $db->qn('a.kommentar'),
                    'CASE WHEN b.anzahl IS NOT NULL THEN (CAST(a.bestand AS INT)-b.Anzahl) ELSE a.bestand END AS verfuegbar'
                ]
            )
            ->from('(select count(schluessel) as Anzahl, schluessel as schluessel_id from #__iwf_schluessel group by schluessel) AS b')
            ->rightJoin($db->qn('#__iwf_schliessanlage', 'a'), $db->qn('a.id') . '=' . $db->qn('b.schluessel_id'));
        $schluesseltyp = $this->getState('filter.schluessel');
        if ($schluesseltyp) {
            $query->where($db->qn('a.schluessel') . '=:schluesseltyp')
                ->bind(':schluesseltyp', $schluesseltyp, ParameterType::INTEGER);
        }
        $verfuegbar = $this->getState('filter.verfuegbar');
        if ($verfuegbar) {
            $query->where('CAST(a.bestand AS INT)-b.Anzahl >=' . $verfuegbar);
        }
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = '%' . trim($search) . '%';
            $where = [
                $db->qn('a.bezeichnung') . " LIKE \"$search\"",
                $db->qn('a.raum_gruppe') . " LIKE \"$search\"",
                $db->qn('a.transponderkennung') . " LIKE \"$search\"",
                $db->qn('a.tuer') . " LIKE \"$search\"",
                $db->qn('a.kommentar') . " LIKE \"$search\""
            ];
            if (is_null($query->where)) {
                $query->where($where, 'OR');
            } else {
                $query->andWhere($where, 'OR');
            }
        }
        $orderCol  = $this->state->get('list.ordering', 'a.bezeichnung');
        $orderDirn = $this->state->get('list.direction', 'ASC');
		if ($orderCol && $orderDirn) {
			$query->order($orderCol . ' ' . $orderDirn);
		}
        return $query;
    }
}
