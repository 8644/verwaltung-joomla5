<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Model;

use Exception;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Model */
class ProjectsModel extends ListModel
{
    
    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'a.projekt',
                'abteilung',
                'a.abteilung',
                'ma_id',
                'a.projektbez', 
                'bezeichnung',
                'projektnummer',
                'a.projektnummer',
                'aktuell',
                'a.aktuell',
                'verantwortlicher',
            );
        }
        parent::__construct($config);
    }

    /**
     * @param string $ordering 
     * @param string $direction 
     * @return void 
     * @throws Exception 
     */
    protected function populateState($ordering = 'a.lfdnr', $direction = 'ASC')
    {
        $search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);
        parent::populateState($ordering, $direction);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     */
    protected function getListQuery()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select(
                [
                    $db->qn('a.id', 'id'),
                    $db->qn('a.projekt', 'projekt'),
                    $db->qn('a.projektbez', 'bezeichnung'),
                    $db->qn('a.projektnummer', 'projektnummer'),
                    $db->qn('a.aktuell', 'aktuell'),
                    $query->concatenate([$db->qn('ab.institut'), $db->qn('ab.abteilung')], '-') . ' AS abteilung',
                    $query->concatenate([$db->qn('m.nachname'), $db->qn('m.vorname')], ' ') . ' AS verantwortlicher'
                ]
            )
            ->from($db->qn('#__iwf_projekte', 'a'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('m.id') . '=' . $db->qn('a.ma_id'))
            ->leftJoin($db->qn('#__iwf_abteilungen', 'ab'), $db->qn('ab.id') . '=' . $db->qn('m.abteilung'));
        $abteilung = $this->getState('filter.abteilung'); 
        if ($abteilung) {
            $query->where($db->qn('ab.id') . '=:abteilung')
                ->bind(':abteilung', $abteilung, ParameterType::STRING);
        }
        $verantwortlicher = $this->getState('filter.ma_id');
        if (is_numeric($verantwortlicher)) {
            $query->where($db->qn('ma_id') . '=:verantwortlicher')
                ->bind(':verantwortlicher', $verantwortlicher, ParameterType::INTEGER);
        }
        $aktuell = $this->getState('filter.aktuell');
        if (is_numeric($aktuell)) {
            $query->where($db->qn('a.aktuell') . '=:aktuell')
                ->bind(':aktuell', $aktuell, ParameterType::INTEGER);
        }
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = '%' . trim($search) . '%';
            $where = [
                $db->qn('a.projekt') . " LIKE \"$search\"",
                $db->qn('a.projektbez') . " LIKE \"$search\"",
            ];
            if (\is_null($query->where)) {
                $query->where($where, 'OR');    
            } else {
                $query->andWhere($where, 'OR');    
            }
        }
        $orderCol  = $this->state->get('list.ordering', 'a.lfdnr');
        $orderDirn = $this->state->get('list.direction', 'ASC');
		if ($orderCol && $orderDirn) {
			$query->order($orderCol . ' ' . $orderDirn);
		}
        return $query;
    }
}
