<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Model;

use Exception;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\Database\ParameterType;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Model */
class AkalistsModel extends ListModel
{
    
    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'kategorie',
                'a.kategorie',
            );
        }
        parent::__construct($config);
    }

    /**
     * @param string $ordering 
     * @param string $direction 
     * @return void 
     * @throws Exception 
     */
    protected function populateState($ordering = 'a.id', $direction = 'ASC')
    {
        $search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);
        parent::populateState($ordering, $direction);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     */
    protected function getListQuery()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select
            (
                [
                    $db->qn('a.id', 'id'),
                    $db->qn('a.inhalt', 'inhalt'),
                    $db->qn('a.value', 'value'),
                    $db->qn('a.kategorie', 'kategorie'),
                    $db->qn('a.reihenfolge', 'reihenfolge'),
                    $db->qn('a.jtext', 'jtext'),
                    $db->qn('a.aktiv', 'aktuell'),
                ]
            )
            ->from($db->qn('#__iwf_aka_listen', 'a'));
        $kategorie = $this->getState('filter.kategorie');
        if (!empty($kategorie)) {
            $query->where($db->qn('a.kategorie') . '=:kategorie')
                ->bind(':kategorie', $kategorie, ParameterType::STRING);
        }
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            if (!empty($search)) {
                $search = '%' . trim($search) . '%';
                $where = [
                    $db->qn('a.inhalt') . " LIKE \"$search\"",
                    $db->qn('a.kategorie') . " LIKE \"$search\""
                ];
                $query->where($where, 'OR');
            }
        }
        $orderCol  = $this->state->get('list.ordering', 'a.reihenfolge');
        $orderDirn = $this->state->get('list.direction', 'ASC');
		if ($orderCol && $orderDirn) {
			$query->order($orderCol . ' ' . $orderDirn);
		}
        return $query;
    }
}
