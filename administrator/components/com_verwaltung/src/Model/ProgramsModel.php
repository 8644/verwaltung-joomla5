<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Model;

use Exception;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Model */
class ProgramsModel extends ListModel
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    function __construct($config = array()) {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'produktgruppe',
                'a.produktgruppe',
                'a.produkt',
                'a.baramundi_version',
                'installed',
                'sprache',
                'aktuell',
                'abdeckung',
                'baramundi',
            );
        }
        parent::__construct($config);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     */
    protected function getListQuery() {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select
            (
                [
                    $db->qn('a.id', 'id'),
                    $db->qn('a.produkt', 'produkt'),
                    $db->qn('a.produktgruppe', 'produktgruppe'),
                    $db->qn('a.version', 'version'),
                    $db->qn('a.interne_version', 'intversion'),
                    $db->qn('a.baramundi_version', 'baramundiversion'),
                    $db->qn('a.aktuell', 'aktuell'),
                    $db->qn('a.abdeckung', 'abdeckung'),
                    $db->qn('a.anmerkungen', 'anmerkungen'),
                    $db->qn('a.kommentarchef', 'kommentarchef'),
                    $db->qn('a.mit_baramundi_abgeglichen', 'baramundi'),
                    $db->qn('l.inhalt', 'sprache'),
                    'CASE WHEN ' . $db->qn('a.basisgekauftfloat') . '>0 THEN "&infin;" WHEN ' . $db->qn('a.basisgekauftfix') . '>99 THEN "&infin;" ELSE ' . $db->qn('a.basisgekauftfix') . ' END AS gesamtlizenzen',
                    '(SELECT count(' . $db->qn('software_id') . ') FROM ' . $db->qn('#__iwf_lizenzen', 'z') . ' WHERE ' . $db->qn('z.software_id') . '=' . $db->qn('a.id') . ') AS installed'
                ]
            )
            ->from($db->qn('#__iwf_software', 'a'))
            ->leftJoin($db->qn('#__iwf_listen', 'l'), $db->qn('l.id') . '=' . $db->qn('a.sprache'));
        $produktgruppe = $this->getState('filter.produktgruppe');
        if (!empty($produktgruppe)) {
            $query->where($db->qn('a.produktgruppe') . '=:produktgruppe')
                ->bind(':produktgruppe', $produktgruppe, ParameterType::STRING);
        }
        $sprache = $this->getState('filter.sprache');
        if (!empty($sprache)) {
            $query->where($db->qn('a.sprache') . ' LIKE :sprache')
                ->bind(':sprache', $sprache, ParameterType::STRING);
        }
        $aktuell = $this->getState('filter.aktuell');
        if (is_numeric($aktuell)) {
            $query->where($db->qn('a.aktuell') . '=:aktuell')
                ->bind(':aktuell', $aktuell, ParameterType::INTEGER);
        }
        $abdeckung = $this->getState('filter.abdeckung');
        if (is_numeric($abdeckung)) {
            $query->where($db->qn('a.abdeckung') . '=:abdeckung')
                ->bind(':abdeckung', $abdeckung, ParameterType::INTEGER);
        }
        $baramundi = $this->getState('filter.baramundi');
        if (is_numeric($baramundi)) {
            $query->where($db->qn('a.mit_baramundi_abgeglichen') . '=:baramundi')
                ->bind(':baramundi', $baramundi, ParameterType::INTEGER);
        }
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = '%' . trim($search) . '%';
            $where = $db->qn('a.produkt') . " LIKE \"$search\"";
            if (is_null($query->where)) {
                $query->where($where, 'OR');
            } else {
                $query->andWhere($where, 'OR');
            }
        }
        $orderCol = $this->state->get('list.ordering', 'a.produktgruppe');
        $orderDirn = $this->state->get('list.direction', 'ASC');
		if ($orderCol && $orderDirn) {
			$query->order($orderCol . ' ' . $orderDirn);
		}
        return $query;
    }

}