<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Model;

use Exception;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Model */
class BaramundiprogramsdiffModel extends ListModel
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'company',
                'a.company',
                'set',
                'a.set',
                'found',
                'a.found',
            );
        }
        parent::__construct($config);
    }

    /**
     * @param string $ordering 
     * @param string $direction 
     * @return void 
     * @throws Exception 
     */
    protected function populateState($ordering = 'a.found', $direction = 'ASC')
    {
        $search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);
        parent::populateState($ordering, $direction);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     */
    protected function getListQuery()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select("DISTINCT a.*")
            ->from($db->qn('#__iwf_baramundi_sw_diff', 'a'));
        $company = $this->getState('filter.company');
        if ($company) {
            $query->where($db->qn('a.company') . '=:company')
                ->bind(':company', $company, ParameterType::STRING);
        }
        $set = $this->getState('filter.set');
        if ($set) {
            $query->where($db->qn('a.set') . '=:set')
                ->bind(':set', $set, ParameterType::STRING);
        }
        $found = $this->getState('filter.found');
        if ($found) {
            $query->where($db->qn('a.found') . '=:found')
                ->bind(':found', $found, ParameterType::STRING);
        }
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = '%' . trim($search) . '%';
            $where = [
                $db->qn('a.company') . " LIKE \"$search\"",
                $db->qn('a.product_name') . " LIKE \"$search\"",
                $db->qn('a.set') . " LIKE \"$search\"",
                $db->qn('a.found') . " LIKE \"$search\"",
            ];
            if (is_null($query->where)) {
                $query->where($where, 'OR');
            } else {
                $query->andWhere($where, 'OR');
            }
        }
        $orderCol  = $this->state->get('list.ordering', 'a.found');
        $orderDirn = $this->state->get('list.direction', 'ASC');
		if ($orderCol && $orderDirn) {
			$query->order($orderCol . ' ' . $orderDirn);
		}
        return $query;
    }
}
