<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 *
 *
 */

namespace Iwf\Component\Verwaltung\Administrator\Model;

use Exception;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Model */
class HostsModel extends ListModel
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'ipnummer',
                'a.id',
                'id',
                'a.hostname',
                'hostname',
                'a.wname',
                'wname',
                'a.ipnummer',
                'ipnummer',
                'a.mac',
                'mac',
            );
        }
        parent::__construct($config);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     */
    protected function getListQuery()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select
            (
                [
                    $db->qn('a.id', 'id'),
                    $db->qn('a.hostname', 'hostname'),
                    $db->qn('a.wname', 'wname'),
                    $db->qn('a.anmerkungen', 'anmerkungen'),
                    $db->qn('a.titel', 'titel'),
                    $db->qn('a.ipnummer', 'ipnummer'),
                    $db->qn('a.mac', 'mac'),
                    $db->qn('a.baramundi_id'),
                ]
            )
            ->from($db->qn('#__iwf_dns', 'a'))
            ->leftJoin($db->qn('#__iwf_abteilungen', 'ab'), $db->qn('ab.id') . '=' . $db->qn('a.abteilung'));
        $iprange = $this->getState('filter.ipnummer');
        if (!empty($iprange)) {
            $iprange = $iprange . '%';
            $query->where($db->qn('a.ipnummer') . ' Like :ipnummer')
                ->bind(':ipnummer', $iprange, ParameterType::STRING);
        }
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = '%' . trim($search) . '%';
            $where = [
                $db->qn('a.hostname') . " LIKE \"$search\"",
                $db->qn('a.wname') . " LIKE \"$search\"",
                $db->qn('a.ipnummer') . " LIKE \"$search\"",
                $db->qn('a.titel') . " LIKE \"$search\"",
                $db->qn('a.anmerkungen') . " LIKE \"$search\"",
                $db->qn('a.mac') . " LIKE \"$search\""
            ];
            if (is_null($query->where)) {
                $query->where($where, 'OR');
            } else {
                $query->andWhere($where, 'OR');
            }
        }
        $orderCol  = $this->state->get('list.ordering', 'a.hostname');
        $orderDirn = $this->state->get('list.direction', 'ASC');
        if ($orderCol == 'a.ipnummer') {
            $orderCol = "INET_ATON($orderCol)";
        }
		if ($orderCol && $orderDirn) {
			$query->order($orderCol . ' ' . $orderDirn);
		}
        return $query;
    }
}
