<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 * 
 * 
 */

namespace Iwf\Component\Verwaltung\Administrator\Model;

use Exception;
use Joomla\Database\ParameterType;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Model */
class MediaModel extends ListModel
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'abteilung',
                'a.abteilung',
                'a.label',
                'label',
                'hersteller',
                'a.hersteller',
                'a.produkt',
                'produkt',
                'sprache',
                'betriebssystem',
                'a.betriebssystem',
                'a.version',
                'version',
                'l.inhalt',
            );
        }
        parent::__construct($config);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     */
    protected function getListQuery()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select
            (
                [
                    $db->qn('a.id', 'id'),
                    $db->qn('a.label', 'label'),
                    $db->qn('a.abteilung', 'abteilung'),
                    $db->qn('a.hersteller', 'hersteller'),
                    $db->qn('a.produkt', 'produkt'),
                    $db->qn('a.version', 'version'),
                    $db->qn('l.inhalt', 'sprache'),
                ]
            )
            ->from($db->qn('#__iwf_medien', 'a'))
            ->leftJoin($db->qn('#__iwf_listen', 'l'), $db->qn('l.id') . '=' . $db->qn('a.sprache'));
        $abteilung = $this->getState('filter.abteilung');
        if ($abteilung) {
            $query->where($db->qn('a.abteilung') . '=:abteilung')
                ->bind(':abteilung', $abteilung, ParameterType::INTEGER);
        }
        $hersteller = $this->getState('filter.hersteller');
        if (!empty($hersteller)) {
            $query->where($db->qn('a.hersteller') . '=:hersteller')
                ->bind(':hersteller', $hersteller, ParameterType::STRING);
        }
        $sprache = $this->getState('filter.sprache');
        if ($sprache) {
            $query->where($db->qn('l.id') . '=:sprache')
                ->bind(':sprache', $sprache, ParameterType::INTEGER);
        }
        $os = $this->getState('filter.betriebssystem');
        if (!empty($os)) {
            $query->where($db->qn('a.betriebssystem') . '=:os')
                ->bind(':os', $os, ParameterType::INTEGER);
        }
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = '%' . trim($search) . '%';
            $where = [
                $db->qn('a.label') . " LIKE \"$search\"",
                $db->qn('a.produkt') . " LIKE \"$search\"",
                $db->qn('a.hersteller') . " LIKE \"$search\"",
                $db->qn('a.betriebssystem') . " LIKE \"$search\"",
            ];
            if (is_null($query->where)) {
                $query->where($where, 'OR');
            } else {
                $query->andWhere($where, 'OR');
            }
        }
        $orderCol  = $this->state->get('list.ordering', 'a.produkt');
        $orderDirn = $this->state->get('list.direction', 'ASC');
		if ($orderCol && $orderDirn) {
			$query->order($orderCol . ' ' . $orderDirn);
		}
        return $query;
    }
}
