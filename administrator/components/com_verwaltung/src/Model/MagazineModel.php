<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Model;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\MVC\Model\AdminModel;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Form;
use Joomla\Utilities\ArrayHelper;
use Joomla\Database\ParameterType;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Table\Table;
use Joomla\Database\Exception\DatabaseNotFoundException;
use UnexpectedValueException;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;

/** @package Iwf\Component\Verwaltung\Administrator\Model */
class MagazineModel extends AdminModel
{
	
	/**
	 * @param string $name 
	 * @param string $prefix 
	 * @param array $options 
	 * @return Table 
	 * @throws DatabaseNotFoundException 
	 * @throws UnexpectedValueException 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 */
	public function getTable($name = '', $prefix = '', $options = array())
	{
		$name = 'iwf_zeitschriften';
		$prefix = 'Table';
		if ($table = $this->_createTable($name, $prefix, $options)) {
			return $table;
		}
		throw new \Exception(Text::sprintf('JLIB_APPLICATION_ERROR_TABLE_NAME_NOT_SUPPORTED', $name), 0);
	}

	/**
	 * @param array $data 
	 * @param bool $loadData 
	 * @return Form 
	 * @throws Exception 
	 */
	public function getForm($data = array(), $loadData = true)
	{
		$form = $this->loadForm('com_verwaltung.magazine', 'zeitschriften', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form)) {
			return false;
		}
		return $form;
	}

	/**
	 * @return array 
	 * @throws Exception 
	 * @throws InvalidArgumentException 
	 * @throws RuntimeException 
	 * @throws UnexpectedValueException 
	 */
	protected function loadFormData()
	{
		$data = Factory::getApplication()->getUserState('com_verwaltung.edit.magazine.data', array());
		if (empty($data)) {
			$data = $this->getItem();
		}
		return $data;
	}

}
