<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Model;

use Exception;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Model */
class LitprojectsModel extends ListModel
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'aktuell',
                'a.aktuell',
                'id',
                'a.id',
                'projekt',
                'a.projekt',
                'a.akademis_projekt_node',
                'node',
                'reihenfolge',
                'a.reihenfolge',
                'aktuell',
                'a.aktuell',
            );
        }
        parent::__construct($config);
    }

    /**
     * @param string $ordering 
     * @param string $direction 
     * @return void 
     * @throws Exception 
     */
    protected function populateState($ordering = 'a.id', $direction = 'ASC')
    {
        $search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);
        parent::populateState($ordering, $direction);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     */
    protected function getListQuery()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select(
                [
                    $db->qn('a.id', 'id'),
                    $db->qn('a.projekt', 'projekt'),
                    $db->qn('a.akademis_projekt_node', 'node'),
                    $db->qn('a.reihenfolge'),
                    $db->qn('a.aktuell', 'aktuell'),
                ]
            )
            ->from($db->qn('#__iwf_lit_projekte', 'a'));
        $aktuell = $this->getState('filter.aktuell');
        if (is_numeric($aktuell)) {
            $query->where($db->qn('a.aktuell') . '=:aktuell')
                ->bind(':aktuell', $aktuell, ParameterType::INTEGER);
        }
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = '%' . trim($search) . '%';
            $where = [
                $db->qn('a.projekt') . " LIKE \"$search\"",
            ];
            if (is_null($query->where)) {
                $query->where($where, 'OR');
            } else {
                $query->andWhere($where, 'OR');
            }
        }
        $query->order([$db->qn('a.reihenfolge'), $db->qn('a.projekt')]);
        return $query;
    }

}
