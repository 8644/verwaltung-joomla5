<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Model;

use Exception;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use UnexpectedValueException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Model */
class RoomsModel extends ListModel
{

    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'stockwerk',
                'schlosstyp',
                'a.schlosstyp',
                'zimmer',
                'a.zimmer',
                'raumbezeichnung',
                'a.raumbezeichnung',
                'abteilung',
                'a.abteilung',
            );
        };
        parent::__construct($config);
    }

    /**
     * @param string $ordering 
     * @param string $direction 
     * @return void 
     * @throws Exception 
     */
    protected function populateState($ordering = 'a.id', $direction = 'asc')
    {
        $search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);
        parent::populateState($ordering, $direction);
    }
    
    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     */
    protected function getListQuery()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select
            (
                [
                    $db->qn('a.id', 'id'),
                    $db->qn('a.zimmer', 'zimmer'),
                    $db->qn('a.raumbezeichnung', 'raumbezeichnung'),
                    $db->qn('a.abteilung', 'abteilung'),
                    $query->concatenate([$db->qn('ab.institut'), $db->qn('ab.abteilung')], '-') . ' AS abteilung',
                    $db->qn('a.istbuero'),
                ]
            )
            ->from($db->qn('#__iwf_zimmer', 'a'))
            ->leftJoin($db->qn('#__iwf_abteilungen', 'ab'), $db->qn('a.abteilung') . '=' . $db->qn('ab.id'));
        $abteilung = $this->getState('filter.abteilung'); 
        if ($abteilung) {
            $query->where($db->qn('a.abteilung') . '=:abteilung')
                ->bind(':abteilung', $abteilung, ParameterType::STRING);
        }
        $schlosstyp = $this->getState('filter.schlosstyp');
        if (!empty($schlosstyp)) {
            $query->where($db->qn('a.schlosstyp') . '=:schlosstyp')
                ->bind(':schlosstyp', $schlosstyp, ParameterType::STRING);
        }
        $stockwerk = $this->getState('filter.stockwerk');
        if (is_numeric($stockwerk)) {
            $query->where($db->qn('a.stockwerk') . '=:stockwerk')
                ->bind(':stockwerk', $stockwerk, ParameterType::INTEGER);
        }
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = '%' . trim($search) . '%';
            $where = [
                $db->qn('a.zimmer') . " LIKE \"$search\"",
                $db->qn('a.raumbezeichnung') . " LIKE \"$search\"",
                $db->qn('a.abteilung') . " LIKE \"$search\"",
            ];
            if (is_null($query->where)) {
                $query->where($where, 'OR');
            } else {
                $query->andWhere($where, 'OR');
            }
        }
        $orderCol = $this->state->get('list.ordering', 'a.reihenfolge');
        $orderDirn = $this->state->get('list.direction', 'ASC');
		if ($orderCol && $orderDirn) {
			$query->order($orderCol . ' ' . $orderDirn);
		}
        return $query;
    }
}
