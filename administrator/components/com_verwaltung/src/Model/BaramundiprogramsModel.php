<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Model;

use Exception;
use Joomla\CMS\MVC\Model\ListModel;
use Joomla\Database\DatabaseQuery;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;
use Joomla\DI\Exception\KeyNotFoundException;
use UnexpectedValueException;

\defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Model */
class BaramundiprogramsModel extends ListModel
{
    
    /**
     * @param array $config 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = array())
    {
        if (empty($config['filter_fields'])) {
            $config['filter_fields'] = array(
                'product_name',
                'a.product_name',
                'iwf_software',
                'a.iwf_software',
                'a.company',
                'company',
            );
        }
        parent::__construct($config);
    }

    /**
     * @param string $ordering 
     * @param string $direction 
     * @return void 
     * @throws Exception 
     */
    protected function populateState($ordering = 'a.product_name', $direction = 'ASC')
    {
        $search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
        $this->setState('filter.search', $search);
        parent::populateState($ordering, $direction);
    }

    /**
     * @return DatabaseQuery|string 
     * @throws DatabaseNotFoundException 
     */
    protected function getListQuery()
    {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select('DISTINCT a.*,' . $db->qn('b.id') . ',' . $db->qn('b.produkt'))
            ->from($db->qn('#__iwf_baramundi_sw', 'a'))
            ->leftJoin($db->qn('#__iwf_software', 'b'), $db->qn('a.iwf_software') . '=' . $db->qn('b.id'));
        $baramundicompany = $this->getState('filter.company');
        if ($baramundicompany) {
            $query->where($db->qn('a.company') . '=:baramundicompany')
                ->bind(':baramundicompany', $baramundicompany, ParameterType::STRING);
        }
        $search = $this->getState('filter.search');
        if (!empty($search)) {
            $search = '%' . trim($search) . '%';
            $where = [
                $db->qn('a.product_name') . " LIKE \"$search\"",
                $db->qn('a.company') . " LIKE \"$search\"",
            ];
            if (is_null($query->where)) {
                $query->where($where, 'OR');
            } else {
                $query->andWhere($where, 'OR');
            }
            $query->where($db->qn('a.product_name') . ' LIKE :inh')
                ->bind(':inh', $search, ParameterType::STRING);
        }
        $orderCol = $this->state->get('list.ordering', 'a.product_name');
        $orderDirn = $this->state->get('list.direction', 'ASC');
		if ($orderCol && $orderDirn) {
			$query->order($orderCol . ' ' . $orderDirn);
		}
        return $query;
    }

}
