<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Model;

defined('_JEXEC') or die;

use Exception;
use Joomla\CMS\Component\ComponentHelper;
use Joomla\CMS\MVC\Model\AdminModel;
use Joomla\CMS\Factory;
use Joomla\CMS\Form\Form;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Table\Table;
use Joomla\Database\Exception\DatabaseNotFoundException;
use Joomla\Database\ParameterType;
use UnexpectedValueException;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;

/** @package Iwf\Component\Verwaltung\Administrator\Model */
class LicensestateModel extends AdminModel
{

    private $compare_mode = 0;
    var $itemid = 0;
    var $title = "";
    var $produktgruppe = "";
    var $software_ids = [];
    var $gesamtinstallationen = 0;  // Gesamtanzahl aller Installationen (Fix, Float, Laptops)
    var $gesamtaktuell = 0;  // relevante Gesamtinstallationen an Fixlizenzen
    var $gesamtalt = 0;  // relevante Gesamtinstallationen an Floatlizenzen
    var $relevanteinstallationen = 0;
    var $fixlizenzenaktuell = 0;
    var $fixlizenzenaktuell_verfuegbar = 0;
    var $fixlizenzenalt = 0;
    var $fixlizenzenalt_verfuegbar = 0;
    var $floatlizenzenaktuell = 0;
    var $floatlizenzenaktuell_verfuegbar = 0;
    var $floatlizenzenalt = 0;
    var $floatlizenzenalt_verfuegbar = 0;
    var $gesamtaktuell_nach_produkt = [];
    var $gesamtalt_nach_produkt = [];
    var $gesamtaktuell_nach_produkt_nicht_relevant = [];
    var $gesamtalt_nach_produkt_nicht_relevant = [];

    /**
     * @param array $config 
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws UnexpectedValueException 
     */
    public function __construct($config = []) {
        parent::__construct($config);
        $params = ComponentHelper::getParams('com_verwaltung');
        $this->compare_mode = $params->get('license_reference_host_compare_mode', 0);

        // Die Id der Software muss vorhanden sein, weil wir von dort kommen
        $this->itemid = Factory::getApplication()->getUserState('com_verwaltung.licensestate.program');
        $db = $this->getDatabase();
        $query = $db->createQuery();
        if ($this->itemid && Factory::getApplication()->input->get('layout', 'default') == 'default') {
            // Bilde Gruppe der Ids der Softwaregruppe
            $query->select(
                    [
                        $db->qn('s.produktgruppe'),
                        'CONCAT(' . $db->qn('s.produktgruppe') . '," (",' . $db->qn('a.institut') . ',"-",' . $db->qn('a.abteilung') . ',")") AS titel'
                    ]
                )
                ->from($db->qn('#__iwf_software', 's'))
                ->leftJoin($db->qn('#__iwf_abteilungen', 'a'), $db->qn('s.abteilung') . '=' . $db->qn('a.id'))
                ->where($db->qn('s.id') . '=:id')
                ->bind(':id', $this->itemid, ParameterType::INTEGER);
            if ($result = $db->setQuery($query)->loadObject()) {
                $this->titel = $result->titel;
                $this->produktgruppe = $result->produktgruppe;
            }
            $query->clear()
                ->select($db->qn('id'))
                ->from($db->qn('#__iwf_software'))
                ->where($db->qn('produktgruppe') . '=:pgruppe')
                ->bind(':pgruppe', $this->produktgruppe, ParameterType::STRING);
            $items = $db->setQuery($query)->loadObjectList();
            $this->software_ids = [];
            foreach ($items as $item) {
                $this->software_ids[] = $item->id;
            }
        }
    }

	/**
	 * @param string $name 
	 * @param string $prefix 
	 * @param array $options 
	 * @return Table 
	 * @throws DatabaseNotFoundException 
	 * @throws UnexpectedValueException 
	 * @throws Exception 
	 * @throws KeyNotFoundException 
	 */
	public function getTable($name = '', $prefix = '', $options = array())
	{
		$name = 'iwf_lizenzen';
		$prefix = 'Table';
		if ($table = $this->_createTable($name, $prefix, $options)) {
			return $table;
		}
		throw new \Exception(Text::sprintf('JLIB_APPLICATION_ERROR_TABLE_NAME_NOT_SUPPORTED', $name), 0);
	}

    /**
     * @param array $data 
     * @param bool $loadData 
     * @return Form 
     */
    public function getForm($data = array(), $loadData = true) {
        return null;
    }

    /**
     * Ermittelt alle installierten Lizenzen zur gewählten Softwaregruppe
     * sowie die Summen der relevanten und nichtrelevanten Installationen.
     * Muss als erstes in der View aufgerufen werden!
     * @param array $ids 
     * @return mixed 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     */
    public function getLicenses($ids = []) {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        $query->select(
                [
                    $db->qn('d.id'),
                    $db->qn('d.hostname'),
                    $query->concatenate([$db->qn('m.nachname'), $db->qn('m.vorname')], ' ') . ' AS name',
                    $db->qn('z.zimmer'),
                    $db->qn('d.ipnummer'),
                    $db->qn('l.inhalt', 'rechnertyp'),
                    $db->qn('r.referenzhost'),
                    $db->qn('s.id', 'sw_id'),
                    $db->qn('s.interne_version'),
                    $db->qn('s.produkt'),
                    $db->qn('s.version'),
                    $db->qn('s.aktuell'),
                    '1 AS relevant',
                    'CASE WHEN (' . $db->qn('s.basisgekauftfloat') . '>0) OR (' . $db->qn('s.updatesgekauftfloat') . '>0) THEN 1 ELSE 0 END AS isfloat'
                ]
            )
            ->from($db->qn('#__iwf_lizenzen', 'a'))
            ->leftJoin($db->qn('#__iwf_software', 's'), $db->qn('a.software_id') . '=' . $db->qn('s.id'))
            ->leftJoin($db->qn('#__iwf_rechner', 'r'), $db->qn('a.rechner_id') . '=' . $db->qn('r.id'))
            ->leftJoin($db->qn('#__iwf_dns', 'd'), $db->qn('r.dns_id') . '=' . $db->qn('d.id'))
            ->leftJoin($db->qn('#__iwf_inventar', 'i'), $db->qn('r.inventar_id') . '=' . $db->qn('i.id'))
            ->leftJoin($db->qn('#__iwf_listen', 'l'), $db->qn('i.inventartyp') . '=' . $db->qn('l.id'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('i.ma_id') . '=' . $db->qn('m.id'))
            ->leftJoin($db->qn('#__iwf_zimmer', 'z'), $db->qn('i.standort') . '=' . $db->qn('z.id'));
        if ($ids) {
            $query->whereIn($db->qn('a.software_id'), $ids);
        } else {
            $query->whereIn($db->qn('a.software_id'), $this->software_ids);
        }
        $query->order($db->qn('s.interne_version'));
        $items = $db->setQuery($query)->loadObjectList();
        if ($this->gesamtinstallationen = count($items)) {
            $this->relevanteinstallationen = $this->gesamtinstallationen;
            $this->gesamtaktuell = 0;
            $this->floatlizenzenaktuell = 0;
            $this->fixlizenzenaktuell = 0;
            $this->fixlizenzenaktuell_verfuegbar = 0;
            $this->floatlizenzenaktuell_verfuegbar = 0;
            $this->floatlizenzenalt = 0;
            $this->floatlizenzenalt_verfuegbar = 0;
            $this->fixlizenzenalt = 0;
            $this->fixlizenzenalt_verfuegbar = 0;
            foreach ($items as $item) {
                if (!isset($this->gesamtaktuell_nach_produkt[$item->produkt])) {
                    $this->gesamtaktuell_nach_produkt[$item->produkt] = 0;
                }
                if (!isset($this->gesamtalt_nach_produkt[$item->produkt])) {
                    $this->gesamtalt_nach_produkt[$item->produkt] = 0;
                }
                if (!isset($this->gesamtalt_nach_produkt_nicht_relevant[$item->produkt])) {
                    $this->gesamtalt_nach_produkt_nicht_relevant[$item->produkt] = 0;
                }
                // Lizenz am Zweitrechner wird nur dann nicht gezählt, wenn es sich um das gleiche Produkt handelt!!
                $doCount = true;
                if ($item->referenzhost) {  // es gibt einen Hauptrechner
                    if ($this->isNotRelevant($item, $items)) {
                        $doCount = false;
                        $items[$i]->relevant = 0;
                        $this->relevanteinstallationen--;
                        $item->aktuell ? $this->gesamtaktuell_nach_produkt_nicht_relevant[$item->produkt]++ : $this->gesamtalt_nach_produkt_nicht_relevant[$item->produkt]++;
                    }
                }
                if ($doCount) {
                    if ($item->aktuell) {
                        $this->gesamtaktuell++;
                        $this->gesamtaktuell_nach_produkt[$item->produkt]++;
                        if ($item->isfloat) {
                            $this->floatlizenzenaktuell++;
                            $this->floatlizenzenaktuell_verfuegbar--;
                        } else {
                            $this->fixlizenzenaktuell++;
                            $this->fixlizenzenaktuell_verfuegbar--;
                        }
                    } else {
                        $this->gesamtalt++;
                        $this->gesamtalt_nach_produkt[$item->produkt]++;
                        if ($item->isfloat) {
                            $this->floatlizenzenalt++;
                            $this->floatlizenzenalt_verfuegbar--;
                        } else {
                            $this->fixlizenzenalt++;
                            $this->fixlizenzenalt_verfuegbar--;
                        }
                    }
                }
            }
        }
        return $items;
    }

    /**
     * Stellt fest, ob eine Lizenz relevant ist.
     * Das ist dann der Fall, wenn es keinen Hauptrechner gibt oder wenn am Hauptrechner diese Lizenz nicht installiert ist
     * @param mixed $nebenlizenz 
     * @param mixed $licenses 
     * @return bool 
     */
    private function isNotRelevant($nebenlizenz, $licenses) {
        $result = false;
        foreach ($licenses as $license) {
            // $license->id = id des Rechners in fzg_dns
            // $nebenlizenz->referenzhost = id des Hauptrechners in fzg_dns
            if ($license->id == $nebenlizenz->referenzhost) {
                switch ($this->compare_mode) {
                    case 0:
                        if ($license->sw_id == $nebenlizenz->sw_id) {
                            $result = true;
                        }
                        break;
                    case 1:
                        if ($license->interne_version == $nebenlizenz->interne_version) {
                            $result = true;
                        }
                        break;
                }
                if ($result) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Ermittelt eine Übersicht für die Zahl der installierten Lizenzen der Softwaregruppe,
     * zu der die gewählte Software gehört
     * @return mixed 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     */
    public function getGroupLicenses() {
        $db = $this->getDatabase();
        $query = $db->createQuery();
        if (!$this->produktgruppe) {
            $query->select($db->qn('produktgrupper'))
                ->from($db->qn('#__iwf_software'))
                ->where($db->qn('id') . '=:id')
                ->bind(':id', $this->itemid, ParameterType::INTEGER);
            if ($item = $db->setQuery($query)->loadObject()) {
                $this->produktgruppe = $item->produktgruppe;
            } else {
                return null;
            }
        }
        $query->clear()
            ->select(
                [
                    $db->qn('a.software_id'),
                    $db->qn('s.produkt'),
                    $db->qn('s.anmerkungen'),
                    $db->qn('s.abdeckung'),
                    'COUNT(' . $db->qn('s.produkt') . ') AS installationen',
                    'COUNT(' . $db->qn('s.produkt') . ') AS relevant',
                    $db->qn('s.aktuell'),
                    'SUM(' . $db->qn('s.basispreis') . ') AS basispreis',
                    'SUM(' . $db->qn('s.updatepreis') . ') AS updatepreis',
                    $db->qn('s.produktgruppe'),
                    $db->qn('s.wartungspreisfix'),
                    $db->qn('s.wartungspreisfloat'),
                    $db->qn('s.basisgekauftfix'),
                    $db->qn('s.basisgekauftfloat'),
                    $db->qn('s.basisupgedatetfix'),
                    $db->qn('s.basisupgedatetfloat'),
                    $db->qn('s.updatesgekauftfix'),
                    $db->qn('s.updatesgekauftfloat'),
                    'CASE WHEN (' . $db->qn('s.basisgekauftfloat') . '>0) OR (' . $db->qn('s.updatesgekauftfloat') . '>0) THEN 1 ELSE 0 END AS hasfloat'
                ]
            )
            ->from($db->qn('#__iwf_lizenzen', 'a'))
            ->leftJoin($db->qn('#__iwf_software', 's'), $db->qn('s.id') . '=' . $db->qn('a.software_id'))
            ->leftJoin($db->qn('#__iwf_rechner', 'r'), $db->qn('a.rechner_id') . '=' . $db->qn('r.id'))
            ->leftJoin($db->qn('#__iwf_inventar', 'i'), $db->qn('r.inventar_id') . '=' . $db->qn('r.id'))
            ->leftJoin($db->qn('#__iwf_dns', 'd'), $db->qn('r.dns_id') . '=' . $db->qn('d.id'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('i.ma_id') . '=' . $db->qn('m.id'))
            ->where($db->qn('s.produktgruppe') . '=:pgruppe')
            ->bind(':pgruppe', $this->produktgruppe, ParameterType::STRING)
            ->group($db->qn('a.software_id'))
            ->order($db->qn('s.interne_version'));
        $items = $db->setQuery($query)->loadObjectList();
        // jetzt zu jedem Produkt die relevanten Installationen (Referenzhost) ermitteln
        foreach ($items as $item) {
            $lizenzen_verfuegbar = $item->basisgekauftfix + $item->updatesgekauftfix;
            $item->aktuell ? $this->fixlizenzenaktuell_verfuegbar += $lizenzen_verfuegbar : $this->fixlizenzenalt_verfuegbar += $lizenzen_verfuegbar;
            if ($item->hasfloat) {
                $item->aktuell ? $this->floatlizenzenaktuell_verfuegbar = '&infin;' : $this->floatlizenzenalt_verfuegbar = '&infin;';
            }
            $query->clear()
                ->select($db->qn('r.referenzhost'))
                ->from($db->qn('#__iwf_lizenzen', 'l'))
                ->leftJoin($db->qn('#__iwf_software', 's'), $db->qn('s.id') . '=' . $db->qn('l.software_id'))
                ->leftJoin($db->qn('#__iwf_rechner', 'r'), $db->qn('l.rechner_id') . '=' . $db->qn('r.id'))
                ->leftJoin($db->qn('#__iwf_dns', 'd'), $db->qn('r.dns_id') . '=' . $db->qn('d.id'))
                ->leftJoin($db->qn('#__iwf_inventar', 'i'), $db->qn('r.inventar_id') . '=' . $db->qn('i.id'))
                ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('i.ma_id') . '=' . $db->qn('m.id'))
                ->where($db->qn('l.software_id') . '=:swid')
                ->where($db->qn('r.referenzhost') . '<>0')
                ->bind(':swid', $item->software_id, ParameterType::INTEGER);
            $count = count($db->setQuery($query)->loadObjectlist());
            $item->relevant = $item->installationen - $count;
        }
        return $items;
    }

    /**
     * Ermittelt zusätzlich Infos für verfügbare Lizenzen usw.
     * @return array 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     */
    public function getLicenseStatistic() {
        $versions = [];
        $db = $this->getDatabase();
        // vorhandene Lizenzen früherer Versionen
        $query = $db->createQuery()
            ->select(
                [
                    'SUM(' . $db->qn('basisgekauftfix') . ') + SUM(' . $db->qn('basisupgedatetfix') . ') + SUM(' . $db->qn('updatesgekauftfix') . ') AS fix',
                    'SUM(' . $db->qn('basisgekauftfloat') . ') + SUM(' . $db->qn('basisupgedatetfloat') . ') + SUM(' . $db->qn('updatesgekauftfloat') . ') AS flt',
                ]
            )
            ->from($db->qn('#__iwf_software'))
            ->where($db->qn('produktgruppe') . '=:pgruppe')
            ->where($db->qn('aktuell') . '=0')
            ->bind(':pgruppe', $this->produktgruppe, ParameterType::STRING);
        if ($items = $db->setQuery($query)->loadObjectList()) {
            $versions['alt']['fix'] = $items[0]->fix ? $items[0]->fix : 0;
            $versions['alt']['flt'] = $items[0]->flt ? $items[0]->flt : 0;
        }
        $versions['alt']['installiert'] = $this->gesamtalt;
        $versions['aktuell']['installiert'] = $this->gesamtaktuell;
        // vorhandene Lizenzen der aktuellen Version
        $query->clear()
            ->select(
                [
                    'SUM(' . $db->qn('basisgekauftfix') . ') + SUM(' . $db->qn('basisupgedatetfix') . ') + SUM(' . $db->qn('updatesgekauftfix') . ') AS fix',
                    'SUM(' . $db->qn('basisgekauftfloat') . ') + SUM(' . $db->qn('basisupgedatetfloat') . ') + SUM(' . $db->qn('updatesgekauftfloat') . ') AS flt',
                ]
            )
            ->from($db->qn('#__iwf_software'))
            ->where($db->qn('produktgruppe') . '=:pgruppe')
            ->where($db->qn('aktuell') . '=1')
            ->bind(':pgruppe', $this->produktgruppe, ParameterType::STRING);
        if ($items = $db->setQuery($query)->loadObjectList()) {
            $versions['aktuell']['fix'] = $items[0]->fix ? $items[0]->fix : 0;
            $versions['aktuell']['flt'] = $items[0]->flt ? $items[0]->flt : 0;
        }
        // Gesamtzahl der Installationen	
        /* $versions['installationen']['total'] = $this->gesamtinstallationen;
          $versions['installationen']['laptop'] = $this->gesamtinstallationen - $this->relevanteinstallationen; */
          return $versions;
    }

    /**
     * @param mixed $produktgruppe 
     * @return mixed|mixed 
     * @throws DatabaseNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     */
    private function getProductGroupInfo($produktgruppe) {
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('a.software_id'),
                    $db->qn('s.produkt'),
                    $db->qn('s.anmerkungen'),
                    'COUNT(' . $db->qn('s.produkt') . ') AS installationen',
                    'COUNT(' . $db->qn('s.produkt') . ') AS relevant',
                    $db->qn('s.aktuell'),
                    $db->qn('s.basisgekauftfix'),
                    $db->qn('s.basisgekauftfloat'),
                    $db->qn('s.version'),
                    $db->qn('s.interne_version'),
                    $db->qn('s.lizenzpflichtig'),
                    'CASE WHEN (' . $db->qn('s.basisgekauftfloat') . '>0) OR (' . $db->qn('s.updatesgekauftfloat') . '>0) THEN 1 ELSE 0 END AS isfloat'
                ]
            )
            ->from($db->qn('#__iwf_lizenzen', 'a'))
            ->leftJoin($db->qn('#__iwf_software', 's'), $db->qn('s.id') . '=' . $db->qn('a.software_id'))
            ->leftJoin($db->qn('#__iwf_rechner', 'r'), $db->qn('a.rechner_id') . '=' . $db->qn('r.id'))
            ->leftJoin($db->qn('#__iwf_inventar', 'i'), $db->qn('r.inventar_id') . '=' . $db->qn('i.id'))
            ->leftJoin($db->qn('#__iwf_dns', 'd'), $db->qn('r.dns_id') . '=' . $db->qn('d.id'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('i.ma_id') . '=' . $db->qn('m.id'))
            ->leftJoin($db->qn('#__iwf_abteilungen', 'ab'), $db->qn('ab.id') . '=' . $db->qn('s.abteilung'))
            ->where($db->qn('s.produktgruppe') . '=:pgruppe')
            ->bind(':pgruppe', $produktgruppe, ParameterType::STRING);
        $programsModel = Factory::getApplication()->bootComponent('com_verwaltung')->getMVCFactory()->createModel('programs', 'Administrator', []);
        if ($programsModel) {
            $produktgruppefilter = $programsModel->getState('filter.produktgruppe');
            if (!empty($produktgruppefilter)) {
                $query->where($db->qn('s.produktgruppe') . '=:pgfilter')
                    ->bind(':pgfilter', $produktgruppefilter, ParameterType::STRING);
            }
            $sprache = $programsModel->getState('filter.sprache');
            if (!empty($sprache)) {
                $query->where($db->qn('s.sprache') . ' LIKE :sprache')
                    ->bind(':sprache', $sprache, ParameterType::STRING);
            }
            $aktuell = $programsModel->getState('filter.aktuell');
            if (is_numeric($aktuell)) {
                $query->where($db->qn('s.aktuell') . '=:aktuell')
                    ->bind(':aktuell', $aktuell, ParameterType::INTEGER);
            }
            $obsolet = $programsModel->getState('filter.obsolet');
            if (is_numeric($obsolet)) {
                $query->where($db->qn('s.obsolet') .'=:obsolte')
                    ->bind(':obsolet', $obsolet, ParameterType::INTEGER);
            }
            $abdeckung = $this->getState('filter.abdeckung');
            if (is_numeric($abdeckung)) {
                $query->where($db->qn('s.abdeckung') . '=:abdeckung')
                    ->bind(':abdeckung', $abdeckung, ParameterType::INTEGER);
            }
            $beliebig = $programsModel->getState('filter.beliebig');
            if (is_numeric($beliebig)) {
                $query->where($db->qn('s.beliebig') . '=:beliebig')
                    ->bind(':beliebig', $beliebig, ParameterType::INTEGER);
            }
            $search = $programsModel->getState('filter.search');
            if (!empty($search)) {
                $search = '%' . trim($search) . '%';
                $where = [
                    $db->qn('s.produkt') . " LIKE \"$search\"",
                    $db->qn('s.produktgruppe') . " LIKE \"$search\"",
                    $db->qn('s.hersteller') . " LIKE \"$search\"",
                    $db->qn('s.kommentarchef') . " LIKE \"$search\"",
                    $db->qn('s.anmerkungen') . " LIKE \"$search\"",
                ];
                if (is_null($query->where)) {
                    $query->where($where, 'OR');
                } else {
                    $query->andWhere($where, 'OR');
                }
            }
            $query->group($db->qn('a.software_id'))
                ->order($db->qn('s.interne_version'), 'DESC');
            if ($items = $db->setQuery($query)->loadObjectList()) {
                $query->clear()
                    ->select(
                        [
                            '-1 AS software_id',
                            $db->qn('s.produkt'),
                            $db->qn('s.anmerkungen'),
                            '0 AS installationen',
                            '0 AS relevant',
                            $db->qn('s.aktuell'),
                            $db->qn('s.basisgekauftfix'),
                            $db->qn('s.basisgekauftfloat'),
                            $db->qn('s.version'),
                            $db->qn('s.interne_version'),
                            $db->qn('s.lizenzpflichtig'),
                            'CASE WHEN (' . $db->qn('s.basisgekauftfloat') . '>0) OR (' . $db->qn('s.updatesgekauftfloat') . '>0) THEN 1 ELSE 0 END AS isfloat',
                        ]
                    )
                    ->from($db->qn('#__iwf_software', 's') . ' JOIN ' . $db->qn('#__iwf_lizenzen', 'a'));
                    //$query->join($db->qn('#_iwf_lizenzen', 'a'))
                    $query->where($db->qn('s.produktgruppe') . '=:pgruppe')
                    ->bind(':pgruppe', $produktgruppe, ParameterType::STRING)
                    ->where('NOT EXISTS (SELECT ' . $db->qn('l.id') . ' FROM ' . $db->qn('#__iwf_lizenzen', 'l') . ' WHERE ' . $db->qn('l.software_id') . '=' . $db->qn('s.id') . ')')
                    ->group($db->qn('s.produkt'))
                    ->order($db->qn('s.interne_version'), 'DESC');
                if ($items2 = $db->setQuery($query)->loadObjectList()) {
                    $items = array_merge($items, $items2);
                    $sortArray = [];
                    foreach ($items as $item) {
                        foreach ($item as $key => $value) {
                            if (!isset($sortArray[$key])) {
                                $sortArray[$key] = [];
                            }
                            $sortArray[$key][] = $value;
                        }
                    }
                    $orderby = 'interne_version';
                    array_multisort($sortArray[$orderby], SORT_ASC, $items);
                }
                // jetzt zu jeder Produktgruppe verschiedene Summen und die relevanten Installationen (Referenzhost) ermitteln
                $lizenz_version_array = []; //enthält die nach interner Version aufsteigend sortierten Anzahlen verfügbarer Lizenzen der jeweiligen internen Version
                foreach($items as $item) {
                    $lizenzen_verfuegbar = $item->basisgekauftfix;
                    $item->floatlizenzen_verfuegbar = '&infin;';
                    $item->lizenzen_verfuegbar = $lizenzen_verfuegbar;
                    $query->clear()
                        ->select($db->qn('r.referenzhost'))
                        ->from($db->qn('#__iwf_lizenzen', 'l'))
                        ->leftJoin($db->qn('#__iwf_software', 's'), $db->qn('s.id') . '=' . $db->qn('l.software_id'))
                        ->leftJoin($db->qn('#__iwf_rechner', 'r'), $db->qn('l.rechner_id') . '=' . $db->qn('r.id'))
                        ->leftJoin($db->qn('#__iwf_dns', 'd'), $db->qn('r.dns_id') . '=' . $db->qn('d.id'))
                        ->leftJoin($db->qn('#__iwf_inventar', 'i'), $db->qn('r.inventar_id') . '=' . $db->qn('i.id'))
                        ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('i.ma_id') . '=' . $db->qn('m.id'))
                        ->where($db->qn('l.software_id') . '=:id')
                        ->bind(':id', $item->software_id, ParameterType::INTEGER)
                        ->where($db->qn('r.referenzhost') . '<>0');
                    $count = count($db->setQuery($query)->loadObjectList());
                    if ($count > 0) {
                        $item->relevant = $item->installationen - $count;
                    }
                    $item->verfuegbar = $lizenzen_verfuegbar - $item->relevant;
                    $item->abgedeckt = $item->verfuegbar >= 0 || $item->isfloat;
                    $lizenz_version_array[$item->interne_version] = $item->verfuegbar;
                }
                ksort($lizenz_version_array); // nach interner Version aufsteigend sortieren
                // jetzt nochmal die Einzelprodukte durchgehen und eventuell nicht abgedeckte Lizenzen durch die noch verfügbaren vorhandenen Lizenzen höherer Versionen abdecken
                foreach ($items as $i => &$item) {
                    if ($item->verfuegbar < 0) {
                        $item->abgedeckt = $this->getAvailableLicensesFromHigherVersions($item->interne_version, $lizenz_version_array);
                    }
                }
            }
        }
        return $items;
    }

    /**
     * @param mixed $start_version 
     * @param mixed &$version_array 
     * @return bool 
     */
    private function getAvailableLicensesFromHigherVersions($start_version, &$version_array) {
        $missing = $version_array[$start_version];
        foreach ($version_array as $version => &$available) {
            if ($version > $start_version && $available > 0) {
                $missing += $available;
                if ($missing > 0) {
                    $version_array[$start_version] = 0;
                    $available = $missing;
                    break;
                } else {
                    $available = 0;
                }
            }
        }
        return $missing >= 0;
    }

    /**
     * @return mixed 
     * @throws DatabaseNotFoundException 
     * @throws RuntimeException 
     * @throws Exception 
     */
    public function getLicenseOverview() {
        // zuerst alle Produktgruppen holen
        $db = $this->getDatabase();
        $query = $db->createQuery()
            ->select(
                [
                    $db->qn('produkt'),
                    $db->qn('produktgruppe', 'name'),
                    '0 AS info'
                ]
            )
            ->from($db->qn('#__iwf_software'))
            ->group($db->qn('produktgruppe'))
            ->order($db->qn('produktgruppe'));
        $produktgruppen = $db->setQuery($query)->loadObjectList();
        foreach ($produktgruppen as $produktgruppe) {
            $items = $this->getProductGroupInfo($produktgruppe->name);
            if ($items) {
                $produktgruppe->info = $items;
            }
            continue;
        }
        return $produktgruppen;
    }

}