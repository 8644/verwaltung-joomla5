<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Extension;

defined('JPATH_PLATFORM') or die;

use Exception;
use Joomla\CMS\Component\Router\RouterServiceInterface;
use Joomla\CMS\Component\Router\RouterServiceTrait;
use Joomla\CMS\Extension\BootableExtensionInterface;
use Joomla\CMS\Extension\MVCComponent;
use Joomla\CMS\HTML\HTMLRegistryAwareTrait;
use Psr\Container\ContainerInterface;
use Iwf\Component\Verwaltung\Administrator\Helper\VerwaltungHelper;
use Iwf\Verwaltung\ActiveDirectory;
use Iwf\Verwaltung\Extensions;
use Joomla\CMS\Factory;
use Joomla\DI\Exception\KeyNotFoundException;

/** @package Iwf\Component\Verwaltung\Administrator\Extension */
class VerwaltungComponent extends MVCComponent implements BootableExtensionInterface, RouterServiceInterface
{
    use RouterServiceTrait;
    use HTMLRegistryAwareTrait;

    /**
     * @param ContainerInterface $container 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     */
    public function boot(ContainerInterface $container)
    {
        //$q = Extensions::getPluralOf('query');
        if (!Extensions::isAllowed('use.verwaltung')) {
            Factory::getApplication()->redirect('index.php');
            exit;
        }
        VerwaltungHelper::loadDefines();
        VerwaltungHelper::loadLogger();
        if (VerwaltungHelper::isTimeForCronJob(Factory::getApplication()->getName())) {
            VerwaltungHelper::updateDatabases();
        }
        if (Factory::getApplication()->isClient('administrator')) {
            ActiveDirectory::updateDhcpSettings();
        }
    }
}
