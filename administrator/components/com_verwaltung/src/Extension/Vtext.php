<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Extension;

use Exception;
use Joomla\CMS\Language\Text;
use Joomla\DI\Exception\KeyNotFoundException;

defined('JPATH_PLATFORM') or die;

/** @package Iwf\Component\Verwaltung\Administrator\Extension */
class VText
{

    /**
     * @param mixed $orgText 
     * @param mixed $jtext 
     * @return mixed 
     * @throws KeyNotFoundException 
     * @throws Exception 
     */
    public static function _($orgText, $jtext)
    {
        $text = Text::_($jtext);
        if (strcmp($text, $jtext) == 0) {
            return $orgText;
        }
        return $text;
    }

    /**
     * Bereinigt Zeichenfolge von unerwünschten (UTF-8)Zeichen, Leerzeichen usw.
     * @param mixed $text 
     * @param int $replace_linebreaks 
     * @return string 
     */
    public static function __($text, $replace_linebreaks = 1)
    {
        // Unerwünschte UTF-8-Zeichen entfernen.
        $text2 = str_replace(
            array("\xe2\x80\x93", "\xe2\x80\x94", "\xe2\x80\xa6"),
            array('-', '-', '...'),
            $text
        );
        // Next, their Windows-1252 equivalents.  These shouldn't be here as Joomla uses UTF-8, but I'm including this just in case.
        /* $text = str_replace(
          array(chr(150), chr(151), chr(133)),
          array('-', '-', '...'),
          $text); */
        // Nun Zeilenumbrüche
        if ($replace_linebreaks) {
            $text2 = preg_replace('/((\x20)*(\r\n)(\x20)*)|(\x20{2,})/', ' ', $text2);
        }
        // Überzählige Leerzeichen entfernen
        return trim(preg_replace('/\x20{2,}/', ' ', $text2));
    }
}
