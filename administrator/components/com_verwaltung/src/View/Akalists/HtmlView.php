<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\View\Akalists;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\ToolbarHelper;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\View\Akalists */
class HtmlView extends BaseHtmlView
{

	/**
	 * @return void 
	 * @throws KeyNotFoundException 
	 * @throws Exception 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	protected function addToolBar()
	{
		IwfToolbarHelper::title(Text::_('COM_VERWALTUNG_MANAGER_AKALISTS'), 'liste');                  //<----
		$toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
		if (Extensions::isAllowed('manage.literatur')) {
			$toolbar->addNew('akalist.add');
			$toolbar->delete('akalists.delete')
				->text('JTOOLBAR_DELETE')
				->message('JGLOBAL_CONFIRM_DELETE')
				->listCheck(true);
		}
		if (Extensions::isAllowed('admin')) {
			$toolbar->preferences('com_verwaltung');
		}
	}

	/**
	 * @param string $tpl 
	 * @return void 
	 * @throws KeyNotFoundException 
	 * @throws Exception 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	function display($tpl = null)
	{
		$this->header = Extensions::getListHeader('iwfactionicon-liste-dark-xxl-blue', 'COM_VERWALTUNG_MANAGER_AKALISTS');
		$this->items = $this->get('Items');
		$this->pagination    = $this->get('Pagination');
		$this->state         = $this->get('State');
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		$this->addToolBar();
		parent::display($tpl);
	}
}
