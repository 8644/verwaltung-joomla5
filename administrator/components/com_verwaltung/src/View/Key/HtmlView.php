<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\View\Key;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\Toolbar;
use Joomla\Database\DatabaseInterface;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use Joomla\Database\ParameterType;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\View\Key */
class HtmlView extends BaseHtmlView
{

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        $isNew = ($this->item->id == 0);
        IwfToolbarHelper::title($isNew ? Text::_('COM_VERWALTUNG_MANAGER_KEY_NEW') : Text::_('COM_VERWALTUNG_MANAGER_KEY_EDIT'), 'key');
        $toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if (Extensions::isAllowed('manage.schluessel')) {
            $toolbar->apply('key.apply');
            if ($isNew) {
                $toolbar->cancel('key.save', 'JTOOLBAR_SAVE');
            } else {
                $saveGroup = $toolbar->dropdownButton('save-group');
                $saveGroup->configure(
                    function (Toolbar $childBar) {
                        $childBar->save('key.save');
                        $childBar->save2new('key.save2new');
                        $childBar->save2copy('key.save2copy');
                    }
                );
            }
            $toolbar->cancel('key.cancel', 'JTOOLBAR_CLOSE');
        }
        $toolbar->divider();
        $toolbar->inlinehelp();
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws UnexpectedValueException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws InvalidArgumentException 
     */
    public function display($tpl = null)
    {
        $this->form = $this->get('Form');
        $this->item = $this->get('Item');
        $this->keyOwners = $this->getKeyOwners();
        $this->keyHistory = $this->getKeyHistory();
        $this->addToolBar();
        parent::display($tpl);
    }

    /**
     * @return mixed 
     * @throws KeyNotFoundException 
     */
    protected function getKeyOwners()
    {
        $result = null;
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery();
        $query->select
            (
                [
                    $db->qn('s.ma_id'),
                    'concat(m.nachname," ",m.vorname) AS name',

                ]
            )
            ->from($db->qn('#__iwf_schluessel', 's'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('m.id') . '=' . $db->qn('s.ma_id'))
            ->where($db->qn('s.schluessel') . '=:id')
            ->bind(':id', $this->item->id, ParameterType::INTEGER);
        return $db->setQuery($query)->loadObjectList();
    }

    /**
     * @return mixed 
     * @throws KeyNotFoundException 
     */
    protected function getKeyHistory()
    {
        $db = Factory::getContainer()->get(DatabaseInterface::class);
        $query = $db->createQuery();
        $query->select
            (
                [
                    $db->qn('s.ausgabe'),
                    $db->qn('s.rueckgabe'),

                    'CONCAT (m.nachname, " ", m.vorname) AS name',
                ]
            )
            ->from($db->qn('#__iwf_schluesselhistorie', 's'))
            ->leftJoin($db->qn('#__iwf_mitarbeiter', 'm'), $db->qn('m.id') . '=' . $db->qn('s.ma_id'))
            ->where($db->qn('s.schluessel_id') . '=:id')
            ->bind(':id', $this->item->id, ParameterType::INTEGER)
            ->order($db->qn('s.ausgabe') . ' ASC');
        return $db->setQuery($query)->loadObjectList();
    }
}
