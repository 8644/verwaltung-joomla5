<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\View\Hosts;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Language\Text;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\View\Hosts */
class HtmlView extends BaseHtmlView
{

	/**
	 * @return void 
	 * @throws KeyNotFoundException 
	 * @throws Exception 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	protected function addToolBar()
	{
		IwfToolbarHelper::title(Text::_('COM_VERWALTUNG_SUBMENU_DNS'), 'dns');
		$toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        $cmd = "startLoader();Joomla.submitbutton('host.baramundiid');";
		if (Extensions::isAllowed('manage.hw')) {
			$toolbar->addNew('host.add');
			$toolbar->delete('hosts.delete')
				->text('JTOOLBAR_DELETE')
				->message('JGLOBAL_CONFIRM_DELETE')
				->listCheck(true);
            $toolbar->standardButton('baramundi', 'COM_VERWALTUNG_JTOOLBAR_HOST_BARAMUNDIID')
                ->onclick($cmd);
		}
		if (Extensions::isAllowed('admin')) {
			$toolbar->preferences('com_verwaltung');
		}
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    function display($tpl = null)
    {
        $this->header = Extensions::getListHeader('iwfactionicon-dns-dark-xxl-blue', 'COM_VERWALTUNG_SUBMENU_DNS');
        $this->state    = $this->get('State');
        $this->items         = $this->get('Items');
        $this->pagination    = $this->get('Pagination');
        $this->filterForm = $this->get('FilterForm');
        $this->activeFilters = $this->get('ActiveFilters');
        $this->addToolBar();
        parent::display($tpl);
    }
}
