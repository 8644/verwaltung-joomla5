<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\View\Akalist;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\Toolbar;
use Joomla\CMS\Language\Text;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\View\Akalist */
class HtmlView extends BaseHtmlView
{

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        $isNew      = ($this->item->id == 0);
        IwfToolbarHelper::title($isNew ? Text::_('COM_VERWALTUNG_MANAGER_AKALIST_NEW') : Text::_('COM_VERWALTUNG_MANAGER_AKALIST_EDIT'), 'liste');
        $toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if (Extensions::isAllowed('manage.literatur')) {
            $toolbar->apply('akalist.apply');
            if ($isNew) {
                $toolbar->cancel('akalist.save', 'JTOOLBAR_SAVE');
            } else {
                $saveGroup = $toolbar->dropdownButton('save-group');
                $saveGroup->configure(
                    function (Toolbar $childBar) {
                        $childBar->save('akalist.save');
                        $childBar->save2new('akalist.save2new');
                        $childBar->save2copy('akalist.save2copy');
                    }
                );
            }
            $toolbar->cancel('akalist.cancel', 'JTOOLBAR_CLOSE');
        }
        $toolbar->divider();
        $toolbar->inlinehelp();
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    function display($tpl = null)
    {
        Text::script('COMBOBOXINITSTRING');
        $this->form = $this->get('Form');
        $this->item  = $this->get('Item');
        $this->addToolBar();
        parent::display($tpl);
    }
}
