<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\View\Programs;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\View\Programs */
class HtmlView extends BaseHtmlView
{
	/**
	 * @return void 
	 * @throws KeyNotFoundException 
	 * @throws Exception 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	protected function addToolBar()
	{
		IwfToolbarHelper::title(Text::_('COM_VERWALTUNG_SUBMENU_PROGRAMS'), 'software');
		$this->toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
		if (Extensions::isAllowed('manage.sw')) {
			$this->toolbar->addNew('program.add');
			$this->toolbar->delete('programs.delete')
				->text('JTOOLBAR_DELETE')
				->message('JGLOBAL_CONFIRM_DELETE')
				->listCheck(true);
			$this->toolbar->customHtml('<span style="width:4rem"></span>');
			$cmd1 = "startLoader();Joomla.submitbutton('licensestate.statistik');";
			$cmd2 = "startLoader();Joomla.submitbutton('licensestate.abdeckung');";
			$cmd3 = "startLoader();Joomla.submitbutton('licensestate.gesamtuebersicht');";
			$cmd4 = "startLoader();Joomla.submitbutton('licensestate.baramundisync');";
			$cmd5 = "startLoader();Joomla.submitbutton('licensestate.cleanlicenses');";
            $this->toolbar->standardButton('lizenz_statistik', 'COM_VERWALTUNG_JTOOLBAR_LIZENZ_STATISTIK')
				->listCheck(true)
				->onclick($cmd1);
            $this->toolbar->standardButton('lizenz_gesamtuebersicht', 'COM_VERWALTUNG_JTOOLBAR_LIZENZ_GESAMTUEBERSICHT')
				->onclick($cmd3);
            $this->toolbar->standardButton('lizenz_baramundisync', 'COM_VERWALTUNG_JTOOLBAR_LIZENZ_BARAMUNDISYNC')
				->onclick($cmd4);
			$this->toolbar->standardButton('lizenz_abdeckung', 'COM_VERWALTUNG_JTOOLBAR_LIZENZ_ABDECKUNG')
				->onclick($cmd2);
			$this->toolbar->standardButton('lizenz_bereinigung', 'COM_VERWALTUNG_JTOOLBAR_LIZENZEN_BEREINIGEN')
				->onclick($cmd5);
			$this->toolbar->preferences('com_verwaltung');	
		}
	}

	/**
	 * @param string $tpl 
	 * @return void 
	 * @throws KeyNotFoundException 
	 * @throws Exception 
	 * @throws RuntimeException 
	 * @throws QueryTypeAlreadyDefinedException 
	 * @throws InvalidArgumentException 
	 * @throws UnexpectedValueException 
	 */
	public function display($tpl = null)
	{
		$this->header = Extensions::getListHeader('iwfactionicon-software-dark-xxl-blue', 'COM_VERWALTUNG_SUBMENU_PROGRAMS');
		$this->state = $this->get('State');
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->filterForm = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		$this->addToolBar();
		parent::display($tpl);
	}
}
