<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\View\Program;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\Toolbar;
use Joomla\CMS\Toolbar\ToolbarHelper;
use Joomla\CMS\Language\Text;
use Iwf\Verwaltung\Extensions;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\View\Medium */
class HtmlView extends BaseHtmlView
{

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        $isNew      = ($this->item->id == 0);
        ToolbarHelper::title($isNew ? Text::_('COM_VERWALTUNG_MANAGER_PROGRAM_NEW') : Text::_('COM_VERWALTUNG_MANAGER_PROGRAM_EDIT'), 'medium');
        $toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if (Extensions::isAllowed('manage.sw')) {
            $toolbar->apply('program.apply');    //
            if ($isNew) {
                $toolbar->cancel('program.save', 'JTOOLBAR_SAVE');
            } else {
                $saveGroup = $toolbar->dropdownButton('save-group');
                $saveGroup->configure(
                    function (Toolbar $childBar) {
                        $childBar->save('program.save');
                        $childBar->save2new('program.save2new');
                        $childBar->save2copy('program.save2copy');
                    }
                );
            }
            $toolbar->cancel('program.cancel', 'JTOOLBAR_CLOSE');
        }
        $toolbar->divider();
        $toolbar->inlinehelp();
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    function display($tpl = null)
    {
        Text::script('COMBOBOXINITSTRING');
        $this->form = $this->get('Form');
        $this->item  = $this->get('Item');
        $this->addToolBar();
        parent::display($tpl);
    }
}
