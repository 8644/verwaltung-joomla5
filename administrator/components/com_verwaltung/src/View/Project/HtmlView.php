<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\View\Project;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\Toolbar;
use Joomla\CMS\Language\Text;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\View\Project */
class HtmlView extends BaseHtmlView
{

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        $isNew      = ($this->item->id == 0);
        IwfToolbarHelper::title($isNew ? Text::_('COM_VERWALTUNG_MANAGER_PROJECT_NEW') : Text::_('COM_VERWALTUNG_MANAGER_PROJECT_EDIT'), 'project');
        $toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if (Extensions::isAllowed('manage.abteilung')) {
            $toolbar->apply('project.apply');
            if ($isNew) {
                $toolbar->cancel('project.save', 'JTOOLBAR_SAVE');
            } else {
                $saveGroup = $toolbar->dropdownButton('save-group');
                $saveGroup->configure(
                    function (Toolbar $childBar) {
                        $childBar->save('project.save');
                        $childBar->save2new('project.save2new');
                        $childBar->save2copy('project.save2copy');
                    }
                );
            }
            $toolbar->cancel('project.cancel', 'JTOOLBAR_CLOSE');
        }
        $toolbar->divider();
        $toolbar->inlinehelp();
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws InvalidArgumentException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws UnexpectedValueException 
     */
    function display($tpl = null)
    {
        Text::script('COM_VERWALTUNG_VERWALTUNG_ERROR_UNACCEPTABLE');
        Text::script('COMBOBOXINITSTRING');
        $this->form = $this->get('Form');
        $this->item  = $this->get('Item');
        $this->addToolBar();
        parent::display($tpl);
    }
}
