<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\View\Department;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Joomla\CMS\Factory;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\CMS\Toolbar\Toolbar;
use Joomla\CMS\Language\Text;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\View\Department */
class HtmlView extends BaseHtmlView
{

    /**
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function addToolBar()
    {
        Factory::getApplication()->getInput()->set('hidemainmenu', true);
        $isNew      = ($this->item->id == 0);
        IwfToolbarHelper::title($isNew ? Text::_('COM_VERWALTUNG_DEPARTMENT_CREATING') : Text::_('COM_VERWALTUNG_DEPARTMENT_EDITING'), 'departments');
        $toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        if (Extensions::isAllowed('manage.abteilung')) {
            $toolbar->apply('department.apply');
            if ($isNew) {
                $toolbar->cancel('department.save', 'JTOOLBAR_SAVE');
            } else {
                $saveGroup = $toolbar->dropdownButton('save-group');
                $saveGroup->configure(
                    function (Toolbar $childBar) {
                        $childBar->save('department.save');
                        $childBar->save2new('department.save2new');
                        $childBar->save2copy('department.save2copy');
                    }
                );
            }
            $toolbar->cancel('department.cancel', 'JTOOLBAR_CLOSE');
        }
        $toolbar->divider();
        $toolbar->inlinehelp();
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     * @throws KeyNotFoundException 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    function display($tpl = null)
    {
        $this->form = $this->get('Form');
        $this->item  = $this->get('Item');
        $this->addToolBar();
        parent::display($tpl);
    }
}
