<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\View\Licensestate;

defined('_JEXEC') or die;

use Exception;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;

/** @package Iwf\Component\Verwaltung\Administrator\View\Keys */
class HtmlView extends BaseHtmlView
{

    /**
     * @param mixed $title 
     * @return void 
     * @throws Exception 
     */
    protected function addToolBar($title)
	{
		IwfToolbarHelper::title($title, 'software');
		$toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        $toolbar->cancel('licensestate.cancel', 'JTOOLBAR_CLOSE');
	}

    /**
     * @param string $tpl 
     * @return void 
     * @throws Exception 
     */
    public function display($tpl = null)
    {
        $returnFlag = Factory::getApplication()->getUserState('com_verwaltung.computer.return', 0);
        switch ($returnFlag) {
            case 0: // Standard
            case 1: 
                Extensions::setReturnRoute();
                Factory::getApplication()->setUserState('com_verwaltung.computer.return', 2);
                break;
            defaut:
            break;
        }
        $this->model = $this->getModel();
        $layout = $this->getLayout();
        switch ($layout) {
            case 'default':
                $this->header = Extensions::getListHeader('iwfactionicon-lizenz_statistik-dark-xxl-blue', 'COM_VERWALTUNG_MANAGER_STATISTIK_VIEW');
                $tpl = 'statistic';
                $this->items = $this->model->getLicenses();
                $this->addToolBar(sprintf(Text::_('COM_VERWALTUNG_MANAGER_LIZENZEN_VIEW'), $this->model->produktgruppe));
                $this->items1 = $this->model->getGroupLicenses();
                $this->statistik = Text::_('COM_VERWALTUNG_MANAGER_STATISTIK_VIEW');
                $this->versions = $this->model->getLicenseStatistic();
                break;
            case 'overview':
                $this->header = Extensions::getListHeader('iwfactionicon-lizenz_gesamtuebersicht-dark-xxl-blue', 'COM_VERWALTUNG_MANAGER_LIZENZEN_OVERVIEW');
                $tpl = 'overview';
                $this->addToolBar(sprintf(Text::_('COM_VERWALTUNG_MANAGER_LIZENZEN_OVERVIEW'), $this->model->produktgruppe));
                $this->items = $this->model->getLicenseOverview();
                break;
        }
        parent::display($tpl);
    }
}