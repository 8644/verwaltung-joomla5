<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\View\Baramundiprograms;

defined('_JEXEC') or die;

use Exception;
use InvalidArgumentException;
use Iwf\Verwaltung\Extensions;
use Iwf\Verwaltung\IwfToolbarHelper;
use Joomla\CMS\Factory;
use Joomla\CMS\Language\Text;
use Joomla\CMS\MVC\View\HtmlView as BaseHtmlView;
use Joomla\DI\Exception\KeyNotFoundException;
use RuntimeException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;
use UnexpectedValueException;

/** @package Iwf\Component\Verwaltung\Administrator\View\Baramundiprograms */
class HtmlView extends BaseHtmlView
{

    /**
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    protected function addToolBar()
    {
        IwfToolbarHelper::title(Text::_('COM_VERWALTUNG_SUBMENU_BARAMUNDIPROGRAMS'), 'software');
        $toolbar = Factory::getApplication()->getDocument()->getToolbar('toolbar');
        $cmd1 = "startLoader();Joomla.submitbutton('baramundiprograms.baramundisoftware');";
        $cmd2 = "startLoader();Joomla.submitbutton('baramundiprograms.iwfsoftware2baramundi');";
        if (Extensions::isAllowed('manage.sw')) {
            $toolbar->standardButton('baramundi', 'COM_VERWALTUNG_JTOOLBAR_LIZENZ_BARAMUNDISOFTWARE')
                ->onclick($cmd1);
            $toolbar->standardButton('baramundi', 'COM_VERWALTUNG_JTOOLBAR_LIZENZ_IWFSOFTWARE2BARAMUNDI')
            ->onclick($cmd2);
        }
        if (Extensions::isAllowed('admin')) {
            $toolbar->preferences('com_verwaltung');
        }
    }

    /**
     * @param string $tpl 
     * @return void 
     * @throws KeyNotFoundException 
     * @throws Exception 
     * @throws RuntimeException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws InvalidArgumentException 
     * @throws UnexpectedValueException 
     */
    public function display($tpl = null)
    {
        $this->header = Extensions::getListHeader('iwfactionicon-software-dark-xxl-blue', 'COM_VERWALTUNG_SUBMENU_BARAMUNDIPROGRAMS');
        $this->items = $this->get('Items');
        $this->pagination = $this->get('Pagination');
        $this->state = $this->get('State');
        $this->filterForm = $this->get('FilterForm');
        $this->activeFilters = $this->get('ActiveFilters');
        $this->addToolBar();
        parent::display($tpl);
    }
}
