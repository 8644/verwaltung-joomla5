<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

namespace Iwf\Component\Verwaltung\Administrator\Rule;

use Joomla\CMS\Form\FormRule;

defined('_JEXEC') or die;

/** @package Iwf\Component\Verwaltung\Site\Rule */
class IntRule extends FormRule
{
    protected $regex = '^[0-9]+$';
}
