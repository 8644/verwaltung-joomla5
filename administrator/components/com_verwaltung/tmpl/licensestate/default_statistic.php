<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;

defined('_JEXEC') or die('Restricted Access');

$wa = $this->document->getWebAssetManager();
$wa->useScript('table.columns');
$egal_total = 0;
$relevant_total = 0;
$produktgruppe = "";
?>
<?php echo $this->header; ?>
<form action="<?php echo Route::_('index.php?option=com_verwaltung&view=licensestate'); ?>" method="post" name="adminForm" id="adminForm">
    <div class="row">
        <div class="col-md-12">
            <div id="j-main-container" class="j-main-container">
                <p style="height:3rem"></p>
                <?php if (empty($this->items)) : ?>
                    <div class="alert alert-info">
                        <span class="icon-info-circle" aria-hidden="true"></span>
                        <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                    </div>
                <?php else : ?>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col" class="w-40 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_STATISTIK_HEADING_PRODUKT'); ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_STATISTIK_HEADING_INSTALLATIONEN'); ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_STATISTIK_HEADING_BASISGEKAUFTFIX'); ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_STATISTIK_HEADING_BASISGEKAUFTFLOAT'); ?>
                                </th>
                                <th scope="col" class="w-45 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_STATISTIK_HEADING_STATUS'); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $n = count($this->items1); ?>
                            <?php foreach ($this->items1 as $i => $stat): ?>
                                <tr class="row<?php echo $i % 2; ?>">
                                    <td class="d-none d-md-table-cell" nowrap="nowrap">
                                        <span title="<?php echo htmlspecialchars($stat->anmerkungen, ENT_COMPAT, 'UTF-8'); ?>"><?php echo $stat->produkt; ?></span>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php
                                        $relevant = 0;
                                        $egal = 0;
                                        if (!$produktgruppe) {
                                            $produktgruppe = $stat->produktgruppe;
                                        }
                                        if ($stat->aktuell) {
                                            $relevant = $this->model->gesamtaktuell_nach_produkt[$stat->produkt];
                                            $egal = $this->model->gesamtaktuell_nach_produkt_nicht_relevant[$stat->produkt];
                                        } else {
                                            $relevant = $this->model->gesamtalt_nach_produkt[$stat->produkt];
                                            $egal = $this->model->gesamtalt_nach_produkt_nicht_relevant[$stat->produkt];
                                        }
                                        echo "$relevant / $egal";
                                        $relevant_total += $relevant;
                                        $egal_total += $egal;
                                        ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $stat->basisgekauftfix ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $stat->basisgekauftfloat ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php if ($stat->basisgekauftfloat > 0)
                                            echo '<span class="ok">&nbsp;</span>';
                                        else
                                            echo ($stat->abdeckung) ? '<span class="ok">&nbsp;</span>' : '<span class="nok">&nbsp;</span>'; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            <tr class="row0">
                                <td nowrap="nowrap" class="summe"><?php echo "Gruppe <i>&gt;$produktgruppe&lt;</i>"; ?></td>
                                <td class="summe"><?php echo "$relevant_total / $egal_total"; ?></td>
                                <td class="summe">&nbsp;</td>
                                <td class="summe">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </tbody>
                    </table>
                <?php endif; ?>

                <div class="pagetitle" style="margin-top:40px;margin-bottom:2rem;">
                    <h2><?php echo JText::_('COM_VERWALTUNG_MANAGER_LIZENZ_RECHNER_VIEW'); ?></h2>
                </div>
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th scope="col" class="w-5 d-none d-md-table-cell">
                                <?php echo Text::_('COM_VERWALTUNG_LIZENZEN_HEADING_HOST'); ?>
                            </th>
                            <th scope="col" class="w-5 d-none d-md-table-cell">
                                <?php echo Text::_('COM_VERWALTUNG_LIZENZEN_HEADING_TYP'); ?>
                            </th>
                            <th nowrap="nowrap" scope="col" class="w-15 d-none d-md-table-cell">
                                <?php echo Text::_('COM_VERWALTUNG_LIZENZEN_HEADING_PRODUKT'); ?>
                            </th>
                            <th wscope="col" class="w-5 d-none d-md-table-cell">
                                <?php echo Text::_('COM_VERWALTUNG_LIZENZEN_HEADING_VERSION'); ?>
                            </th>
                            <th nowrap="nowrap" scope="col" class="w-5 d-none d-md-table-cell">
                                <?php echo Text::_('COM_VERWALTUNG_LIZENZEN_HEADING_INTERNE_VERSION'); ?>
                            </th>
                            <th nowrap="nowrap" scope="col" class="w-5 d-none d-md-table-cell">
                                <?php echo Text::_('COM_VERWALTUNG_LIZENZEN_HEADING_ZIMMER'); ?>
                            </th>
                            <th nowrap="nowrap" scope="col" class="w-5 d-none d-md-table-cell">
                                <?php echo Text::_('COM_VERWALTUNG_LIZENZEN_HEADING_IPNUMMER'); ?>
                            </th>
                            <th scope="col" class="w-40 d-none d-md-table-cell">
                                <?php echo Text::_('COM_VERWALTUNG_LIZENZEN_HEADING_NAME'); ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($this->items as $i => $lizenz): ?>
                            <tr class="row<?php echo $i % 2; ?><?php echo (!$lizenz->relevant ? ' ignore_license' : ''); ?>">
                                <td class="<?php echo ($lizenz->aktuell ? 'active_license' : ''); ?>">
                                    <?php echo $lizenz->hostname; ?>
                                </td>
                                <td>
                                    <?php echo $lizenz->rechnertyp ?>
                                </td>
                                <td nowrap="nowrap">
                                    <?php echo $lizenz->produkt ?>
                                </td>
                                <td nowrap="nowrap">
                                    <?php echo $lizenz->version ?>
                                </td>
                                <td>
                                    <?php echo $lizenz->interne_version ?>
                                </td>
                                <td>
                                    <?php echo $lizenz->zimmer ?>
                                </td>
                                <td>
                                    <?php echo $lizenz->ipnummer ?>
                                </td>
                                <td>
                                    <?php echo $lizenz->name ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <input type="hidden" name="task" value="">
    <?php echo HTMLHelper::_('form.token'); ?>
</form>