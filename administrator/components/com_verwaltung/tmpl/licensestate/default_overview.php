<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Router\Route;
use Joomla\CMS\Language\Text;

defined('_JEXEC') or die('Restricted Access');

$wa = $this->document->getWebAssetManager();
$wa->useScript('table.columns');
$egal_total = 0;
$relevant_total = 0;
$produktgruppe = "";
?>
<?php echo $this->header; ?>
<form action="<?php echo Route::_('index.php?option=com_verwaltung&view=licensestate'); ?>" method="post" name="adminForm" id="adminForm">
    <div class="row">
        <div class="col-md-12">
            <div id="j-main-container" class="j-main-container">
                <p style="height:3rem"></p>
                <?php if (empty($this->items)) : ?>
                    <div class="alert alert-info">
                        <span class="icon-info-circle" aria-hidden="true"></span>
                        <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                    </div>
                <?php else : ?>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col" class="w-10 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_LIZENZUEBERSICHT_HEADING_PRODUKTGRUPPE'); ?>
                                </th>
                                <th scope="col" class="w-90 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_LIZENZUEBERSICHT_HEADING_DETAILS'); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $i = 0; foreach ($this->items as $i => $produktgruppe): if ($produktgruppe->info) : ?>
                                <tr class="row<?php echo $i % 2; ?>">
                                    <td nowrap="nowrap" width="10%">
                                        <?php echo $produktgruppe->name; ?>
                                    </td>
                                    <td>
                                        <table class="lizenzinfo">
                                            <thead>
                                                <th>Produkt</th>
                                                <th>Gesamtlizenzen</th>
                                                <th>Installationen</th>
                                                <th>Relevant</th>
                                                <th>Verfügbar</th>
                                                <th nowrap="nowrap">Int. Version</th>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($produktgruppe->info as $info): ?>
                                                    <tr>
                                                        <td width="50%" class="<?php if ($info->aktuell) echo 'aktuell' ?>"><?php echo $info->produkt; ?></td>
                                                        <td width="10%"><?php if ($info->basisgekauftfloat > 0) {
                                                                            echo "&infin;";
                                                                        } else {
                                                                            echo $info->basisgekauftfix;
                                                                        } ?></td>
                                                        <td width="10%"><?php echo $info->installationen ?></td>
                                                        <td width="10%"><?php echo $info->relevant ?></td>
                                                        <td width="10%" <?php if ($info->verfuegbar < 0 && !$info->isfloat && $info->lizenzpflichtig) echo 'class="lizenzmangel"'; ?>>
                                                            <?php if ($info->isfloat) {
                                                                echo $info->floatlizenzen_verfuegbar;
                                                            } else {
                                                                echo $info->verfuegbar;
                                                            } ?></td>
                                                        <td width="10%"><?php echo $info->interne_version ?></td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            <?php endif; endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <input type="hidden" name="task" value="">
    <?php echo HTMLHelper::_('form.token'); ?>
</form>