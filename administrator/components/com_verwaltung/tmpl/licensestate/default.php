<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Router\Route;

defined('_JEXEC') or die('Restricted Access');

$wa = $this->document->getWebAssetManager();
$wa->useScript('table.columns');
?>
<?php echo $this->header; ?>
<form action="<?php echo Route::_('index.php?option=com_verwaltung&view=lists'); ?>" method="post" name="adminForm" id="adminForm">
    <div class="row">
        Hier sollte man nicht landen.
        <input type="hidden" name="task" value="">
        <?php echo HTMLHelper::_('form.token'); ?>
    </div>
</form>
