<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;

$wa = $this->document->getWebAssetManager();
$wa->useScript('table.columns')
    ->useScript('multiselect');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
?>
<?php echo $this->header; ?>
<form action="<?php echo Route::_('index.php?option=com_verwaltung&view=baramundiprogramsdiff'); ?>" method="post" name="adminForm" id="adminForm">
    <div class="row">
        <div class="col-md-12">
            <div id="j-main-container" class="j-main-container">
                <?php echo LayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
                <?php if (empty($this->items)): ?>
                    <div class="alert alert-info">
                        <span class="icon-info-circle" aria-hidden="true"></span>
                        <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                    </div>
                <?php else: ?>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col" class="w-30 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_BARAMUNDIPROGRAM_HEADING_PRODUCTNAME', 'a.product_name', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-10">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_BARAMUNDIPROGRAM_HEADING_VERSION', 'a.version', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-10 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_BARAMUNDIPROGRAM_HEADING_COMPANY', 'a.company', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-10 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_BARAMUNDIPROGRAM_HEADING_SET', 'a.set', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-30 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_BARAMUNDIPROGRAM_HEADING_FOUND', 'a.found', $listDirn, $listOrder); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $n = count($this->items);
                            foreach ($this->items as $i => $item): ?>
                                <tr class="row<?php echo $i % 2; ?>">
                                    <td class="">
                                        <?php echo $item->product_name; ?>
                                    </td>
                                    <td class="">
                                        <?php echo $item->version; ?>
                                    </td>
                                    <td class="">
                                        <?php echo $item->company; ?>
                                    </td>
                                    <td class="">
                                        <?php echo $item->set; ?>
                                    </td>
                                    <td class="">
                                        <?php echo $item->found; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
                <?php echo $this->pagination->getListFooter(); ?>
                <input type="hidden" name="task" value="">
                <input type="hidden" name="boxchecked" value="0">
                <?php echo HTMLHelper::_('form.token'); ?>
            </div>
        </div>
    </div>
</form>