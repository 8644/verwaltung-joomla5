<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Iwf\Component\Verwaltung\Administrator\Helper\RouterHelper;
use Iwf\Verwaltung\IwfActionButton;
use Iwf\Verwaltung\IwfSpinningWheel;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;

$wa = $this->document->getWebAssetManager();
$wa->useScript('table.columns')
    ->useScript('multiselect')
    ->useScript('actionbutton');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$editIcon = '<span class="fa fa-pen-square me-2" aria-hidden="true"></span>';
?>
<?php echo $this->header; ?>
<form action="<?php echo Route::_('index.php?option=com_verwaltung&view=programs'); ?>" method="post" name="adminForm" id="adminForm">
    <?php echo (new IwfSpinningWheel('loader'))->render('Führe Lizenzberechnungen aus...'); ?>
    <div class="row">
        <div class="col-md-12">
            <div id="j-main-container" class="j-main-container">
                <?php echo LayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
                <?php if (empty($this->items)): ?>
                    <div class="alert alert-info">
                        <span class="icon-info-circle" aria-hidden="true"></span>
                        <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                    </div>
                <?php else: ?>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th scope="col" class="w-1 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('grid.checkall'); ?>
                                </th>
                                <th scope="col" class="w-2 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_PROGRAMS_HEADING_INSTALLIERT'); ?>
                                </th>
                                <th scope="col" class="w-15 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_PROGRAMS_HEADING_PRODUKT', 'a.produkt', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-10 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_PROGRAMS_HEADING_PRODUKTGROUP', 'a.produktgruppe', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_PROGRAMS_HEADING_VERSION'); ?>
                                </th>
                                <th scope="col" class="w-2 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_PROGRAMS_HEADING_INTERNAL_VERSION'); ?>
                                </th>
                                <th scope="col" class="w-15 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_PROGRAMS_HEADING_BARAMUNDI_VERSION'); ?>
                                </th>
                                <th scope="col" class="w-1 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_PROGRAMS_HEADING_AKTUELL'); ?>
                                </th>
                                <th scope="col" class="w-1 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_PROGRAMS_HEADING_ABDECKUNG'); ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_PROGRAMS_HEADING_SPRACHE'); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $n = count($this->items);
                            foreach ($this->items as $i => $item) : ?>
                                <tr class="row<?php echo $i % 2; ?>">
                                    <td class="">
                                        <?php echo HTMLHelper::_('grid.id', $i, $item->id, false, 'cid', 'cb'); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell<?php if ($item->aktuell) { echo " sw-aktuell"; } ?>">
                                        <?php echo $item->gesamtlizenzen . '&nbsp;/&nbsp;' . $item->installed ?>
                                    </td>
                                    <td class="">
                                        <?php echo RouterHelper::getProgramEditRoute($item->id, $item->produkt, "", $editIcon); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->produktgruppe; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->version; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->intversion; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell" title="<?php echo strlen($item->baramundiversion) > 40 ? str_replace(",", " / ", $item->baramundiversion) : ""; ?>">
                                        <?php echo strlen($item->baramundiversion) > 40 ? str_replace(",", " / ", substr($item->baramundiversion, 0, 50)) . '.....' : str_replace(",", " / ", $item->baramundiversion); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo (new IwfActionButton($this, 0, null, null, '', ''))->render((int) $item->aktuell, $i, true, ['id' => $item->id, 'task_prefix' => 'programs.']); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo (new IwfActionButton($this, 1, null, null, '', ''))->render((int) $item->abdeckung, $i, true, ['id' => $item->id, 'task_prefix' => 'programs.']); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->sprache; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
                <?php echo $this->pagination->getListFooter(); ?>
                <input type="hidden" name="actionButton" id="actionButton" value="">
                <input type="hidden" name="task" value="">
                <input type="hidden" name="boxchecked" value="0">
                <?php echo HTMLHelper::_('form.token'); ?>
            </div>
        </div>
    </div>
</form>