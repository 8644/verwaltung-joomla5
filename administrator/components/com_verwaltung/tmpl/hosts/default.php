<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Iwf\Component\Verwaltung\Administrator\Helper\RouterHelper;
use Iwf\Verwaltung\IwfSpinningWheel;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;

$wa = $this->document->getWebAssetManager();
$wa->useScript('table.columns')
    ->useScript('multiselect');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
$editIcon = '<span class="fa fa-pen-square me-2" aria-hidden="true"></span>';
?>
<?php echo $this->header; ?>
<form action="<?php echo Route::_('index.php?option=com_verwaltung&view=hosts'); ?>" method="post" name="adminForm" id="adminForm">
    <?php echo (new IwfSpinningWheel('loader'))->render('Hole Computerobjekte von baramundi Server - kann lange dauern...');?>
    <div class="row">
        <div class="col-md-12">
            <div id="j-main-container" class="j-main-container">
                <?php echo LayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
                <?php if (empty($this->items)) : ?>
                    <div class="alert alert-info">
                        <span class="icon-info-circle" aria-hidden="true"></span>
                        <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                    </div>
                <?php else : ?>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <td scope="col" class="w-1 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('grid.checkall'); ?>
                                </td>
                                <th scope="col" class="w-7 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_DNS_HEADING_HOSTNAME', 'a.hostname', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_DNS_HEADING_WNAME', 'a.wname', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-15 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_DNS_HEADING_DESCRIPTION'); ?>
                                </th>
                                <th scope="col" class="w-30 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_DNS_HEADING_TITEL'); ?>
                                </th>
                                <th scope="col" class="w-1 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_DNS_HEADING_IPNUMMER', 'a.ipnummer', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-1 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_DNS_HEADING_MAC', 'a.mac', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-25 d-none d-md-table-cell">
                                    <?php echo Text::_('COM_VERWALTUNG_DNS_HEADING_ENDPOINT_ID'); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $n = count($this->items);
                            foreach ($this->items as $i => $host) : ?>
                                <tr class="row<?php echo $i % 2; ?>">
                                    <td class="d-none d-md-table-cell">
                                        <?php echo HTMLHelper::_('grid.id', $i, $host->id, false, 'cid', 'cb'); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo RouterHelper::getHostEditRoute($host->id, $host->hostname, "", $editIcon); ?>
                                    </td>
                                    <td class= "d-none d-md-table-cell">
                                        <?php echo $host->wname; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $host->anmerkungen; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $host->titel; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $host->ipnummer; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $host->mac; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $host->baramundi_id; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
                <?php echo $this->pagination->getListFooter(); ?>
                <input type="hidden" name="task" value="">
                <input type="hidden" name="boxchecked" value="0">
                <?php echo HTMLHelper::_('form.token'); ?>
            </div>
        </div>
    </div>
</form>