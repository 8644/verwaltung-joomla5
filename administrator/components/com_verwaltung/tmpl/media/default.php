<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Iwf\Component\Verwaltung\Administrator\Helper\RouterHelper;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;

$wa = $this->document->getWebAssetManager();
$wa->useScript('table.columns')
    ->useScript('multiselect');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn = $this->escape($this->state->get('list.direction'));
$editIcon = '<span class="fa fa-pen-square me-2" aria-hidden="true"></span>';
?>
<?php echo $this->header; ?>
<form action="<?php echo Route::_('index.php?option=com_verwaltung&view=media'); ?>" method="post" name="adminForm" id="adminForm">
    <div class="row">
        <div class="col-md-12">
            <div id="j-main-container" class="j-main-container">
                <?php echo LayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
                <?php if (empty($this->items)): ?>
                    <div class="alert alert-info">
                        <span class="icon-info-circle" aria-hidden="true"></span>
                        <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                    </div>
                <?php else: ?>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <td scope="col" class="w-1 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('grid.checkall'); ?>
                                </td>
                                <th scope="col" class="w-3 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_MEDIAS_HEADING_LABEL','a.label', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_MEDIAS_HEADING_Hersteller', 'a.hersteller', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-10 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_MEDIAS_HEADING_PRODUKT', 'a.produkt', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-5 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_MEDIAS_HEADING_VERSION', 'a.version', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-20 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_MEDIAS_HEADING_SPRACHE', 'l.inhalt', $listDirn, $listOrder); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $n = count($this->items);
                            foreach ($this->items as $i => $medium): ?>
                                <tr class="row<?php echo $i % 2; ?>">
                                    <td class="d-none d-md-table-cell">
                                        <?php echo HTMLHelper::_('grid.id', $i, $medium->id, false, 'cid', 'cb'); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo RouterHelper::getMediumEditRoute($medium->id, $medium->label, "", $editIcon); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $medium->hersteller; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $medium->produkt; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $medium->version; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $medium->sprache; ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
                <?php echo $this->pagination->getListFooter(); ?>
                <input type="hidden" name="task" value="">
                <input type="hidden" name="boxchecked" value="0">
                <?php echo HTMLHelper::_('form.token'); ?>
            </div>
        </div>
    </div>
</form>