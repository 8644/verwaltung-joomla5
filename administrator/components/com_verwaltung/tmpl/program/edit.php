<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;

$wa = $this->document->getWebAssetManager();
$wa->useScript('keepalive')
  ->useScript('form.validate')
  ->useScript('editablecombobox')
  ->useScript('licenses');
?>
<form action="<?php echo Route::_('index.php?option=com_verwaltung&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="item-form" class="form-validate">
  <div class="main-card">
    <?php echo HTMLHelper::_('uitab.startTabSet', 'myTab', ['active' => 'details', 'recall' => true, 'breakpoint' => 768]); ?>
    <?php echo HTMLHelper::_('uitab.addTab', 'myTab', 'details', Text::_('COM_VERWALTUNG_PROGRAM_DETAILS')); ?>
    <div class="row">
      <div class="col-lg-6">
        <fieldset class="options-form">
          <legend><?php echo Text::_('COM_VERWALTUNG_PROGRAMS_EDITING'); ?></legend>
          <div class="form-grid">
            <?php echo $this->form->renderFieldset('software_fields'); ?>
          </div>
        </fieldset>
      </div>
    </div>
    <?php echo HTMLHelper::_('uitab.endTab'); ?>
    <?php if ($this->form->getFieldset('software_lizenzen')) : ?>
      <?php echo HTMLHelper::_('uitab.addTab', 'myTab', 'details', Text::_('COM_VERWALTUNG_PROGRAM_LICENSE_INFO')); ?>
      <div class="row">
        <div class="col-lg-7">
          <fieldset class="options-form">
            <div class="form-grid">
              <?php echo $this->form->renderFieldset('software_lizenzen'); ?>
            </div>
            <div class="control-group">
              <div class="control-label">
                <label title data-content="<?php echo Text::_('COM_VERWALTUNG_PROGRAM_LICENSE_INFO_DESC', true) ?>"><?php echo Text::_('COM_VERWALTUNG_PROGRAM_LICENSE_INFO_LABEL', true) ?></label>
              </div>
              <div class="controls">
                <div class="input-append">
                  <button class="btn btn-primary" id="installed_licenses_button" type="button"><span class="icon-search"></span></button>
                </div>
              </div>
            </div>
            <div id="license_ajax_result">
              <div class="control-group">
                <div class="control-label">
                  <label><?php echo Text::_('COM_VERWALTUNG_PROGRAM_LICENSE_INFO_RESULT', true) ?></label>
                </div>
                <div class="controls">
                  <div class="input-append" id="licenses_html"></div>
                </div>
              </div>
            </div>
          </fieldset>
        </div>
      </div>
      <?php echo HTMLHelper::_('uitab.endTab'); ?>
    <?php endif; ?>
    <?php echo HTMLHelper::_('uitab.endTabSet'); ?>
  </div>
  <input type="hidden" name="task" value="program.edit" />
  <?php echo HTMLHelper::_('form.token'); ?>
</form>