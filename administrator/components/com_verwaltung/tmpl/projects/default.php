<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die('Restricted Access');

use Iwf\Component\Verwaltung\Administrator\Helper\RouterHelper;
use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Layout\LayoutHelper;
use Joomla\CMS\Router\Route;
use Iwf\Verwaltung\IwfActionButton;

$wa = $this->document->getWebAssetManager();
$wa->useScript('table.columns')
    ->useScript('multiselect')
    ->useScript('actionbutton');
$listOrder = $this->escape($this->state->get('list.ordering'));
$listDirn  = $this->escape($this->state->get('list.direction'));
$editIcon = '<span class="fa fa-pen-square me-2" aria-hidden="true"></span>';
?>
<?php echo $this->header; ?>
<form action="<?php echo Route::_('index.php?option=com_verwaltung&view=projects'); ?>" method="post" name="adminForm" id="adminForm">
    <div class="row">
        <div class="col-md-12">
            <div id="j-main-container" class="j-main-container">
                <?php echo LayoutHelper::render('joomla.searchtools.default', array('view' => $this)); ?>
                <?php if (empty($this->items)) : ?>
                    <div class="alert alert-info">
                        <span class="icon-info-circle" aria-hidden="true"></span>
                        <?php echo Text::_('JGLOBAL_NO_MATCHING_RESULTS'); ?>
                    </div>
                <?php else : ?>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <td scope="col" class="w-1 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('grid.checkall'); ?>
                                </td>
                                <th scope="col" class="w-25 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_PROJECT_HEADING_PROJEKT', 'a.projekt', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-10 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_PROJECT_HEADING_PROJEKTBEZEICHNUNG', 'a.projektbez', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-15 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_PROJECT_HEADING_PROJEKTNUMMER', 'a.projektnummer', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-20 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_PROJECT_HEADING_ABTEILUNG', 'abteilung', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-15 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_PROJECT_HEADING_VERANTWORTLICHER', 'ma_id', $listDirn, $listOrder); ?>
                                </th>
                                <th scope="col" class="w-25 d-none d-md-table-cell">
                                    <?php echo HTMLHelper::_('searchtools.sort', 'COM_VERWALTUNG_PROJECT_HEADING_AKTUELL', 'a.aktuell', $listDirn, $listOrder); ?>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $n = count($this->items);
                            foreach ($this->items as $i => $item) : ?>
                                <tr class="row<?php echo $i % 2; ?>">
                                    <td class="">
                                        <?php echo HTMLHelper::_('grid.id', $i, $item->id, false, 'cid', 'cb'); ?>
                                    </td>
                                    <td class="">
                                        <?php echo RouterHelper::getProjectEditRoute($item->id, $item->projekt, "", $editIcon); ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->bezeichnung; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->projektnummer; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->abteilung; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo $item->verantwortlicher; ?>
                                    </td>
                                    <td class="d-none d-md-table-cell">
                                        <?php echo (new IwfActionButton($this, 0, null, null, '', ''))->render((int) $item->aktuell, $i, true, ['id' => $item->id, 'task_prefix' => 'projects.']); ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endif; ?>
                <?php echo $this->pagination->getListFooter(); ?>
                <input type="hidden" name="actionButton" id="actionButton" value="">
                <input type="hidden" name="task" value="">
                <input type="hidden" name="boxchecked" value="0">
                <?php echo HTMLHelper::_('form.token'); ?>
            </div>
        </div>
    </div>
</form>