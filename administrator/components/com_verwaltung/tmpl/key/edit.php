<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 * 08.08.2024
 *
 *
 * $history nicht definiert??
 */

defined('_JEXEC') or die('Restricted Access');

use Joomla\CMS\HTML\HTMLHelper;
use Joomla\CMS\Language\Text;
use Joomla\CMS\Router\Route;

$wa = $this->document->getWebAssetManager();
$wa->useScript('keepalive');
$wa->useScript('form.validate');
?>
<form action="<?php echo Route::_('index.php?option=com_verwaltung&view=key&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="item-form" class="form-validate">
  <div class="main-card">
    <?php echo HTMLHelper::_('uitab.startTabSet', 'myTab', ['active' => 'details', 'recall' => true, 'breakpoint' => 768]); ?>
    <?php echo HTMLHelper::_('uitab.addTab', 'myTab', 'details', Text::_('COM_VERWALTUNG_KEY_DETAILS')); ?>
    <div class="row">
      <div class="col-lg-6">
        <fieldset class="options-form">
          <legend><?php echo Text::_('COM_VERWALTUNG_KEYS_EDITING'); ?></legend>
          <div class="form-grid">
            <?php echo $this->form->renderFieldset('schluessel_fields'); ?>
          </div>
        </fieldset>
      </div>
    </div>
    <?php echo HTMLHelper::_('uitab.endTab'); ?>
    <?php if ($this->keyOwners): ?>
      <?php echo HTMLHelper::_('uitab.addTab', 'myTab', 'details', Text::_('COM_VERWALTUNG_KEY_OWNERS')); ?>
      <div class="row">
        <div class="col-lg-6">
          <fieldset class="options-form">
            <legend><?php echo Text::_('COM_VERWALTUNG_KEY_OWNERS'); ?></legend>
            <?php foreach ($this->keyOwners as $owner): ?>
            <div class="control-group">
              <div class="control-label">
                <?php echo $owner->name; ?>
              </div>
            </div>
            <?php endforeach; ?>
          </fieldset>
        </div>
      </div>
      <?php echo HTMLHelper::_('uitab.endTab'); ?>
    <?php endif; ?>
    <?php if ($this->keyHistory): ?>
      <?php echo HTMLHelper::_('uitab.addTab', 'myTab', 'details', Text::_('COM_VERWALTUNG_KEY_HISTORY')); ?>
      <div class="row">
        <div class="col-lg-6">
          <fieldset class="options-form">
            <legend><?php echo Text::_('COM_VERWALTUNG_KEY_HISTORY'); ?></legend>
            <?php foreach ($this->keyHistory as $history): ?>
            <div class="control-group">
              <div class="control-label">
                <?php echo $history->name; ?>
              </div>
              <div class="controls">
                <?php echo sprintf("Ausgabe: %s, Rückgabe: %s", $history->ausgabe, $history->rueckgabe); ?>
              </div>
            </div>
            <?php endforeach; ?>
          </fieldset>
        </div>
      </div>
      <?php echo HTMLHelper::_('uitab.endTab'); ?>
    <?php endif; ?>
  <?php echo HTMLHelper::_('uitab.endTabSet'); ?>
  </div>
  <input type="hidden" name="task" value="key.edit" />
  <?php echo HTMLHelper::_('form.token'); ?>
</form>