<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 *
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der
 * Software ist ausdrücklich untersagt.
 */

use Joomla\CMS\Factory;
use Joomla\CMS\Installer\Adapter\ComponentAdapter;
use Joomla\DI\Exception\KeyNotFoundException;
use Joomla\Database\Exception\UnsupportedAdapterException;
use Joomla\Database\Exception\QueryTypeAlreadyDefinedException;

class Com_VerwaltungInstallerScript
{
    /**
     * @param mixed $type 
     * @param mixed $parent 
     * @return true 
     */
    public function preflight($type, $parent)
    {
        return true;
    }
    /**
     * Called after any type of installation / uninstallation action.
     *
     * @param   string          $type    Which action is happening (install|uninstall|discover_install|update)
     * @param   ComponentAdapter  $parent  The object responsible for running this script
     *
     * @return  bool
     * @since   1.0.0
     */
    public function postflight($type, $parent)
    {
        // Do not run on uninstall.
        if ($type === 'uninstall') {
            return true;
        }
        // copy layouts
        $layoutSource = JPATH_ROOT . '/administrator/components/com_verwaltung/assets/layouts/iwf';
        $layoutTarget = JPATH_ROOT . '/layouts/iwf';
        if (is_dir($layoutTarget)) {
            $this->removeDir($layoutTarget);
        }
        rename($layoutSource, $layoutTarget);

        // copy logo
        $logoSource = JPATH_ROOT . '/administrator/components/com_verwaltung/assets/logo.png';
        $logoTarget = JPATH_ROOT . '/images/headers/logo.png';
        if (is_file($logoTarget)) {
            unlink($logoTarget);
        }
        rename($logoSource, $logoTarget);
        
        // Install the dashboard module if necessary
        $this->conditionalInstallDashboard('verwaltung', 'verwaltung');

        return true;
    }

    /**
     * @param string $dir 
     * @return void 
     */
    private function removeDir(string $dir): void
    {
        $it = new RecursiveDirectoryIterator($dir, RecursiveDirectoryIterator::SKIP_DOTS);
        $files = new RecursiveIteratorIterator(
            $it,
            RecursiveIteratorIterator::CHILD_FIRST
        );
        foreach ($files as $file) {
            if ($file->isDir()) {
                rmdir($file->getPathname());
            } else {
                unlink($file->getPathname());
            }
        }
        rmdir($dir);
    }

    /**
     * @param string $dashboard 
     * @param string $preset 
     * @return void 
     * @throws KeyNotFoundException 
     * @throws UnsupportedAdapterException 
     * @throws QueryTypeAlreadyDefinedException 
     * @throws RuntimeException 
     * @throws InvalidArgumentException 
     */
    private function conditionalInstallDashboard(string $dashboard, string $preset): void
    {
        $position = 'cpanel-' . $dashboard;

        /** @var \Joomla\Database\DatabaseDriver $db */
        $db = Factory::getContainer()->get('DatabaseDriver');
        $query = $db->getQuery(true)
            ->select('COUNT(*)')
            ->from($db->qn('#__modules'))
            ->where([
                $db->qn('module') . ' = ' . $db->quote('mod_submenu'),
                $db->qn('client_id') . ' = ' . $db->quote(1),
                $db->qn('position') . ' = :position',
            ])
            ->bind(':position', $position);

        $modules = $db->setQuery($query)->loadResult() ?: 0;

        if ($modules == 0) {
            $this->addDashboardMenu($dashboard, $preset);
        }
    }
}
