--UPDATE `#__assets` SET `rules`='{ "core.manage":{"13":1,"16":1,"19":1,"20":1},"core.create":{"12":1},"core.delete":{"12":1},"core.edit":{"12":1},"core.edit.state":{"12":1},"core.edit.own":{"2":1},"core.manage.institut":{"13":1},"core.manage.fzg":{"14":1},"core.create.ma":{"12":1},"core.edit.ma":{"12":1},"core.delete.ma":{"12":1},"core.manage.hw":{"16":1},"core.manage.sw":{"20":1},"core.create.bestellungen":{"2":1},"core.delete.bestellungen":{"2":1},"core.create.reservierungen":{"2":1},"core.delete.reservierungen":{"2":1},"core.manage.schluessel":{"19":1},"core.manage.inventar":{"17":1},"core.manage.literatur":{"18":1},"core.manage.edv":{"10":1},"core.edit.gastform":{"2":1}}' WHERE  `name`="com_verwaltung";

CREATE TABLE IF NOT EXISTS `#__iwf_abteilungen` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`institut` VARCHAR(45) NOT NULL DEFAULT '',
	`abteilung` VARCHAR(45) NOT NULL DEFAULT '',
	`kuerzel` VARCHAR(10) NOT NULL DEFAULT '',
	`titel` VARCHAR(100) NOT NULL DEFAULT '',
	`state` TINYINT(3) NOT NULL DEFAULT '1',
	`konto` VARCHAR(45) NOT NULL DEFAULT '',
	`mailverteiler` VARCHAR(100) NOT NULL DEFAULT '',
	`mailman_liste` VARCHAR(45) NOT NULL DEFAULT '',
	`ad_gruppe` VARCHAR(25) NOT NULL DEFAULT '',
	`sekretariat_email` MEDIUMTEXT NOT NULL DEFAULT '',
	`lizenzadmin_email` MEDIUMTEXT NOT NULL DEFAULT '',
	`nomail` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
	`reihenfolge` TINYINT(3) NOT NULL DEFAULT 0,
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__iwf_bestellungen` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`bestellnummer` VARCHAR(15) NOT NULL DEFAULT '',
	`ma_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	`projekt` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`sap` TINYTEXT NULL DEFAULT '',
	`sapmailsent` TINYINT(4) NULL DEFAULT '0',
	`preis1` FLOAT NOT NULL DEFAULT '0',
	`preis2` FLOAT NOT NULL DEFAULT '0',
	`preis3` FLOAT NOT NULL DEFAULT '0',
	`anbot1` VARCHAR(40) NULL DEFAULT NULL,
	`anbot2` VARCHAR(40) NULL DEFAULT NULL,
	`anbot3` VARCHAR(40) NULL DEFAULT NULL,
	`anbot` INT(10) UNSIGNED NOT NULL DEFAULT '229' COMMENT 'Anbot der gew. Firma',
	`anbotbegruendung` VARCHAR(255) NULL DEFAULT NULL,
	`bestelldatum` DATE NULL DEFAULT NULL,
	`beschreibung` VARCHAR(100) NOT NULL DEFAULT '',
	`land` INT(10) UNSIGNED NOT NULL DEFAULT '1',
	`kontaktperson` VARCHAR(40) NULL DEFAULT NULL,
	`eigenerklaerung` VARCHAR(100) NULL DEFAULT NULL,
	`rechnungsdatum` DATE NULL DEFAULT NULL,
	`rechnungsnummer` VARCHAR(20) NULL DEFAULT NULL,
	`created_by` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`created_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`modified_by` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`modified_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`modified_by_fzguser_id` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Mitarbeiter-Id; wird bei Erzeugung ausgefüllt',
	`bezahlt` TINYINT(4) NULL DEFAULT '0',
	`bezahldatum` DATE NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE `#__iwf_inventar` (
	`inventarnummer` VARCHAR(20) NOT NULL DEFAULT '',
	`beschreibung` VARCHAR(100) NOT NULL DEFAULT '',
	`abteilung` INT(10) UNSIGNED NOT NULL,
	`kaufdatum` DATE NULL DEFAULT NULL,
	`firma` VARCHAR(45) NOT NULL DEFAULT '',
	`preis` FLOAT NOT NULL DEFAULT '0',
	`standort` INT(10) UNSIGNED NOT NULL,
	`inventartyp` INT(10) UNSIGNED NOT NULL,
	`erb_nr` VARCHAR(45) NULL DEFAULT NULL COMMENT 'Eingangsrechnungsbuchnummer',
	`deinventarisiert` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0',
	`deinventarisierungsdatum` DATE NULL DEFAULT NULL,
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`seriennummer` VARCHAR(25) NULL DEFAULT NULL,
	`ma_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`bestellung` BIGINT(20) UNSIGNED NOT NULL DEFAULT '0',
	`anmerkung` VARCHAR(255) NULL DEFAULT NULL,
	`dhcp_text` TEXT NULL DEFAULT '' COMMENT 'Text aus DHCP-Server; nur wenn Rechnerobjekt existiert',
	`projekt` VARCHAR(40) NOT NULL,
	`exportiert` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'wurde in ÖAW-Datenbank exportiert',
	`rueckgefordert` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Zeigt an, ob ein PC zurückgefordert wurde wegen eines Rechnertausches (und noch nicht zurückkam)',
	`rueckgefordert_time` DATETIME NULL DEFAULT NULL COMMENT 'Datum der Rückforderung',
	`rueckgefordert_mail` TINYINT(4) NULL DEFAULT '0' COMMENT 'Warnmail versendet?',
	`letztkontrolle` DATETIME NULL DEFAULT NULL,
	`created_by` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`created_time` DATETIME NULL DEFAULT NULL,
	`modified_by` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`modified_time` DATETIME NULL DEFAULT NULL,
	`modified_by_fzguser_id` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Mitarbeiter-Id; wird bei Erzeugung ausgefüllt',
	PRIMARY KEY (`id`) USING BTREE,
	UNIQUE INDEX `InventarNr` (`inventarnummer`) USING BTREE
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__iwf_listen` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`value` INT(10) UNSIGNED NOT NULL DEFAULT 0 COMMENT 'Falls die Id nicht gebraucht werden kann (z.B. bei arbeitstyp in iwf_literatur)',
	`inhalt` VARCHAR(100) NOT NULL DEFAULT '',
	`kategorie` VARCHAR(100) NOT NULL DEFAULT '',
	`reihenfolge` INT(10) UNSIGNED NOT NULL DEFAULT '1',
	`aktiv` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1',
	`jtext` VARCHAR(254) NOT NULL DEFAULT '',
	`form` VARCHAR(45) NOT NULL DEFAULT '' COMMENT 'Bei mehreren Formularen steht hier die zugeordnete Formulardatei',
	PRIMARY KEY (`id`)
)
COMMENT='Statische Listen'
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__iwf_mitarbeiter` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `dienstverhaeltnis` int(10) unsigned NOT NULL,
  `personalnummer` int(10) unsigned NOT NULL DEFAULT 0 COMMENT 'Eindeutige Personalnummer, für Nicht-ÖAW-Mitglieder negative Nummer angeben',
  `geschlecht` char(1) DEFAULT '',
  `nachname` varchar(50) DEFAULT '',
  `vorname` varchar(50) DEFAULT '',
  `samaccountname` varchar(50) DEFAULT '' COMMENT 'AD Kontoname',
  `eintritt` date DEFAULT '0000-00-00',
  `titel` varchar(50) DEFAULT '',
  `geburtsdatum` date DEFAULT '0000-00-00',
  `familienstand` varchar(15) DEFAULT '',
  `zimmer` int(10) unsigned DEFAULT 0,
  `tel` varchar(25) DEFAULT '4120-',
  `fax` varchar(25) DEFAULT '',
  `anschrift` varchar(50) DEFAULT '',
  `anschrift2` varchar(50) DEFAULT '',
  `plz` varchar(5) DEFAULT '',
  `wohnort` varchar(50) DEFAULT '',
  `geburtsort` varchar(50) DEFAULT '',
  `kfz` varchar(50) DEFAULT '',
  `email` varchar(40) DEFAULT '',
  `telefon` varchar(25) DEFAULT '',
  `mobil` varchar(25) DEFAULT '',
  `aufgaben` varchar(150) DEFAULT '',
  `abteilung` int(10) unsigned DEFAULT 0,
  `mailabteilung` varchar(50) DEFAULT '',
  `projekte` varchar(254) DEFAULT '',
  `hashomepage` tinyint(4) unsigned DEFAULT 0,
  `title` varchar(50) DEFAULT '',
  `anmerkung` varchar(100) DEFAULT '',
  `notice` varchar(100) DEFAULT '',
  `vertragsende` date DEFAULT '0000-00-00',
  `sekretariatInfo` tinyint(4) unsigned NOT NULL DEFAULT 0,
  `state` tinyint(3) unsigned NOT NULL DEFAULT 0,
  `deleted` tinyint(4) DEFAULT 0 COMMENT 'in AD gelöscht',
  `initialen` varchar(10) DEFAULT '',
  `gastinstitution` varchar(100) DEFAULT '',
  `gastaufenthaltszweck` int(10) unsigned DEFAULT 0,
  `gastort` varchar(100) DEFAULT '',
  `gastland` int(10) unsigned DEFAULT 0,
  `gastverantwortlicher` int(10) unsigned DEFAULT 0,
  `gastfinanzierung` int(10) unsigned DEFAULT 0,
  `gastprojekt` int(10) unsigned NOT NULL DEFAULT 0,
  `akademis_node` int(10) unsigned NOT NULL DEFAULT 0,
  `kopierid` int(10) unsigned NOT NULL DEFAULT 0,
  `kein_akademis_export` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT 'Bei 1 wird Mitarbeiter nicht beim Akademisexport berücksichtigt',
  `kopieradressbuch` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT 'Eintrag im Kopieradressbuch',
  `swb_unterzeichnet` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT 'Software-Bestimmung unterzeichnet',
  `bsv_unterzeichnet` tinyint(3) unsigned NOT NULL DEFAULT 0 COMMENT 'Brandschutzverordnung unterzeichnet',
  `profil` text DEFAULT '',
  `object_sid` int(10) DEFAULT 0,
  `primary_group_id` int(10) DEFAULT 0,
  `deleted_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT 0,
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT 0,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by_fzguser_id` int(10) unsigned NOT NULL DEFAULT 0 COMMENT 'Mitarbeiter-Id; wird bei Erzeugung ausgefüllt',
  PRIMARY KEY (`id`)
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS`#__iwf_gastchecklisten` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`ma_id` INT(10) UNSIGNED NULL DEFAULT 0,
	`einladender_id` INT(10) UNSIGNED NULL DEFAULT 0,
	`edv_abteilung` INT(10) UNSIGNED NULL DEFAULT 0,
	`anmerkung` TINYTEXT NOT NULL DEFAULT '',
	`kosten_hotel` DECIMAL(10,0) NOT NULL DEFAULT 0,
	`kosten_diaeten` DECIMAL(10,0) NOT NULL DEFAULT 0,
	`kosten_reise` DECIMAL(10,0) NOT NULL DEFAULT 0,
	`kosten_erfasst` TINYINT(4) NOT NULL DEFAULT 0,
	`gast_formular_komplett` TINYINT(4) NOT NULL DEFAULT 0,
	`einladung_an_gast_verschickt` TINYINT(4) NOT NULL DEFAULT 0,
	`einladung_an_botschaft` TINYINT(4) NOT NULL DEFAULT 0,
	`erklaerung_fremdenpolizei` TINYINT(4) NOT NULL DEFAULT 0,
	`fremdenpolizei_code` VARCHAR(45) DEFAULT '',
	`projekt` INT(11) NOT NULL DEFAULT 0,
	`finanzierung_anmerkung` TINYTEXT DEFAULT '',
	`diaeten_angefordert` TINYINT(4) NOT NULL DEFAULT 0,
	`scheck_ausgefuellt` TINYINT(4) NOT NULL DEFAULT 0,
	`diaeten_abgehoben` TINYINT(4) NOT NULL DEFAULT 0,
	`schluessel_bereitgestellt` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0 = kein Bedarf, 1 = benötigt, 2=bereitgestellt',
	`tuercode_wochenendanreise` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0 = kein Bedarf, 1 = bereitgestellt',
	`wohnung` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0=kein Bedarf,1=extern,2=IWF',
	`bestaetigung_zimmerreservierung` TINYINT(4) NOT NULL DEFAULT 0,
	`vertrag` INT(11) NOT NULL DEFAULT 0 COMMENT 'Art des Vertrages; aus fzg_listen',
	`vertrag_unterschrieben` TINYINT(4) NOT NULL DEFAULT 0,
	`vertrag_an_personal` TINYINT(4) NOT NULL DEFAULT 0,
	`versicherung` TINYINT(4) NOT NULL DEFAULT 0,
	`edv_ausstattung` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0 = eigener Laptop,1=IWF-Gastrechner,2=beides',
	`edv_netzwerk_zugang` INT(11) NOT NULL DEFAULT 0 COMMENT '0=WLAN,1=LAN',
	`edv_ip` VARCHAR(15) DEFAULT '',
	`edv_mac` VARCHAR(12) DEFAULT '',
	`edv_gastrechner_konfiguriert` TINYINT(4) NOT NULL DEFAULT 0,
	`edv_zugangsdaten` TINYINT(4) NOT NULL DEFAULT 0,
	`diaeten_uebergeben` TINYINT(4) NOT NULL DEFAULT 0,
	`diaetenbestaetigung_unterschrieben` TINYINT(4) NOT NULL DEFAULT 0,
	`diaetenbestaetigung_original_an_akademie` TINYINT(4) NOT NULL DEFAULT 0,
	`diaetenbestaetigung_ablage` TINYINT(4) NOT NULL DEFAULT 0,
	`rkz_angefordert` TINYINT(4) NOT NULL DEFAULT 0,
	`rkz_uebergeben` TINYINT(4) NOT NULL DEFAULT 0,
	`rkzbestaetigung_unterschrieben` TINYINT(4) NOT NULL DEFAULT 0,
	`rkzbestaetigung_original_an_akademie` TINYINT(4) NOT NULL DEFAULT 0,
	`rkzbestaetigung_ablage` TINYINT(4) NOT NULL DEFAULT 0,
	`meldezettel` TINYINT(4) NOT NULL DEFAULT 0,
	`angemeldet` TINYINT(4) NOT NULL DEFAULT 0,
	`originalbelege_an_akademie` TINYINT(4) NOT NULL DEFAULT 0,
	`schluessel_uebergeben` TINYINT(4) NOT NULL DEFAULT 0,
	`schluesseluebergabe_bestaetigt` TINYINT(4) NOT NULL DEFAULT 0,
	`edv_ok` TINYINT(4) NOT NULL DEFAULT 0 COMMENT 'Gast hat alle Infos und kann arbeiten',
	`edv_softwarebedingungen_unterschrieben` TINYINT(4) NOT NULL DEFAULT 0,
	`brandschutzordnung_unterschrieben` TINYINT(4) NOT NULL DEFAULT 0,
	`schluessel_zurueckgegeben` TINYINT(4) NOT NULL DEFAULT 0,
	`gast_abgemeldet` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '0=nicht nötig, 1=ja',
	`gast_deaktiviert` TINYINT(4) NOT NULL DEFAULT 0 COMMENT 'Status in Mitarbeitertabelle auf inaktiv gesetzt',
	`tickets_erhalten` TINYINT(4) NOT NULL DEFAULT 0,
	`tickets_an_akademie` TINYINT(4) NOT NULL DEFAULT 0,
	`created_by` INT(11) NOT NULL DEFAULT 0,
	`created_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`modified_by` INT(11) NOT NULL DEFAULT 0,
	`modified_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`modified_by_fzguser_id` INT(11) NOT NULL DEFAULT 0,
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__iwf_gasthistorie` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ma_id` int(10) unsigned NOT NULL,
  `eintritt` date DEFAULT '0000-00-00',
  `vertragsende` date DEFAULT '0000-00-00',
  PRIMARY KEY (`id`)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__iwf_zimmer` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`zimmer` VARCHAR(25) NOT NULL,
	`raumbezeichnung` VARCHAR(30) NOT NULL,
	`zports` VARCHAR(254) NOT NULL DEFAULT '',
	`patchstw` SMALLINT(6) NOT NULL DEFAULT 0,
	`stockwerk` INT(11) NOT NULL DEFAULT 0,
	`abteilung` INT(10) UNSIGNED NOT NULL DEFAULT 0,
	`schlosstyp` ENUM('T','M') NOT NULL DEFAULT 'M',
	`nurtuer` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0,
	`zylinder` VARCHAR(25) NOT NULL DEFAULT '',
	`istbuero` TINYINT(4) NOT NULL DEFAULT '1' COMMENT 'Zeigt, ob in diesem Zimmer Mitarbeiter sitzen',
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__iwf_schliessanlage` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `bezeichnung` varchar(15) NOT NULL DEFAULT '',
  `raum_gruppe` varchar(26) NOT NULL DEFAULT '',
  `abteilung` varchar(100) NOT NULL DEFAULT '',
  `tuer` varchar(25) NOT NULL DEFAULT '',
  `bestand` int(10) unsigned NOT NULL DEFAULT 0,
  `verlust` int(10) unsigned NOT NULL DEFAULT 0,
  `transponderkennung` varchar(15) NOT NULL DEFAULT '',
  `mutterschluessel` bigint(20) NOT NULL DEFAULT 0,
  `schluessel` tinyint(4) unsigned NOT NULL DEFAULT 0,
  `berechtigungen` varchar(250) NOT NULL DEFAULT '',
  `kommentar` varchar(250) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `bezeichnung` (`bezeichnung`)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__iwf_schluessel` (
	`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
	`ma_id` INT(10) UNSIGNED NULL DEFAULT 0,
	`schluessel` BIGINT(20) UNSIGNED NOT NULL DEFAULT 0,
	`ausgabedatum` DATE NOT NULL DEFAULT '0000-00-00',
	`kommentar` VARCHAR(254) DEFAULT '',
	PRIMARY KEY (`id`)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__iwf_rechner` (
  `dns_id` int(10) unsigned NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `referenzhost` int(10) unsigned DEFAULT 0,
  `zport` int(11) DEFAULT 0,
  `prozessor` varchar(80) DEFAULT '',
  `takt` int(11) DEFAULT 0,
  `bios_version` varchar(50) DEFAULT '',
  `festplatten` varchar(50) DEFAULT '',
  `speicher` varchar(15) DEFAULT '',
  `netzwerkkarte` varchar(80) DEFAULT '',
  `monitor` varchar(50) DEFAULT '',
  `einsatzbereich` int(11) unsigned DEFAULT 0,
  `extras` varchar(50) DEFAULT '',
  `aenderung` varchar(30) DEFAULT '',
  `inventar_id` int(11) DEFAULT 0,
  `image` varchar(45) DEFAULT '',
  `os` varchar(45) DEFAULT '',
  `osversion` varchar(45) DEFAULT '',
  `osservicepack` varchar(45) DEFAULT '',
  `anmerkung` tinytext DEFAULT '',
  `baramundi_sync` datetime DEFAULT '0000-00-00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `dnsid` (`dns_id`)
) 
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__iwf_software` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `produkt` varchar(50) NOT NULL,
  `produktgruppe` varchar(50) DEFAULT '',
  `version` varchar(50) DEFAULT '',
  `interne_version` int(10) unsigned NOT NULL DEFAULT 1,
  `baramundi_product` varchar(100) NOT NULL DEFAULT '' COMMENT 'Teil des baramundi Produktnamens, um die zugehörigen Versionen zu ermitteln',
  `baramundi_version` mediumtext NOT NULL DEFAULT '' COMMENT 'S/W Versionen wie in baramundi',
  `sprache` varchar(15) DEFAULT '',
  `aktuell` tinyint(4) NOT NULL DEFAULT 0,
  `obsolet` tinyint(4) NOT NULL DEFAULT 0,
  `beliebig` tinyint(4) NOT NULL DEFAULT 0,
  `hersteller` varchar(50) DEFAULT '',
  `basispreis` decimal(10,2) DEFAULT 0,
  `updatepreis` decimal(10,2) DEFAULT 0,
  `wartungspreisfix` decimal(10,2) DEFAULT 0,
  `wartungspreisfloat` decimal(10,2) DEFAULT 0,
  `basisgekauftfix` int(11) DEFAULT 0,
  `basisgekauftfloat` int(11) DEFAULT 0,
  `basisupgedatetfix` int(11) DEFAULT 0,
  `basisupgedatetfloat` int(11) DEFAULT 0,
  `updatesgekauftfix` int(11) DEFAULT 0,
  `updatesgekauftfloat` int(11) DEFAULT 0,
  `swkey` varchar(255) DEFAULT '',
  `gekauftbei` varchar(50) DEFAULT '',
  `medienid` int(11) DEFAULT 0,
  `multimedien` tinyint(4) NOT NULL DEFAULT 0,
  `anmerkungen` text DEFAULT '',
  `kommentarchef` text DEFAULT '',
  `abdeckung` tinyint(4) NOT NULL DEFAULT 0,
  `abteilung` int(10) unsigned NOT NULL,
  `abdeckungchef` tinyint(4) NOT NULL DEFAULT 0,
  `lizenzpflichtig` tinyint(4) NOT NULL DEFAULT 1,
  `mit_baramundi_abgeglichen` tinyint(4) NOT NULL DEFAULT 0 COMMENT 'Software wurde mit baramundi abgeglichen',
  `downloadurl` varchar(255) DEFAULT '',
  `seriennummer` varchar(100) DEFAULT '',
  `created_by` int(10) unsigned NOT NULL DEFAULT 0,
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT 0,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Produkt` (`produkt`,`abteilung`)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__iwf_lizenzen` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `software_id` bigint(20) NOT NULL DEFAULT 0,
  `rechner_id` int(10) unsigned NOT NULL,
  `version` varchar(50) DEFAULT '',
  `installationsdatum` date DEFAULT '0000-00-00',
  `bybaramundi` tinyint(4) DEFAULT 0,
  PRIMARY KEY (`id`)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__iwf_projekte` (
  `projekt` varchar(100) NOT NULL DEFAULT '',
  `projektbez` varchar(20) NOT NULL,
  `projektnummer` varchar(45) DEFAULT '',
  `ma_id` int(10) unsigned NOT NULL DEFAULT 0,
  `aktuell` tinyint(3) NOT NULL DEFAULT 1,
  `lfdnr` int(11) NOT NULL DEFAULT 0,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__iwf_medien` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `label` varchar(15) DEFAULT '',
  `nummeriert` tinyint(4) DEFAULT 0,
  `hersteller` varchar(50) DEFAULT '',
  `produkt` varchar(50) DEFAULT '',
  `version` varchar(25) DEFAULT '',
  `medium` tinyint(3) unsigned DEFAULT 0,
  `mediumanzahl` int(11) DEFAULT 0,
  `sprache` int(10) unsigned DEFAULT 5,
  `betriebssystem` varchar(150) DEFAULT '',
  `ma_id` int(10) unsigned DEFAULT 0,
  `standort` varchar(10) DEFAULT '',
  `original` tinyint(4) DEFAULT 0,
  `abteilung` int(10) unsigned DEFAULT 0,
  `lizenzschluessel` varchar(100) DEFAULT '',
  `seriennummer` varchar(100) DEFAULT '',
  PRIMARY KEY (`id`)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__iwf_nationen` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `kurzform` varchar(5) NOT NULL DEFAULT '',
  `land` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__iwf_dns` (
	`hostname` VARCHAR(50) NOT NULL DEFAULT '',
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`wname` VARCHAR(50) NULL DEFAULT NULL COMMENT '\'Windows Domänenname\'',
	`baramundi_id` VARCHAR(50) NULL DEFAULT '',
	`OS` VARCHAR(50) NULL DEFAULT '',
	`ipnummer` VARCHAR(50) NULL DEFAULT NULL,
	`domain` VARCHAR(50) NULL DEFAULT 'iwf.oeaw.ac.at',
	`abteilung` VARCHAR(15) NOT NULL DEFAULT '1',
	`mac` VARCHAR(20) NOT NULL DEFAULT '',
	`dnseintrag` TINYINT(4) NOT NULL DEFAULT '0',
	`anmerkungen` TEXT NULL DEFAULT '',
	`titel` TEXT NULL DEFAULT '',
	PRIMARY KEY (`id`) USING BTREE,
	UNIQUE INDEX `hostname` (`hostname`) USING BTREE
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__iwf_lit_projekte` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`projekt` VARCHAR(50) NULL DEFAULT NULL,
	`akademis_projekt_node` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`reihenfolge` INT(10) NULL DEFAULT '1',
	`aktuell` TINYINT(3) UNSIGNED NOT NULL DEFAULT '1',
	PRIMARY KEY (`id`) USING BTREE,
	UNIQUE INDEX `pro_idx` (`projekt`) USING BTREE
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__iwf_zeitschriften` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`import_node` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`zeitschrift` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`id`) USING BTREE
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `#__iwf_literatur` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`einheitoeaw` VARCHAR(50) NULL DEFAULT NULL,
	`autoren` TEXT NULL DEFAULT NULL,
	`herausgeber` TEXT NULL DEFAULT NULL,
	`titel` TEXT NULL DEFAULT NULL,
	`untertitel` TEXT NULL DEFAULT NULL,
	`gesamttitel` TEXT NULL DEFAULT NULL,
	`abstract` TEXT NULL DEFAULT NULL,
	`beschlagwortung` TEXT NULL DEFAULT NULL,
	`zitat` TEXT NULL DEFAULT NULL,
	`zeitschrift` VARCHAR(50) NULL DEFAULT NULL,
	`seitenzahlen` VARCHAR(50) NULL DEFAULT NULL,
	`doi` VARCHAR(50) NULL DEFAULT NULL,
	`band` VARCHAR(10) NULL DEFAULT NULL,
	`heft` VARCHAR(50) NULL DEFAULT NULL,
	`erscheinungsjahr` INT(11) NULL DEFAULT NULL,
	`isbn` VARCHAR(20) NULL DEFAULT NULL,
	`verlag` VARCHAR(150) NULL DEFAULT NULL,
	`verlagsort` VARCHAR(30) NULL DEFAULT NULL,
	`publikationstyp` INT(11) NULL DEFAULT NULL,
	`status` INT(11) NULL DEFAULT NULL,
	`sprache` VARCHAR(20) NULL DEFAULT NULL,
	`link` VARCHAR(100) NULL DEFAULT NULL,
	`titelprojekte` VARCHAR(100) NULL DEFAULT NULL,
	`berichtnummer` VARCHAR(30) NULL DEFAULT NULL,
	`weltraumagentur` VARCHAR(20) NULL DEFAULT NULL,
	`mission` VARCHAR(30) NULL DEFAULT NULL,
	`dokumentreferenz` VARCHAR(30) NULL DEFAULT NULL,
	`tagungstitel` VARCHAR(100) NULL DEFAULT NULL,
	`tagungsdatum` VARCHAR(10) NULL DEFAULT NULL,
	`tagungsort` VARCHAR(50) NULL DEFAULT NULL,
	`tagungsland` VARCHAR(100) NULL DEFAULT NULL,
	`veranstaltung_id` INT(10) UNSIGNED NULL DEFAULT NULL,
	`veranstaltungtyp` VARCHAR(100) NULL DEFAULT NULL,
	`institut` VARCHAR(250) NULL DEFAULT NULL,
	`betreuer` VARCHAR(50) NULL DEFAULT NULL,
	`berichttyp` VARCHAR(100) NULL DEFAULT NULL,
	`adressat` VARCHAR(100) NULL DEFAULT NULL,
	`einladung` TINYINT(3) NOT NULL DEFAULT '0',
	`arbeitstyp` INT(11) NOT NULL DEFAULT -1 COMMENT 'bei diplomarbeiten: -1=k.a.,0=diplomarbeit,1=bakk.,2=master th.',
	`beteiligtals` INT(11) NOT NULL DEFAULT '0',
	`akademis1` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Hilfsfeld zur Konvertierung in Akademisdaten; auf True setzen, falls Daten exportiert wurden',
	`akademis2` TINYINT(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Hilfsfeld zur Konvertierung in Akademisdaten; dzt. als Kennzeichnung für Keynotes verwendet',
	`begutachtet` SMALLINT(5) NOT NULL DEFAULT '2',
	`qualitaetstyp` SMALLINT(5) NOT NULL DEFAULT '0' COMMENT 'bei Vorträgen; 0=wissenschaftlich,1=populärwissenschaftlich',
	`eingeladen` SMALLINT(5) NOT NULL DEFAULT '2',
	`hauptautor_forschungseinheit` SMALLINT(5) NOT NULL DEFAULT '1',
	`mehr_als_zehntausend_zeichen` SMALLINT(6) NOT NULL DEFAULT '1',
	`ist_populaerwissenschaftlich` SMALLINT(6) NOT NULL DEFAULT -1 COMMENT 'bei Publikationen; -1=nein, 1=ja',
	`ist_international` SMALLINT(6) NOT NULL DEFAULT '0' COMMENT 'bei Vorträgen:  -1=nein, 1=ja, 0=keine Angabe',
	`modified_by_fzguser_id` INT(10) UNSIGNED NOT NULL DEFAULT '0' COMMENT 'Mitarbeiter-Id; wird bei Erzeugung ausgefüllt',
	`created_by` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`created_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`modified_by` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`modified_time` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	PRIMARY KEY (`id`) USING BTREE
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1;

CREATE TABLE `#__iwf_baramundi_sw` (
	`product_name` MEDIUMTEXT NULL DEFAULT '' COMMENT 'Relevant für unsere Datenbank',
	`company` MEDIUMTEXT NULL DEFAULT '',
	`version` MEDIUMTEXT NULL DEFAULT '' COMMENT 'Relevant für unsere Datenbank' COLLATE 'utf8mb4_general_ci',
	`iwf_software` INT(11) NULL DEFAULT NULL COMMENT 'zugeordnete IWF Software',
	UNIQUE INDEX `index_keys` (`product_name`, `version`) USING HASH
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci COMMENT='Alle installierten Programme aus baramundi';

CREATE TABLE `#__iwf_baramundi_sw_diff` (
	`product_name` MEDIUMTEXT NULL DEFAULT '',
	`company` MEDIUMTEXT NULL DEFAULT '',
	`version` MEDIUMTEXT NULL DEFAULT '',
	`set` VARCHAR(50) NULL DEFAULT '',
	`found` DATE NULL DEFAULT NULL
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci COMMENT='Alle unterschiedlichen installierten Programme aus letzter fzg_baramundi_sw Tabelle';

CREATE TABLE `#__iwf_queries` (
	`id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`query` TEXT NOT NULL,
	`beschreibung` VARCHAR(255) NOT NULL DEFAULT '',
	`ma_id` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`) USING BTREE
)
ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 DEFAULT COLLATE=utf8mb4_unicode_ci;

CREATE TABLE `#__iwf_aka_listen` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `value` int(10) unsigned NOT NULL DEFAULT 0 COMMENT 'Falls nicht ein Text, sondern ein Wert gebraucht wird',
  `inhalt` varchar(100) NOT NULL DEFAULT '',
  `kategorie` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `reihenfolge` int(10) unsigned NOT NULL DEFAULT 1,
  `aktiv` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `jtext` varchar(254) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 DEFAULT COLLATE=utf8mb4_unicode_ci AUTO_INCREMENT=1 COMMENT='Akademis Zeichenketten';

INSERT INTO `#__iwf_aka_listen` (`id`, `value`, `inhalt`, `kategorie`, `reihenfolge`, `aktiv`, `jtext`) VALUES
	(1, 2, 'Diplomarbeit', 'arbeitstyp', 1, 1, 'COM_VERWALTUNG_AKALIST_DIPLOMARBEIT'),
	(2, 1, 'Bakkalaureatsarbeit', 'arbeitstyp', 1, 1, 'COM_VERWALTUNG_AKALIST_BAKALAUREATSARBEIT'),
	(3, 3, 'Master Thesis', 'arbeitstyp', 1, 1, 'COM_VERWALTUNG_AKALIST_MASTERTHESIS'),
	(4, 4, 'Doktorarbeit', 'arbeitstyp', 1, 1, 'COM_VERWALTUNG_AKALIST_DOKTORARBEIT');
