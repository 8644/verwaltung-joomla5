<?php

/**
 * Diese Datei ist Teil der IWF Verwaltungskomponente für Joomla 5
 * Copyright 2024 IWF Graz
 * 
 * Jegliche Weitergabe, Verbreitung oder öffentliche Zugänglichmachung der 
 * Software ist ausdrücklich untersagt.
 */

defined('_JEXEC') or die;

use Joomla\CMS\Component\Router\RouterFactoryInterface;
use Joomla\CMS\Dispatcher\ComponentDispatcherFactoryInterface;
use Joomla\CMS\Extension\ComponentInterface;
use Joomla\CMS\Extension\Service\Provider\CategoryFactory;
use Joomla\CMS\Extension\Service\Provider\ComponentDispatcherFactory;
use Joomla\CMS\Extension\Service\Provider\MVCFactory;
use Joomla\CMS\Extension\Service\Provider\RouterFactory;
use Joomla\CMS\HTML\Registry;
use Joomla\CMS\MVC\Factory\MVCFactoryInterface;
use Iwf\Component\Verwaltung\Administrator\Extension\VerwaltungComponent;
use Joomla\DI\Container;
use Joomla\DI\Exception\ProtectedKeyException;
use Joomla\DI\ServiceProviderInterface;

return new class implements ServiceProviderInterface {

    /**
     * @param Container $container 
     * @return void 
     * @throws ProtectedKeyException 
     */
    public function register(Container $container): void
    {
        $container->registerServiceProvider(new CategoryFactory('\\Iwf\\Component\\Verwaltung'));
        $container->registerServiceProvider(new ComponentDispatcherFactory('\\Iwf\\Component\\Verwaltung'));
        $container->registerServiceProvider(new MVCFactory('\\Iwf\\Component\\Verwaltung'));
        $container->registerServiceProvider(new RouterFactory('\\Iwf\\Component\\Verwaltung'));
        $container->set(
            ComponentInterface::class,
            function (Container $container) {
                $component = new VerwaltungComponent($container->get(ComponentDispatcherFactoryInterface::class));
                $component->setRegistry($container->get(Registry::class));
                $component->setMVCFactory($container->get(MVCFactoryInterface::class));
                $component->setRouterFactory($container->get(RouterFactoryInterface::class));
                return $component;
            }
        );
    }
};
